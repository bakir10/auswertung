﻿function onChange(e) {

    let selected = $.map(this.select(), function (item) {
        return $(item).text();
    });

    var grid = $("#Alarm_Statistik").data("kendoGrid");
    var colIdx = grid.select().closest("td").index();
    var columnheader = this.options.columns[colIdx].title;

    if (columnheader == "Datum" || columnheader == "Datum und Zeit") {
        var selectedItem = selected.toString().substring(0, 8);
        var logDate = [selectedItem.slice(0, 6) + "20" + selectedItem.slice(6, 8)].join();

        var url = '/Home/FindLogDate?LogDate=' + logDate;
        window.location.href = url;
    }
    else {
        alert("Please select a column with the date ");
    }

}