﻿function onChange(e) {
    var kesselTyp = $("#dropdownlist_Kesseltyp").data().kendoDropDownList.text();
    var url = '/Alarms/Status?Kesseltyp=' + kesselTyp;
    window.location.href = url;
}

function kesselTypFilter(element) {
    element.kendoDropDownList({
        dataSource: {
            transport: {
                read: '@Url.Action("FilterMenuCustomization_KesselTyp")',
                }
        },
        optionLabel: "--Select Value--"
    });
}

//adding remote rule to handle validation based on Remote attribute set in the model.
$(document).ready(function () {
    $.extend(true, kendo.ui.validator, {
        rules: {
            remote: function (input) {
                if (input.val() == "" || !input.attr("data-val-remote-url")) {
                    return true;
                }

                if (input.attr("data-val-remote-recieved")) {
                    input.attr("data-val-remote-recieved", "");
                    return !(input.attr("data-val-remote"));
                }

                var url = input.attr("data-val-remote-url");
                var postData = {};
                postData[input.attr("data-val-remote-additionalfields").split(".")[1]] = input.val();

                var validator = this;
                var currentInput = input;
                input.attr("data-val-remote-requested", true);
                $.ajax({
                    url: url,
                    type: "POST",
                    data: JSON.stringify(postData),
                    dataType: "json",
                    traditional: true,
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        if (data == true) {
                            input.attr("data-val-remote", "");
                        }
                        else {
                            input.attr("data-val-remote", data);
                        }
                        input.attr("data-val-remote-recieved", true);
                        validator.validateInput(currentInput);

                    },
                    error: function () {
                        input.attr("data-val-remote-recieved", true);
                        validator.validateInput(currentInput);
                    }
                });
                return true;
            }
        },
        messages: {
            remote: function (input) {
                return input.attr("data-val-remote");
            }
        }
    });
});

//show server errors if any
function error_handler(e) {
    if (e.errors) {
        var message = "Errors:\n";
        $.each(e.errors, function (key, value) {
            if ('errors' in value) {
                $.each(value.errors, function () {
                    message += this + "\n";
                });
            }
        });
        alert(message);
    }
}