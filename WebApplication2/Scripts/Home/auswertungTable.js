﻿$(function () {
    $("#next").click(function () {
        const params = new URL(location.href).searchParams;
        const produktserie = params.get('Produktserie');
        const logfile = params.get('Logfile');
        const visibleSeries = params.get('VisibleSeries');

        $.ajax({
            type: "POST",
            url: "/Home/PreviousNext",
            data: {
                page: "next",
                logfile: logfile,
                produktserie: produktserie,
                visibleseries: visibleSeries
            },
            success: function (response) {
                var url = '/Home/Auswertung?Produktserie=' + response.Produktserie + '&Logfile=' + response.Logfile + '&VisibleSeries=' + response.Visibleseries;
                window.location.href = url;
            },
            error: function (response) {
                alert("GRESKA");
            }
        })
    })
});


$(function () {
    $("#previous").click(function () {
        const params = new URL(location.href).searchParams;
        const produktserie = params.get('Produktserie');
        const logfile = params.get('Logfile');
        const visibleSeries = params.get('VisibleSeries');

        $.ajax({
            type: "POST",
            url: "/Home/PreviousNext",
            data: {
                page: "previous",
                logfile: logfile,
                produktserie: produktserie,
                visibleseries: visibleSeries
            },
            success: function (response) {
                var url = '/Home/Auswertung?Produktserie=' + response.Produktserie + '&Logfile=' + response.Logfile + '&VisibleSeries=' + response.Visibleseries;
                window.location.href = url;
            },
            error: function (response) {
                alert("ERROR");
            }
        })
    })
});

$(function () {
    $("#backToChart").click(function () {
        const params = new URL(location.href).searchParams;
        const produktserie = params.get('Produktserie');
        const logfile = params.get('Logfile');
        const VisibleSeries = params.get('VisibleSeries');

        var url = '/Home/Auswertung?Produktserie=' + produktserie + '&Logfile=' + logfile + '&VisibleSeries=' + VisibleSeries;
        window.location.href = url;
    })
});

function removeValue() {
    var chart = $("#Chart_Logdaten").data("kendoChart");
    var toRemove = ["[°C]", "[%]", "[1/10sec]", "[U/min]", "[L/h]", "[W]", "[sek]", "[mA]", "[kV]", " "];
    var visibleseries = Array();

    for (var i = 0; i < chart.options.series.length; i++) {
        if (chart.options.series[i].visible == true) {
            chart.options.series[i].name = chart.options.series[i].name.replace(/\s+/g, '');
            for (var j = 0; j < toRemove.length; j++) {
                if (chart.options.series[i].name.search(toRemove[j]) >= 0) {
                    chart.options.series[i].name = chart.options.series[i].name.replace(toRemove[j], '');
                }
            }
            visibleseries.push(chart.options.series[i].name);
        }
    }
    return visibleseries;
}


function getCurrentURL() {
    const params = new URL(location.href).searchParams;
    const produktserie = params.get('Produktserie');
    const logfile = params.get('Logfile');

    return { produktserie, logfile };
}