﻿function filterKesselTyp(e) {
    return {
        kessel: $("#dropdownlist_Kesseltyp").data("kendoDropDownList").text()
    };
}

$(function () {
    $("#btn_Auswerten").click(function () {
        var logfile = $("#dropdownlist_Logdaten").data().kendoDropDownList.text();
        var kesseltyp = $("#dropdownlist_Kesseltyp").data().kendoDropDownList.text();
        var visibleseries = Array();
        visibleseries[0] = "spaceholder";

        if ((logfile == "Auszuwertendes Logfile auswählen...") && (kesseltyp == "Kesseltyp auswählen...")) {
            alert("Bitte wählen Sie den Kesseltyp und ein Logfile aus!");
            return;
        }
        if ((logfile == "Auszuwertendes Logfile auswählen...")) {
            alert("Bitte wählen Sie ein Logfile aus!");
            return;
        }
        if ((kesseltyp == "Kesseltyp auswählen...")) {
            alert("Bitte wählen Sie den Produktserie aus!");
            return;
        }

        var url = '/Home/Auswertung?Produktserie=' + kesseltyp + '&Logfile=' + logfile + '&Visibleseries=' + visibleseries;
        window.location.href = url;
    });
});

$(function () {
    $("#btn_Loschen").click(function () {
        $.ajax({
            type: "POST",
            url: "DeleteLogFile",
            success: function (response) {
                var dropdownlist = $("#dropdownlist_Logdaten").data("kendoDropDownList");
                dropdownlist.dataSource.read();
                dropdownlist.refresh();
                alert("Geladene Logfiles gelöscht !")
            },
            error: function (response) {
                alert("Fehler :/");
            }
        })
    });
})