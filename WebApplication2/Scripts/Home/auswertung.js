﻿function onLegendItemClick(e) {
    var chart = $("#Chart_Logdaten").data("kendoChart");
    var seriesCount = chart.options.series.length;
    var produktserie = "@ViewBag.produktserie";
    if (produktserie == "EcoTop | EcoTopZero | Pellet Elegance | OctoPlus | Maximus | Regelzentrale") {
        if (e.series.name == "Kesselwerte") {
            //Kesselwerte
            for (var i = 0; i < seriesCount; i++) {
                if (chart.options.series[i].name == "SpeichertempMitte_X31 [°C]") {
                    chart.options.series[i].visible = e.series.visible ? false : true;
                }
                if (chart.options.series[i].name == "Rauchgastemperatur_X34 [°C]") {
                    chart.options.series[i].visible = e.series.visible ? false : true;
                }
                if (chart.options.series[i].name == "Einschubtemperatur_X33 [°C]") {
                    chart.options.series[i].visible = e.series.visible ? false : true;
                }
                if (chart.options.series[i].name == "Luftzahl") {
                    chart.options.series[i].visible = e.series.visible ? false : true;
                }
                if (chart.options.series[i].name == "Einschublaufzeit_SGA") {
                    chart.options.series[i].visible = e.series.visible ? false : true;
                }
                if (chart.options.series[i].name == "Statuszeile") {
                    chart.options.series[i].visible = e.series.visible ? false : true;
                }
                if (chart.options.series[i].name == "SZG_Istdrehzahl [%]") {
                    chart.options.series[i].visible = e.series.visible ? false : true;
                }
                if (chart.options.series[i].name == "Einschubimpuls_LDZ [1/10 sec]") {
                    chart.options.series[i].visible = e.series.visible ? false : true;
                }
                if (chart.options.series[i].name == "SpeichertempUnten_X32 [°C]") {
                    chart.options.series[i].visible = e.series.visible ? false : true;
                }
                if (chart.options.series[i].name == "Kesseltemperatur_X31 [°C]") {
                    chart.options.series[i].visible = e.series.visible ? false : true;
                }
                if (chart.options.series[i].name == "RLA_Mischerposition") {
                    chart.options.series[i].visible = e.series.visible ? false : true;
                }
                if (chart.options.series[i].name == "Ruecklauftemperatur_X32 [°C]") {
                    chart.options.series[i].visible = e.series.visible ? false : true;
                }
                if (chart.options.series[i].name == "Differenzdruck_X61") {
                    chart.options.series[i].visible = e.series.visible ? false : true;
                }
                if (chart.options.series[i].name == "Filter_Spannung") {
                    chart.options.series[i].visible = e.series.visible ? false : true;
                }
                if (chart.options.series[i].name == "Filter_Spg [kV]") {
                    chart.options.series[i].visible = e.series.visible ? false : true;
                }
                if (chart.options.series[i].name == "Filter_Strom [mA]") {
                    chart.options.series[i].visible = e.series.visible ? false : true;
                }
                if (chart.options.series[i].name == "Brennraumtemperatur [°C]") {
                    chart.options.series[i].visible = e.series.visible ? false : true;
                }
                if (chart.options.series[i].name == "Primaerluftklappe [%]") {
                    chart.options.series[i].visible = e.series.visible ? false : true;
                }
                if (chart.options.series[i].name == "Sekundaerluftklappe [%]") {
                    chart.options.series[i].visible = e.series.visible ? false : true;
                }
                if (chart.options.series[i].name == "Filter_Strom [mA]") {
                    chart.options.series[i].visible = e.series.visible ? false : true;
                }
                if (chart.options.series[i].name == "Rezirkulationsluftklappe [%]") {
                    chart.options.series[i].visible = e.series.visible ? false : true;
                }
                if (chart.options.series[i].name == "AGT_Anhebungsklappe [%]") {
                    chart.options.series[i].visible = e.series.visible ? false : true;
                }
                if (chart.options.series[i].name == "Vorlaufsolltemperatur_Kessel [°C]") {
                    chart.options.series[i].visible = e.series.visible ? false : true;
                }
                if (chart.options.series[i].name == "Kesseltemperatur [°C]") {
                    chart.options.series[i].visible = e.series.visible ? false : true;
                }
                if (chart.options.series[i].name == "Aussentemp [°C]") {
                    chart.options.series[i].visible = e.series.visible ? false : true;
                }
                if (chart.options.series[i].name == "Leistung") {
                    chart.options.series[i].visible = e.series.visible ? false : true;
                }
                if (chart.options.series[i].name == "Anforderung") {
                    chart.options.series[i].visible = e.series.visible ? false : true;
                }
                if (chart.options.series[i].name == "benoetigte_Kesseltemperatur [°C]") {
                    chart.options.series[i].visible = e.series.visible ? false : true;
                }
                if (chart.options.series[i].name == "Fuehlereingang_D1I3 [°C]") {
                    chart.options.series[i].visible = e.series.visible ? false : true;
                }
                if (chart.options.series[i].name == "Trinkwassersp_1 [°C]") {
                    chart.options.series[i].visible = e.series.visible ? false : true;
                }
            }
        }
        if (e.series.name == "HK-Regelung") {
            //HK-Regelung
            for (var i = 0; i < seriesCount; i++) {
                if (chart.options.series[i].name == "FWM_Austrittstemp [°C]") {
                    chart.options.series[i].visible = e.series.visible ? false : true;
                }
                if (chart.options.series[i].name == "FWM_Eintrittstemp [°C]") {
                    chart.options.series[i].visible = e.series.visible ? false : true;
                }
                if (chart.options.series[i].name == "SpeichertempOben_X30") {
                    chart.options.series[i].visible = e.series.visible ? false : true;
                }
                if (chart.options.series[i].name == "Aussentemperatur_X42 [°C]") {
                    chart.options.series[i].visible = e.series.visible ? false : true;
                }
                if (chart.options.series[i].name == "Vlt_1 [°C]") {
                    chart.options.series[i].visible = e.series.visible ? false : true;
                }
                if (chart.options.series[i].name == "HK_P_1") {
                    chart.options.series[i].visible = e.series.visible ? false : true;
                }
                if (chart.options.series[i].name == "Vlst_1 [°C]") {
                    chart.options.series[i].visible = e.series.visible ? false : true;
                }
                if (chart.options.series[i].name == "RT_1 [°C]") {
                    chart.options.series[i].visible = e.series.visible ? false : true;
                }
                if (chart.options.series[i].name == "HK_M_1 [%]") {
                    chart.options.series[i].visible = e.series.visible ? false : true;
                }
                if (chart.options.series[i].name == "Wetter_Prognose") {
                    chart.options.series[i].visible = e.series.visible ? false : true;
                }
                if (chart.options.series[i].name == "TWS_Temperatur [°C]") {
                    chart.options.series[i].visible = e.series.visible ? false : true;
                }
                if (chart.options.series[i].name == "TWS_Pumpe") {
                    chart.options.series[i].visible = e.series.visible ? false : true;
                }
                if (chart.options.series[i].name == "Vlt_2 [°C]") {
                    chart.options.series[i].visible = e.series.visible ? false : true;
                }
                if (chart.options.series[i].name == "HK_P_2") {
                    chart.options.series[i].visible = e.series.visible ? false : true;
                }
                if (chart.options.series[i].name == "Vlst_2 [°C]") {
                    chart.options.series[i].visible = e.series.visible ? false : true;
                }
                if (chart.options.series[i].name == "RT_2 [°C]") {
                    chart.options.series[i].visible = e.series.visible ? false : true;
                }
                if (chart.options.series[i].name == "HK_M_2") {
                    chart.options.series[i].visible = e.series.visible ? false : true;
                }
                if (chart.options.series[i].name == "PufferOben [°C]") {
                    chart.options.series[i].visible = e.series.visible ? false : true;
                }
                if (chart.options.series[i].name == "PufferUnten [°C]") {
                    chart.options.series[i].visible = e.series.visible ? false : true;
                }
                if (chart.options.series[i].name == "Puffer_X35 [°C]") {
                    chart.options.series[i].visible = e.series.visible ? false : true;
                }
                if (chart.options.series[i].name == "PuffertempOben [°C]") {
                    chart.options.series[i].visible = e.series.visible ? false : true;
                }
                if (chart.options.series[i].name == "PuffertempUnten [°C]") {
                    chart.options.series[i].visible = e.series.visible ? false : true;
                }
            }
        }
    }
    else if (produktserie == "VampAir K08/K10" || produktserie == "VampAir K12/K15") {
        if (e.series.name == "HK-Regelung") {
            //HK-Regelung
            for (var i = 0; i < seriesCount; i++) {
                if (chart.options.series[i].name == "Vorlauftemp [°C]") {
                    chart.options.series[i].visible = e.series.visible ? false : true;
                }
                if (chart.options.series[i].name == "PufferOben [°C]") {
                    chart.options.series[i].visible = e.series.visible ? false : true;
                }
                if (chart.options.series[i].name == "PufferUnten [°C]") {
                    chart.options.series[i].visible = e.series.visible ? false : true;
                }
                if (chart.options.series[i].name == "Ruecklauftemp [°C]") {
                    chart.options.series[i].visible = e.series.visible ? false : true;
                }
                if (chart.options.series[i].name == "TW_Speichertemp [°C]") {
                    chart.options.series[i].visible = e.series.visible ? false : true;
                }
                if (chart.options.series[i].name == "T1_VLT_WP [°C]") {
                    chart.options.series[i].visible = e.series.visible ? false : true;
                }
                if (chart.options.series[i].name == "Aussentemp [°C]") {
                    chart.options.series[i].visible = e.series.visible ? false : true;
                }
                if (chart.options.series[i].name == "Statuszeile_WP") {
                    chart.options.series[i].visible = e.series.visible ? false : true;
                }
                if (chart.options.series[i].name == "E_Stab") {
                    chart.options.series[i].visible = e.series.visible ? false : true;
                }
            }
        }
        if (e.series.name == "Wärmepumpe") {
            //Wärmepumpe
            for (var i = 0; i < seriesCount; i++) {
                if (chart.options.series[i].name == "Verdampfungstemp [°C]") {
                    chart.options.series[i].visible = e.series.visible ? false : true;
                }
                if (chart.options.series[i].name == "T4_Verdeintrittstemp [°C]") {
                    chart.options.series[i].visible = e.series.visible ? false : true;
                }
                if (chart.options.series[i].name == "Kondensationstemp [°C]") {
                    chart.options.series[i].visible = e.series.visible ? false : true;
                }
                if (chart.options.series[i].name == "Kompressordrehzahl [U/min]") {
                    chart.options.series[i].visible = e.series.visible ? false : true;
                }
                if (chart.options.series[i].name == "T3_Sauggastemp [°C]") {
                    chart.options.series[i].visible = e.series.visible ? false : true;
                }
                if (chart.options.series[i].name == "T6_Fluessigkeitstemp [°C]") {
                    chart.options.series[i].visible = e.series.visible ? false : true;
                }
                if (chart.options.series[i].name == "Heizleistung [W]") {
                    chart.options.series[i].visible = e.series.visible ? false : true;
                }
                if (chart.options.series[i].name == "Leistungsanforderung [W]") {
                    chart.options.series[i].visible = e.series.visible ? false : true;
                }
                if (chart.options.series[i].name == "Oeffnungsgrad_EVI_Ventil [%]") {
                    chart.options.series[i].visible = e.series.visible ? false : true;
                }
                if (chart.options.series[i].name == "Cooling_Valve_Opening") {
                    chart.options.series[i].visible = e.series.visible ? false : true;
                }
                if (chart.options.series[i].name == "EVI_Saturation_Temp [°C]") {
                    chart.options.series[i].visible = e.series.visible ? false : true;
                }
                if (chart.options.series[i].name == "Oeffnungsgrad_Ex_Ventil_Heizen [%]") {
                    chart.options.series[i].visible = e.series.visible ? false : true;
                }
                if (chart.options.series[i].name == "Ventilatordrehzahl [U/min]") {
                    chart.options.series[i].visible = e.series.visible ? false : true;
                }
                if (chart.options.series[i].name == "Primaerkreis_iPWM_Durchfluss [L/h]") {
                    chart.options.series[i].visible = e.series.visible ? false : true;
                }
                if (chart.options.series[i].name == "EVI_Saturation_Temp [°C]") {
                    chart.options.series[i].visible = e.series.visible ? false : true;
                }
            }
        }
    }
}

function onVisual(e) {

    var color = e.options.markers.background;
    var labelColor = e.options.labels.color;
    // This will give us the default box + text
    var defaultVisual = e.createVisual();
    var defaultSize = defaultVisual.bbox().size;
    // Define the target dimensions for the legend item
    var rect = new kendo.geometry.Rect([0, 0], [defaultSize.width + 30, defaultSize.height]);

    // A layout will hold the checkbox and the default visual
    var layout = new kendo.drawing.Layout(rect, {
        spacing: 5,
        alignItems: "center"
    });
    if (e.series.name == "HK-Regelung" || e.series.name == "Kesselwerte" || e.series.name == "Wärmepumpe") {
        var cbSymbol = e.active ? "\u2611" : "\u2610";
        var cb = new kendo.drawing.Text(cbSymbol, [0, 0], {
            fill: {
                color: labelColor
            },
            font: "20px sans-serif"
        });

        // Reflow them together
        layout.append(cb, defaultVisual);
        layout.reflow();
    }
    else {
        layout.append(defaultVisual);
        layout.reflow();
    }

    return layout;
}

function onRender(e) {
    var z = drawOnChart(e);
    //  console.log(e.sender);
}
function onPlotAreaHover(e) {
    var z = removeDrawingElement(e);
    var j = drawOnChart(e);
    console.log("test plot area hover");
}

function onZoomEnd(e) {
    var j = drawOnChart(e);
    var l = onRender(e);
    var chart = $("#Chart_Logdaten").data("kendoChart");
    console.log("nakon zoom-a");
    console.log(chart);
}

function showDialog() {
    $("#dialog_setValue").data("kendoDialog").open();
}

function drawOnChart(e) {
    var chart = $("#Chart_Logdaten").data("kendoChart");
    console.log(chart);
    var produktserie = "@ViewBag.produktserie";
    var index = 0;
    var alarmIndex = Array();
    var storungIndex = Array();
    var minTemp = 0;
    var maxTemp = 0;

    //draw
    var draw = kendo.drawing;
    var geom = kendo.geometry;

    //render a line element for störung
    var gradientStorung = new draw.LinearGradient({
        start: [0, 0],  //bottom left
        end: [0, 1],    //top left
        stops: [{
            offset: 0,
            color: "#FF9F45",
            opacity: 0.2
        }, {
            offset: 1,
            color: "#FF9F45",
            opacity: 0.2
        }]
    });

    //render a line element for alarm
    var gradient = new draw.LinearGradient({
        start: [0, 0],  //bottom left
        end: [0, 1],    //top left
        stops: [{
            offset: 0,
            color: "red",
            opacity: 0.2
        }, {
            offset: 1,
            color: "red",
            opacity: 0.2
        }]
    });

    //X and Y Axis
    var xAxis = chart.getAxis("Vrijeme");
    if (produktserie == "VampAir K08/K10" || produktserie == "VampAir K12/K15") {
        var yAxis = chart.getAxis("Temperaturen");
    }
    else {
        var yAxis = chart.getAxis("Temperaturen/ES Impuls");
    }

    //Min and Max value Temperaturen and Temperaturen/ES Impuls  Y-axis
    for (var k = 0; k < chart.options.valueAxis.length; k++) {
        if (chart.options.valueAxis[k].name == "Temperaturen" || chart.options.valueAxis[k].name == "Temperaturen/ES Impuls") {
            minTemp = chart.options.valueAxis[k].min;
            maxTemp = chart.options.valueAxis[k].max;
        }
    }
    //find the position of Statuszeile_WP series
    for (var i = 0; i < chart.options.series.length; i++) {
        if (chart.options.series[i].name == "Statuszeile_WP" || chart.options.series[i].name == "Statuszeile") {
            index = i;
        }
    }

    //find the position of Alarm and Störung
    if (chart.options.series[index].data != null) {
        for (var j = 0; j < chart.options.series[index].data.length; j++) {
            if (chart.options.series[index].data[j] == "Alarm" || chart.options.series[index].data[j] == "Alarm aktiv") {
                alarmIndex.push(j);
            }
            if (chart.options.series[index].data[j] == "Wärmepumpe startet in Kürze") {
                storungIndex.push(j);
            }
        }
    }

    if (alarmIndex.length > 0) {
        for (var k = 0; k < alarmIndex.length; k++) {
            var xSlot = xAxis.slot(alarmIndex[k], alarmIndex[k]);
            var ySlot = yAxis.slot(minTemp, maxTemp);

            var rect = new geom.Rect([
                xSlot.origin.x, ySlot.origin.y
            ], [
                xSlot.width(), ySlot.height()
            ]);

            var path = draw.Path.fromRect(rect, {
                stroke: null,
                fill: gradient,
            });
            chart.surface.draw(path);
        }
    }

    //draw warning/störung
    if (storungIndex.length > 0) {
        for (var k = 0; k < storungIndex.length; k++) {
            var xSlot = xAxis.slot(storungIndex[k], storungIndex[k]);
            var ySlot = yAxis.slot(minTemp, maxTemp);

            var rect = new geom.Rect([
                xSlot.origin.x, ySlot.origin.y
            ], [
                xSlot.width(), ySlot.height()
            ]);

            var path = draw.Path.fromRect(rect, {
                stroke: null,
                fill: gradientStorung
            });
            chart.surface.draw(path);
        }
    }
}

function removeDrawingElement(e) {
    var chart = $("#Chart_Logdaten").data("kendoChart");
    var produktserie = "@ViewBag.produktserie";
    var chartEvent = e.sender;
    var index = 0;
    var alarmIndex = Array();
    var storungIndex = Array();
    var minTemp = 0;
    var maxTemp = 0;

    //draw
    var draw = kendo.drawing;
    var geom = kendo.geometry;

    //für störung
    var gradientAuS = new draw.LinearGradient({
        start: [0, 0],  //bottom left
        end: [0, 1],    //top left
        stops: [{
            offset: 0,
            color: "white",
            opacity: 0
        }, {
            offset: 1,
            color: "white",
            opacity: 0
        }]
    });

    //X and Y Axis
    var xAxis = chartEvent.getAxis("Vrijeme");
    if (produktserie == "VampAir K08/K10" || produktserie == "VampAir K12/K15") {
        var yAxis = chartEvent.getAxis("Temperaturen");
    }
    else {
        var yAxis = chartEvent.getAxis("Temperaturen/ES Impuls");
    }

    //Min and Max value Temperaturen and Temperaturen/ES Impuls  Y-axis
    for (var c = 0; c < chart.options.valueAxis.length; c++) {
        if (chart.options.valueAxis[c].name == "Temperaturen" || chart.options.valueAxis[c].name == "Temperaturen/ES Impuls") {
            minTemp = chart.options.valueAxis[c].min;
            maxTemp = chart.options.valueAxis[c].max;
        }
    }
    //find the position of Statuszeile_WP series
    for (var i = 0; i < chart.options.series.length; i++) {
        if (chart.options.series[i].name == "Statuszeile_WP" || chart.options.series[i].name == "Statuszeile") {
            index = i;
            console.log(index);
        }
    }

    //find the position of Alarm and Störung
    for (var j = 0; j < chart.options.series[index].data.length; j++) {
        if (chart.options.series[index].data[j] == "Alarm" || chart.options.series[index].data[j] == "Alarm aktiv") {
            console.log("Pronadjen alarm");
            alarmIndex.push(j);
        }
        if (chart.options.series[index].data[j] == "Wärmepumpe startet in Kürze") {
            console.log("Pronadjen störung");
            storungIndex.push(j);
        }
    }



    if (alarmIndex.length > 0) {
        for (var k = 0; k < alarmIndex.length; k++) {
            var xSlot = xAxis.slot(alarmIndex[k], alarmIndex[k]);
            var ySlot = yAxis.slot(minTemp, maxTemp);

            var rect = new geom.Rect([
                xSlot.origin.x, ySlot.origin.y
            ], [
                xSlot.width(), ySlot.height()
            ]);

            var path = draw.Path.fromRect(rect, {
                stroke: null,
                fill: gradientAuS
            });
            chart.surface.draw(path);
        }
    }

    //draw warning/störung
    if (storungIndex.length > 0) {
        for (var k = 0; k < storungIndex.length; k++) {
            var xSlot = xAxis.slot(storungIndex[k], storungIndex[k]);
            var ySlot = yAxis.slot(minTemp, maxTemp);

            var rect = new geom.Rect([
                xSlot.origin.x, ySlot.origin.y
            ], [
                xSlot.width(), ySlot.height()
            ]);

            var path = draw.Path.fromRect(rect, {
                stroke: null,
                fill: gradientAuS
            });
            chart.surface.draw(path);
        }
    }
}

function onConfirm() {
    var dropdownlist = $("#dropdownlist_Axis").data().kendoDropDownList.text();
    var minValue = $("#minValue").data("kendoTextBox").value();
    var maxValue = $("#maxValue").data("kendoTextBox").value();
    var chart = $("#Chart_Logdaten").data("kendoChart");

    if (parseInt(minValue) > parseInt(maxValue) || parseInt(minValue) == parseInt(maxValue)) {
        alert("Min Value must be less than Max value");
    }
    else if (minValue && maxValue && dropdownlist.localeCompare("Y-Axis auswählen...") != 0) {
        for (var i = 0; i < chart.options.valueAxis.length; i++) {
            if (dropdownlist.localeCompare(chart.options.valueAxis[i].name) == 0) {
                chart.options.valueAxis[i].min = parseInt(minValue);
                chart.options.valueAxis[i].max = parseInt(maxValue);
                chart.redraw();
            }
        }
    }
    else {
        alert("Please fill all field");
    }

    minValue = $("#minValue").data("kendoTextBox").value("");
    maxValue = $("#maxValue").data("kendoTextBox").value("");
    dropdownlist = $("#dropdownlist_Axis").data().kendoDropDownList.text("Y-Axis...");
}


function onSeriesHover(e) {
    var chart = $("#Chart_Logdaten").data("kendoChart");

    if (e.category <= chart.options.categoryAxis.categories[140]) {
        const findTooltip = document.getElementsByClassName('k-tooltip k-chart-tooltip k-chart-tooltip-inverse k-chart-shared-tooltip');
        for (var i = 0; i < findTooltip.length; i++) {
            findTooltip[i].setAttribute('style', 'margin-left:600px !important');
        }
    }
    else {
        const findTooltip = document.getElementsByClassName('k-tooltip k-chart-tooltip k-chart-tooltip-inverse k-chart-shared-tooltip');
        for (var i = 0; i < findTooltip.length; i++) {
            findTooltip[i].removeAttribute('style');
        }
    }
}

$(document).ready(function () {
    $(document).bind("kendo:skinChange", updateTheme);
});

function updateTheme() {
    $("#Chart_Logdaten").getKendoChart().setOptions({ theme: kendoTheme });
    var chart = $("#Chart_Logdaten").data("kendoChart");

    console.log(chart);
}

function legendHide(e) {
    var chart = $("#Chart_Logdaten").data("kendoChart");
    var currentState = chart.options.legend.visible;
    if (e.checked) {
        if (currentState) {
            chart.options.legend.visible = false;
            chart.redraw();
        }
    }
    else {
        chart.options.legend.visible = true;
        chart.redraw();
    }
}

$(function () {
    $("#Reset").click(function () {
        var dropdownlist = $("#dropdownlist_Axis").data().kendoDropDownList.text();
        var chart = $("#Chart_Logdaten").data("kendoChart");
        var typ = "@ViewBag.Produktserie";
        for (var i = 0; i < chart.options.valueAxis.length; i++) {
            if (chart.options.valueAxis[i].name == "Temperaturen/ES Impuls" || chart.options.valueAxis[i].name == "Temperaturen") {
                chart.options.valueAxis[i].min = -25;
                chart.options.valueAxis[i].max = 100;
            }
            if (chart.options.valueAxis[i].name == "Abgastemp/ ES-Laufzeit SGA") {
                chart.options.valueAxis[i].min = 0;
                chart.options.valueAxis[i].max = 400;
            }
            if (chart.options.valueAxis[i].name == "Verdichteraustritt") {
                chart.options.valueAxis[i].min = 0;
                chart.options.valueAxis[i].max = 150;
            }
            if (chart.options.valueAxis[i].name == "Luftzahl/COP" || chart.options.valueAxis[i].name == "COP") {
                chart.options.valueAxis[i].min = 1;
                chart.options.valueAxis[i].max = 7.4;
            }
            if (chart.options.valueAxis[i].name == "Prozente/Filterspannung" || chart.options.valueAxis[i].name == "Prozente") {
                chart.options.valueAxis[i].min = 0;
                chart.options.valueAxis[i].max = 100;
            }
            if (chart.options.valueAxis[i].name == "Pumpe/Filterstrom") {
                chart.options.valueAxis[i].min = 0;
                chart.options.valueAxis[i].max = 5;
            }
            if (chart.options.valueAxis[i].name == "Differenzdruck") {
                chart.options.valueAxis[i].min = 0;
                chart.options.valueAxis[i].max = 1.2;
            }
            if (chart.options.valueAxis[i].name == "Ventilatordrehzahl - Ventilatorleistung") {
                chart.options.valueAxis[i].min = 0;
                chart.options.valueAxis[i].max = 700;
            }
            if (chart.options.valueAxis[i].name == "Kompressordrezahl/Leistung") {
                chart.options.valueAxis[i].min = 0;
                if (typ == "VampAir K12/K15") {
                    chart.options.valueAxis[i].max = 18000;
                }
                else {
                    chart.options.valueAxis[i].max = 12000;
                }
            }
        }
        chart.redraw();
        minValue = $("#minValue").data("kendoTextBox").value("");
        maxValue = $("#maxValue").data("kendoTextBox").value("");
        dropdownlist = $("#dropdownlist_Axis").data().kendoDropDownList.text("Y-Axis auswählen...");
    })
});

$(function () {
    $("#disableTooltip").click(function () {
        var chart = $("#Chart_Logdaten").data("kendoChart");
        var currentState = chart.options.tooltip.visible;

        if (currentState) {
            chart.options.tooltip.visible = false;
        }
        else {
            chart.options.tooltip.visible = true;
        }
    })
});

$(function () {
    $("#Tabela").click(function () {
        var visibleseries = removeValue();
        var url = '/Home/AuswertungTable?Produktserie=' + "@ViewBag.Produktserie" + '&Logfile=' + "@ViewBag.Logfile" + "&VisibleSeries=" + visibleseries;
        window.location.href = url;
    });
});

$(function () {
    //find alarm file (activate after click on Alarm icon)
    $("#Alarm").click(function () {
        $.ajax({
            type: "POST",
            url: '/Alarms/FindAlarmName',
            success: function (response) {
                console.log(response.alarmfile);
                console.log("Successs :)");
                var url = '/Alarms/AlarmTable?Alarmfile=' + response.alarmfile;
                window.location.href = url;
            },
            error: function (response) {
                console.log("Fehler :/");
            }
        });
    });
});


$(function () {
    //find einstellungen file (activate after click on Einstellungen icon)
    $("#parameter").click(function () {
        $.ajax({
            type: "POST",
            url: '/BoilerParameter/FindEinstellungenName',
            success: function (response) {
                console.log("Success");
                var url = '/BoilerParameter/Initialize?Parameter=' + response.einstellungen;
                window.location.href = url;
            },
            error: function (response) {
                console.log("Fehler :/");
            }
        })
    })
});

$(function () {
    $("#disabledItem").click(function () {
        var chart = $("#Chart_Logdaten").data("kendoChart");

        if ($("#disabledItem").is(':checked') == true) {
            if (chart != null) {
                for (var i = 0; i < chart.options.series.length; i++) {
                    if (chart.options.series[i].visible) {
                        chart.options.series[i].visible = false;
                    }
                }
                chart.redraw();
            }
        }
        if ($("#disabledItem").is(':checked') == false) {
            for (var i = 0; i < chart.options.series.length; i++) {
                if (chart.options.series[i].visible == false) {
                    chart.options.series[i].visible = true;
                }
            }
            chart.redraw();
        }
    })
});

function tooltipHide(e) {
    var chart = $("#Chart_Logdaten").data("kendoChart");
    var currentState = chart.options.tooltip.visible;

    if (currentState) {
        chart.options.tooltip.visible = false;
    }
    else {
        chart.options.tooltip.visible = true;
    }
}

function legendItem(e) {
    var chart = $("#Chart_Logdaten").data("kendoChart");
    if (e.checked) {
        if (chart != null) {
            for (var i = 0; i < chart.options.series.length; i++) {
                if (chart.options.series[i].visible) {
                    chart.options.series[i].visible = false;
                }
            }
            chart.redraw();
        }
    }
    else {
        for (var i = 0; i < chart.options.series.length; i++) {
            if (chart.options.series[i].visible == false) {
                chart.options.series[i].visible = true;
            }
        }
        chart.redraw();
    }
}

function takeFile(events) {
    var dropdownfile = $("#dropdownlist_Logdaten").data().kendoDropDownList.text();
    console.log(dropdownfile);

    if (dropdownfile.includes("log")) {
        if (dropdownfile.includes("wp")) {
            var dropdowntyp = "VampAir K08/K10";
        }
        else {
            var dropdowntyp = "EcoTop | EcoTopZero | Pellet Elegance | OctoPlus | Maximus | Regelzentrale";
        }
    }
    else {
        var dropdowntyp = "Therminator";
    }
    var chart = $("#Chart_Logdaten").data("kendoChart");
    var visibleseries = removeValue();
    var visibleseries = Array();


    for (var i = 0; i < chart.options.series.length; i++) {
        if (chart.options.series[i].visible == true) {
            visibleseries.push(chart.options.series[i].name);
        }
    }

    if (dropdownfile != null && dropdowntyp != null) {
        var url = '/Home/Auswertung?Produktserie=' + dropdowntyp + '&Logfile=' + dropdownfile + '&VisibleSeries=' + visibleseries;
        window.location.href = url;
    }
}

$(function () {
    $("#next").click(function () {
        var visibleseries = removeValue();
        var dataURL = getCurrentURL();

        $.ajax({
            type: "POST",
            url: "/Home/PreviousNext",
            data: {
                page: "next",
                logfile: dataURL.logfile,
                Produktserie: dataURL.produktserie,
                visibleseries: visibleseries
            },
            success: function (response) {
                console.log(response.Visibleseries);
                var url = '/Home/Auswertung?Produktserie=' + response.Produktserie + '&Logfile=' + response.Logfile + '&VisibleSeries=' + response.Visibleseries;
                console.log(response.Visibleseries);
                window.location.href = url;
                console.log(response.Visibleseries);
            },
            error: function (response) {
                alert("Error");
            }
        })
    })
});

$(function () {
    $("#previous").click(function () {
        var visibleseries = removeValue();
        var dataURL = getCurrentURL();
        console.log(visibleseries);
        $.ajax({
            type: "POST",
            url: "/Home/PreviousNext",
            data: {
                page: "previous",
                logfile: dataURL.logfile,
                Produktserie: dataURL.produktserie,
                visibleseries: visibleseries
            },
            success: function (response) {
                var url = '/Home/Auswertung?Produktserie=' + response.Produktserie + '&Logfile=' + response.Logfile + '&VisibleSeries=' + response.Visibleseries;
                window.location.href = url;
            },
            error: function (response) {
                alert("ERROR");
            }
        })
    })
});

$(function () {
    $("#resetZoom").click(function () {
        location.reload();
    })
})

$(function () {
    var chart = $("#Chart_Logdaten").data("kendoChart");
    if ("@ViewBag.Produktserie" == "EcoTop | EcoTopZero | Pellet Elegance | OctoPlus | Maximus | Regelzentrale") {
        for (var i = 0; i < chart.options.series.length; i++) {
            if (chart.options.series[i].name == "Vorlaufsolltemperatur_Kessel") {
                chart.options.series[i].name = "Vorlaufsolltemperatur_Kessel [°C]";
            }
            if (chart.options.series[i].name == "Rezirkulationsluftklappe") {
                chart.options.series[i].name = "Rezirkulationsluftklappe [%]";
            }
            if (chart.options.series[i].name == "AGT_Anhebungsklappe") {
                chart.options.series[i].name = "AGT_Anhebungsklappe [%]";
            }
            if (chart.options.series[i].name == "Primaerluftklappe") {
                chart.options.series[i].name = "Primaerluftklappe [%]";
            }
            if (chart.options.series[i].name == "Sekundaerluftklappe") {
                chart.options.series[i].name = "Sekundaerluftklappe [%]";
            }
            if (chart.options.series[i].name == "SpeichertempMitte_X31") {
                chart.options.series[i].name = "SpeichertempMitte_X31 [°C]";
            }
            if (chart.options.series[i].name == "Kesseltemperatur") {
                chart.options.series[i].name = "Kesseltemperatur [°C]";
            }
            if (chart.options.series[i].name == "Aussentemp") {
                chart.options.series[i].name = "Aussentemp [°C]";
            }
            if (chart.options.series[i].name == "benoetigte_Kesseltemperatur") {
                chart.options.series[i].name = "benoetigte_Kesseltemperatur [°C]";
            }
            if (chart.options.series[i].name == "PuffertempUnten") {
                chart.options.series[i].name = "PuffertempUnten [°C]";
            }
            if (chart.options.series[i].name == "PuffertempOben") {
                chart.options.series[i].name = "PuffertempOben [°C]";
            }
            if (chart.options.series[i].name == "Trinkwassersp_1") {
                chart.options.series[i].name = "Trinkwassersp_1 [°C]";
            }
            if (chart.options.series[i].name == "Fuehlereingang_D1I3") {
                chart.options.series[i].name = "Fuehlereingang_D1I3 [°C]";
            }
            if (chart.options.series[i].name == "Kesseltemperatur_X31") {
                chart.options.series[i].name = "Kesseltemperatur_X31 [°C]";
            }
            if (chart.options.series[i].name == "Rauchgastemperatur_X34") {
                chart.options.series[i].name = "Rauchgastemperatur_X34 [°C]";
            }
            if (chart.options.series[i].name == "Einschubtemperatur_X33") {
                chart.options.series[i].name = "Einschubtemperatur_X33 [°C]";
            }
            if (chart.options.series[i].name == "FWM_Austrittstemp") {
                chart.options.series[i].name = "FWM_Austrittstemp [°C]";
            }
            if (chart.options.series[i].name == "FWM_Eintrittstemp") {
                chart.options.series[i].name = "FWM_Eintrittstemp [°C]";
            }
            if (chart.options.series[i].name == "Brennraumtemperatur") {
                chart.options.series[i].name = "Brennraumtemperatur [°C]";
            }
            if (chart.options.series[i].name == "SZG_Istdrehzahl") {
                chart.options.series[i].name = "SZG_Istdrehzahl [%]";
            }
            if (chart.options.series[i].name == "Ruecklauftemperatur") {
                chart.options.series[i].name = "Ruecklauftemperatur [°C]";
            }
            if (chart.options.series[i].name == "Ruecklauftemperatur_X32") {
                chart.options.series[i].name = "Ruecklauftemperatur_X32 [°C]";
            }
            if (chart.options.series[i].name == "Einschubimpuls_LDZ") {
                chart.options.series[i].name = "Einschubimpuls_LDZ [1/10 sec]";
            }
            if (chart.options.series[i].name == "Aussentemperatur_X42") {
                chart.options.series[i].name = "Aussentemperatur_X42 [°C]";
            }
            if (chart.options.series[i].name == "SpeichertempUnten_X32") {
                chart.options.series[i].name = "SpeichertempUnten_X32 [°C]";
            }
            if (chart.options.series[i].name == "PufferOben") {
                chart.options.series[i].name = "PufferOben [°C]";
            }
            if (chart.options.series[i].name == "PufferUnten") {
                chart.options.series[i].name = "PufferUnten [°C]";
            }
            if (chart.options.series[i].name == "TWS_Temperatur") {
                chart.options.series[i].name = "TWS_Temperatur [°C]";
            }
            if (chart.options.series[i].name == "Vlt_1") {
                chart.options.series[i].name = "Vlt_1 [°C]";
            }
            if (chart.options.series[i].name == "Puffer_X35") {
                chart.options.series[i].name = "Puffer_X35 [°C]";
            }
            if (chart.options.series[i].name == "RT_1") {
                chart.options.series[i].name = "RT_1 [°C]";
            }
            if (chart.options.series[i].name == "Vlst_1") {
                chart.options.series[i].name = "Vlst_1 [°C]";
            }
            if (chart.options.series[i].name == "Vlt_2") {
                chart.options.series[i].name = "Vlt_2 [°C]";
            }
            if (chart.options.series[i].name == "Vlst_2") {
                chart.options.series[i].name = "Vlst_2 [°C]";
            }
            if (chart.options.series[i].name == "RT_2") {
                chart.options.series[i].name = "RT_2 [°C]";
            }
            if (chart.options.series[i].name == "HK_M_1") {
                chart.options.series[i].name = "HK_M_1 [%]";
            }
            if (chart.options.series[i].name == "Filter_Spg") {
                chart.options.series[i].name = "Filter_Spg [kV]";
            }
            if (chart.options.series[i].name == "Filter_Strom") {
                chart.options.series[i].name = "Filter_Strom [mA]";
            }
            if (chart.options.series[i].name == "Statuszeile") {
                chart.options.series[i].name = "Statuszeile";
            }
        }
    }
    else if (("@ViewBag.Produktserie" == "VampAir K08/K10") || ("@ViewBag.Produktserie" == "VampAir K12/K15")) {
        for (var i = 0; i < chart.options.series.length; i++) {
            if (chart.options.series[i].name == "Vorlauftemp") {
                chart.options.series[i].name = "Vorlauftemp [°C]";
            }
            if (chart.options.series[i].name == "Ruecklauftemp") {
                chart.options.series[i].name = "Ruecklauftemp [°C]";
            }
            if (chart.options.series[i].name == "TW_Speichertemp") {
                chart.options.series[i].name = "TW_Speichertemp [°C]";
            }
            if (chart.options.series[i].name == "T1_VLT_WP") {
                chart.options.series[i].name = "T1_VLT_WP [°C]";
            }
            if (chart.options.series[i].name == "Aussentemp") {
                chart.options.series[i].name = "Aussentemp [°C]";
            }
            if (chart.options.series[i].name == "Verdampfungstemp") {
                chart.options.series[i].name = "Verdampfungstemp [°C]";
            }
            if (chart.options.series[i].name == "T4_Verdeintrittstemp") {
                chart.options.series[i].name = "T4_Verdeintrittstemp [°C]";
            }
            if (chart.options.series[i].name == "Kondensationstemp") {
                chart.options.series[i].name = "Kondensationstemp [°C]";
            }
            if (chart.options.series[i].name == "PufferOben") {
                chart.options.series[i].name = "PufferOben [°C]";
            }
            if (chart.options.series[i].name == "PufferUnten") {
                chart.options.series[i].name = "PufferUnten [°C]";
            }
            if (chart.options.series[i].name == "Kompressordrehzahl") {
                chart.options.series[i].name = "Kompressordrehzahl [U/min]";
            }
            if (chart.options.series[i].name == "T3_Sauggastemp") {
                chart.options.series[i].name = "T3_Sauggastemp [°C]";
            }
            if (chart.options.series[i].name == "T6_Fluessigkeitstemp") {
                chart.options.series[i].name = "T6_Fluessigkeitstemp [°C]";
            }
            if (chart.options.series[i].name == "Heizleistung") {
                chart.options.series[i].name = "Heizleistung [W]";
            }
            if (chart.options.series[i].name == "Leistungsanforderung") {
                chart.options.series[i].name = "Leistungsanforderung [W]";
            }
            if (chart.options.series[i].name == "Oeffnungsgrad_EVI_Ventil") {
                chart.options.series[i].name = "Oeffnungsgrad_EVI_Ventil [%]";
            }
            if (chart.options.series[i].name == "EVI_Saturation_Temp") {
                chart.options.series[i].name = "EVI_Saturation_Temp [°C]";
            }
            if (chart.options.series[i].name == "Oeffnungsgrad_Ex_Ventil_Heizen") {
                chart.options.series[i].name = "Oeffnungsgrad_Ex_Ventil_Heizen [%]";
            }
            if (chart.options.series[i].name == "Ventilatordrehzahl") {
                chart.options.series[i].name = "Ventilatordrehzahl [U/min]";
            }
            if (chart.options.series[i].name == "Primaerkreis_iPWM_Durchfluss") {
                chart.options.series[i].name = "Primaerkreis_iPWM_Durchfluss [L/h]";
            }
            if (chart.options.series[i].name == "Ventilatorleistung") {
                chart.options.series[i].name = "Ventilatorleistung [W]";
            }
            if (chart.options.series[i].name == "Überhitzung_Verdampfer") {
                chart.options.series[i].name = "Überhitzung_Verdampfer [°C]";
            }
            if (chart.options.series[i].name == "Überhitzung_Kompressor") {
                chart.options.series[i].name = "Überhitzung_Kompressor [°C]";
            }
            if (chart.options.series[i].name == "Statuszeile_WP") {
                chart.options.series[i].name = "Statuszeile_WP";
            }
            if (chart.options.series[i].name == "E_Stab") {
                chart.options.series[i].name = "E_Stab";
            }
        }
    }
    else if ("@ViewBag.Produktserie" == "Therminator") {
        for (var i = 0; i < chart.options.series.length; i++) {
            if (chart.options.series[i].name == "Kesseltemperatur") {
                chart.options.series[i].name = "Kesseltemperatur [°C]";
            }
            if (chart.options.series[i].name == "Ruecklauftemperatur") {
                chart.options.series[i].name = "Ruecklauftemperatur [°C]";
            }
            if (chart.options.series[i].name == "SZG_Istdrehzahl") {
                chart.options.series[i].name = "SZG_Istdrehzahl [%]";
            }
            if (chart.options.series[i].name == "Abgastemperatur") {
                chart.options.series[i].name = "Abgastemperatur [°C]";
            }
            if (chart.options.series[i].name == "Einschubtemperatur") {
                chart.options.series[i].name = "Einschubtemperatur [°C]";
            }
            if (chart.options.series[i].name == "SLK_Ist_Laufzeit") {
                chart.options.series[i].name = "SLK_Ist_Laufzeit [sek]";
            }
            if (chart.options.series[i].name == "DRZ_Impuls") {
                chart.options.series[i].name = "DRZ_Impuls [sek]";
            }
            if (chart.options.series[i].name == "Puffertemperatur_oben") {
                chart.options.series[i].name = "Puffertemperatur_oben [°C]";
            }
            if (chart.options.series[i].name == "Puffertemperatur_unten") {
                chart.options.series[i].name = "Puffertemperatur_unten [°C]";
            }
            if (chart.options.series[i].name == "Filter_Spannung") {
                chart.options.series[i].name = "Filter_Spannung [kV]";
            }
            if (chart.options.series[i].name == "Filter_Strom") {
                chart.options.series[i].name = "Filter_Strom [mA]";
            }
        }
    }
});

function removeValue() {
    var chart = $("#Chart_Logdaten").data("kendoChart");
    var toRemove = ["[°C]", "[%]", "[1/10sec]", "[U/min]", "[L/h]", "[W]", "[sek]", "[mA]", "[kV]"];
    var visibleseries = Array();

    for (var i = 0; i < chart.options.series.length; i++) {
        if (chart.options.series[i].visible == true) {
            chart.options.series[i].name = chart.options.series[i].name.replace(/\s+/g, '');
            for (var j = 0; j < toRemove.length; j++) {
                if (chart.options.series[i].name.search(toRemove[j]) >= 0) {
                    chart.options.series[i].name = chart.options.series[i].name.replace(toRemove[j], '');
                }
            }
            visibleseries.push(chart.options.series[i].name);
        }
    }
    return visibleseries;
}

function getCurrentURL() {
    const params = new URL(location.href).searchParams;
    const produktserie = params.get('Produktserie');
    const logfile = params.get('Logfile');

    return { produktserie, logfile };
}