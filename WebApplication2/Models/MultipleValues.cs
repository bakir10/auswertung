﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApplication2.MyCode.BoilerParameter;

namespace Models.BoilerParameter
{
    public class MultipleValues
    {
        public string identifier { get; set; }
        public string name { get; set; }
        public int groupOrder { get; set; }  //Einzelzeiten (0 = Sonntag, 1 = Montag, 2 = Dienstag, ...)     //Blockzeiten (0 = Woche, 1 = Wochenende)
        public string value { get; set; }
        public string value1 { get; set; }
        public string value2 { get; set; }
        public string value3 { get; set; }
        public string value4 { get; set; }
        public string value5 { get; set; }
        public string value6 { get; set; }
        public string value7 { get; set; }
        public string value8 { get; set; }


        public MultipleValues(string identifier, LANGUAGE language, string[] values, int groupOrder)
        {
            this.identifier = identifier;
            this.name = Datasets.dict.tanslate(identifier, language);
            this.groupOrder = groupOrder;
            if (values.Length > 0) this.value1 = Datasets.dict.formate(identifier, values[0]);
            if (values.Length > 1) this.value2 = Datasets.dict.formate(identifier, values[1]);
            if (values.Length > 2) this.value3 = Datasets.dict.formate(identifier, values[2]);
            if (values.Length > 3) this.value4 = Datasets.dict.formate(identifier, values[3]);
            if (values.Length > 4) this.value5 = Datasets.dict.formate(identifier, values[4]);
            if (values.Length > 5) this.value6 = Datasets.dict.formate(identifier, values[5]);
            if (values.Length > 6) this.value7 = Datasets.dict.formate(identifier, values[6]);
            if (values.Length > 7) this.value8 = Datasets.dict.formate(identifier, values[7]);
        }

        public MultipleValues(string identifier, LANGUAGE language, int groupOrder)
        {
            this.identifier = identifier;
            this.name = Datasets.dict.tanslate(identifier, language);
            this.value = Datasets.dict.getValueAndformatValueKdaten(identifier);
            this.groupOrder = groupOrder;
        }

        public MultipleValues(string name, string k1, string k2, string k3, string k4, string k5, string k6, string k7, string k8)
        {   
            this.name = name;
            this.value1 = k1;
            this.value2 = k2;
            this.value3 = k3;
            this.value4 = k4;
            this.value5 = k5;
            this.value6 = k6;
            this.value7 = k7;
            this.value8 = k8;
        }
    }
}