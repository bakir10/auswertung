﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApplication2.MyCode.BoilerParameter;

namespace Models.BoilerParameter
{
    public class SingleValue
    {
        public string name { get; set; }
        public string value { get; set; }
        public string identifier { get; set; }
        public int groupOrder { get; set; }
        
        
        public SingleValue(string identifier, LANGUAGE language, object value)
        {
            this.identifier = identifier;
            this.name = Datasets.dict.tanslate(identifier, language);
            this.value = Datasets.dict.formate(identifier, value.ToString());
        }

        public SingleValue(string identifier, LANGUAGE language, object value, int groupOrder) : this(identifier, language, value)
        {
            this.groupOrder = groupOrder;
        }

        public SingleValue(string identifier, LANGUAGE language, int groupOrder)
        {
            this.identifier = identifier;
            this.name = Datasets.dict.tanslate(identifier, language);
            this.value = Datasets.dict.getValueAndformatValueKdaten(identifier);
            this.groupOrder = groupOrder;
        }
    }
}