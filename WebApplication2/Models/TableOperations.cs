﻿using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;
using System.Collections.Generic;

namespace WebApplication2.Models
{
    public interface IOperationsTable
    {
        Alarm filterTable(string partitionKey, string rowKey);
    }

    public class TableOperations : IOperationsTable
    {
        CloudStorageAccount storageAccount;
        CloudTableClient tableClient;
       
        public TableOperations()
        {
            try
            {
                //cloud storage account with connection string from azure keys
                storageAccount = CloudStorageAccount.Parse("DefaultEndpointsProtocol=https;AccountName=auswertetool;AccountKey=65BWUVr+5DCZ6XxwYcytaedErYR4Zl28M8IUW2DfA+Jdd3K6NV22VPz4VtiEyr5L2O9peaeqvZo5Af6N458zmQ==;EndpointSuffix=core.windows.net");
                //The Table Service Client used to perform operations on Table
                tableClient = storageAccount.CreateCloudTableClient();
                CloudTable table = tableClient.GetTableReference("alarms");
                //CloudTable tableUsers = tableClient.GetTableReference("users");
                table.CreateIfNotExists();
            }
            catch (StorageException StorageExceptionObj) 
            {
                throw StorageExceptionObj;
            }
        }

        public Alarm filterTable(string partitionKey, string rowKey)
        {
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse("DefaultEndpointsProtocol=https;AccountName=auswertetool;AccountKey=65BWUVr+5DCZ6XxwYcytaedErYR4Zl28M8IUW2DfA+Jdd3K6NV22VPz4VtiEyr5L2O9peaeqvZo5Af6N458zmQ==;EndpointSuffix=core.windows.net");
            CloudTable table = tableClient.GetTableReference("alarms");
            TableOperation tableOperation = TableOperation.Retrieve<Alarm>(partitionKey, rowKey);
            var a = table.Execute(tableOperation).Result as Alarm;

            return a;
        }

      /*  public User UserCredentials(string PartitionKey, string rowKey)
        {
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse("DefaultEndpointsProtocol=https;AccountName=auswertetool;AccountKey=65BWUVr+5DCZ6XxwYcytaedErYR4Zl28M8IUW2DfA+Jdd3K6NV22VPz4VtiEyr5L2O9peaeqvZo5Af6N458zmQ==;EndpointSuffix=core.windows.net");
            CloudTable tableUsers = tableClient.GetTableReference("users");
            TableOperation tableOperationUser = TableOperation.Retrieve<User>("Users", rowKey);
            var a = tableUsers.Execute(tableOperationUser).Result as User;

            return a;
        }*/
        
        public List<Alarm> setFilter(string filter)
        {
            List<Alarm> alarm = new List<Alarm>();
            CloudTable table = tableClient.GetTableReference("alarms");
            TableQuery<Alarm> query = new TableQuery<Alarm>().Where(TableQuery.GenerateFilterCondition("RowKey", QueryComparisons.Equal, filter));

            return alarm;
        }
    }
}