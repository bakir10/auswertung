﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;
using System.ComponentModel;
using WebApplication2.Controllers;
using Microsoft.WindowsAzure.Storage.Table;
using Microsoft.Azure.Storage;

namespace WebApplication2.Models
{
    public class Alarm : TableEntity
    {
        public Alarm() { }
       
        public Alarm(string partitionKey, string rowKey)
        {
            PartitionKey = partitionKey;
            RowKey = rowKey;
        }
       
        [Column("en")]
        public string en { get; set; }

        [Column("de")]
        public string de { get; set; }
      
       
    }
}