﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using System.Web.Mvc;

namespace WebApplication2.Session
{
    public class SessionTimeoutAttribute : System.Web.Mvc.ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            HttpContext ctx = HttpContext.Current;
            if(HttpContext.Current.Session["sid"] == null)
            {
                filterContext.Result = new RedirectResult("/Users/Login");
                return;
            }
            base.OnActionExecuting(filterContext);
        }
    }
}