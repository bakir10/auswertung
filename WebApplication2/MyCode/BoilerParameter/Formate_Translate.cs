﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Hosting;
using WebApplication2.Controllers;

namespace WebApplication2.MyCode.BoilerParameter
{

    public class Formate_Translate : BoilerParameterController
    {
        public static List<Formate_Translate_Node> dict = new List<Formate_Translate_Node>();
        public static List<KItems> items = new List<KItems>();
        public static string sid = System.Web.HttpContext.Current.Session["sid"].ToString();
        
        public static string Parameter2 = "k_daten.csv";

        public class KItems
        {
            public string identifier { get; }
            public string value { get; }

            public KItems(string identifier, string value)
            {
                this.identifier = identifier;
                this.value = value;
            }
        }

        /// <summary>
        /// Class which stores the values of one line of Übersetzung.csv (and k_daten.csv)
        /// </summary>
        public class Formate_Translate_Node
        {
            public string identifier { get; }
            public string formate { get; }
            public string de { get; }
            public string en { get; }

            public string value { get; }
            

            public Formate_Translate_Node(string identifier, string formate, string de, string en)
            {
                this.identifier = identifier;
                this.formate = formate;
                this.de = de;
                this.en = en;
            }
        }

        /// <summary>
        /// reads Übersetzungs.csv & k_daten.csv and adds each line to the List dict
        /// </summary>
        public Formate_Translate()
        {
            string strLine;
            string[] strArray;

            FileStream myFile = new FileStream(HostingEnvironment.MapPath(@"~/App_Data/Kesselparameter/Uebersetzung.csv"), FileMode.Open);
            
            using (StreamReader myReader = new StreamReader(myFile, System.Text.Encoding.Default))
            {
                while (myReader.Peek() > -1 )
                {
                    strLine = myReader.ReadLine();
                    strArray = strLine.Split(';');
                    dict.Add(new Formate_Translate_Node(strArray[0], strArray[1], strArray[2], strArray[3]));
                }
            }
            myFile.Close();
            
        }

        /// <summary>
        /// Reading kdaten.cvs and add item to KItem list
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        static public IList<KItems> getZaehlerstaende(string kdaten)
        {
            string strLine;
            string[] strArray;
            int rows = 0;

            FileStream file = new FileStream(Directory.GetFiles(Environment.GetEnvironmentVariable("Temp"), @"Einstellungen" + sid.ToString() + "\\" + kdaten).First(), FileMode.Open);
           // FileStream file = new FileStream(path, FileMode.Open);
            using (StreamReader reader = new StreamReader(file, System.Text.Encoding.Default))
            {
                while (reader.Peek() > -1)
                {
                    strLine = reader.ReadLine();
                    rows++;
                    if (rows > 2)
                    {
                        strArray = strLine.Split(',');
                        items.Add(new KItems(strArray[0], strArray[1]));
                    }
                }
            }
            file.Close();

            return items;
        }
       
       public string getValueAndformatValueKdaten(string identifier)
        {
            string result = "";
            if (identifier == "Betriebsstunden_seit_Wartung")
            {
                double seit_wartung;
                KItems pelletsbetrieb = items.Find(x => x.identifier == "Brenner1.sPelletsbetrieb_BMIN");
                KItems pelletsbetriebTL = items.Find(x => x.identifier == "Brenner1.sPelletsbetrieb_BMIN_TL");
                KItems betriebstunde = items.Find(x => x.identifier == "Kessel1.sBM_zum_Wartungszeitpunkt");
                
                if (pelletsbetrieb != null && pelletsbetriebTL != null) 
                {
                    double pelletsb = Convert.ToDouble(pelletsbetrieb.value) / 6;
                    double pelletTL = Convert.ToDouble(pelletsbetriebTL.value) / 6;
                    if (betriebstunde == null)
                    {
                        seit_wartung = (pelletsb + pelletTL) / 10;
                        seit_wartung = Math.Round(seit_wartung, 1);
                        result = formatValue(seit_wartung.ToString(), "Stunden");
                    }
                    else
                    {
                        double betrieb = Convert.ToDouble(betriebstunde.value) / 6;
                        seit_wartung = Math.Round((((pelletsb + pelletTL) - betrieb) / 10),2);
                        result = formatValue(seit_wartung.ToString(), "Stunden");
                    }
                }
            }
            else
            {
                foreach (KItems line in items)
                {
                    if (line.identifier == identifier)
                    {
                        string format = checkFormat(identifier);
                        result = formatValue(line.value, format);
                    }
                }
            }
            return result;
        }
        
        public string checkFormat(string identifier)
        {
            string result = "";
            foreach (Formate_Translate_Node line in dict)
            {
                if (line.identifier == identifier)
                {
                    result =  line.formate;
                }
            }
            return result;
        }
       
        /// <summary>
        /// Translates a value to language
        /// </summary>
        /// <param name="identifier">word which should be translated (must be in column 0 of Übersetzung.csv)</param>
        /// <param name="language">language to which is translated</param>
        /// <returns></returns>
        public string tanslate(string identifier, LANGUAGE language)
        {
            string result = identifier;
            
            foreach (Formate_Translate_Node line in dict)
            {
                if (line.identifier == identifier)
                {
                    if (language == LANGUAGE.de)
                    {
                        result = line.de;
                    }
                    else if (language == LANGUAGE.en)
                    {
                        result = line.en;
                    }
                }
            }
            return result;
        }

        /// <summary>
        /// Formates a value 
        /// </summary>
        /// <param name="identifier"> Identifier of the value that should be formated (must be in column 0 of Übersetzung.csv)</param>
        /// <param name="value">value which is formated</param>
        /// <returns></returns>
        public string formate(string identifier, string value)
        {
            string result = value;
           
            foreach (Formate_Translate_Node line in dict)
            {
                if (line.identifier == identifier)
                {
                    result = formatValue(value, line.formate);
                }
            }
            
            return result;
        }

        public static string ReverseString(string address)
        {
            string[] ipaddress = address.Split('.');
            string newAddress = ipaddress[3] + "." + ipaddress[2] + "." + ipaddress[1] + "." + ipaddress[0];
            return newAddress;
        }

        /// <summary>
        /// applyes a formate to a value (Additional formates can be added here)
        /// </summary>
        /// <param name="value">value which is formated</param>
        /// <param name="format">formate which is apllyed. From Übersetzungs.csv</param>
        /// <returns></returns>
        static private string formatValue(string value, string format)
        {
            if (value == string.Empty || value == null)
            {
                return value;
            }

            string returnvalue = "";
            switch (format)
            {
                case "Einheitslos/10":
                    returnvalue = (Convert.ToDouble(value) / 10).ToString();
                    break;
                case "Einheitslos/100":
                    returnvalue = (Convert.ToDouble(value) / 100).ToString();
                    break;
                case "Temperatur/100":
                    returnvalue = (Convert.ToDouble(value) / 100).ToString() + " °C";
                    break;
                case "Temperatur/10":
                    returnvalue = (Convert.ToDouble(value) / 10).ToString() + " °C";
                    break;
                case "Temperatur":
                    returnvalue = (Convert.ToDouble(value)).ToString() + " °C";
                    break;
                case "Temperaturdifferenz/100":
                    returnvalue = (Convert.ToDouble(value) / 100).ToString() + " K";
                    break;
                case "Temperaturdifferenz/10":
                    returnvalue = (Convert.ToDouble(value) / 10).ToString() + " K";
                    break;
                case "Kelvin/Minute/10":
                    returnvalue = (Convert.ToDouble(value) / 10).ToString() + " K/min";
                    break;
                case "Uhrzeit":
                    returnvalue = value.Substring(0, 5) + " - " + value.Substring(11, 5) + " Uhr";
                    break;
                case "Ein/Aus":
                    if (value == "0") returnvalue = "Aus";
                    else returnvalue = "Ein";
                    break;
                case "U/min":
                    returnvalue = value.ToString() + " U/min";
                    break;
                case "Mischerfunktion":
                    if (value == "RLA_Mischer_AUS") returnvalue = "Mischer Aus";
                    else returnvalue = "Mischer Ein";
                    break;
                case "Prozent":
                    returnvalue = (value + " %");
                    break;
                case "Prozent/10":
                    returnvalue = (Convert.ToDouble(value) / 10).ToString() + " %";
                    break;
                case "Prozent/100":
                    returnvalue = (Convert.ToDouble(value) / 100).ToString() + " %";
                    break;
                case "Prozent/1000":
                    returnvalue = (Convert.ToDouble(value) / 1000).ToString() + " %";
                    break;
                case "Sekunden":
                    returnvalue = (value + " Sek.");
                    break;
                case "Sekunden/10":
                    returnvalue = (Convert.ToDouble(value) / 10).ToString() + " Sek.";
                    break;
                case "Minuten":
                    returnvalue = (value + " Min.");
                    break;
                case "Stunden":
                    returnvalue = (value + " h");
                    break;
                case "Tage":
                    returnvalue = value + (value == "1" ? " Tag" : " Tage");
                    break;
                case "Monate":
                    returnvalue = value + (value == "1" ? " Monat" : " Monate");
                    break;
                case "uiVentilstellung_Invers":
                    if (value == "0") returnvalue = "Heizkreis";
                    else returnvalue = "Trinkwasserspeicher";
                    break;
                case "uiVentilBereitschaftTWS":
                    if (value == "0") returnvalue = "Heizkreis";
                    else returnvalue = "Trinkwasserbereich";
                    break;
                case "Ansteuerung":
                    if (value == "s0") returnvalue = "Phasenanschnitt";
                    else if (value == "s1") returnvalue = "Wellenpaket";
                    else if (value == "h0") returnvalue = "0-10V";
                    else if (value == "h1") returnvalue = "PWM";
                    break;
                case "l/Impuls":
                    returnvalue = (Convert.ToDouble(value) / 10).ToString() + " l/Imp";
                    break;
                case "l/h":
                    returnvalue = (Convert.ToDouble(value)).ToString() + " l/h";
                    break;
                case "HKModus":
                    if (value == "HK_DAUERBETRIEB") returnvalue = "Dauerbetrieb";
                    else if (value == "HK_ABSENKUNG") returnvalue = "Absenkung";
                    else if (value == "HK_AUTOMATIK") returnvalue = "Automatik";
                    else if (value == "HK_AUS") returnvalue = "Aus";
                    else if (value == "HK_FERIEN") returnvalue = "Ferien";
                    else if (value == "HK_ESTRICH") returnvalue = "Estrich";
                    else if (value == "HK_PARTY") returnvalue = "Party";
                    else if (value == "HK_KURZZEIT") returnvalue = "Kurzzeit";
                    break;

                case "HKBetriebsart":
                    if (value == "HK_EINZELZEITEN") returnvalue = "Tagweise";
                    else returnvalue = "Blockweise";
                    break;

                case "Sperrzeit":
                    if (value == "_WP_SPERRE_Zusatzheizung") returnvalue = "E-Stab";
                    else if (value == "_WP_SPERRE_Verdichter") returnvalue = "Wärmepumpe";
                    else if (value == "_WP_SPERRE_ZH_u_Verdichter") returnvalue = "E-Stab + Wärmepumpe";
                    break;
                case "Kp":
                    returnvalue = (Convert.ToDouble(value) / 1000).ToString();
                    break;
                case "Ki":
                    returnvalue = (Convert.ToDouble(value) / 1000).ToString() + " 1/s";
                    break;
                case "Kd":
                    returnvalue = (Convert.ToDouble(value) / 1000).ToString() + " s";
                    break;
                case "Compressor_Model":
                    if (value == "RCC_CM_ZHW_16") returnvalue = "RCC CM ZHW 16";
                    else if (value == "RCC_CM_ZHW_08") returnvalue = "vampair Vorserie ZHW08";
                    else if (value == "RCC_CM_ZHW_08_K1_1") returnvalue = "vampair Vorserie ZHW08 K1.1";
                    else if (value == "RCC_CM_ZHW_015K2") returnvalue = "vampair K10 3~";
                    else if (value == "RCC_CM_ZHW_030K2") returnvalue = "vampair K15 3~";
                    else if (value == "RCC_CM_ZHW_015K2_1Ph") returnvalue = "vampair K10 1~";
                    else if (value == "RCC_CM_ZHW_030K2_1Ph") returnvalue = "vampair K15 1~";
                    break;
                case "Leistung(W)":
                    returnvalue = (Convert.ToDouble(value)).ToString() + " W";
                    break;
                case "Strom":
                    returnvalue = (Convert.ToDouble(value)).ToString() + " A";
                    break;
                case "Strom/10":
                    returnvalue = (Convert.ToDouble(value) / 10).ToString() + " A";
                    break;
                case "Strom(mA)":
                    returnvalue = (Convert.ToDouble(value)).ToString() + " mA";
                    break;
                case "Strom(mA)/100":
                    returnvalue = (Convert.ToDouble(value) / 100).ToString() + " mA";
                    break;
                case "Leistung(kW)":
                    returnvalue = (Convert.ToDouble(value)).ToString() + " kW";
                    break;
                case "Leistung(kW)/10":
                    returnvalue = (Convert.ToDouble(value) / 10).ToString() + " kW";
                    break;
                case "Spannung(V)":
                    returnvalue = (Convert.ToDouble(value)).ToString() + " V";
                    break;
                case "Spannung(kV)":
                    returnvalue = (Convert.ToDouble(value)).ToString() + " kV";
                    break;
                case "Spannung(kV)/10":
                    returnvalue = (Convert.ToDouble(value) / 10).ToString() + " kV";
                    break;
                case "Freq(Hz)":
                    returnvalue = (Convert.ToDouble(value)).ToString() + " Hz";
                    break;
                case "Freq(Hz)/100":
                    returnvalue = (Convert.ToDouble(value)  / 100).ToString() + " Hz";
                    break;
                case "Digital_input_1":
                    if (value == "RCC_DI1_EVULock") returnvalue = "EVU Lock";
                    else if (value == "RCC_DI1_Defrost") returnvalue = "Defrost Request";
                    else if (value == "RCC_DI1_NoFunction") returnvalue = "No function";
                    else if (value == "RCC_DI1_EVULock_Inv") returnvalue = "EVU Lock invertiert";
                    else returnvalue = value;
                    break;
                case "Digital_input_2":
                    if (value == "RCC_DI2_Cooling") returnvalue = "";
                    else if (value == "RCC_DI2_Defrost_Req") returnvalue = "";
                    else if (value == "RCC_DI2_NoFunction") returnvalue = "";
                    else if (value == "RCC_DI2_HP_Switch") returnvalue = "";
                    else if (value == "RCC_DI2_STL_Check") returnvalue = "";
                    else returnvalue = value;
                    break;
                case "Digital_output_2":
                    if (value == "0") returnvalue = "Compressor";
                    else if (value == "1") returnvalue = "Interface";
                    else if (value == "2") returnvalue = "Digital input 1";
                    break;
                case "Digital_output_3":
                    if (value == "0") returnvalue = "Alarm";
                    else if (value == "1") returnvalue = "Interface";
                    else if (value == "2") returnvalue = "Digital input 1";
                    else if (value == "3") returnvalue = "Digital input 2";
                    else if (value == "4") returnvalue = "Alarm inverted";
                    else if (value == "5") returnvalue = "Stator heater";
                    break;
                case "Sensor":
                    if (value == "RCC_PST_7bar") returnvalue = "7 bar";
                    else if (value == "RCC_PST_18bar") returnvalue = "18 bar";
                    else if (value == "RCC_PST_30bar") returnvalue = "30 bar";
                    else if (value == "RCC_PST_50bar") returnvalue = "50 bar";
                    break;
                case "Druck/100":
                    returnvalue = (Convert.ToDouble(value) / 100).ToString() + " bar";
                    break;
                case "Ueberdruck/100":
                    returnvalue = (Convert.ToDouble(value) / 100).ToString() + " barg";
                    break;
                case "Capacity_input_mode":
                    if (value == "0") returnvalue = "0 - 10V";
                    else returnvalue = "Modbus";
                    break;
                case "Statorheater_control_Source":
                    if (value == "0") returnvalue = "Discharge temp.";
                    else returnvalue = "Oil temp T7";
                    break;
                case "ProduktRevison":
                    if (value == "K10_1to3") returnvalue = "K10 Revision 1-3";
                    else if (value == "K10_4to5") returnvalue = "K10 Revision 4-5";
                    else if (value == "K10_6") returnvalue = "K10 Revision 6+";
                    else if (value == "K15_10_to_11") returnvalue = "K15 Revision 10-11";
                    else if (value == "K15_12") returnvalue = "K15 Revision 12+";
                    break;
                case "Pumpentyp":
                    if (value == "PKPT_iPWM1") returnvalue = "Wilo iPWM1";
                    else if (value == "PKPT_iPWM2") returnvalue = "Wilo iPWM2";
                    else if (value == "PKPT_0_10V_DN15") returnvalue = "0-10V Pump & FlowSensor DN15";
                    else if (value == "PKPT_0_10V_DN20") returnvalue = "0-10V Pump & FlowSensor DN20";
                    break;
                case "WPBZS4_Freigabe":
                    if (value == "WPBZS4_FG_nurKomp") returnvalue = "nur Kompressor";
                    else if (value == "WPBZS4_FG_Komp_Zusatzheizung") returnvalue = "Kompressor und Zusatzheizung";
                    break;
                case "Photovoltaik":
                    if (value == "_PV_nichtvorhanden") returnvalue = "nicht vorhanden";
                    else if (value == "_PV_SolarLog") returnvalue = "SolarLog";
                    else if (value == "_PV_Fronius") returnvalue = "Fronius";
                    else if (value == "_PV_SolarEdge") returnvalue = "SolarEdge";
                    else if (value == "_PV_extern") returnvalue = "Extern";
                    break;
                case "KesselreglerTyp":
                    if (value == "HK_FixKT") returnvalue = "fix";
                    else if (value == "HK_Gleitende_KT") returnvalue = "gleitend";
                    break;
                case "Fremdkesselfuehler":
                    if (value == "0") returnvalue = "H1X31";
                    else if (value == "1") returnvalue = "D2I1";
                    else if (value == "2") returnvalue = "D2I3";
                    break;
                case "Modus_Analogausgang":
                    if (value == "0") returnvalue = "Kesselleistung";
                    else if (value == "1") returnvalue = "ber. Kesselsolltemp.";
                    break;
                case "FunktionAusgangX7":
                    if (value == "X7_Keine_Funktion") returnvalue = "Keine Funktion";
                    else if (value == "X7_RLA_Pumpe") returnvalue = "Fremdkessel RLA Pumpe";
                    else if (value == "X7_Kuehlventil") returnvalue = "Kühlventil";
                    else if (value == "X7_VentilSpeichergruppe1") returnvalue = "Ventilspeichergruppe 1";
                    break;
                case "FunktionAusgangX5":
                    if (value == "X5_Standard") returnvalue = "Standard";
                    else if (value == "X5_VentilSpeichergruppe1") returnvalue = "Ventilspeichergruppe 1";
                    break;
                case "FunktionAusgangH1X7":
                    if (value == "H1X7_Standard") returnvalue = "Standard";
                    else if (value == "H1X7_VentilSpeichergruppe2") returnvalue = "Ventilspeichergruppe 2";
                    break;
                case "FunktionAusgangH1X5":
                    if (value == "H1X5_Standard") returnvalue = "Standard";
                    else if (value == "H1X5_VentilSpeichergruppe2") returnvalue = "Ventilspeichergruppe 2";
                    break;
                case "Kuehlventil_invers":
                    if (value == "Stromlos_Puffer") returnvalue = "Stromlos Puffer";
                    else if (value == "Stromlos_HK") returnvalue = "Stromlos Heizkreis";
                    break;
                case "FunktionX28":
                    if (value == "X28_Fremdkessel") returnvalue = "Fremdkessel";
                    else if (value == "X28_Stoerung") returnvalue = "Störung";
                    else if (value == "X28_WP_in_Betrieb") returnvalue = "WP in Betrieb";
                    else if (value == "X28_Elektro_Heizung") returnvalue = "Elektro Heizung";
                    else if (value == "X28_Kuehlbetrieb") returnvalue = "Kühlbetrieb";
                    break;
                case "PufferpumpeTyp":
                    if (value == "Standart") returnvalue = "Rücklauf";
                    else if (value == "Bypasspumpe") returnvalue = "Bypass";
                    break;
                case "KaskadeEIN":
                    if (value == "0") returnvalue = "Deaktiviert";
                    else if (value == "1") returnvalue = "Kessel als Master";
                    else if (value == "2") returnvalue = "Kessel als Slave";
                    break;
                case "Zielpuffer":
                    if (true) returnvalue = value;
                    if (value == "0") returnvalue = "Puffer 1";
                    else if (value == "1") returnvalue = "Puffer 2";
                    else if (value == "2") returnvalue = "Puffer 3";
                    else if (value == "3") returnvalue = "Puffer 4";
                    break;
                case "Kesselwechsel":
                    if (value == "0") returnvalue = "Konstant";
                    else if (value == "1") returnvalue = "Automatisch";
                    break;
                case "Notbetrieb":
                    if (value == "keineFunktion") returnvalue = "keine Funktion";
                    else if (value == "HK1_Pumpe") returnvalue = "HK-Pumpe 1 (X9)";
                    else if (value == "HK2_Pumpe") returnvalue = "HK-Pumpe 2 (X10)";
                    else if (value == "BoilerPumpe") returnvalue = "TWS-Pumpe (X8)";
                    else if (value == "Puffer_Pumpe") returnvalue = "Pufferpumpe (X7)";
                    else if (value == "RLAPumpe") returnvalue = "RLA-Pumpe (X15)";
                    else if (value == "Zirkulationspumpe") returnvalue = "Zirkulation (X5)";
                    else if (value == "HK1_Mischer") returnvalue = "Mischer 1 (X11)";
                    else if (value == "HK2_Mischer") returnvalue = "Mischer 2 (X12)";
                    else if (value == "RLA_Mischer") returnvalue = "RLA-Mischer (X13)";
                    else if (value == "SGT") returnvalue = "Saugturbine (X3)";
                    else if (value == "Gluehstab") returnvalue = "Zündung (X4)";
                    else if (value == "WTR") returnvalue = "Wärmetauscher (X24)";
                    else if (value == "Einschubmotor") returnvalue = "Einschub (X23)";
                    else if (value == "RA_Saug") returnvalue = "Lagerraum Saug (X14)";
                    else if (value == "RA_Direkt") returnvalue = "Austragung (X22)";
                    else if (value == "AschenaustrMotor") returnvalue = "Aschenaustragung (X16)";
                    else if (value == "Rueckbrandschieber") returnvalue = "RBS (X25)";
                    else if (value == "Reserve") returnvalue = "Reserve (X6)";
                    else if (value == "Primaerluft") returnvalue = "Primaerluft";
                    else if (value == "Zuendzuluft") returnvalue = "Zuendzuluft";
                    else if (value == "Magnet_Reserve") returnvalue = "Magnet_Reserve";
                    break;
                case "AusgaengeZuendung":
                    if (value == "_Z_X4X4") returnvalue = "X4;X4";
                    else if (value == "_Z_X4X5") returnvalue = "X4;X5";
                    else if (value == "_Z_X4X7") returnvalue = "X4;X7";
                    else if (value == "_Z_X4X14") returnvalue = "X4;X14";
                    break;
                case "FunktionX51":
                    if (value == "EXTERNE_ANF") returnvalue = "Exterene Anf.";
                    else if (value == "FREMDKESSEL") returnvalue = "Fremdkessel Fg";
                    else if (value == "EXT_NACHRICHT") returnvalue = "Externe Nachricht";
                    break;
                case "Funktion_Reserverelais":
                    if (value == "KEINE_FUNKTION") returnvalue = "keine Funktion";
                    else if (value == "KESSELBETRIEB") returnvalue = "Kesselbetrieb";
                    else if (value == "DREI_WEGE_MOTORVENTIL") returnvalue = "3-Wege-Motorventil";
                    else if (value == "VERSORGUNG_SSUE") returnvalue = "VERSORGUNG_SSUE";
                    else if (value == "BRENNWERT_SPUELUNG") returnvalue = "BRENNWERT_SPUELUNG";
                    break;
                case "VentStellung_stromlos":
                    if (value == "Vent_to_Puffer") returnvalue = "Puffer";
                    else if (value == "Vent_to_FK") returnvalue = "Fremdkessel";
                    break;
                case "KESSELTYP":
                    if (value == "Plus") returnvalue = "Octoplus";
                    else if (value == "Top") returnvalue = "PelletTop";
                    else if (value == "T2") returnvalue = "Therminator II";
                    else if (value == "HK_Regelung") returnvalue = "Heizkreisregelzentrale";
                    else if (value == "Elegance") returnvalue = "Pellet Elegance";
                    else if (value == "WP") returnvalue = "Wärmepumpe";
                    else if (value == "EcoTopZero") returnvalue = "EcoTopZero";
                    else if (value == "Maximus") returnvalue = "Maximus";
                    else if (value == "EcoHackZero") returnvalue = "EcoHackZero";
                    break;
                case "KESSELLEISTUNG":
                    if (value == "_9_9kW") returnvalue = "9,9 kW";
                    else if (value == "_14_9kW") returnvalue = "15 kW";
                    else if (value == "_35_kW") returnvalue = "35 kW";
                    else if (value == "_70_kW") returnvalue = "70 kW";
                    else if (value == "_49_kW") returnvalue = "49 kW";
                    else if (value == "_45_kW") returnvalue = "45 kW";
                    else if (value == "0") returnvalue = "150 kW";
                    else if (value == "1") returnvalue = "200 kW";
                    else if (value == "2") returnvalue = "250 kW";
                    else if (value == "3") returnvalue = "300 kW";
                    break;
                case "NENNLLEISTUNG":
                    if (value == "0") returnvalue = "150 kW";
                    else if (value == "1") returnvalue = "200 kW";
                    else if (value == "2") returnvalue = "250 kW";
                    else if (value == "3") returnvalue = "300 kW";
                    else returnvalue = value;
                    break;
                case "ANLAGENTYP":
                    if (value == "MANUELLE_BEFUELLUNG") returnvalue = "Man. Befüllung";
                    else if (value == "SAUGAUSTRAGUNG") returnvalue = "Saugaustragung";
                    else if (value == "RAUMAUSTRAGUNG") returnvalue = "Raumaustragung";
                    else if (value == "ManVORRATSBEH_INTERN") returnvalue = "Vorratsbehälter intern";
                    else if (value == "STEIGSCHNECKE") returnvalue = "Steigschnecke";
                    else if (value == "STEIGSCHNECKE_KNICKARM") returnvalue = "Steigschnecke Knickarm";
                    else returnvalue = value;
                    break;
                case "ANLAGENTYPMAXIMUS":
                    if (value == "1") returnvalue = "Saugaustragung";
                    else if (value == "2") returnvalue = "Direktaustragung";
                    else if (value == "4") returnvalue = "Steigeschnecke";
                    else if (value == "5") returnvalue = "Steignschnecke + Knickarm ";
                    else returnvalue = value;
                    break;
                case "KESSELBETRIEBSART":
                    if (value == "KB_KESSEL_AUSGESCHALTET") returnvalue = "Ausgeschaltet";
                    else if (value == "KB_KESSEL_AUTOMATIK") returnvalue = "Automatik";
                    else if (value == "KB_KESSEL_MANUELLER_START") returnvalue = "man. Start";
                    else if (value == "KB_KESSEL_KAMINFEGER") returnvalue = "Kaminfegerfkt";
                    break;
                case "uiZuendfoenzuendung":
                    if (value == "0") returnvalue = "Gluehstab";
                    else if (value == "1") returnvalue = "Heißluftgeblaese";
                    break;
                case "SensorTyp_Temp":
                    if (value == "0") returnvalue = "PT1000";
                    else if (value == "1") returnvalue = "KTY81-110";
                    break;
                case "Widerstand/10":
                    returnvalue = (Convert.ToDouble(value) / 10).ToString() + " Ω";
                    break;
                case "Umschalteinheit":
                    if (value == "NEIN") returnvalue = "Nicht vorhanden";
                    else if (value == "MIT_3_SONDEN") returnvalue = "Für max. 3 Sonden";
                    else if (value == "MIT_4_SONDEN") returnvalue = "Für max. 4 Sonden";
                    else if (value == "MIT_5_SONDEN") returnvalue = "Für max. 5 Sonden";
                    else if (value == "MIT_6_SONDEN") returnvalue = "Für max. 6 Sonden";
                    else if (value == "MIT_7_SONDEN") returnvalue = "Für max. 7 Sonden";
                    else if (value == "MIT_8_SONDEN") returnvalue = "Für max. 8 Sonden";
                    else if (value == "MIT_9_SONDEN") returnvalue = "Für max. 9 Sonden";
                    else if (value == "MIT_10_SONDEN") returnvalue = "Für max. 10 Sonden";
                    else if (value == "MIT_12_SONDEN") returnvalue = "Für max. 12 Sonden";
                    else if (value == "KASKADE_6_SONDEN") returnvalue = "Verteilbox";
                    break;
                case "Sondenumschaltung":
                    if (value == "Automatik") returnvalue = "automatisch";
                    else if (value == "punktuell") returnvalue = "punktuell";
                    else if (value == "nur_Sonde_1") returnvalue = "nur Sonde 1";
                    else if (value == "nur_Sonde_2") returnvalue = "nur Sonde 2";
                    else if (value == "nur_Sonde_3") returnvalue = "nur Sonde 3";
                    else if (value == "nur_Sonde_4") returnvalue = "nur Sonde 4";
                    else if (value == "nur_Sonde_5") returnvalue = "nur Sonde 5";
                    else if (value == "nur_Sonde_6") returnvalue = "nur Sonde 6";
                    else if (value == "nur_Sonde_7") returnvalue = "nur Sonde 7";
                    else if (value == "nur_Sonde_8") returnvalue = "nur Sonde 8";
                    else if (value == "nur_Sonde_9") returnvalue = "nur Sonde 9";
                    else if (value == "nur_Sonde_10") returnvalue = "nur Sonde 10";
                    else if (value == "nur_Sonde_11") returnvalue = "nur Sonde 11";
                    else if (value == "nur_Sonde_12") returnvalue = "nur Sonde 12";
                    break;
                case "EnergiequellenTyp":
                    if (value == "RGZ_Kessel") returnvalue = "Kessel";
                    else if (value == "RGZ_Fernwaerme") returnvalue = "Fernwärme";
                    break;
                case "Raumeinfluss":
                    if (value == "RAUM_AUS") returnvalue = "Aus";
                    else if (value == "RAUM_EIN") returnvalue = "Ein";
                    else if (value == "RAUM_GLEITEND") returnvalue = "Gleitend";
                    break;
                case "DT_FreigabeArt":
                    if (value == "FG_IMMERAUS") returnvalue = "Immer Aus";
                    else if (value == "FG_IMMEREIN") returnvalue = "Immer Ein";
                    else if (value == "FG_ZEITEN") returnvalue = "Montag - Sonntag";
                    else if (value == "FG_MOFRSASO") returnvalue = "Blockweise";
                    else if (value == "FG_TAGWEISE") returnvalue = "Tagweise";
                    break;
                case "Puffer_Betriebsart":
                    if (value == "PUFFER_AUS") returnvalue = "Immer Aus";
                    else if (value == "PUFFER_EIN") returnvalue = "Immer Ein";
                    else if (value == "PUFFER_ZEIT") returnvalue = "Zeitschaltung";
                    break;
                case "Puffer_ENERGIEQUELLE":
                    if (value == "EQ_KESSEL") returnvalue = "Kessel";
                    else if (value == "EQ_PUFFER1") returnvalue = "Puffer 1";
                    else if (value == "EQ_PUFFER2") returnvalue = "Puffer 2";
                    else if (value == "EQ_PUFFER3") returnvalue = "Puffer 3";
                    else if (value == "EQ_PUFFER4") returnvalue = "Puffer 4";
                    break;
                case "Energiequelle":
                    if (value == "0") returnvalue = "Kessel";
                    else if (value == "1") returnvalue = "Puffer 1";
                    else if (value == "2") returnvalue = "Puffer 2";
                    else if (value == "3") returnvalue = "Puffer 3";
                    else if (value == "4") returnvalue = "Puffer 4";
                    break;

                case "Solar_udPumpensteuerung":
                    if (value == "0") returnvalue = "Immer Aus";
                    else if (value == "1") returnvalue = "Immer Ein";
                    else if (value == "2") returnvalue = "Automatik";
                    break;
                case "Solarschema":
                    if (value == "_2_PUMPEN_1_FELD") returnvalue = "2 Pumpen";
                    else if (value == "_PUMPE_UMSCHALTVENTIL") returnvalue = "Pumpe/Ventil";
                    else if (value == "_2Zonen") returnvalue = "Schichtlademodul";
                    else if (value == "_2Felder2Pumpen") returnvalue = "2-Felder - 2-Pumpen";
                    else if (value == "_2Felder_PumpeVentil") returnvalue = "2-Felder - Pumpe-Ventil";
                    else if (value == "_2_EKR") returnvalue = "2 Einkreisregler";
                    else if (value == "_3K_3Pumpen") returnvalue = "3 Pumpen";
                    else if (value == "_3K_Pumpe_Ventil_Pumpe") returnvalue = "Pumpe Ventil Pumpe";
                    else if (value == "_3K_Pumpe_Ventil_Ventil") returnvalue = "Pumpe 2 Ventile";
                    else if (value == "_3K_Schichtlademodul_Pumpe") returnvalue = "Schichtlademodul Pumpe";
                    else if (value == "_2Felder_2Pumpen_2Speicher") returnvalue = "2-Felder - 2-Speicher";
                    break;
                case "Speichervorrang":
                    if (value == "-1") returnvalue = "nicht verwendet";
                    else if (value == "0") returnvalue = "Speicher 1";
                    else if (value == "1") returnvalue = "Speicher 2";
                    else if (value == "2") returnvalue = "Speicher 3";
                    break;
                case "Ventilruhestellung":
                    if (value == "0") returnvalue = "Speicher 1";
                    else if (value == "1") returnvalue = "Speicher 2";
                    break;
                case "byVentilruhestellung2":
                    if (value == "0") returnvalue = "Speicher 1,2";
                    else if (value == "1") returnvalue = "Speicher 3";
                    break;
                case "PumpenEinteilung":
                    if (value == "0") returnvalue = "Speicher 1 Pumpe";
                    else if (value == "1") returnvalue = "Speicher 2 Pumpe";
                    break;
                case "DurchflussmengengeberTyp":
                    if (value == "0") returnvalue = "mech. Durchflussgeber";
                    else if (value == "1") returnvalue = "elektr. Durchflussgeber DN10";
                    else if (value == "2") returnvalue = "elektr. Durchflussgeber DN15";
                    break;
                case "Kollektorfuehlertyp":
                    if (value == "Modul_PT1000") returnvalue = "Modul PT1000";
                    else if (value == "Modul_KTY") returnvalue = "Modul KTY";
                    else if (value == "S1I1_PT1000") returnvalue = "S1I1";
                    else if (value == "S2I1_PT1000") returnvalue = "S2I1";
                    else if (value == "S3I1_PT1000") returnvalue = "S3I1";
                    else if (value == "S4I1_PT1000") returnvalue = "S4I1";
                    break;
                case "Speicherfuehler1Typ":
                    if (value == "Modul_SPF_KTY_1K") returnvalue = "I9 KTY-1K";
                    else if (value == "Modul_SPF_PT1000") returnvalue = "I9 PT1000";
                    else if (value == "Modul_I8_KTY_2K") returnvalue = "I8";
                    else if (value == "Modul_I7_KTY_2K") returnvalue = "I7";
                    else if (value == "Octoplus_X32_SpUnten") returnvalue = "X32";
                    else if (value == "Octoplus_X31_SpMitte") returnvalue = "X31";
                    else if (value == "LT_X35_PufferMitte") returnvalue = "X35";
                    else if (value == "LT_X36_PufferUnten") returnvalue = "X36";
                    else if (value == "LT_X44_PufferOben") returnvalue = "X44";
                    else if (value == "HKMod1_I6") returnvalue = "H1 I6";
                    else if (value == "HKMod1_I5") returnvalue = "H1 I5";
                    else if (value == "HKMod2_I6") returnvalue = "H2 I6";
                    else if (value == "HKMod2_I5") returnvalue = "H2 I5";
                    else if (value == "HKMod3_I6") returnvalue = "H3 I6";
                    else if (value == "HKMod3_I5") returnvalue = "H3 I5";
                    else if (value == "HKMod4_I6") returnvalue = "H4 I6";
                    else if (value == "HKMod4_I5") returnvalue = "H4 I5";
                    else if (value == "D1i1") returnvalue = "D1 I1";
                    else if (value == "D1i2") returnvalue = "D1 I2";
                    else if (value == "D2i1") returnvalue = "D2 I1";
                    else if (value == "D2i2") returnvalue = "D2 I2";
                    else if (value == "D3i1") returnvalue = "D3 I1";
                    else if (value == "D3i2") returnvalue = "D3 I2";
                    else if (value == "D4i1") returnvalue = "D4 I1";
                    else if (value == "D4i2") returnvalue = "D4 I2";
                    break;
                case "Speicherfuehler2Typ":
                    if (value == "Modul_SPF_KTY_1K") returnvalue = "I4 KTY-1K";
                    else if (value == "Modul_SPF_PT1000") returnvalue = "I4 PT1000";
                    else if (value == "Modul_I8_KTY_2K") returnvalue = "I8";
                    else if (value == "Modul_I7_KTY_2K") returnvalue = "I7";
                    else if (value == "Octoplus_X32_SpUnten") returnvalue = "X32";
                    else if (value == "Octoplus_X31_SpMitte") returnvalue = "X31";
                    else if (value == "LT_X35_PufferMitte") returnvalue = "X35";
                    else if (value == "LT_X36_PufferUnten") returnvalue = "X36";
                    else if (value == "LT_X44_PufferOben") returnvalue = "X44";
                    else if (value == "HKMod1_I6") returnvalue = "H1 I6";
                    else if (value == "HKMod1_I5") returnvalue = "H1 I5";
                    else if (value == "HKMod2_I6") returnvalue = "H2 I6";
                    else if (value == "HKMod2_I5") returnvalue = "H2 I5";
                    else if (value == "HKMod3_I6") returnvalue = "H3 I6";
                    else if (value == "HKMod3_I5") returnvalue = "H3 I5";
                    else if (value == "HKMod4_I6") returnvalue = "H4 I6";
                    else if (value == "HKMod4_I5") returnvalue = "H4 I5";
                    else if (value == "D1i1") returnvalue = "D1 I1";
                    else if (value == "D1i2") returnvalue = "D1 I2";
                    else if (value == "D2i1") returnvalue = "D2 I1";
                    else if (value == "D2i2") returnvalue = "D2 I2";
                    else if (value == "D3i1") returnvalue = "D3 I1";
                    else if (value == "D3i2") returnvalue = "D3 I2";
                    else if (value == "D4i1") returnvalue = "D4 I1";
                    else if (value == "D4i2") returnvalue = "D4 I2";
                    break;
                case "bsiFuehlertyp":
                    if (value == "0") returnvalue = "KTY81-110";
                    else returnvalue = "PT1000";
                    break;
                case "Wellenpaket":
                    if (value == "0") returnvalue = "Phasenanschnitt";
                    else if (value == "1") returnvalue = "Wellenpaket";
                    break;
                case "Signaltyp_AnOut":
                    if (value == "0") returnvalue = "0-10V";
                    else if (value == "1") returnvalue = "PWM";
                    break;
                case "PWM_AnOut_Invers":
                    if (value == "0") returnvalue = "0-100%";
                    else if (value == "1") returnvalue = "100-0%";
                    break;
                case "Pumpenbetriebsart":
                    if (value == "FWM_PUMPE_AUS") returnvalue = "Aus";
                    else if (value == "FWM_PUMPE_EIN") returnvalue = "Ein";
                    else if (value == "FWM_PUMPE_AUTO") returnvalue = "Automatisch";
                    break;
                case "TWS_Eigenschaft":
                    if (value == "0") returnvalue = "Trinkwasserspeicher";
                    else if (value == "1") returnvalue = "Trinkwasserbereich";
                    break;
                case "Vorrang":
                    if (value == "AUS_AUTOMATIK") returnvalue = "Aus";
                    else if (value == "EIN") returnvalue = "Ein";
                    else if (value == "VERMINDERT") returnvalue = "Vermindert";
                    else if (value == "TWR_VORR_Nur_HK_Min") returnvalue = "Automatik";
                    break;
                case "Bezugsfuehler_SpeicherOben":
                    if (value == "SpeicherOben_OctoPlus") returnvalue = "Trinkwassertemp. 1";
                    else if (value == "FWModul_I4_KTY1k") returnvalue = "Fühler FWM KTY 1k";
                    else if (value == "FWModul_I4_PT") returnvalue = "Fühler FWM PT1000";
                    else if (value == "TWSF_2") returnvalue = "Trinkwassertemp. 2";
                    else if (value == "TWSF_3") returnvalue = "Trinkwassertemp. 3";
                    else if (value == "TWSF_4") returnvalue = "Trinkwassertemp. 4";
                    break;
                case "Hocheffizenzpumpe":
                    if (value == "0") returnvalue = "Standardpumpe";
                    else if (value == "1") returnvalue = "Hocheffizenzpumpe";
                    break;
                case "uiFWM_konvent":
                    if (value == "0") returnvalue = "Strömungschalter + Sensor schnell";
                    else if (value == "10") returnvalue = "FWM konvent 20 bis 40 l/min";
                    else if (value == "20") returnvalue = "FWM konvent 50 l/min";
                    break;
                case "Zk_FreigabeArt":
                    if (value == "Zk_ImmerAus") returnvalue = "Immer Aus";
                    else if (value == "ZK_ImmerEin") returnvalue = "Immer Ein";
                    else if (value == "Zk_Zeitregelung") returnvalue = "Montag - Sonntag";
                    else if (value == "Zk_Blockweise") returnvalue = "Blockweise";
                    else if (value == "Zk_Tagweise") returnvalue = "Tagweise";
                    break;
                case "DIFF_BETRIEBSART":
                    if (value == "BA_Aus") returnvalue = "Immer Aus";
                    else if (value == "BA_Ein") returnvalue = "Immer Ein";
                    else if (value == "BA_Auto") returnvalue = "Automatik";
                    break;
                case "Kessel_Diff_Fuehlereingang":
                    if (value == "DFA_MODUL") returnvalue = "Modul";
                    else if (value == "DFA_X30") returnvalue = "X30";
                    else if (value == "DFA_X31") returnvalue = "X31";
                    else if (value == "DFA_X32") returnvalue = "X32";
                    else if (value == "DFA_X35") returnvalue = "X35";
                    else if (value == "DFA_X36") returnvalue = "X36";
                    else if (value == "DFA_X39") returnvalue = "X39";
                    else if (value == "DFA_X44") returnvalue = "X44";
                    else if (value == "DFA_H1I5") returnvalue = "H1X44 (I5)";
                    else if (value == "DFA_H1I6") returnvalue = "H1X36 (I6)";
                    else if (value == "DFA_H1I7") returnvalue = "H1X37 (I7)";
                    else if (value == "DFA_H2I5") returnvalue = "H2X44 (I5)";
                    else if (value == "DFA_H2I6") returnvalue = "H2X36 (I6)";
                    else if (value == "DFA_H2I7") returnvalue = "H2X39 (I7)";
                    else if (value == "DFA_H3I5") returnvalue = "H3X44 (I5)";
                    else if (value == "DFA_H3I6") returnvalue = "H3X36 (I6)";
                    else if (value == "DFA_H3I7") returnvalue = "H3X39 (I7)";
                    else if (value == "DFA_H4I6") returnvalue = "S1I1";
                    else if (value == "DFA_H4I7") returnvalue = "S1I3";
                    else if (value == "DFA_H4I10") returnvalue = "S1I4";
                    else if (value == "DFA_H4I11") returnvalue = "S1I5";
                    else if (value == "DFA_S1I1") returnvalue = "S1I9";
                    else if (value == "DFA_S1I3") returnvalue = "D1I1 (I3)";
                    else if (value == "DFA_S1I4") returnvalue = "D1I2 (I4)";
                    else if (value == "DFA_S1I5") returnvalue = "D1I3 (I9)";
                    else if (value == "DFA_S1I9") returnvalue = "D1I4 (I10)";
                    else if (value == "DFA_D1I3") returnvalue = "D2I1 (I3)";
                    else if (value == "DFA_D1I4") returnvalue = "D2I2 (I4)";
                    else if (value == "DFA_D1I9") returnvalue = "D2I3 (I9)";
                    else if (value == "DFA_D1I10") returnvalue = "D2I4 (I10)";
                    else if (value == "DFA_D2I3") returnvalue = "D3I1 (I3)";
                    else if (value == "DFA_D2I4") returnvalue = "D3I2 (I4)";
                    else if (value == "DFA_D2I9") returnvalue = "D3I3 (I9)";
                    else if (value == "DFA_D2I10") returnvalue = "D3I4 (I10)";
                    else if (value == "DFA_D3I3") returnvalue = "D4I1 (I3)";
                    else if (value == "DFA_D3I4") returnvalue = "D4I2 (I4)";
                    else if (value == "DFA_D3I9") returnvalue = "D4I3 (I9)";
                    else if (value == "DFA_D3I10") returnvalue = "D4I4 (I10)";
                    else if (value == "DFA_D4I3") returnvalue = "";
                    else if (value == "DFA_D4I4") returnvalue = "";
                    else if (value == "DFA_D4I9") returnvalue = "";
                    else if (value == "DFA_D4I10") returnvalue = "";
                    else if (value == "DFA_S2I1") returnvalue = "";
                    else if (value == "DFA_S2I3") returnvalue = "";
                    else if (value == "DFA_S2I4") returnvalue = "";
                    else if (value == "DFA_S2I5") returnvalue = "";
                    else if (value == "DFA_S2I9") returnvalue = "";
                    break;
                case "WP_Diff_Fuehlereingang":
                    if (value == "DFA_MODUL") returnvalue = "Modul";
                    else if (value == "DFA_X30") returnvalue = "H1X44 (I5)";
                    else if (value == "DFA_X31") returnvalue = "H1X36 (I6)";
                    else if (value == "DFA_X32") returnvalue = "H1X39 (I7)";
                    else if (value == "DFA_X35") returnvalue = "H1X31 (I10)";
                    else if (value == "DFA_X36") returnvalue = "H1X32 (I11)";
                    else if (value == "DFA_X39") returnvalue = "H2X44 (I5)";
                    else if (value == "DFA_X44") returnvalue = "H2X36 (I6)";
                    else if (value == "DFA_H1I5") returnvalue = "H2X39 (I7)";
                    else if (value == "DFA_H1I6") returnvalue = "H2X31 (I10)";
                    else if (value == "DFA_H1I7") returnvalue = "H2X32 (I11)";
                    else if (value == "DFA_H2I5") returnvalue = "H3X44 (I5)";
                    else if (value == "DFA_H2I6") returnvalue = "H3X36 (I6)";
                    else if (value == "DFA_H2I7") returnvalue = "H3X39 (I7)";
                    else if (value == "DFA_H3I5") returnvalue = "H3X31 (I10)";
                    else if (value == "DFA_H3I6") returnvalue = "H3X32 (I11)";
                    else if (value == "DFA_H3I7") returnvalue = "H4X44 (I5)";
                    else if (value == "DFA_H4I6") returnvalue = "H4X36 (I6)";
                    else if (value == "DFA_H4I7") returnvalue = "H4X39 (I7)";
                    else if (value == "DFA_H4I10") returnvalue = "H4X31 (I10)";
                    else if (value == "DFA_H4I11") returnvalue = "H4X32 (I11)";
                    else if (value == "DFA_S1I1") returnvalue = "S1I1";
                    else if (value == "DFA_S1I3") returnvalue = "S1I3";
                    else if (value == "DFA_S1I4") returnvalue = "S1I4";
                    else if (value == "DFA_S1I5") returnvalue = "S1I5";
                    else if (value == "DFA_S1I9") returnvalue = "S1I9";
                    else if (value == "DFA_D1I3") returnvalue = "D1I1 (I3)";
                    else if (value == "DFA_D1I4") returnvalue = "D1I2 (I4)";
                    else if (value == "DFA_D1I9") returnvalue = "D1I3 (I9)";
                    else if (value == "DFA_D1I10") returnvalue = "D1I4 (I10)";
                    else if (value == "DFA_D2I3") returnvalue = "D2I1 (I3)";
                    else if (value == "DFA_D2I4") returnvalue = "D2I2 (I4)";
                    else if (value == "DFA_D2I9") returnvalue = "D2I3 (I9)";
                    else if (value == "DFA_D2I10") returnvalue = "D2I4 (I10)";
                    else if (value == "DFA_D3I3") returnvalue = "D3I1 (I3)";
                    else if (value == "DFA_D3I4") returnvalue = "D3I2 (I4)";
                    else if (value == "DFA_D3I9") returnvalue = "D3I3 (I9)";
                    else if (value == "DFA_D3I10") returnvalue = "D3I4 (I10)";
                    else if (value == "DFA_D4I3") returnvalue = "D4I1 (I3)";
                    else if (value == "DFA_D4I4") returnvalue = "D4I2 (I4)";
                    else if (value == "DFA_D4I9") returnvalue = "D4I3 (I9)";
                    else if (value == "DFA_D4I10") returnvalue = "D4I4 (I10)";
                    else if (value == "DFA_S2I1") returnvalue = "";
                    else if (value == "DFA_S2I3") returnvalue = "";
                    else if (value == "DFA_S2I4") returnvalue = "";
                    else if (value == "DFA_S2I5") returnvalue = "";
                    else if (value == "DFA_S2I9") returnvalue = "";
                    break;
                case "Diff_Kreis_zusaetzlicheEinschaltbedingung":
                    if (value == "_KEINE_ZUSBED") returnvalue = "Deaktiviert";
                    else if (value == "_RLA_Pumpe") returnvalue = "RLA-Pumpe";
                    else if (value == "_HK1_Pumpe") returnvalue = "Heizkreis 1";
                    else if (value == "_HK2_Pumpe") returnvalue = "Heizkreis 2";
                    else if (value == "_HK3_Pumpe") returnvalue = "Heizkreis 3";
                    else if (value == "_HK4_Pumpe") returnvalue = "Heizkreis 4";
                    else if (value == "_HK5_Pumpe") returnvalue = "Heizkreis 5";
                    else if (value == "_HK6_Pumpe") returnvalue = "Heizkreis 6";
                    else if (value == "_HK7_Pumpe") returnvalue = "Heizkreis 7";
                    else if (value == "_HK8_Pumpe") returnvalue = "Heizkreis 8";
                    else if (value == "_PU1_Pumpe") returnvalue = "Puffer 1";
                    else if (value == "_PU2_Pumpe") returnvalue = "Puffer 2";
                    else if (value == "_PU3_Pumpe") returnvalue = "Puffer 3";
                    else if (value == "_PU4_Pumpe") returnvalue = "Puffer 4";
                    else if (value == "_BO1_Pumpe") returnvalue = "TWS Pumpe 1";
                    else if (value == "_BO2_Pumpe") returnvalue = "TWS Pumpe 2";
                    else if (value == "_BO3_Pumpe") returnvalue = "TWS Pumpe 3";
                    else if (value == "_BO4_Pumpe") returnvalue = "TWS Pumpe 4";
                    else if (value == "_SOL1_O1") returnvalue = "Solarmodul 1 O1";
                    else if (value == "_SOL1_O2") returnvalue = "Solarmodul 1 O2";
                    else if (value == "_SOL2_O1") returnvalue = "Solarmodul 2 O1";
                    else if (value == "_SOL2_O2") returnvalue = "Solarmodul 2 O2";
                    else if (value == "_SOL3_O1") returnvalue = "Solarmodul 3 O1";
                    else if (value == "_SOL3_O2") returnvalue = "Solarmodul 3 O2";
                    else if (value == "_SOL4_O1") returnvalue = "Solarmodul 4 O1";
                    else if (value == "_SOL4_O2") returnvalue = "Solarmodul 4 O2";
                    else if (value == "_ZIRK1_Pumpe") returnvalue = "Zirkulation 1";
                    else if (value == "_ZIRK2_Pumpe") returnvalue = "Zirkulation 2";
                    else if (value == "_ZIRK3_Pumpe") returnvalue = "Zirkulation 3";
                    else if (value == "_ZIRK4_Pumpe") returnvalue = "Zirkulation 4";
                    else if (value == "_X51") returnvalue = "ext.Anforderung X51";
                    else if (value == "_Reserverelais") returnvalue = "Reserverelais";
                    else if (value == "_MOD1_O1") returnvalue = "MOD1 O1";
                    else if (value == "_MOD1_O2") returnvalue = "MOD1 O2";
                    else if (value == "_MOD2_O1") returnvalue = "MOD2 O1";
                    else if (value == "_MOD2_O2") returnvalue = "MOD2 O2";
                    else if (value == "_MOD3_O1") returnvalue = "MOD3 O1";
                    else if (value == "_MOD3_O2") returnvalue = "MOD3 O2";
                    else if (value == "_MOD4_O1") returnvalue = "MOD4 O1";
                    else if (value == "_MOD4_O2") returnvalue = "MOD4 O2";
                    else if (value == "_PE_BETRIEB") returnvalue = "Pelletsbetrieb";
                    else if (value == "_TWS_ANF1") returnvalue = "TWS Anforderung 1";
                    else if (value == "_TWS_ANF2") returnvalue = "TWS Anforderung 2";
                    else if (value == "_TWS_ANF3") returnvalue = "TWS Anforderung 3";
                    else if (value == "_TWS_ANF4") returnvalue = "TWS Anforderung 4";
                    break;

                case "TWR_VORRANG":
                    if (value == "AUS_AUTOMATIK") returnvalue = "Automatik";
                    else if (value == "EIN") returnvalue = "Ein";
                    else if (value == "VERMINDERT") returnvalue = "Vermindert";
                    else if (value == "TWR_VORR_Nur_HK_Min") returnvalue = "Aus";
                    break;
                case "Pellet/Hackgut":
                    if (value == "_BA_PELLETS") returnvalue = "Pellets";
                    else if (value == "_BA_HACKGUT") returnvalue = "Hackgut";
                    break;
                case "Version":
                    if(value != null)
                    {
                        string year = "V"+ value.Substring(0, 2);
                        string version = value.Substring(2, 3);
                        value = year + "." + version;
                        return value;
                    }
                    else { value = "Unknown"; }
                    break;
                case "/6 + h":
                    returnvalue = (Convert.ToInt32(value) / 6).ToString() + " h";
                    break;
                case "/6 + /10 + h":
                    returnvalue = Math.Round(((Convert.ToDouble(value) / 6) / 10), 1).ToString() + " h";
                    break;
                case "/60 + h":
                    returnvalue = (Convert.ToInt32(value) / 60).ToString() + " h";
                    break;
                case "kWh":
                    returnvalue = value.ToString() + " kWh";
                    break;
                case "/1000 + MWh":
                    returnvalue = (Convert.ToInt32(value) / 1000).ToString() + " MWh";
                    break;
                case "/1000 + kg":
                    returnvalue = (Convert.ToInt32(value) / 1000).ToString() + " kg";
                    break;
                case "mV":
                    returnvalue = value.ToString() + " mV";
                    break;
                case "EinheitlosSpCh":
                    if (value.Contains("[1]\"\"")) returnvalue = "No Data";
                    else returnvalue = value;
                    break;
                case "IPAdress":
                    if (value.Contains("[1]\"\"")) { returnvalue = "No Data"; }
                    else
                    {
                        IPAddress address = IPAddress.Parse(value);
                        returnvalue = ReverseString(address.ToString());
                    }
                    break;
                case "Modultyp":
                    if (value == "0") returnvalue = "RS485";
                    else if (value == "1") returnvalue = "CAN Modul";
                    else returnvalue = "kein Modul";
                    break;
                default:
                    returnvalue = value;
                    break;
            }

            return returnvalue;
        }
    }
}