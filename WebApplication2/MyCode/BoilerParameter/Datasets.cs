﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Models.BoilerParameter;
using LogdataReader;
using System.IO;
using System.Runtime.InteropServices;
using System.Web.Hosting;
using WebApplication2.MyCode.BoilerParameter;
using System.Web.UI;

namespace WebApplication2.MyCode.BoilerParameter
{
    public enum LANGUAGE
    {
        de = 2,
        en = 3
    }
    static public class Datasets 
    {
        static public Formate_Translate dict = new Formate_Translate();     //dict with all formats and translations, which is created from Übersetzung.csv at the start of the programm
        static public LANGUAGE language = LANGUAGE.de;                      //Language in which the parameters are returned. Can later be set with the ChangeLanguage Methode in the BoilerParameterController. Default is de
        static public _KESSELPARAMETER boilerParameter;                     //Kesselparameter struct. Is created in the Initialize Methode of the BoilerParameterController
        static public _MAXIMUS maximusParameter;
       
        #region CustomerMenu
        static public IList<SingleValue> customerMenu()
        {
        
            IList<SingleValue> data = new List<SingleValue>();

            //0 Kesselsollwerte
            data.Add(new SingleValue("SOFTWAREVERSION", language, boilerParameter.KESSELVERWALTUNG.SOFTWAREVERSION, 0));
            data.Add(new SingleValue("udKesselstartdifferenz", language, boilerParameter.KESSELVERWALTUNG.udKesselstartdifferenz, 0));
            data.Add(new SingleValue("udKesselsolltemperatur", language, boilerParameter.KESSELVERWALTUNG.udKesselsolltemperatur, 0));
            data.Add(new SingleValue("hdStart1", language, boilerParameter.KESSELVERWALTUNG.hdStart1.ToString() + boilerParameter.KESSELVERWALTUNG.hdEnde1.ToString(), 0));
            data.Add(new SingleValue("hdStart2", language, boilerParameter.KESSELVERWALTUNG.hdStart2.ToString() + boilerParameter.KESSELVERWALTUNG.hdEnde2.ToString(), 0));
            data.Add(new SingleValue("hdStart2", language, boilerParameter.KESSELVERWALTUNG.hdStart3.ToString() + boilerParameter.KESSELVERWALTUNG.hdEnde3.ToString(), 0));
            data.Add(new SingleValue("Brennersperre_AT", language, boilerParameter.WETTER_BRENNERSPERRE.Brennersperre_AT, 0));
            data.Add(new SingleValue("LDZ_Nachtabsenkung", language, maximusParameter.ALLGEMEINE_EINSTELLUNGEN.LDZ_Nachtabsenkung, 0));
            data.Add(new SingleValue("Fremdkesselfreigabe", language, boilerParameter.KESSELVERWALTUNG.Fremdkesselfreigabe, 0));

            if (boilerParameter.KESSELTYP.ToString() == "Maximus")
            {
                data.Add(new SingleValue("Nachtabsenkung_Freigabezeit", language, maximusParameter.ALLGEMEINE_EINSTELLUNGEN.NachtabsenkungStart.ToString() + maximusParameter.ALLGEMEINE_EINSTELLUNGEN.NachtabsenkungEnde.ToString(), 0));
            }

            //1 Saugaustragung/Ascheaustragung
            if (boilerParameter.SAUGAUSTRAGUNG.udSGA_Anz_Zwangsbefuellung == 0)
            {  
                data.Add(new SingleValue("befuellzeit1fg1", language, boilerParameter.SAUGAUSTRAGUNG.hdFG1_StartZeit.ToString() + boilerParameter.SAUGAUSTRAGUNG.hdFG1_EndZeit.ToString(), 1));
                data.Add(new SingleValue("befuellzeit2fg2", language, boilerParameter.SAUGAUSTRAGUNG.hdFG2_StartZeit.ToString() + boilerParameter.SAUGAUSTRAGUNG.hdFG2_EndZeit.ToString(), 1));
                data.Add(new SingleValue("befuellzeit3fg3", language, boilerParameter.SAUGAUSTRAGUNG.hdFG3_StartZeit.ToString() + boilerParameter.SAUGAUSTRAGUNG.hdFG3_EndZeit.ToString(), 1));
            }
            else if (boilerParameter.SAUGAUSTRAGUNG.udSGA_Anz_Zwangsbefuellung == 1)
            {
                data.Add(new SingleValue("befuellzeit1", language, boilerParameter.SAUGAUSTRAGUNG.hdFG1_StartZeit.ToString() + boilerParameter.SAUGAUSTRAGUNG.hdFG1_EndZeit.ToString(), 1));
                data.Add(new SingleValue("befuellzeit2fg2", language, boilerParameter.SAUGAUSTRAGUNG.hdFG2_StartZeit.ToString() + boilerParameter.SAUGAUSTRAGUNG.hdFG2_EndZeit.ToString(), 1));
                data.Add(new SingleValue("befuellzeit3fg3", language, boilerParameter.SAUGAUSTRAGUNG.hdFG3_StartZeit.ToString() + boilerParameter.SAUGAUSTRAGUNG.hdFG3_EndZeit.ToString(), 1));
            }
            else  if (boilerParameter.SAUGAUSTRAGUNG.udSGA_Anz_Zwangsbefuellung == 2)
            {
                data.Add(new SingleValue("befuellzeit1fg1", language, boilerParameter.SAUGAUSTRAGUNG.hdFG1_StartZeit.ToString() + boilerParameter.SAUGAUSTRAGUNG.hdFG1_EndZeit.ToString(), 1));
                data.Add(new SingleValue("befuellzeit2fg2", language, boilerParameter.SAUGAUSTRAGUNG.hdFG2_StartZeit.ToString() + boilerParameter.SAUGAUSTRAGUNG.hdFG2_EndZeit.ToString(), 1));
                data.Add(new SingleValue("befuellzeit3", language, boilerParameter.SAUGAUSTRAGUNG.hdFG3_StartZeit.ToString() + boilerParameter.SAUGAUSTRAGUNG.hdFG3_EndZeit.ToString(), 1));
            }
            else if (boilerParameter.SAUGAUSTRAGUNG.udSGA_Anz_Zwangsbefuellung == 3)
            {
                data.Add(new SingleValue("befuellzeit1", language, boilerParameter.SAUGAUSTRAGUNG.hdFG1_StartZeit.ToString() + boilerParameter.SAUGAUSTRAGUNG.hdFG1_EndZeit.ToString(), 1));
                data.Add(new SingleValue("befuellzeit2", language, boilerParameter.SAUGAUSTRAGUNG.hdFG2_StartZeit.ToString() + boilerParameter.SAUGAUSTRAGUNG.hdFG2_EndZeit.ToString(), 1));
                data.Add(new SingleValue("befuellzeit3", language, boilerParameter.SAUGAUSTRAGUNG.hdFG3_StartZeit.ToString() + boilerParameter.SAUGAUSTRAGUNG.hdFG3_EndZeit.ToString(), 1));
            }
          
            //2 Wärmetauscher
            data.Add(new SingleValue("hdFg1_StartZeit", language, boilerParameter.WAERMETAUSCHERREINUNG.hdFg1_StartZeit.ToString() + boilerParameter.WAERMETAUSCHERREINUNG.hdFg1_EndZeit.ToString(), 2));
            data.Add(new SingleValue("hdFg2_StartZeit", language, boilerParameter.WAERMETAUSCHERREINUNG.hdFg2_StartZeit.ToString() + boilerParameter.WAERMETAUSCHERREINUNG.hdFg2_EndZeit.ToString(), 2));
            data.Add(new SingleValue("hdFg3_StartZeit", language, boilerParameter.WAERMETAUSCHERREINUNG.hdFg3_StartZeit.ToString() + boilerParameter.WAERMETAUSCHERREINUNG.hdFg3_EndZeit.ToString(), 2));

            //3 Kaskade
            data.Add(new SingleValue("KaskadeEIN", language, boilerParameter.SYSTEMPARAMETER.KaskadeEIN, 3));
            data.Add(new SingleValue("AnzahlSlaves", language, boilerParameter.SYSTEMPARAMETER.AnzahlSlaves, 3));
            data.Add(new SingleValue("POPlusOffset_Leistung100", language, boilerParameter.KASKADE.POPlusOffset_Leistung100, 3));
            data.Add(new SingleValue("POPlusOffset_Leistung0", language, boilerParameter.KASKADE.POPlusOffset_Leistung0, 3));
            data.Add(new SingleValue("Zielpuffer", language, boilerParameter.KASKADE.Zielpuffer, 3));
            data.Add(new SingleValue("KaskadenFreischaltung", language, boilerParameter.KASKADE.KaskadenFreischaltung, 3));
            data.Add(new SingleValue("Leistung_Kessel_dazuschalten", language, boilerParameter.KASKADE.Leistung_Kessel_dazuschalten, 3));
            data.Add(new SingleValue("Leistung_Kessel_wegschalten", language, boilerParameter.KASKADE.Leistung_Kessel_wegschalten, 3));
            //Kaskade (k_daten_csv)
            data.Add(new SingleValue("Kaskade1.sBMIN_Master", language, 3));
            data.Add(new SingleValue("Kaskade1.sBMIN_Slave1", language, 3));
            data.Add(new SingleValue("Kaskade1.sBMIN_Slave2", language, 3));
            data.Add(new SingleValue("Kaskade1.sBMIN_Slave3", language, 3));
            data.Add(new SingleValue("Kaskade1.sBMIN_Slave4", language, 3));
            data.Add(new SingleValue("Kaskade1.sBMIN_Slave5", language, 3));
            data.Add(new SingleValue("\"IP_Adresse1.Data", language, 3));
            data.Add(new SingleValue("\"IP_Adresse2.Data", language, 3));
            data.Add(new SingleValue("\"IP_Adresse3.Data", language, 3));
            data.Add(new SingleValue("\"IP_Adresse4.Data", language, 3));
            data.Add(new SingleValue("\"IP_Adresse5.Data", language, 3));
           
            for (int i = 0; i < 5; i++)
            {
                data.Add(new SingleValue("Starterlaubnis " + (i + 2) + ". Kessel AT <", language, boilerParameter.KASKADE._KASKADE_ARRAY[i].AT_kein_Start, 3));
                data.Add(new SingleValue("Zeitverzögerung " + (i + 2) + ". Kessel", language, boilerParameter.KASKADE._KASKADE_ARRAY[i].Startverzoegerung, 3));
                data.Add(new SingleValue("Sofortstart " + (i + 2) + ". Kessel AT <", language, boilerParameter.KASKADE._KASKADE_ARRAY[i].AT_SofortStart, 3));
            }
            data.Add(new SingleValue("Betriebsstunden_Kesselfolgewechsel", language, boilerParameter.KASKADE.Betriebsstunden_Kesselfolgewechsel, 3));
            data.Add(new SingleValue("Kesselwechsel", language, boilerParameter.KASKADE.Kesselwechsel, 3));
            for (int i = 0; i < 8; i++)
            {
                data.Add(new SingleValue("Leistungsbedarf_HK_" + (i + 1), language, boilerParameter.KASKADE2._HK_ANF_ARRAY[0].Leistungsbedarf_kW, 3));
            }

            //4 Betriebsstundenzähler
            if (boilerParameter.KESSELTYP.ToString() == "Maximus")
            { 
                data.Add(new SingleValue("BMZ_Zuendung_Geblaese.Betriebsminuten", language, 4));
                data.Add(new SingleValue("BMZ_Einschub.Betriebsminuten", language, 4));
                data.Add(new SingleValue("BMZ_AscheWTR.Betriebsminuten", language, 4));
                data.Add(new SingleValue("BMZ_AscheBrenner.Betriebsminuten", language, 4));
                data.Add(new SingleValue("BMZ_Stufenrost.Betriebsminuten", language, 4));
                data.Add(new SingleValue("BMZ_StandbyRelais.Betriebsminuten", language, 4));
                data.Add(new SingleValue("BMZ_Raumaustragung.Betriebsminuten", language, 4));
                data.Add(new SingleValue("SaugTurbine1.sSGA_BMIN", language, 4));
                data.Add(new SingleValue("BMZ_RA_Saug.Betriebsminuten", language, 4));
                data.Add(new SingleValue("BMZ_Raumaustragung_2.Betriebsminuten", language, 4));
                data.Add(new SingleValue("BMZ_Raumaustragung_3.Betriebsminuten", language, 4));
                data.Add(new SingleValue("Brenner1.sHackgutbetrieb_BMIN", language, 4));
                data.Add(new SingleValue("Brenner1.sHackgutbetrieb_BMIN_TL", language, 4));
                data.Add(new SingleValue("Brenner1.sAnzahlKesselstarts_HG", language, 4));
            }
            data.Add(new SingleValue("saugzuggeblaese1.sSZG_BMIN", language, 4));
            data.Add(new SingleValue("LambdaSonde1.sLBDSonde_BMIN", language, 4));
            data.Add(new SingleValue("Waermetauscher_Reinigung1.sWTR_BMIN", language, 4));
            data.Add(new SingleValue("Zuendung1.sGluehstab_BMIN", language, 4));
            data.Add(new SingleValue("Einschub1.sES_BMIN", language, 4));
            data.Add(new SingleValue("SaugTurbine1.sSGA_BMIN", language, 4));
            data.Add(new SingleValue("Aschenaustragung1.sASCHENAUSTRAGUNG_BMIN", language, 4));
            data.Add(new SingleValue("BMZ_BM_Spuelung.Betriebsminuten", language, 4));
            data.Add(new SingleValue("BMZ_Partikelfilter.Betriebsminuten", language, 4));
            data.Add(new SingleValue("Brenner1.sPelletsbetrieb_BMIN", language, 4));
            data.Add(new SingleValue("Brenner1.sPelletsbetrieb_BMIN_TL", language, 4));
            data.Add(new SingleValue("Brenner1.sAnzahlKesselstarts", language, 4));
            data.Add(new SingleValue("Kessel1.sBM_zum_Wartungszeitpunkt", language, 4));
            data.Add(new SingleValue("Betriebsstunden_seit_Wartung", language, 4));
            data.Add(new SingleValue("Einschub1.sVerbrauch_g", language, 4));
            data.Add(new SingleValue("Einschub1.sVerbrauchResetDatum", language, 4));

            //5 Wärmeverteilung
            data.Add(new SingleValue("BMZ_HK1.Betriebsminuten", language, 5));
            data.Add(new SingleValue("BMZ_HK2.Betriebsminuten", language, 5));
            data.Add(new SingleValue("BMZ_HK3.Betriebsminuten", language, 5));
            data.Add(new SingleValue("BMZ_HK4.Betriebsminuten", language, 5));
            data.Add(new SingleValue("BMZ_HK5.Betriebsminuten", language, 5));
            data.Add(new SingleValue("BMZ_HK6.Betriebsminuten", language, 5));
            data.Add(new SingleValue("BMZ_HK7.Betriebsminuten", language, 5));
            data.Add(new SingleValue("BMZ_HK8.Betriebsminuten", language, 5));
            data.Add(new SingleValue("BMZ_RLA.Betriebsminuten", language, 5));
            data.Add(new SingleValue("BMZ_PUFFER1.Betriebsminuten", language, 5));
            data.Add(new SingleValue("BMZ_PUFFER2.Betriebsminuten", language, 5));
            data.Add(new SingleValue("BMZ_PUFFER3.Betriebsminuten", language, 5));
            data.Add(new SingleValue("BMZ_PUFFER4.Betriebsminuten", language, 5));
            data.Add(new SingleValue("BMZ_TWS1.Betriebsminuten", language, 5));
            data.Add(new SingleValue("BMZ_TWS2.Betriebsminuten", language, 5));
            data.Add(new SingleValue("BMZ_TWS3.Betriebsminuten", language, 5));
            data.Add(new SingleValue("BMZ_TWS4.Betriebsminuten", language, 5));
            data.Add(new SingleValue("BMZ_FWM1.Betriebsminuten", language, 5));
            data.Add(new SingleValue("BMZ_FWM2.Betriebsminuten", language, 5));
            data.Add(new SingleValue("BMZ_FWM3.Betriebsminuten", language, 5));
            data.Add(new SingleValue("BMZ_FWM4.Betriebsminuten", language, 5));
            data.Add(new SingleValue("BMZ_ZIRK1.Betriebsminuten", language, 5));
            data.Add(new SingleValue("BMZ_ZIRK2.Betriebsminuten", language, 5));
            data.Add(new SingleValue("BMZ_ZIRK3.Betriebsminuten", language, 5));
            data.Add(new SingleValue("BMZ_ZIRK4.Betriebsminuten", language, 5));
            data.Add(new SingleValue("BMZ_DIFF1_O1.Betriebsminuten", language, 5));
            data.Add(new SingleValue("BMZ_DIFF1_O2.Betriebsminuten", language, 5));
            data.Add(new SingleValue("BMZ_DIFF2_O1.Betriebsminuten", language, 5));
            data.Add(new SingleValue("BMZ_DIFF2_O2.Betriebsminuten", language, 5));
            data.Add(new SingleValue("BMZ_DIFF3_O1.Betriebsminuten", language, 5));
            data.Add(new SingleValue("BMZ_DIFF3_O2.Betriebsminuten", language, 5));
            data.Add(new SingleValue("BMZ_DIFF4_O1.Betriebsminuten", language, 5));
            data.Add(new SingleValue("BMZ_DIFF4_O2.Betriebsminuten", language, 5));
            data.Add(new SingleValue("BMZ_DIFF_OCTO.Betriebsminuten", language, 5));
            data.Add(new SingleValue("Solar1.sSOL_Pumpe1_BMIN", language, 5));
            data.Add(new SingleValue("Solar1.sSOL_Pumpe2_BMIN", language, 5));
            data.Add(new SingleValue("Solar1.sSOL_Pumpe3_BMIN", language, 5));
            data.Add(new SingleValue("Solar1.sWMZ_Ertrag", language, 5));
            data.Add(new SingleValue("Solar2.sSOL_Pumpe1_BMIN", language, 5));
            data.Add(new SingleValue("Solar2.sSOL_Pumpe2_BMIN", language, 5));
            data.Add(new SingleValue("Solar2.sSOL_Pumpe3_BMIN", language, 5));
            data.Add(new SingleValue("Solar2.sWMZ_Ertrag", language, 5));
            data.Add(new SingleValue("Solar3.sSOL_Pumpe1_BMIN", language, 5));
            data.Add(new SingleValue("Solar3.sSOL_Pumpe2_BMIN", language, 5));
            data.Add(new SingleValue("Solar3.sSOL_Pumpe3_BMIN", language, 5));
            data.Add(new SingleValue("Solar3.sWMZ_Ertrag", language, 5));
            data.Add(new SingleValue("Solar4.sSOL_Pumpe1_BMIN", language, 5));
            data.Add(new SingleValue("Solar4.sSOL_Pumpe2_BMIN", language, 5));
            data.Add(new SingleValue("Solar4.sSOL_Pumpe3_BMIN", language, 5));
            data.Add(new SingleValue("Solar4.sWMZ_Ertrag", language, 5));

            //6 IP APP Mail (k_daten.csv)
            data.Add(new SingleValue("HandleNetwork_SF1.ActivateDHCP", language, 6));
            data.Add(new SingleValue("HandleNetwork_SF1.IPAdresse", language, 6));
            data.Add(new SingleValue("HandleNetwork_SF1.SubNet", language, 6));
            data.Add(new SingleValue("HandleNetwork_SF1.Gateway", language, 6));
            data.Add(new SingleValue("HandleNetwork_SF1.DNS_IP", language, 6));
            //App
            data.Add(new SingleValue("Http_to_JSON1.Registrierung_ok", language, 6));
            data.Add(new SingleValue("Http_to_JSON1.SavePin", language, 6));
            data.Add(new SingleValue("Http_to_JSON1.Automatik_ein", language, 6));
            //Mail
            data.Add(new SingleValue("\"SCL_EMailClient1\\SCL_Mail_SMTPServer.Data", language, 6));
            data.Add(new SingleValue("\"SCL_EMailClient1\\SCL_Mail_SenderAddress.Data", language, 6));
            data.Add(new SingleValue("\"SCL_EMailClient1\\SCL_Mail_Username.Data", language, 6));
            data.Add(new SingleValue("SCL_EMailClient1\\SCL_Mail_SMTPPort.Data", language, 6));
            data.Add(new SingleValue("SCL_EMailClient1.UseSSL", language, 6));

            //7 Servicemenü
            data.Add(new SingleValue("touch_lock1.sLock_Enable", language, 7));

            return data;
        }

        static public IList<SingleValue> customerMenuWP1()
        {

            IList<SingleValue> data = new List<SingleValue>();

            //0 EVU Lock 
            data.Add(new SingleValue("SOFTWAREVERSION", language, boilerParameter.KESSELVERWALTUNG.SOFTWAREVERSION, 0));    
            data.Add(new SingleValue("hdStart1", language, boilerParameter._WP_PARA._WP_SYSREG_SET.EVU_Lock.hdStart1.ToString() + boilerParameter._WP_PARA._WP_SYSREG_SET.EVU_Lock.hdEnde1.ToString(), 0)); 
            data.Add(new SingleValue("hdStart2", language, boilerParameter._WP_PARA._WP_SYSREG_SET.EVU_Lock.hdStart2.ToString() + boilerParameter._WP_PARA._WP_SYSREG_SET.EVU_Lock.hdEnde2.ToString(), 0));
            data.Add(new SingleValue("hdStart3", language, boilerParameter._WP_PARA._WP_SYSREG_SET.EVU_Lock.hdStart3.ToString() + boilerParameter._WP_PARA._WP_SYSREG_SET.EVU_Lock.hdEnde3.ToString(), 0));
            data.Add(new SingleValue("hdStart4", language, boilerParameter._WP_PARA._WP_SYSREG_SET.EVU_Lock.hdStart4.ToString() + boilerParameter._WP_PARA._WP_SYSREG_SET.EVU_Lock.hdEnde4.ToString(), 0));
            data.Add(new SingleValue("hdStart5", language, boilerParameter._WP_PARA._WP_SYSREG_SET.EVU_Lock.hdStart5.ToString() + boilerParameter._WP_PARA._WP_SYSREG_SET.EVU_Lock.hdEnde5.ToString(), 0));

            //1 Nachtabsenkung
            data.Add(new SingleValue("hdStart1", language, boilerParameter._WP_PARA._WP_SYSREG_SET.SilentMode_Zeiten.hdStart1.ToString() + boilerParameter._WP_PARA._WP_SYSREG_SET.SilentMode_Zeiten.hdEnde1.ToString(), 1));  
            data.Add(new SingleValue("hdStart2", language, boilerParameter._WP_PARA._WP_SYSREG_SET.SilentMode_Zeiten.hdStart2.ToString() + boilerParameter._WP_PARA._WP_SYSREG_SET.SilentMode_Zeiten.hdEnde2.ToString(), 1));
            data.Add(new SingleValue("hdStart3", language, boilerParameter._WP_PARA._WP_SYSREG_SET.SilentMode_Zeiten.hdStart3.ToString() + boilerParameter._WP_PARA._WP_SYSREG_SET.SilentMode_Zeiten.hdEnde3.ToString(), 1));
            data.Add(new SingleValue("hdStart4", language, boilerParameter._WP_PARA._WP_SYSREG_SET.SilentMode_Zeiten.hdStart4.ToString() + boilerParameter._WP_PARA._WP_SYSREG_SET.SilentMode_Zeiten.hdEnde4.ToString(), 1));
            data.Add(new SingleValue("hdStart5", language, boilerParameter._WP_PARA._WP_SYSREG_SET.SilentMode_Zeiten.hdStart5.ToString() + boilerParameter._WP_PARA._WP_SYSREG_SET.SilentMode_Zeiten.hdEnde5.ToString(), 1));

            data.Add(new SingleValue("max_DrehzahlenLuefter1", language, boilerParameter._WP_PARA._WP_SYSREG_SET.max_DrehzahlenLuefter[0].ToString(), 1));
            data.Add(new SingleValue("max_DrehzahlenLuefter2", language, boilerParameter._WP_PARA._WP_SYSREG_SET.max_DrehzahlenLuefter[1].ToString(), 1));
            data.Add(new SingleValue("max_DrehzahlenLuefter3", language, boilerParameter._WP_PARA._WP_SYSREG_SET.max_DrehzahlenLuefter[2].ToString(), 1));
            data.Add(new SingleValue("max_DrehzahlenLuefter4", language, boilerParameter._WP_PARA._WP_SYSREG_SET.max_DrehzahlenLuefter[3].ToString(), 1));
            data.Add(new SingleValue("max_DrehzahlenLuefter5", language, boilerParameter._WP_PARA._WP_SYSREG_SET.max_DrehzahlenLuefter[4].ToString(), 1));

            data.Add(new SingleValue("max_DRZ_Kompressor1", language, boilerParameter._WP_PARA._WP_SYSREG_SET.max_DRZ_Kompressor[0].ToString(), 1));
            data.Add(new SingleValue("max_DRZ_Kompressor2", language, boilerParameter._WP_PARA._WP_SYSREG_SET.max_DRZ_Kompressor[1].ToString(), 1));
            data.Add(new SingleValue("max_DRZ_Kompressor3", language, boilerParameter._WP_PARA._WP_SYSREG_SET.max_DRZ_Kompressor[2].ToString(), 1));
            data.Add(new SingleValue("max_DRZ_Kompressor4", language, boilerParameter._WP_PARA._WP_SYSREG_SET.max_DRZ_Kompressor[3].ToString(), 1));
            data.Add(new SingleValue("max_DRZ_Kompressor5", language, boilerParameter._WP_PARA._WP_SYSREG_SET.max_DRZ_Kompressor[4].ToString(), 1));

            //2 Betriebsstundenzähler
            data.Add(new SingleValue("Compressor_LzInfo.Betriebsminuten", language, 2));
            data.Add(new SingleValue("Compressor_LzInfo.Starts", language, 2));
            data.Add(new SingleValue("Primarkreisp_LzInfo.Betriebsminuten", language, 2));
            data.Add(new SingleValue("Primarkreisp_LzInfo.Starts", language, 2));
            data.Add(new SingleValue("Defrost_LzInfo.Betriebsminuten", language, 2));
            data.Add(new SingleValue("Defrost_LzInfo.Starts", language, 2));
            data.Add(new SingleValue("Boilerladung_LzInfo.Betriebsminuten", language, 2));
            data.Add(new SingleValue("Boilerladung_LzInfo.Starts", language, 2));
            data.Add(new SingleValue("Komp_Heizen_aktiv_LzInfo.Betriebsminuten", language, 2));
            data.Add(new SingleValue("Komp_Heizen_aktiv_LzInfo.Starts", language, 2));
            data.Add(new SingleValue("Komp_Kuehlen_aktiv_LzInfo.Betriebsminuten", language, 2));
            data.Add(new SingleValue("Komp_Kuehlen_aktiv_LzInfo.Starts", language, 2));
            data.Add(new SingleValue("Standby_aktiv_LzInfo.Betriebsminuten", language, 2));
            data.Add(new SingleValue("Standby_aktiv_LzInfo.Starts", language, 2));
            data.Add(new SingleValue("Luefter_LzInfo.Betriebsminuten", language, 2));
            data.Add(new SingleValue("Luefter_LzInfo.Starts", language, 2));
            data.Add(new SingleValue("E_Stab_LzInfo.Betriebsminuten", language, 2));
            data.Add(new SingleValue("E_Stab_LzInfo.Starts", language, 2));
            data.Add(new SingleValue("Emerson_RCC1.Heating_Energy_769", language, 2));
            data.Add(new SingleValue("Emerson_RCC1.Electrical_Energy_heating_770", language, 2));
            data.Add(new SingleValue("Emerson_RCC1.Envelope_Ext_Optime", language, 2));
            data.Add(new SingleValue("Emerson_RCC1.Defrost_Count", language, 2));
            data.Add(new SingleValue("Emerson_RCC1.CompStartCount", language, 2));
            data.Add(new SingleValue("Emerson_RCC1.CoolingEnergy_778", language, 2));
            data.Add(new SingleValue("Emerson_RCC1.Electrical_Energy_CoolingMode_779", language, 2));
            data.Add(new SingleValue("Emerson_RCC1.CompressorOperatingTime", language, 2));

            //Wärmeverteilung - 3
            data.Add(new SingleValue("BMZ_HK1.Betriebsminuten", language, 3));
            data.Add(new SingleValue("BMZ_HK2.Betriebsminuten", language, 3));
            data.Add(new SingleValue("BMZ_HK3.Betriebsminuten", language, 3));
            data.Add(new SingleValue("BMZ_HK4.Betriebsminuten", language, 3));
            data.Add(new SingleValue("BMZ_HK5.Betriebsminuten", language, 3));
            data.Add(new SingleValue("BMZ_HK6.Betriebsminuten", language, 3));
            data.Add(new SingleValue("BMZ_HK7.Betriebsminuten", language, 3));
            data.Add(new SingleValue("BMZ_HK8.Betriebsminuten", language, 3));
            data.Add(new SingleValue("BMZ_RLA.Betriebsminuten", language, 3));
            data.Add(new SingleValue("BMZ_PUFFER1.Betriebsminuten", language, 3));
            data.Add(new SingleValue("BMZ_PUFFER2.Betriebsminuten", language, 3));
            data.Add(new SingleValue("BMZ_PUFFER3.Betriebsminuten", language, 3));
            data.Add(new SingleValue("BMZ_PUFFER4.Betriebsminuten", language, 3));
            data.Add(new SingleValue("BMZ_TWS1.Betriebsminuten", language, 3));
            data.Add(new SingleValue("BMZ_TWS2.Betriebsminuten", language, 3));
            data.Add(new SingleValue("BMZ_TWS3.Betriebsminuten", language, 3));
            data.Add(new SingleValue("BMZ_TWS4.Betriebsminuten", language, 3));
            data.Add(new SingleValue("BMZ_FWM1.Betriebsminuten", language, 3));
            data.Add(new SingleValue("BMZ_FWM2.Betriebsminuten", language, 3));
            data.Add(new SingleValue("BMZ_FWM3.Betriebsminuten", language, 3));
            data.Add(new SingleValue("BMZ_FWM4.Betriebsminuten", language, 3));
            data.Add(new SingleValue("BMZ_ZIRK1.Betriebsminuten", language, 3));
            data.Add(new SingleValue("BMZ_ZIRK2.Betriebsminuten", language, 3));
            data.Add(new SingleValue("BMZ_ZIRK3.Betriebsminuten", language, 3));
            data.Add(new SingleValue("BMZ_ZIRK4.Betriebsminuten", language, 3));
            data.Add(new SingleValue("BMZ_DIFF1_O1.Betriebsminuten", language, 3));
            data.Add(new SingleValue("BMZ_DIFF1_O2.Betriebsminuten", language, 3));
            data.Add(new SingleValue("BMZ_DIFF2_O1.Betriebsminuten", language, 3));
            data.Add(new SingleValue("BMZ_DIFF2_O2.Betriebsminuten", language, 3));
            data.Add(new SingleValue("BMZ_DIFF3_O1.Betriebsminuten", language, 3));
            data.Add(new SingleValue("BMZ_DIFF3_O2.Betriebsminuten", language, 3));
            data.Add(new SingleValue("BMZ_DIFF4_O1.Betriebsminuten", language, 3));
            data.Add(new SingleValue("BMZ_DIFF4_O2.Betriebsminuten", language, 3));
            data.Add(new SingleValue("BMZ_DIFF_OCTO.Betriebsminuten", language, 3));
            data.Add(new SingleValue("Solar1.sSOL_Pumpe1_BMIN", language, 3));
            data.Add(new SingleValue("Solar1.sSOL_Pumpe2_BMIN", language, 3));
            data.Add(new SingleValue("Solar1.sSOL_Pumpe3_BMIN", language, 3));
            data.Add(new SingleValue("Solar1.sWMZ_Ertrag", language, 3));
            data.Add(new SingleValue("Solar2.sSOL_Pumpe1_BMIN", language, 3));
            data.Add(new SingleValue("Solar2.sSOL_Pumpe2_BMIN", language, 3));
            data.Add(new SingleValue("Solar2.sSOL_Pumpe3_BMIN", language, 3));
            data.Add(new SingleValue("Solar2.sWMZ_Ertrag", language, 3));
            data.Add(new SingleValue("Solar3.sSOL_Pumpe1_BMIN", language, 3));
            data.Add(new SingleValue("Solar3.sSOL_Pumpe2_BMIN", language, 3));
            data.Add(new SingleValue("Solar3.sSOL_Pumpe3_BMIN", language, 3));
            data.Add(new SingleValue("Solar3.sWMZ_Ertrag", language, 3));
            data.Add(new SingleValue("Solar4.sSOL_Pumpe1_BMIN", language, 3));
            data.Add(new SingleValue("Solar4.sSOL_Pumpe2_BMIN", language, 3));
            data.Add(new SingleValue("Solar4.sSOL_Pumpe3_BMIN", language, 3));
            data.Add(new SingleValue("Solar4.sWMZ_Ertrag", language, 3));

            //IP APP Mail - 4 (k_daten.csv)
            data.Add(new SingleValue("HandleNetwork_SF1.ActivateDHCP", language, 4));
            data.Add(new SingleValue("HandleNetwork_SF1.IPAdresse", language, 4));
            data.Add(new SingleValue("HandleNetwork_SF1.SubNet", language, 4));
            data.Add(new SingleValue("HandleNetwork_SF1.Gateway", language, 4));
            data.Add(new SingleValue("HandleNetwork_SF1.DNS_IP", language, 4));
            //App
            data.Add(new SingleValue("Http_to_JSON1.Registrierung_ok", language, 4));
            data.Add(new SingleValue("Http_to_JSON1.SavePin", language, 4));
            data.Add(new SingleValue("Http_to_JSON1.Automatik_ein", language, 4));
            //Mail
            data.Add(new SingleValue("\"SCL_EMailClient1\\SCL_Mail_SMTPServer.Data", language, 4));
            data.Add(new SingleValue("\"SCL_EMailClient1\\SCL_Mail_SenderAddress.Data", language, 4));
            data.Add(new SingleValue("\"SCL_EMailClient1\\SCL_Mail_Username.Data", language, 4));
            data.Add(new SingleValue("SCL_EMailClient1\\SCL_Mail_SMTPPort.Data", language, 4));
            data.Add(new SingleValue("SCL_EMailClient1.UseSSL", language, 4));

            //Servicemenü
            data.Add(new SingleValue("touch_lock1.sLock_Enable", language, 5));
            return data;
        }

        static public IList<SingleValue> customerMenuWPBlockingTimes()
        {

            IList<SingleValue> data = new List<SingleValue>();

            #region Montag
            data.Add(new SingleValue("hdStart1", language, boilerParameter._WP_PARA._WP_SYSREG_SET.Sperrzeiten_Montag.hdStart1.ToString() + boilerParameter._WP_PARA._WP_SYSREG_SET.Sperrzeiten_Montag.hdEnde1.ToString(), 0));  
            data.Add(new SingleValue("hdStart2", language, boilerParameter._WP_PARA._WP_SYSREG_SET.Sperrzeiten_Montag.hdStart2.ToString() + boilerParameter._WP_PARA._WP_SYSREG_SET.Sperrzeiten_Montag.hdEnde2.ToString(), 0));
            data.Add(new SingleValue("hdStart3", language, boilerParameter._WP_PARA._WP_SYSREG_SET.Sperrzeiten_Montag.hdStart3.ToString() + boilerParameter._WP_PARA._WP_SYSREG_SET.Sperrzeiten_Montag.hdEnde3.ToString(), 0));
            data.Add(new SingleValue("hdStart4", language, boilerParameter._WP_PARA._WP_SYSREG_SET.Sperrzeiten_Montag.hdStart4.ToString() + boilerParameter._WP_PARA._WP_SYSREG_SET.Sperrzeiten_Montag.hdEnde4.ToString(), 0));
            data.Add(new SingleValue("hdStart5", language, boilerParameter._WP_PARA._WP_SYSREG_SET.Sperrzeiten_Montag.hdStart5.ToString() + boilerParameter._WP_PARA._WP_SYSREG_SET.Sperrzeiten_Montag.hdEnde5.ToString(), 0));

            data.Add(new SingleValue("sperrzeit1", language, boilerParameter._WP_PARA._WP_SYSREG_SET.Sperren_Montag[0].ToString(), 0));
            data.Add(new SingleValue("sperrzeit2", language, boilerParameter._WP_PARA._WP_SYSREG_SET.Sperren_Montag[1].ToString(), 0));
            data.Add(new SingleValue("sperrzeit3", language, boilerParameter._WP_PARA._WP_SYSREG_SET.Sperren_Montag[2].ToString(), 0));
            data.Add(new SingleValue("sperrzeit4", language, boilerParameter._WP_PARA._WP_SYSREG_SET.Sperren_Montag[3].ToString(), 0));
            data.Add(new SingleValue("sperrzeit5", language, boilerParameter._WP_PARA._WP_SYSREG_SET.Sperren_Montag[4].ToString(), 0));
            #endregion

            #region Dienstag
            data.Add(new SingleValue("hdStart1", language, boilerParameter._WP_PARA._WP_SYSREG_SET.Sperrzeiten_Dienstag.hdStart1.ToString() + boilerParameter._WP_PARA._WP_SYSREG_SET.Sperrzeiten_Dienstag.hdEnde1.ToString(), 1));  
            data.Add(new SingleValue("hdStart2", language, boilerParameter._WP_PARA._WP_SYSREG_SET.Sperrzeiten_Dienstag.hdStart2.ToString() + boilerParameter._WP_PARA._WP_SYSREG_SET.Sperrzeiten_Dienstag.hdEnde2.ToString(), 1));
            data.Add(new SingleValue("hdStart3", language, boilerParameter._WP_PARA._WP_SYSREG_SET.Sperrzeiten_Dienstag.hdStart3.ToString() + boilerParameter._WP_PARA._WP_SYSREG_SET.Sperrzeiten_Dienstag.hdEnde3.ToString(), 1));
            data.Add(new SingleValue("hdStart4", language, boilerParameter._WP_PARA._WP_SYSREG_SET.Sperrzeiten_Dienstag.hdStart4.ToString() + boilerParameter._WP_PARA._WP_SYSREG_SET.Sperrzeiten_Dienstag.hdEnde4.ToString(), 1));
            data.Add(new SingleValue("hdStart5", language, boilerParameter._WP_PARA._WP_SYSREG_SET.Sperrzeiten_Dienstag.hdStart5.ToString() + boilerParameter._WP_PARA._WP_SYSREG_SET.Sperrzeiten_Dienstag.hdEnde5.ToString(), 1));

            data.Add(new SingleValue("sperrzeit1", language, boilerParameter._WP_PARA._WP_SYSREG_SET.Sperren_Dienstag[0].ToString(), 1));
            data.Add(new SingleValue("sperrzeit2", language, boilerParameter._WP_PARA._WP_SYSREG_SET.Sperren_Dienstag[1].ToString(), 1));
            data.Add(new SingleValue("sperrzeit3", language, boilerParameter._WP_PARA._WP_SYSREG_SET.Sperren_Dienstag[2].ToString(), 1));
            data.Add(new SingleValue("sperrzeit4", language, boilerParameter._WP_PARA._WP_SYSREG_SET.Sperren_Dienstag[3].ToString(), 1));
            data.Add(new SingleValue("sperrzeit5", language, boilerParameter._WP_PARA._WP_SYSREG_SET.Sperren_Dienstag[4].ToString(), 1));
            #endregion

            #region Mittwoch
            data.Add(new SingleValue("hdStart1", language, boilerParameter._WP_PARA._WP_SYSREG_SET.Sperrzeiten_Mittwoch.hdStart1.ToString() + boilerParameter._WP_PARA._WP_SYSREG_SET.Sperrzeiten_Mittwoch.hdEnde1.ToString(), 2));  
            data.Add(new SingleValue("hdStart2", language, boilerParameter._WP_PARA._WP_SYSREG_SET.Sperrzeiten_Mittwoch.hdStart2.ToString() + boilerParameter._WP_PARA._WP_SYSREG_SET.Sperrzeiten_Mittwoch.hdEnde2.ToString(), 2));
            data.Add(new SingleValue("hdStart3", language, boilerParameter._WP_PARA._WP_SYSREG_SET.Sperrzeiten_Mittwoch.hdStart3.ToString() + boilerParameter._WP_PARA._WP_SYSREG_SET.Sperrzeiten_Mittwoch.hdEnde3.ToString(), 2));
            data.Add(new SingleValue("hdStart4", language, boilerParameter._WP_PARA._WP_SYSREG_SET.Sperrzeiten_Mittwoch.hdStart4.ToString() + boilerParameter._WP_PARA._WP_SYSREG_SET.Sperrzeiten_Mittwoch.hdEnde4.ToString(), 2));
            data.Add(new SingleValue("hdStart5", language, boilerParameter._WP_PARA._WP_SYSREG_SET.Sperrzeiten_Mittwoch.hdStart5.ToString() + boilerParameter._WP_PARA._WP_SYSREG_SET.Sperrzeiten_Mittwoch.hdEnde5.ToString(), 2));

            data.Add(new SingleValue("sperrzeit1", language, boilerParameter._WP_PARA._WP_SYSREG_SET.Sperren_Mittwoch[0].ToString(), 2));
            data.Add(new SingleValue("sperrzeit2", language, boilerParameter._WP_PARA._WP_SYSREG_SET.Sperren_Mittwoch[1].ToString(), 2));
            data.Add(new SingleValue("sperrzeit3", language, boilerParameter._WP_PARA._WP_SYSREG_SET.Sperren_Mittwoch[2].ToString(), 2));
            data.Add(new SingleValue("sperrzeit4", language, boilerParameter._WP_PARA._WP_SYSREG_SET.Sperren_Mittwoch[3].ToString(), 2));
            data.Add(new SingleValue("sperrzeit5", language, boilerParameter._WP_PARA._WP_SYSREG_SET.Sperren_Mittwoch[4].ToString(), 2));
            #endregion

            #region Donnerstag
            data.Add(new SingleValue("hdStart1", language, boilerParameter._WP_PARA._WP_SYSREG_SET.Sperrzeiten_Donnerstag.hdStart1.ToString() + boilerParameter._WP_PARA._WP_SYSREG_SET.Sperrzeiten_Donnerstag.hdEnde1.ToString(), 3));  //3 Donnerstag
            data.Add(new SingleValue("hdStart2", language, boilerParameter._WP_PARA._WP_SYSREG_SET.Sperrzeiten_Donnerstag.hdStart2.ToString() + boilerParameter._WP_PARA._WP_SYSREG_SET.Sperrzeiten_Donnerstag.hdEnde2.ToString(), 3));
            data.Add(new SingleValue("hdStart3", language, boilerParameter._WP_PARA._WP_SYSREG_SET.Sperrzeiten_Donnerstag.hdStart3.ToString() + boilerParameter._WP_PARA._WP_SYSREG_SET.Sperrzeiten_Donnerstag.hdEnde3.ToString(), 3));
            data.Add(new SingleValue("hdStart4", language, boilerParameter._WP_PARA._WP_SYSREG_SET.Sperrzeiten_Donnerstag.hdStart4.ToString() + boilerParameter._WP_PARA._WP_SYSREG_SET.Sperrzeiten_Donnerstag.hdEnde4.ToString(), 3));
            data.Add(new SingleValue("hdStart5", language, boilerParameter._WP_PARA._WP_SYSREG_SET.Sperrzeiten_Donnerstag.hdStart5.ToString() + boilerParameter._WP_PARA._WP_SYSREG_SET.Sperrzeiten_Donnerstag.hdEnde5.ToString(), 3));

            data.Add(new SingleValue("sperrzeit1", language, boilerParameter._WP_PARA._WP_SYSREG_SET.Sperren_Donnerstag[0].ToString(), 3));
            data.Add(new SingleValue("sperrzeit2", language, boilerParameter._WP_PARA._WP_SYSREG_SET.Sperren_Donnerstag[1].ToString(), 3));
            data.Add(new SingleValue("sperrzeit3", language, boilerParameter._WP_PARA._WP_SYSREG_SET.Sperren_Donnerstag[2].ToString(), 3));
            data.Add(new SingleValue("sperrzeit4", language, boilerParameter._WP_PARA._WP_SYSREG_SET.Sperren_Donnerstag[3].ToString(), 3));
            data.Add(new SingleValue("sperrzeit5", language, boilerParameter._WP_PARA._WP_SYSREG_SET.Sperren_Donnerstag[4].ToString(), 3));
            #endregion

            #region Freitag
            data.Add(new SingleValue("hdStart1", language, boilerParameter._WP_PARA._WP_SYSREG_SET.Sperrzeiten_Freitag.hdStart1.ToString() + boilerParameter._WP_PARA._WP_SYSREG_SET.Sperrzeiten_Freitag.hdEnde1.ToString(), 4));  //4 Freitag
            data.Add(new SingleValue("hdStart2", language, boilerParameter._WP_PARA._WP_SYSREG_SET.Sperrzeiten_Freitag.hdStart2.ToString() + boilerParameter._WP_PARA._WP_SYSREG_SET.Sperrzeiten_Freitag.hdEnde2.ToString(), 4));
            data.Add(new SingleValue("hdStart3", language, boilerParameter._WP_PARA._WP_SYSREG_SET.Sperrzeiten_Freitag.hdStart3.ToString() + boilerParameter._WP_PARA._WP_SYSREG_SET.Sperrzeiten_Freitag.hdEnde3.ToString(), 4));
            data.Add(new SingleValue("hdStart4", language, boilerParameter._WP_PARA._WP_SYSREG_SET.Sperrzeiten_Freitag.hdStart4.ToString() + boilerParameter._WP_PARA._WP_SYSREG_SET.Sperrzeiten_Freitag.hdEnde4.ToString(), 4));
            data.Add(new SingleValue("hdStart5", language, boilerParameter._WP_PARA._WP_SYSREG_SET.Sperrzeiten_Freitag.hdStart5.ToString() + boilerParameter._WP_PARA._WP_SYSREG_SET.Sperrzeiten_Freitag.hdEnde5.ToString(), 4));

            data.Add(new SingleValue("sperrzeit1", language, boilerParameter._WP_PARA._WP_SYSREG_SET.Sperren_Freitag[0].ToString(), 4));
            data.Add(new SingleValue("sperrzeit2", language, boilerParameter._WP_PARA._WP_SYSREG_SET.Sperren_Freitag[1].ToString(), 4));
            data.Add(new SingleValue("sperrzeit3", language, boilerParameter._WP_PARA._WP_SYSREG_SET.Sperren_Freitag[2].ToString(), 4));
            data.Add(new SingleValue("sperrzeit4", language, boilerParameter._WP_PARA._WP_SYSREG_SET.Sperren_Freitag[3].ToString(), 4));
            data.Add(new SingleValue("sperrzeit5", language, boilerParameter._WP_PARA._WP_SYSREG_SET.Sperren_Freitag[4].ToString(), 4));
            #endregion

            #region Samstag
            data.Add(new SingleValue("hdStart1", language, boilerParameter._WP_PARA._WP_SYSREG_SET.Sperrzeiten_Samstag.hdStart1.ToString() + boilerParameter._WP_PARA._WP_SYSREG_SET.Sperrzeiten_Samstag.hdEnde1.ToString(), 5));  //5 Samstag
            data.Add(new SingleValue("hdStart2", language, boilerParameter._WP_PARA._WP_SYSREG_SET.Sperrzeiten_Samstag.hdStart2.ToString() + boilerParameter._WP_PARA._WP_SYSREG_SET.Sperrzeiten_Samstag.hdEnde2.ToString(), 5));
            data.Add(new SingleValue("hdStart3", language, boilerParameter._WP_PARA._WP_SYSREG_SET.Sperrzeiten_Samstag.hdStart3.ToString() + boilerParameter._WP_PARA._WP_SYSREG_SET.Sperrzeiten_Samstag.hdEnde3.ToString(), 5));
            data.Add(new SingleValue("hdStart4", language, boilerParameter._WP_PARA._WP_SYSREG_SET.Sperrzeiten_Samstag.hdStart4.ToString() + boilerParameter._WP_PARA._WP_SYSREG_SET.Sperrzeiten_Samstag.hdEnde4.ToString(), 5));
            data.Add(new SingleValue("hdStart5", language, boilerParameter._WP_PARA._WP_SYSREG_SET.Sperrzeiten_Samstag.hdStart5.ToString() + boilerParameter._WP_PARA._WP_SYSREG_SET.Sperrzeiten_Samstag.hdEnde5.ToString(), 5));

            data.Add(new SingleValue("sperrzeit1", language, boilerParameter._WP_PARA._WP_SYSREG_SET.Sperren_Samstag[0].ToString(), 5));
            data.Add(new SingleValue("sperrzeit2", language, boilerParameter._WP_PARA._WP_SYSREG_SET.Sperren_Samstag[1].ToString(), 5));
            data.Add(new SingleValue("sperrzeit3", language, boilerParameter._WP_PARA._WP_SYSREG_SET.Sperren_Samstag[2].ToString(), 5));
            data.Add(new SingleValue("sperrzeit4", language, boilerParameter._WP_PARA._WP_SYSREG_SET.Sperren_Samstag[3].ToString(), 5));
            data.Add(new SingleValue("sperrzeit5", language, boilerParameter._WP_PARA._WP_SYSREG_SET.Sperren_Samstag[4].ToString(), 5));
            #endregion

            #region Sonntag
            data.Add(new SingleValue("hdStart1", language, boilerParameter._WP_PARA._WP_SYSREG_SET.Sperrzeiten_Sonntag.hdStart1.ToString() + boilerParameter._WP_PARA._WP_SYSREG_SET.Sperrzeiten_Sonntag.hdEnde1.ToString(), 6));  //6 Sonntag
            data.Add(new SingleValue("hdStart2", language, boilerParameter._WP_PARA._WP_SYSREG_SET.Sperrzeiten_Sonntag.hdStart2.ToString() + boilerParameter._WP_PARA._WP_SYSREG_SET.Sperrzeiten_Sonntag.hdEnde2.ToString(), 6));
            data.Add(new SingleValue("hdStart3", language, boilerParameter._WP_PARA._WP_SYSREG_SET.Sperrzeiten_Sonntag.hdStart3.ToString() + boilerParameter._WP_PARA._WP_SYSREG_SET.Sperrzeiten_Sonntag.hdEnde3.ToString(), 6));
            data.Add(new SingleValue("hdStart4", language, boilerParameter._WP_PARA._WP_SYSREG_SET.Sperrzeiten_Sonntag.hdStart4.ToString() + boilerParameter._WP_PARA._WP_SYSREG_SET.Sperrzeiten_Sonntag.hdEnde4.ToString(), 6));
            data.Add(new SingleValue("hdStart5", language, boilerParameter._WP_PARA._WP_SYSREG_SET.Sperrzeiten_Sonntag.hdStart5.ToString() + boilerParameter._WP_PARA._WP_SYSREG_SET.Sperrzeiten_Sonntag.hdEnde5.ToString(), 6));

            data.Add(new SingleValue("sperrzeit1", language, boilerParameter._WP_PARA._WP_SYSREG_SET.Sperren_Sonntag[0].ToString(), 6));
            data.Add(new SingleValue("sperrzeit2", language, boilerParameter._WP_PARA._WP_SYSREG_SET.Sperren_Sonntag[1].ToString(), 6));
            data.Add(new SingleValue("sperrzeit3", language, boilerParameter._WP_PARA._WP_SYSREG_SET.Sperren_Sonntag[2].ToString(), 6));
            data.Add(new SingleValue("sperrzeit4", language, boilerParameter._WP_PARA._WP_SYSREG_SET.Sperren_Sonntag[3].ToString(), 6));
            data.Add(new SingleValue("sperrzeit5", language, boilerParameter._WP_PARA._WP_SYSREG_SET.Sperren_Sonntag[4].ToString(), 6));
            #endregion

            return data;
        }

        static public IList<SingleValue> customerMenuWPKaskade()
        {

            IList<SingleValue> data = new List<SingleValue>();

            data.Add(new SingleValue("KaskadeEIN", language, boilerParameter.SYSTEMPARAMETER.KaskadeEIN, 0));    //0 Kaskade
            data.Add(new SingleValue("AnzahlSlaves", language, boilerParameter.SYSTEMPARAMETER.AnzahlSlaves, 0));
            data.Add(new SingleValue("POPlusOffset_Leistung100", language, boilerParameter.KASKADE.POPlusOffset_Leistung100, 0));
            data.Add(new SingleValue("POPlusOffset_Leistung0", language, boilerParameter.KASKADE.POPlusOffset_Leistung0, 0));
            data.Add(new SingleValue("Zielpuffer", language, boilerParameter.KASKADE.Zielpuffer, 0));
            data.Add(new SingleValue("KaskadenFreischaltung", language, boilerParameter.KASKADE.KaskadenFreischaltung, 0));
            data.Add(new SingleValue("Leistung_Kessel_dazuschalten", language, boilerParameter.KASKADE.Leistung_Kessel_dazuschalten, 0));
            data.Add(new SingleValue("Leistung_Kessel_wegschalten", language, boilerParameter.KASKADE.Leistung_Kessel_wegschalten, 0));
            for (int i = 0; i < 5; i++)
            {
                data.Add(new SingleValue("Starterlaubnis " + (i + 2) + ". Kessel AT <", language, boilerParameter.KASKADE._KASKADE_ARRAY[i].AT_kein_Start, 0));
                data.Add(new SingleValue("Zeitverzögerung " + (i + 2) + ". Kessel", language, boilerParameter.KASKADE._KASKADE_ARRAY[i].Startverzoegerung, 0));
                data.Add(new SingleValue("Sofortstart " + (i + 2) + ". Kessel AT <", language, boilerParameter.KASKADE._KASKADE_ARRAY[i].AT_SofortStart, 0));
            }
            data.Add(new SingleValue("Betriebsstunden_Kesselfolgewechsel", language, boilerParameter.KASKADE.Betriebsstunden_Kesselfolgewechsel, 0));
            data.Add(new SingleValue("Kesselwechsel", language, boilerParameter.KASKADE.Kesselwechsel, 0));
            for (int i = 0; i < 8; i++)
            {
                data.Add(new SingleValue("Leistungsbedarf_HK_" + (i + 1), language, boilerParameter.KASKADE2._HK_ANF_ARRAY[0].Leistungsbedarf_kW, 0));
            }

            //Kaskae (k_daten_csv)
            data.Add(new SingleValue("Kaskade1.sBMIN_Master", language, 0));
            data.Add(new SingleValue("Kaskade1.sBMIN_Slave1", language, 0));
            data.Add(new SingleValue("Kaskade1.sBMIN_Slave2", language, 0));
            data.Add(new SingleValue("Kaskade1.sBMIN_Slave3", language, 0));
            data.Add(new SingleValue("Kaskade1.sBMIN_Slave4", language, 0));
            data.Add(new SingleValue("Kaskade1.sBMIN_Slave5", language, 0));
            data.Add(new SingleValue("\"IP_Adresse1.Data", language, 0));
            data.Add(new SingleValue("\"IP_Adresse2.Data", language, 0));
            data.Add(new SingleValue("\"IP_Adresse3.Data", language, 0));
            data.Add(new SingleValue("\"IP_Adresse4.Data", language, 0));
            data.Add(new SingleValue("\"IP_Adresse5.Data", language, 0));

            return data;
        }

        static public IList<SingleValue> customerMenuRZ()
        {

            IList<SingleValue> data = new List<SingleValue>();

            //Kesselsollwerte - 0
            data.Add(new SingleValue("SOFTWAREVERSION", language, boilerParameter.KESSELVERWALTUNG.SOFTWAREVERSION, 0));    //0 Kesselsollwerte
            data.Add(new SingleValue("udKesselsolltemperatur", language, boilerParameter.KESSELVERWALTUNG.udKesselsolltemperatur, 0));    //0 Kesselsollwerte
            data.Add(new SingleValue("hdStart1", language, boilerParameter.KESSELVERWALTUNG.hdStart1.ToString() + boilerParameter.KESSELVERWALTUNG.hdEnde1.ToString(), 0));
            data.Add(new SingleValue("hdStart2", language, boilerParameter.KESSELVERWALTUNG.hdStart2.ToString() + boilerParameter.KESSELVERWALTUNG.hdEnde2.ToString(), 0));
            data.Add(new SingleValue("hdStart2", language, boilerParameter.KESSELVERWALTUNG.hdStart3.ToString() + boilerParameter.KESSELVERWALTUNG.hdEnde3.ToString(), 0));
            data.Add(new SingleValue("Brennersperre_AT", language, boilerParameter.WETTER_BRENNERSPERRE.Brennersperre_AT, 0));

            //Betribsstundenzähler - 1 
            data.Add(new SingleValue("Kessel1.sStartFKA", language, 1));
            data.Add(new SingleValue("Kessel1.sFKA_Hk_Bmin", language, 1));

            //Wärmeverteilung - 2
            data.Add(new SingleValue("BMZ_HK1.Betriebsminuten", language, 2));
            data.Add(new SingleValue("BMZ_HK2.Betriebsminuten", language, 2));
            data.Add(new SingleValue("BMZ_HK3.Betriebsminuten", language, 2));
            data.Add(new SingleValue("BMZ_HK4.Betriebsminuten", language, 2));
            data.Add(new SingleValue("BMZ_HK5.Betriebsminuten", language, 2));
            data.Add(new SingleValue("BMZ_HK6.Betriebsminuten", language, 2));
            data.Add(new SingleValue("BMZ_HK7.Betriebsminuten", language, 2));
            data.Add(new SingleValue("BMZ_HK8.Betriebsminuten", language, 2));
            data.Add(new SingleValue("BMZ_RLA.Betriebsminuten", language, 2));
            data.Add(new SingleValue("BMZ_PUFFER1.Betriebsminuten", language, 2));
            data.Add(new SingleValue("BMZ_PUFFER2.Betriebsminuten", language, 2));
            data.Add(new SingleValue("BMZ_PUFFER3.Betriebsminuten", language, 2));
            data.Add(new SingleValue("BMZ_PUFFER4.Betriebsminuten", language, 2));
            data.Add(new SingleValue("BMZ_TWS1.Betriebsminuten", language, 2));
            data.Add(new SingleValue("BMZ_TWS2.Betriebsminuten", language, 2));
            data.Add(new SingleValue("BMZ_TWS3.Betriebsminuten", language, 2));
            data.Add(new SingleValue("BMZ_TWS4.Betriebsminuten", language, 2));
            data.Add(new SingleValue("BMZ_FWM1.Betriebsminuten", language, 2));
            data.Add(new SingleValue("BMZ_FWM2.Betriebsminuten", language, 2));
            data.Add(new SingleValue("BMZ_FWM3.Betriebsminuten", language, 2));
            data.Add(new SingleValue("BMZ_FWM4.Betriebsminuten", language, 2));
            data.Add(new SingleValue("BMZ_ZIRK1.Betriebsminuten", language, 2));
            data.Add(new SingleValue("BMZ_ZIRK2.Betriebsminuten", language, 2));
            data.Add(new SingleValue("BMZ_ZIRK3.Betriebsminuten", language, 2));
            data.Add(new SingleValue("BMZ_ZIRK4.Betriebsminuten", language, 2));
            data.Add(new SingleValue("BMZ_DIFF1_O1.Betriebsminuten", language, 2));
            data.Add(new SingleValue("BMZ_DIFF1_O2.Betriebsminuten", language, 2));
            data.Add(new SingleValue("BMZ_DIFF2_O1.Betriebsminuten", language, 2));
            data.Add(new SingleValue("BMZ_DIFF2_O2.Betriebsminuten", language, 2));
            data.Add(new SingleValue("BMZ_DIFF3_O1.Betriebsminuten", language, 2));
            data.Add(new SingleValue("BMZ_DIFF3_O2.Betriebsminuten", language, 2));
            data.Add(new SingleValue("BMZ_DIFF4_O1.Betriebsminuten", language, 2));
            data.Add(new SingleValue("BMZ_DIFF4_O2.Betriebsminuten", language, 2));
            data.Add(new SingleValue("BMZ_DIFF_OCTO.Betriebsminuten", language, 2));
            data.Add(new SingleValue("Solar1.sSOL_Pumpe1_BMIN", language, 2));
            data.Add(new SingleValue("Solar1.sSOL_Pumpe2_BMIN", language, 2));
            data.Add(new SingleValue("Solar1.sSOL_Pumpe3_BMIN", language, 2));
            data.Add(new SingleValue("Solar1.sWMZ_Ertrag", language, 2));
            data.Add(new SingleValue("Solar2.sSOL_Pumpe1_BMIN", language, 2));
            data.Add(new SingleValue("Solar2.sSOL_Pumpe2_BMIN", language, 2));
            data.Add(new SingleValue("Solar2.sSOL_Pumpe3_BMIN", language, 2));
            data.Add(new SingleValue("Solar2.sWMZ_Ertrag", language, 2));
            data.Add(new SingleValue("Solar3.sSOL_Pumpe1_BMIN", language, 2));
            data.Add(new SingleValue("Solar3.sSOL_Pumpe2_BMIN", language, 2));
            data.Add(new SingleValue("Solar3.sSOL_Pumpe3_BMIN", language, 2));
            data.Add(new SingleValue("Solar3.sWMZ_Ertrag", language, 2));
            data.Add(new SingleValue("Solar4.sSOL_Pumpe1_BMIN", language, 2));
            data.Add(new SingleValue("Solar4.sSOL_Pumpe2_BMIN", language, 2));
            data.Add(new SingleValue("Solar4.sSOL_Pumpe3_BMIN", language, 2));
            data.Add(new SingleValue("Solar4.sWMZ_Ertrag", language, 2));

            //IP APP Mail - 4 
            data.Add(new SingleValue("HandleNetwork_SF1.ActivateDHCP", language, 3));
            data.Add(new SingleValue("HandleNetwork_SF1.IPAdresse", language, 3));
            data.Add(new SingleValue("HandleNetwork_SF1.SubNet", language, 3));
            data.Add(new SingleValue("HandleNetwork_SF1.Gateway", language, 3));
            data.Add(new SingleValue("HandleNetwork_SF1.DNS_IP", language, 3));
            //App
            data.Add(new SingleValue("Http_to_JSON1.Registrierung_ok", language, 3));
            data.Add(new SingleValue("Http_to_JSON1.SavePin", language, 3));
            data.Add(new SingleValue("Http_to_JSON1.Automatik_ein", language, 3));
            //Mail
            data.Add(new SingleValue("\"SCL_EMailClient1\\SCL_Mail_SMTPServer.Data", language, 3));
            data.Add(new SingleValue("\"SCL_EMailClient1\\SCL_Mail_SenderAddress.Data", language, 3));
            data.Add(new SingleValue("\"SCL_EMailClient1\\SCL_Mail_Username.Data", language, 3));
            data.Add(new SingleValue("SCL_EMailClient1\\SCL_Mail_SMTPPort.Data", language, 3));
            data.Add(new SingleValue("SCL_EMailClient1.UseSSL", language, 3));

            //Servicemenü
            data.Add(new SingleValue("touch_lock1.sLock_Enable", language, 4));

            return data;
        }

        #endregion

        #region ServiceMenu

        #region Kessel

        static public IList<SingleValue> serviceMenuRLA()
        {

            IList<SingleValue> data = new List<SingleValue>();

            //RLA - allgemeine Einstellungen
            data.Add(new SingleValue("Ausschalttemp_RLA_Differenz", language, boilerParameter.RLA.udRLAAusschalttemperatur, 0));
            data.Add(new SingleValue("udRLAStartdifferenz", language, boilerParameter.RLA.udRLAStartdifferenz, 0));
            data.Add(new SingleValue("udRLADiff_KT_RLF_PU", language, boilerParameter.RLA.udRLADiff_KT_RLF_PU, 0));
            data.Add(new SingleValue("udRLADiff_KT_PO", language, boilerParameter.RLA.udRLADiff_KT_PO, 0));
            data.Add(new SingleValue("udRLA_Hysterese", language, boilerParameter.RLA.udRLA_Hysterese, 0));
            data.Add(new SingleValue("RLA_Nachlaufzeit", language, boilerParameter.RLA_MISCHER.RLA_Nachlaufzeit, 0));
            data.Add(new SingleValue("PufferpumpeTyp", language, boilerParameter.HEIZKREISPARAMETER._HeizkreisKesselregler.PufferpumpeTyp, 0));
           
            //RLA - Mixer(X13)
            data.Add(new SingleValue("min_KT_Pellets", language, boilerParameter.RLA_MISCHER.min_KT_Pellets, 1));
            data.Add(new SingleValue("uRLA_Solltemperatur_Min", language, boilerParameter.RLA.uRLA_Solltemperatur_Min, 1));
            data.Add(new SingleValue("uRLA_Solltemperatur", language, boilerParameter.RLA.uRLA_Solltemperatur, 1)); //Rücklauf Solltemp. Max
            data.Add(new SingleValue("uminPause", language, boilerParameter.RLA.uminPause, 1));
            data.Add(new SingleValue("umaxPause", language, boilerParameter.RLA.umaxPause, 1));
            data.Add(new SingleValue("uMischerlaufzeit", language, boilerParameter.RLA.uMischerlaufzeit, 1));
            data.Add(new SingleValue("uPBereich", language, boilerParameter.RLA.uPBereich, 1));
            data.Add(new SingleValue("udPBereich_Plus", language, boilerParameter.RLA_MISCHER.udPBereich_Plus, 1));
            data.Add(new SingleValue("uPulsdauer", language, boilerParameter.RLA.uPulsdauer, 1));
            data.Add(new SingleValue("uiPumpe_ohne_Puffer", language, boilerParameter.RLA.uiPumpe_ohne_Puffer, 1));
            data.Add(new SingleValue("udRLAAusschalttemperatur", language, boilerParameter.RLA.udRLAAusschalttemperatur, 1));
            data.Add(new SingleValue("Mischerfunktion", language, boilerParameter.RLA.Mischerfunktion, 1));
            data.Add(new SingleValue("PufferpumpeTyp", language, boilerParameter.HEIZKREISPARAMETER._HeizkreisKesselregler.PufferpumpeTyp, 1));

            return data;
        }
        static public IList<SingleValue> serviceMenuPZ()
        {

            IList<SingleValue> data = new List<SingleValue>();

            data.Add(new SingleValue("udESZKurzerErsterEinschubimpuls", language, boilerParameter._EINSCHUB_ZUENDPHASE.udESZKurzerErsterEinschubimpuls, 0));
            data.Add(new SingleValue("udESZMaxEinschubimpuls", language, boilerParameter._EINSCHUB_ZUENDPHASE.udESZMaxEinschubimpuls, 0));
            data.Add(new SingleValue("udMinEinschubimpuls", language, boilerParameter._EINSCHUB_ZUENDPHASE.udMinEinschubimpuls, 0));

            return data;
        }
        static public IList<SingleValue> ServiceMenuPZF()
        {
            IList<SingleValue> data = new List<SingleValue>();
           
            data.Add(new SingleValue("udESZKurzerErsterEinschubimpuls", language, boilerParameter._EINSCHUB_ZUENDPHASE.udESZKurzerErsterEinschubimpuls, 0));    //0 Einschub
            data.Add(new SingleValue("udESZMaxEinschubimpuls", language, boilerParameter._EINSCHUB_ZUENDPHASE.udESZMaxEinschubimpuls, 0));
            data.Add(new SingleValue("udMinEinschubimpuls", language, boilerParameter._EINSCHUB_ZUENDPHASE.udMinEinschubimpuls, 0));
            data.Add(new SingleValue("udMinPause", language, boilerParameter._EINSCHUB_ZUENDPHASE.udMinPause, 0));
            data.Add(new SingleValue("udMaxPause", language, boilerParameter._EINSCHUB_ZUENDPHASE.udMaxPause, 0));
            data.Add(new SingleValue("udMaxAnzahlEinschuebe", language, boilerParameter._EINSCHUB_ZUENDPHASE.udMaxAnzahlEinschuebe, 0));
            data.Add(new SingleValue("udAnzahlSondereinschuebe", language, boilerParameter._EINSCHUB_ZUENDPHASE.udAnzahlSondereinschuebe, 0));
            data.Add(new SingleValue("udESZRauchgasanstieg", language, boilerParameter._EINSCHUB_ZUENDPHASE.udESZRauchgasanstieg, 0));
            data.Add(new SingleValue("udSGMaxStartRGT", language, boilerParameter.BRENNER.udSGMaxStartRGT, 0));

            data.Add(new SingleValue("udPEAnheizdrehzahl", language, boilerParameter._SAUGZUGGEBLAESE_PELLETS.udPEAnheizdrehzahl, 1));    //1 Saugzuggebläse
            data.Add(new SingleValue("udPEMaxDrehzahlZuendphase", language, boilerParameter._SAUGZUGGEBLAESE_PELLETS.udPEMaxDrehzahlZuendphase, 1));
            data.Add(new SingleValue("udPEMinRampeZuendphase", language, boilerParameter._SAUGZUGGEBLAESE_PELLETS.udPEMinRampeZuendphase, 1));
            data.Add(new SingleValue("udPEMaxRampeZuendphase", language, boilerParameter._SAUGZUGGEBLAESE_PELLETS.udPEMaxRampeZuendphase, 1));
            data.Add(new SingleValue("unPEStartLuefterdrehzahl", language, boilerParameter._SAUGZUGGEBLAESE_PELLETS.unPEStartLuefterdrehzahl, 1));
            data.Add(new SingleValue("udPEVorLuefterzeit", language, boilerParameter._SAUGZUGGEBLAESE_PELLETS.udPEVorLuefterzeit, 1));

            data.Add(new SingleValue("udPEMaxStartRGT", language, boilerParameter.BRENNER.udPEMaxStartRGT, 2));    //2 Abgastemperatur
            data.Add(new SingleValue("udPERGTAnstiegZuendphase", language, boilerParameter._SAUGZUGGEBLAESE_PELLETS.udPERGTAnstiegZuendphase, 2));
            data.Add(new SingleValue("udRGT_Hysterese", language, boilerParameter.KESSELVERWALTUNG.udRGT_Hysterese, 2));
            data.Add(new SingleValue("udRGTWarmStart", language, boilerParameter.KESSELVERWALTUNG.udRGTWarmStart, 2));

            data.Add(new SingleValue("udMaxZuendzeit", language, boilerParameter._ZUENDUNG.udMaxZuendzeit, 3));    //3 Zündung
            data.Add(new SingleValue("udErsterZuendimpuls", language, boilerParameter._ZUENDUNG.udErsterZuendimpuls, 3));
            data.Add(new SingleValue("udZuendimpuls", language, boilerParameter._ZUENDUNG.udZuendimpuls, 3));
            data.Add(new SingleValue("udZuendpause", language, boilerParameter._ZUENDUNG.udZuendpause, 3));
            data.Add(new SingleValue("udLambdaZuendimpulsEin", language, boilerParameter._ZUENDUNG.udLambdaZuendimpulsEin, 3));
            data.Add(new SingleValue("udLambdaZuendimpulsAus", language, boilerParameter._ZUENDUNG.udLambdaZuendimpulsAus, 3));
            data.Add(new SingleValue("udPrimMagnet_Zuendphase_Ein", language, boilerParameter._ZUENDUNG.udPrimMagnet_Zuendphase_Ein, 3));
            data.Add(new SingleValue("udPrimMagnet_Zuendphase_Lz", language, boilerParameter._ZUENDUNG.udPrimMagnet_Zuendphase_Lz, 3));
            data.Add(new SingleValue("GeblaeseNachlauf", language, boilerParameter._ZUENDUNG.GeblaeseNachlauf, 3));
            data.Add(new SingleValue("ZuendZuluft_Betrieb", language, boilerParameter._ZUENDUNG.ZuendZuluft_Betrieb, 3));
            
            return data;
        }

        static public IList<SingleValue> serviceMenuPSF()
        {

            IList<SingleValue> data = new List<SingleValue>();

            data.Add(new SingleValue("O2_Grenze_PLM_Ein", language, boilerParameter.PE_STARTPHASE.O2_Grenze_PLM_Ein, 0));
            data.Add(new SingleValue("O2_Grenze_PLM_Aus", language, boilerParameter.PE_STARTPHASE.O2_Grenze_PLM_Aus, 0));
            data.Add(new SingleValue("Rampe_min_LBD_kleiner_Soll", language, boilerParameter.PE_STARTPHASE.Rampe_min_LBD_kleiner_Soll, 0));
            data.Add(new SingleValue("Rampe_max_LBD_kleiner_Soll", language, boilerParameter.PE_STARTPHASE.Rampe_max_LBD_kleiner_Soll, 0));
            data.Add(new SingleValue("Rampe_min_LBD_groesser_Soll", language, boilerParameter.PE_STARTPHASE.Rampe_min_LBD_groesser_Soll, 0));
            data.Add(new SingleValue("Rampe_max_LBD_groesser_Soll", language, boilerParameter.PE_STARTPHASE.Rampe_max_LBD_groesser_Soll, 0));
            data.Add(new SingleValue("LDZ_Totbereich", language, boilerParameter.PE_STARTPHASE.LDZ_Totbereich, 0));
            data.Add(new SingleValue("Lambda_Offset_Zuend", language, boilerParameter.PE_STARTPHASE.Lambda_Offset_Zuend, 0));
            data.Add(new SingleValue("Lambda_Offset_3punkt", language, boilerParameter.PE_STARTPHASE.Lambda_Offset_3punkt, 0));
            data.Add(new SingleValue("Lambda_Offset_Start", language, boilerParameter.PE_STARTPHASE.Lambda_Offset_Start, 0));
            data.Add(new SingleValue("udPEMaxDrehzahlZuendphase", language, boilerParameter._SAUGZUGGEBLAESE_PELLETS.udPEMaxDrehzahlZuendphase, 0));
            data.Add(new SingleValue("Lambda_DRZ_Offset_3punkt", language, boilerParameter.PE_STARTPHASE.Lambda_DRZ_Offset_3punkt, 0));
            data.Add(new SingleValue("udPEMax_LDZ_Startphase", language, boilerParameter._SAUGZUGGEBLAESE_PELLETS.udPEMax_LDZ_Startphase, 0));
           
            return data;
        }

        static public IList<SingleValue> serviceMenuPB()
        {

            IList<SingleValue> data = new List<SingleValue>();

            data.Add(new SingleValue("udESEinschubimpuls100", language, boilerParameter._EINSCHUB_BETRIEB.udESEinschubimpuls100, 0));    //0 Pellets Betrieb
            data.Add(new SingleValue("udESPause", language, boilerParameter._EINSCHUB_BETRIEB.udESPause, 0));
            // StockerWerte[0,10,..]
            for (int i = 0; i <= 100; i += 10)
            {
                data.Add(new SingleValue("StockerWerte_" + i.ToString(), language, boilerParameter.StockerWerte[i], 0));
            }


            return data;
        }

        static public IList<SingleValue> serviceMenuPBF()
        {

            IList<SingleValue> data = new List<SingleValue>();

            data.Add(new SingleValue("udESLambdaSollEinschub", language, boilerParameter._EINSCHUB_BETRIEB.udESLambdaSollEinschub, 0));    //0 Einschub Betrieb
            data.Add(new SingleValue("udESLbd_Tot_Bereich", language, boilerParameter._EINSCHUB_BETRIEB.udESLbd_Tot_Bereich, 0));
            data.Add(new SingleValue("udESLambdaUG", language, boilerParameter._EINSCHUB_BETRIEB.udESLambdaUG, 0));
            data.Add(new SingleValue("udESLambdaOG", language, boilerParameter._EINSCHUB_BETRIEB.udESLambdaOG, 0));
            data.Add(new SingleValue("udESMaxImpulsverlaengerung", language, boilerParameter._EINSCHUB_BETRIEB.udESMaxImpulsverlaengerung, 0));
            data.Add(new SingleValue("udESStartphaseMaxImpVerlaengerung", language, boilerParameter._EINSCHUB_BETRIEB.udESStartphaseMaxImpVerlaengerung, 0));
            data.Add(new SingleValue("udESMaxImpulsverkuerzung", language, boilerParameter._EINSCHUB_BETRIEB.udESMaxImpulsverkuerzung, 0));
            data.Add(new SingleValue("udESReduzierimpuls", language, boilerParameter._EINSCHUB_BETRIEB.udESReduzierimpuls, 0));
            data.Add(new SingleValue("LambdaoffsetTeillast", language, boilerParameter._EINSCHUB_BETRIEB.LambdaoffsetTeillast, 0));

            data.Add(new SingleValue("udESEinschubimpuls100", language, boilerParameter._EINSCHUB_BETRIEB.udESEinschubimpuls100, 1));    //1 Einschub Betrieb Optimierung
            data.Add(new SingleValue("udESPause", language, boilerParameter._EINSCHUB_BETRIEB.udESPause, 1));
            data.Add(new SingleValue("udESMitterwertzeit", language, boilerParameter._EINSCHUB_BETRIEB.udESMitterwertzeit, 1));
            data.Add(new SingleValue("udESzulLDZAenderung", language, boilerParameter._EINSCHUB_BETRIEB.udESzulLDZAenderung, 1));
            data.Add(new SingleValue("udESFaktorImpAenderungLDZBereich", language, boilerParameter._EINSCHUB_BETRIEB.udESFaktorImpAenderungLDZBereich, 1));
            data.Add(new SingleValue("udESMinEinschubimpuls", language, boilerParameter._EINSCHUB_BETRIEB.udESMinEinschubimpuls, 1));
            data.Add(new SingleValue("udESMaxEinschubimpuls", language, boilerParameter._EINSCHUB_BETRIEB.udESMaxEinschubimpuls, 1));
            data.Add(new SingleValue("udESLbdMittelwert", language, boilerParameter._EINSCHUB_BETRIEB.udESLbdMittelwert, 1));
            data.Add(new SingleValue("MaxAnzahlBlockaden", language, boilerParameter._EINSCHUB_BETRIEB.MaxAnzahlBlockaden, 1));
            data.Add(new SingleValue("udES_LDZMittelwert", language, boilerParameter._EINSCHUB_BETRIEB.udES_LDZMittelwert, 1));

            data.Add(new SingleValue("udPEMax_Luefterdrehzahl", language, boilerParameter._SAUGZUGGEBLAESE_PELLETS.udPEMax_Luefterdrehzahl, 2));    //2 Saugzuggebläse
            data.Add(new SingleValue("udPEMin_Luefterdrehzahl", language, boilerParameter._SAUGZUGGEBLAESE_PELLETS.udPEMin_Luefterdrehzahl, 2));
            data.Add(new SingleValue("udKesselsolltemperatur", language, boilerParameter.KESSELVERWALTUNG.udKesselsolltemperatur, 2));
            data.Add(new SingleValue("udPEKesseltemperaturPBereich", language, boilerParameter._SAUGZUGGEBLAESE_PELLETS.udPEKesseltemperaturPBereich, 2));
            data.Add(new SingleValue("udPEMax_LDZ_Startphase", language, boilerParameter._SAUGZUGGEBLAESE_PELLETS.udPEMax_LDZ_Startphase, 2));
            data.Add(new SingleValue("udESStartphase", language, boilerParameter._EINSCHUB_BETRIEB.udESStartphase, 2));
            data.Add(new SingleValue("udRGTSoll", language, boilerParameter.KESSELVERWALTUNG.udRGTSoll, 2));
            data.Add(new SingleValue("udMinRGT", language, boilerParameter.KESSELVERWALTUNG.udMinRGT, 2));
            data.Add(new SingleValue("udMaxRGT", language, boilerParameter.KESSELVERWALTUNG.udMaxRGT, 2));
            data.Add(new SingleValue("udPEMinLuefterrampePelletsbetrieb", language, boilerParameter._SAUGZUGGEBLAESE_PELLETS.udPEMinLuefterrampePelletsbetrieb, 2));
            data.Add(new SingleValue("udPEMaxLuefterrampePelletsbetrieb", language, boilerParameter._SAUGZUGGEBLAESE_PELLETS.udPEMaxLuefterrampePelletsbetrieb, 2));
            data.Add(new SingleValue("udPEMinLuefterrampePelletsbetrieb_schnell", language, boilerParameter._SAUGZUGGEBLAESE_PELLETS.udPEMinLuefterrampePelletsbetrieb_schnell, 2));
            data.Add(new SingleValue("udPEMaxLuefterrampePelletsbetrieb_schnell", language, boilerParameter._SAUGZUGGEBLAESE_PELLETS.udPEMaxLuefterrampePelletsbetrieb_schnell, 2));
            data.Add(new SingleValue("udPEAusbrenndrehzahl", language, boilerParameter._SAUGZUGGEBLAESE_PELLETS.udPEAusbrenndrehzahl, 2));

            return data;
        }

        static public IList<SingleValue> serviceMenuSA()
        {

            IList<SingleValue> data = new List<SingleValue>();

            data.Add(new SingleValue("udMaxSauglaufzeit", language, boilerParameter.SAUGAUSTRAGUNG.udMaxSauglaufzeit, 0));    //0 Saugaustragung
            data.Add(new SingleValue("udSGA_Anz_Zwangsbefuellung", language, boilerParameter.SAUGAUSTRAGUNG.udSGA_Anz_Zwangsbefuellung, 0));
            data.Add(new SingleValue("udMaxEinschublaufzeit", language, boilerParameter.SAUGAUSTRAGUNG.udMaxEinschublaufzeit, 0));

            return data;
        }

        static public IList<SingleValue> serviceMenuSAF()
        {

            IList<SingleValue> data = new List<SingleValue>();

            data.Add(new SingleValue("udMaxEinschublaufzeit", language, boilerParameter.SAUGAUSTRAGUNG.udMaxEinschublaufzeit, 0));    //0 Saugaustragung
            data.Add(new SingleValue("udMinEinschubProzente", language, boilerParameter.SAUGAUSTRAGUNG.udMinEinschubProzente, 0));
            data.Add(new SingleValue("udStartEinschubProzente", language, boilerParameter.SAUGAUSTRAGUNG.udStartEinschubProzente, 0));
            data.Add(new SingleValue("udMaxSauglaufzeit", language, boilerParameter.SAUGAUSTRAGUNG.udMaxSauglaufzeit, 0));
            data.Add(new SingleValue("udSGA_Anz_Zwangsbefuellung", language, boilerParameter.SAUGAUSTRAGUNG.udSGA_Anz_Zwangsbefuellung, 0));
            data.Add(new SingleValue("SGA_Abschalttemperatur", language, boilerParameter.SAUGAUSTRAGUNG.SGA_Abschalttemperatur, 0));
            data.Add(new SingleValue("udPE_Saugen_Drz", language, boilerParameter._SAUGZUGGEBLAESE_PELLETS.udPE_Saugen_Drz, 0));

            if (boilerParameter.KESSELTYP.ToString() == "Plus")
            {
                data.Add(new SingleValue("SGT_Wartezeit", language, boilerParameter.ALLGEMEINE_EINSTELLUNGEN2.SGT_Wartezeit, 0));
            }
            if(boilerParameter.ANLAGENTYP.ToString() == "MANUELLE_BEFUELLUNG" || boilerParameter.ANLAGENTYP.ToString() == "ManVORRATSBEH_INTERN" || boilerParameter.ANLAGENTYP.ToString() == "SAUGAUSTRAGUNG")
            {
                data.Add(new SingleValue("udEinschaltverzoegerung", language, boilerParameter.SAUGAUSTRAGUNG.udEinschaltverzoegerung, 1));    //1 Raumaustragung
                data.Add(new SingleValue("udAusschaltverzoegerung", language, boilerParameter.SAUGAUSTRAGUNG.udAusschaltverzoegerung, 1));
                data.Add(new SingleValue("udAustragungImpuls_Min", language, boilerParameter.SAUGAUSTRAGUNG.udAustragungImpuls_Min, 1));
                data.Add(new SingleValue("udAustragungImpuls_Sek", language, boilerParameter.SAUGAUSTRAGUNG.udAustragungImpuls_Sek, 1));
                data.Add(new SingleValue("udAustragungPause_Sek", language, boilerParameter.SAUGAUSTRAGUNG.udAustragungPause_Sek, 1));
                data.Add(new SingleValue("udThermokontaktUeberwachung", language, boilerParameter.SAUGAUSTRAGUNG.udThermokontaktUeberwachung, 1));
            }
            else if(boilerParameter.ANLAGENTYP.ToString() == "RAUMAUSTRAGUNG")
            {
                data.Add(new SingleValue("udEinschaltverzoegerung", language, boilerParameter.SAUGAUSTRAGUNG.udEinschaltverzoegerung, 1));    //1 Raumaustragung
                data.Add(new SingleValue("udAusschaltverzoegerung", language, boilerParameter.SAUGAUSTRAGUNG.udAusschaltverzoegerung, 1));
                data.Add(new SingleValue("udMaxAustragungsImpulsRA", language, boilerParameter.SAUGAUSTRAGUNG.udMaxAustragungsImpulsRA, 1));
                data.Add(new SingleValue("udSensorUeberwachungRA", language, boilerParameter.SAUGAUSTRAGUNG.udSensorUeberwachungRA, 1));
                data.Add(new SingleValue("udMaxEinschubImpulsRA", language, boilerParameter.SAUGAUSTRAGUNG.udMaxEinschubImpulsRA, 1));
                data.Add(new SingleValue("udThermokontaktUeberwachung", language, boilerParameter.SAUGAUSTRAGUNG.udThermokontaktUeberwachung, 1));
            }
            else
            {
                data.Add(new SingleValue("udEinschaltverzoegerung", language, boilerParameter.SAUGAUSTRAGUNG.udEinschaltverzoegerung, 1));    //1 Raumaustragung
                data.Add(new SingleValue("udAusschaltverzoegerung", language, boilerParameter.SAUGAUSTRAGUNG.udAusschaltverzoegerung, 1));
                data.Add(new SingleValue("udAustragungImpuls_Min", language, boilerParameter.SAUGAUSTRAGUNG.udAustragungImpuls_Min, 1));
                data.Add(new SingleValue("udAustragungImpuls_Sek", language, boilerParameter.SAUGAUSTRAGUNG.udAustragungImpuls_Sek, 1));
                data.Add(new SingleValue("udAustragungPause_Sek", language, boilerParameter.SAUGAUSTRAGUNG.udAustragungPause_Sek, 1));
                data.Add(new SingleValue("udMaxAustragungsImpulsRA", language, boilerParameter.SAUGAUSTRAGUNG.udMaxAustragungsImpulsRA, 1));
                data.Add(new SingleValue("udSensorUeberwachungRA", language, boilerParameter.SAUGAUSTRAGUNG.udSensorUeberwachungRA, 1));
                data.Add(new SingleValue("udMaxEinschubImpulsRA", language, boilerParameter.SAUGAUSTRAGUNG.udMaxEinschubImpulsRA, 1));
                data.Add(new SingleValue("udThermokontaktUeberwachung", language, boilerParameter.SAUGAUSTRAGUNG.udThermokontaktUeberwachung, 1));
            }
            return data;
        }

        static public IList<SingleValue> serviceMenuSPP()
        {

            IList<SingleValue> data = new List<SingleValue>();

            data.Add(new SingleValue("udMaximalerStromEinschub", language, boilerParameter._EINSCHUB_ZUENDPHASE.udMaximalerStromEinschub, 0));    //0 Systemparameter Pellets

            return data;
        }

        static public IList<SingleValue> serviceMenuSPPF()
        {

            IList<SingleValue> data = new List<SingleValue>();

            data.Add(new SingleValue("udESMaxTemperatur", language, boilerParameter.KESSELVERWALTUNG.udESMaxTemperatur, 0));    //0 Seite 1
            data.Add(new SingleValue("udKesselsollueberhoeung", language, boilerParameter.KESSELVERWALTUNG.udKesselsollueberhoeung, 0));
            data.Add(new SingleValue("udKesseltempPrimaerluftZu", language, boilerParameter.KESSELVERWALTUNG.udKesseltempPrimaerluftZu, 0));
            data.Add(new SingleValue("udLambdaueberwachung", language, boilerParameter.BRENNER.udLambdaueberwachung, 0));
            data.Add(new SingleValue("udZeitLambdaUeberwachung", language, boilerParameter.BRENNER.udZeitLambdaUeberwachung, 0));
            data.Add(new SingleValue("StrommessungAktiv", language, boilerParameter._EINSCHUB_ZUENDPHASE.StrommessungAktiv, 0));
            data.Add(new SingleValue("udMaximalerStromEinschub", language, boilerParameter._EINSCHUB_ZUENDPHASE.udMaximalerStromEinschub, 0));

            data.Add(new SingleValue("RauchgasAbfallueberwachung", language, boilerParameter.BRENNER.RauchgasAbfallueberwachung, 1));    //1 Seite 2
            data.Add(new SingleValue("udRGTAbfall_keine_Verkuerzung", language, boilerParameter.BRENNER.udRGTAbfall_keine_Verkuerzung, 1));
            data.Add(new SingleValue("udRGTAbfall_max_Verkuerzung", language, boilerParameter.BRENNER.udRGTAbfall_max_Verkuerzung, 1));
            data.Add(new SingleValue("udRGTAbfall_Stop", language, boilerParameter.BRENNER.udRGTAbfall_Stop, 1));
            data.Add(new SingleValue("udRGT_maxImpulsverkuerzung", language, boilerParameter.BRENNER.udRGT_maxImpulsverkuerzung, 1));
            data.Add(new SingleValue("udLDZKonstantzeit", language, boilerParameter.BRENNER.udLDZKonstantzeit, 1));
            data.Add(new SingleValue("RauchgasfehlertempUeberw", language, boilerParameter.BRENNER.RauchgasfehlertempUeberw, 1));
            data.Add(new SingleValue("udFehlerRGTmax", language, boilerParameter.BRENNER.udFehlerRGTmax, 1));
            data.Add(new SingleValue("FehlerRGTmin", language, boilerParameter.BRENNER.FehlerRGTmin, 1));
            data.Add(new SingleValue("udFehlerRGTZeitBetrieb", language, boilerParameter.BRENNER.udFehlerRGTZeitBetrieb, 1));
            data.Add(new SingleValue("udFehlerRGTZeitStart", language, boilerParameter.BRENNER.udFehlerRGTZeitStart, 1));

            data.Add(new SingleValue("udDrehzahlrueckfuehrung", language, boilerParameter._SAUGZUGGEBLAESE_PELLETS.udDrehzahlrueckfuehrung, 2));    //2 Seite 3
            data.Add(new SingleValue("udMaxLuefterdrehzahl", language, boilerParameter.SGZ_DREHZAHL.udMaxLuefterdrehzahl, 2));
            data.Add(new SingleValue("udImpulseproUmdrehung", language, boilerParameter.SGZ_DREHZAHL.udImpulseproUmdrehung, 2));
            data.Add(new SingleValue("Startphase_Neu", language, boilerParameter.PE_STARTPHASE.Startphase_Neu, 2));
            data.Add(new SingleValue("Umschaltung_Regler1.sReglerUmschaltung", language, 0)); //(k_daten.csv)
            //PID Kesselregler k_daten.csv
            data.Add(new SingleValue("PAnteil", language, boilerParameter.PIDREGLER.KESSEL.PAnteil, 2));
            data.Add(new SingleValue("IAnteil", language, boilerParameter.PIDREGLER.KESSEL.IAnteil, 2));
            data.Add(new SingleValue("DAnteil", language, boilerParameter.PIDREGLER.KESSEL.DAnteil, 2));
            data.Add(new SingleValue("I_Max", language, boilerParameter.PIDREGLER.KESSEL.I_Max, 2));
            data.Add(new SingleValue("dGleitendeKTSoll", language, boilerParameter.BRENNER.dGleitendeKTSoll, 2));
            return data;
        }

        static public IList<SingleValue> serviceMenuSP()
        {

            IList<SingleValue> data = new List<SingleValue>();

            //Allgemeine Einstellungen - 0
            data.Add(new SingleValue("FBM_Server1.sServer_aktiv", language, 0));
            data.Add(new SingleValue("FBM_Server1.sPort", language, 0)); 
            data.Add(new SingleValue("Kessel1.sNurAdminEinstieg", language, 0)); 
            data.Add(new SingleValue("udKesselstartdifferenz", language, boilerParameter.KESSELVERWALTUNG.udKesselstartdifferenz, 0));   
            data.Add(new SingleValue("udMaxKesseltemperatur", language, boilerParameter.KESSELVERWALTUNG.udMaxKesseltemperatur, 0));
            data.Add(new SingleValue("udLbdSonde_Kalibrieren_Nach", language, boilerParameter.SYSTEMPARAMETER.udLbdSonde_Kalibrieren_Nach, 0));
            data.Add(new SingleValue("Ram4.Data", language, 0));
            data.Add(new SingleValue("AusgaengeZuendung", language, boilerParameter._ZUENDUNG.AusgaengeZuendung, 0));
            data.Add(new SingleValue("udPELuefternachlauf_kurz", language, boilerParameter._SAUGZUGGEBLAESE_PELLETS.udPELuefternachlauf_kurz, 0));
            data.Add(new SingleValue("udPELuefternachlauf_normal", language, boilerParameter._SAUGZUGGEBLAESE_PELLETS.udPELuefternachlauf_normal, 0));
            data.Add(new SingleValue("udPELuefternachlauf_lang", language, boilerParameter._SAUGZUGGEBLAESE_PELLETS.udPELuefternachlauf_lang, 0));
            data.Add(new SingleValue("Reinigungshinweis_Ein", language, boilerParameter.ASCHENAUSTRAGUNG.Reinigungshinweis_Ein, 0));
            data.Add(new SingleValue("udmaxPelletsbetrieb_bis_Entleerhinweis", language, boilerParameter.ASCHENAUSTRAGUNG.udmaxPelletsbetrieb_bis_Entleerhinweis, 0));
            data.Add(new SingleValue("TuerKontrolleEINAUS", language, boilerParameter.SYSTEMPARAMETER.TuerKontrolleEINAUS, 0));
            data.Add(new SingleValue("Bildschirmschoner_nach", language, boilerParameter.ALLGEMEINE_EINSTELLUNGEN.Bildschirmschoner_nach, 0));
            data.Add(new SingleValue("iATFrostschutzTempKessel", language, boilerParameter.BRENNER.iATFrostschutzTempKessel, 0));
            data.Add(new SingleValue("FunktionX51", language, boilerParameter.SYSTEMPARAMETER.FunktionX51, 0));
            data.Add(new SingleValue("Standby_Ein", language, boilerParameter.SYSTEMPARAMETER.Standby_Ein, 0));
            data.Add(new SingleValue("Standby_ZeitbisZfg", language, boilerParameter.SYSTEMPARAMETER.Standby_ZeitbisZfg, 0));
            data.Add(new SingleValue("udStandby_Zeitnachtouch", language, boilerParameter.SYSTEMPARAMETER.udStandby_Zeitnachtouch, 0));
            data.Add(new SingleValue("HKXL_Modul", language, boilerParameter.ALLGEMEINE_EINSTELLUNGEN.HKXL_Modul, 0));

            //Wartungstermin -2 
            data.Add(new SingleValue("WartungsterminEINAUS", language, boilerParameter.SYSTEMPARAMETER.WartungsterminEINAUS, 2)); 
            data.Add(new SingleValue("Wartung_Monate", language, boilerParameter.SYSTEMPARAMETER.Wartung_Monate, 2));
            data.Add(new SingleValue("naechster_wartungstermin", language, 2));
            data.Add(new SingleValue("CPU_Info1.sDatumBatterieGetauscht", language, 2));
            data.Add(new SingleValue("Betriebsstunden_seit_Wartung", language, 2));

            //Fremdkessel - 3
            data.Add(new SingleValue("Fremdkesselfreigabe", language, boilerParameter.KESSELVERWALTUNG.Fremdkesselfreigabe, 3));   
            data.Add(new SingleValue("udZeitverzoegerung_FK", language, boilerParameter.SYSTEMPARAMETER.udZeitverzoegerung_FK, 3));
            data.Add(new SingleValue("Funktion_Reserverelais", language, boilerParameter.SYSTEMPARAMETER.Funktion_Reserverelais, 3));
            data.Add(new SingleValue("Kesselbetrieb_Verzoegerung", language, boilerParameter.BRENNER.Kesselbetrieb_Verzoegerung, 3));

            //3-Wege-Motorventil - 4
            data.Add(new SingleValue("VentStellung_stromlos", language, boilerParameter.DREI_WEGE_MOTVENT.VentStellung_stromlos, 4));  
            data.Add(new SingleValue("Schalttemp_Ventil", language, boilerParameter.DREI_WEGE_MOTVENT.Schalttemp_Ventil, 4));
            data.Add(new SingleValue("FK_Fuehler_Steckplatz", language, boilerParameter.DREI_WEGE_MOTVENT.FK_Fuehler_Steckplatz, 4));
            data.Add(new SingleValue("Zielpuffer", language, boilerParameter.KASKADE.Zielpuffer, 4));
            data.Add(new SingleValue("Uebertemperatur", language, boilerParameter.DREI_WEGE_MOTVENT.Uebertemperatur, 4));
            data.Add(new SingleValue("AusschalthystereseFK", language, boilerParameter.DREI_WEGE_MOTVENT.AusschalthystereseFK, 4));
            data.Add(new SingleValue("EinschalthystereseFK", language, boilerParameter.DREI_WEGE_MOTVENT.EinschalthystereseFK, 4));

            //Fuehlereinstellungen - 5
            data.Add(new SingleValue("SensorTyp_Temp3", language, boilerParameter.SYSTEMPARAMETER.SensorTyp_Temp3, 5));
            data.Add(new SingleValue("SensorTyp_Temp4", language, boilerParameter.SYSTEMPARAMETER.SensorTyp_Temp4, 5));
            data.Add(new SingleValue("SensorTyp_Temp5", language, boilerParameter.SYSTEMPARAMETER.SensorTyp_Temp5, 5));
            data.Add(new SingleValue("SensorTyp_Temp6", language, boilerParameter.SYSTEMPARAMETER.SensorTyp_Temp6, 5));
            data.Add(new SingleValue("SensorTyp_Temp7", language, boilerParameter.SYSTEMPARAMETER.SensorTyp_Temp7, 5));
            data.Add(new SingleValue("SensorTyp_Temp8", language, boilerParameter.SYSTEMPARAMETER.SensorTyp_Temp8, 5));
            data.Add(new SingleValue("SensorTyp_Temp9", language, boilerParameter.SYSTEMPARAMETER.SensorTyp_Temp9, 5));
            data.Add(new SingleValue("SensorTyp_Temp10", language, boilerParameter.SYSTEMPARAMETER.SensorTyp_Temp10, 5));
            data.Add(new SingleValue("SensorTyp_Temp11", language, boilerParameter.SYSTEMPARAMETER.SensorTyp_Temp11, 5));
            data.Add(new SingleValue("SensorTyp_Temp12", language, boilerParameter.SYSTEMPARAMETER.SensorTyp_Temp12, 5));
            data.Add(new SingleValue("SensorTyp_Temp13", language, boilerParameter.SYSTEMPARAMETER.SensorTyp_Temp13, 5));
            data.Add(new SingleValue("SensorTyp_Temp14", language, boilerParameter.SYSTEMPARAMETER.SensorTyp_Temp14, 5));
            data.Add(new SingleValue("SensorTyp_Temp15", language, boilerParameter.SYSTEMPARAMETER.SensorTyp_Temp15, 5));
            data.Add(new SingleValue("SensorTypSol_Temp1", language, boilerParameter.SYSTEMPARAMETER.SensorTypSol_Temp1, 5));
            data.Add(new SingleValue("SensorTypSol_Temp2", language, boilerParameter.SYSTEMPARAMETER.SensorTypSol_Temp2, 5));
            data.Add(new SingleValue("SensorTypSol_Temp3", language, boilerParameter.SYSTEMPARAMETER.SensorTypSol_Temp3, 5));
            data.Add(new SingleValue("SensorTypSol_Temp4", language, boilerParameter.SYSTEMPARAMETER.SensorTypSol_Temp4, 5));
            data.Add(new SingleValue("SensorTypSol_Temp5", language, boilerParameter.SYSTEMPARAMETER.SensorTypSol_Temp5, 5));
            data.Add(new SingleValue("WiderstandsOffset_RF1", language, boilerParameter.SYSTEMPARAMETER.WiderstandsOffset_RF1, 5));
            data.Add(new SingleValue("WiderstandsOffset_RF2", language, boilerParameter.SYSTEMPARAMETER.WiderstandsOffset_RF2, 5));
            data.Add(new SingleValue("AT_Abgleich_X42", language, boilerParameter.SYSTEMPARAMETER.AT_Abgleich_X42, 5));


            //Wärmetauscher - 6
            data.Add(new SingleValue("udGesamtlaufzeit", language, boilerParameter.WAERMETAUSCHERREINUNG.udGesamtlaufzeit, 6));
            data.Add(new SingleValue("udPulsdauer", language, boilerParameter.WAERMETAUSCHERREINUNG.udPulsdauer, 6));
            data.Add(new SingleValue("udPulspause", language, boilerParameter.WAERMETAUSCHERREINUNG.udPulspause, 6));
            data.Add(new SingleValue("udMinimaleLaufzeitPellets", language, boilerParameter.WAERMETAUSCHERREINUNG.udMinimaleLaufzeitPellets, 6));
            data.Add(new SingleValue("udMaximaleLaufzeitPellets", language, boilerParameter.WAERMETAUSCHERREINUNG.udMaximaleLaufzeitPellets, 6));
            data.Add(new SingleValue("MaxAnzAbgebrochenerReinigungen", language, boilerParameter.WAERMETAUSCHERREINUNG.MaxAnzAbgebrochenerReinigungen, 6));
            data.Add(new SingleValue("Blockadestrom", language, boilerParameter.WAERMETAUSCHERREINUNG.Blockadestrom, 6));
            data.Add(new SingleValue("FreigabeRauchgastemperatur", language, boilerParameter.WAERMETAUSCHERREINUNG.FreigabeRauchgastemperatur, 6));
            data.Add(new SingleValue("StrommessungAktiv", language, boilerParameter.WAERMETAUSCHERREINUNG.StrommessungAktiv, 6));
           

            //Aschenbox - 7
            if (boilerParameter.KESSELTYP.ToString() == "Elegance")
            {
                data.Add(new SingleValue("Elegance_udAschenwalzefreigabezeit", language, boilerParameter.ASCHENAUSTRAGUNG.udAschenwalzefreigabezeit, 7));  
                data.Add(new SingleValue("Elegance_udAschenaustr_Pause", language, boilerParameter.ASCHENAUSTRAGUNG.udAschenaustr_Pause, 7));
                data.Add(new SingleValue("Elegance_udAschenaustr_Impuls", language, boilerParameter.ASCHENAUSTRAGUNG.udAschenaustr_Impuls, 7));
                data.Add(new SingleValue("udESLZ_bis_Abbrandimpuls", language, boilerParameter.WAERMETAUSCHERREINUNG.udESLZ_bis_Abbrandimpuls, 7));   // Pause bis enleerung
                data.Add(new SingleValue("Elegance_udImpuls_nach_LNL", language, boilerParameter.ASCHENAUSTRAGUNG.udImpuls_nach_LNL, 7));
                data.Add(new SingleValue("Elegance_udEntleerungIntervall", language, boilerParameter.ASCHENAUSTRAGUNG.udEntleerungIntervall, 7));
                data.Add(new SingleValue("StrommessungAktiv_Asche", language, boilerParameter.ALLGEMEINE_EINSTELLUNGEN.StrommessungAktiv_Asche, 7));
                data.Add(new SingleValue("MaxStrom_Asche", language, boilerParameter.ALLGEMEINE_EINSTELLUNGEN.MaxStrom_Asche, 7));
            }
            else if (boilerParameter.KESSELTYP.ToString() == "Plus")
            {
                data.Add(new SingleValue("Plus_udAschenwalzefreigabezeit", language, boilerParameter.ASCHENAUSTRAGUNG.udAschenwalzefreigabezeit, 7));
                data.Add(new SingleValue("Plus_udAschenaustr_erster_Impuls", language, boilerParameter.ASCHENAUSTRAGUNG.udAschenaustr_erster_Impuls, 7));
                data.Add(new SingleValue("Plus_udAschenaustr_Pause", language, boilerParameter.ASCHENAUSTRAGUNG.udAschenaustr_Pause, 7));
                data.Add(new SingleValue("Plus_udAschenaustr_Impuls", language, boilerParameter.ASCHENAUSTRAGUNG.udAschenaustr_Impuls, 7));
                data.Add(new SingleValue("Plus_udEntleerungIntervall", language, boilerParameter.ASCHENAUSTRAGUNG.udEntleerungIntervall, 7));
                data.Add(new SingleValue("Plus_udImpuls_nach_LNL", language, boilerParameter.ASCHENAUSTRAGUNG.udImpuls_nach_LNL, 7));
                data.Add(new SingleValue("Plus_Impuls_bei_Start", language, boilerParameter.WAERMETAUSCHERREINUNG.Impuls_bei_Start, 7));
            }
            else
            {
                data.Add(new SingleValue("udEntleerungIntervall", language, boilerParameter.ASCHENAUSTRAGUNG.udEntleerungIntervall, 7));
                data.Add(new SingleValue("uiLaufzeit_pro_h", language, boilerParameter.ASCHENAUSTRAGUNG.uiMinLaufzeit_pro_h.ToString() + " Min. " + boilerParameter.ASCHENAUSTRAGUNG.uiSekLaufzeit_pro_h.ToString() + " Sek.", 7));
                data.Add(new SingleValue("StrommessungAktiv_Asche", language, boilerParameter.ALLGEMEINE_EINSTELLUNGEN.StrommessungAktiv_Asche, 7));
                data.Add(new SingleValue("MaxStrom_Asche", language, boilerParameter.ALLGEMEINE_EINSTELLUNGEN.MaxStrom_Asche, 7));
            }

            //Notbetrieb für Ausgänge - 8
            data.Add(new SingleValue("Funktion1", language, boilerParameter._NOTBETRIEB[0].Notbetrieb_Ausgang_IN, 8));      
            data.Add(new SingleValue("Ausgang1", language, boilerParameter._NOTBETRIEB[0].Notbetrieb_Ausgang_OUT, 8));
            data.Add(new SingleValue("Funktion2", language, boilerParameter._NOTBETRIEB[1].Notbetrieb_Ausgang_IN, 8));
            data.Add(new SingleValue("Ausgang2", language, boilerParameter._NOTBETRIEB[1].Notbetrieb_Ausgang_OUT, 8));
            data.Add(new SingleValue("Funktion3", language, boilerParameter._NOTBETRIEB[2].Notbetrieb_Ausgang_IN, 8));
            data.Add(new SingleValue("Ausgang3", language, boilerParameter._NOTBETRIEB[2].Notbetrieb_Ausgang_OUT, 8));

            //Anlagentyp & Nennleistung - 9
            data.Add(new SingleValue("KESSELTYP", language, boilerParameter.KESSELTYP, 9));     
            data.Add(new SingleValue("KESSELLEISTUNG", language, boilerParameter.KESSELLEISTUNG, 9));
            data.Add(new SingleValue("uiAschenbox_vorhanden", language, boilerParameter.BRENNER.uiAschenbox_vorhanden, 9));
            data.Add(new SingleValue("ANLAGENTYP", language, boilerParameter.ANLAGENTYP, 9));
            data.Add(new SingleValue("KESSELBETRIEBSART", language, boilerParameter.KESSELBETRIEBSART, 9));
            data.Add(new SingleValue("uiZuendfoenzuendung", language, boilerParameter._ZUENDUNG.uiZuendfoenzuendung, 9));


            //Saugaustragung Anlagentyp
            data.Add(new SingleValue("Umschalteinheit", language, boilerParameter.SAUGAUSTRAGUNG.Umschalteinheit, 10));      //10 Saugaustragung Anlagentyp
            data.Add(new SingleValue("Sondenumschaltung", language, boilerParameter.SAUGAUSTRAGUNG.Sondenumschaltung, 10));
            data.Add(new SingleValue("verwendete_Saugsonden", language, boilerParameter.SAUGAUSTRAGUNG.verwendete_Saugsonden, 10));
            data.Add(new SingleValue("Saugsonde_mit_Freisaugen_beginnen", language, boilerParameter.SAUGAUSTRAGUNG.Saugsonde_mit_Freisaugen_beginnen, 10));
            data.Add(new SingleValue("udSaugimpuls_SSUE", language, boilerParameter.SAUGAUSTRAGUNG.udSaugimpuls_SSUE, 10));

            //Erweiterungsmodul Modultyp (k_daten.csv)
            data.Add(new SingleValue("CAN_Erweiterung1.sHK_XL_MOD1_Typ", language, 11));
            data.Add(new SingleValue("CAN_Erweiterung1.sHK_MOD1_Typ", language, 11));
            data.Add(new SingleValue("CAN_Erweiterung1.sHK_MOD2_Typ", language, 11));
            data.Add(new SingleValue("CAN_Erweiterung1.sHK_MOD3_Typ", language, 11));
            data.Add(new SingleValue("CAN_Erweiterung1.sFW_MOD1_Typ", language, 11));
            data.Add(new SingleValue("CAN_Erweiterung1.sFW_MOD2_Typ", language, 11));
            data.Add(new SingleValue("CAN_Erweiterung1.sFW_MOD3_Typ", language, 11));
            data.Add(new SingleValue("CAN_Erweiterung1.sFW_MOD4_Typ", language, 11));
            data.Add(new SingleValue("CAN_Erweiterung1.sDIFF_MOD1_Typ", language, 11));
            data.Add(new SingleValue("CAN_Erweiterung1.sDIFF_MOD2_Typ", language, 11));
            data.Add(new SingleValue("CAN_Erweiterung1.sDIFF_MOD3_Typ", language, 11));
            data.Add(new SingleValue("CAN_Erweiterung1.sDIFF_MOD4_Typ", language, 11));
            data.Add(new SingleValue("CAN_Erweiterung1.sSolar_MOD1_Typ", language, 11));
            data.Add(new SingleValue("CAN_Erweiterung1.sSolar_MOD2_Typ", language, 11));
            data.Add(new SingleValue("CAN_Erweiterung1.sSolar_MOD3_Typ", language, 11));
            data.Add(new SingleValue("CAN_Erweiterung1.sSolar_MOD4_Typ", language, 11));
            data.Add(new SingleValue("CAN_Erweiterung1.sSSUE_MOD1_Typ", language, 11));

            //Heizkreismodule?
            if (boilerParameter.KESSELTYP == _KESSELPARAMETER._KESSELTYP.HK_Regelung || boilerParameter.KESSELTYP == _KESSELPARAMETER._KESSELTYP.Maximus)
            {
                if (boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[0].Heizkreis_vorhanden == 1 || boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[1].Heizkreis_vorhanden == 1 || boilerParameter.HEIZKREISPARAMETER.BOILER_ARRAY[0].Boiler_vorhanden == 1 || boilerParameter.HEIZKREISPARAMETER.PUFFER_ARRAY[0].Puffer_vorhanden == 1)
                {
                    data.Add(new SingleValue("Heizkreismodul1_vorhanden", language, 1, 11));
                }
                else
                {
                    data.Add(new SingleValue("Heizkreismodul1_vorhanden", language, 0, 11));
                }

                if (boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[2].Heizkreis_vorhanden == 1 || boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[3].Heizkreis_vorhanden == 1 || boilerParameter.HEIZKREISPARAMETER.BOILER_ARRAY[1].Boiler_vorhanden == 1 || boilerParameter.HEIZKREISPARAMETER.PUFFER_ARRAY[1].Puffer_vorhanden == 1)
                {
                    data.Add(new SingleValue("Heizkreismodul2_vorhanden", language, 1, 11));
                }
                else
                {
                    data.Add(new SingleValue("Heizkreismodul2_vorhanden", language, 0, 11));
                }

                if (boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[4].Heizkreis_vorhanden == 1 || boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[5].Heizkreis_vorhanden == 1 || boilerParameter.HEIZKREISPARAMETER.BOILER_ARRAY[2].Boiler_vorhanden == 1 || boilerParameter.HEIZKREISPARAMETER.PUFFER_ARRAY[2].Puffer_vorhanden == 1)
                {
                    data.Add(new SingleValue("Heizkreismodul3_vorhanden", language, 1, 11));
                }
                else
                {
                    data.Add(new SingleValue("Heizkreismodul3_vorhanden", language, 0, 11));
                }

                if (boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[6].Heizkreis_vorhanden == 1 || boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[7].Heizkreis_vorhanden == 1 || boilerParameter.HEIZKREISPARAMETER.BOILER_ARRAY[3].Boiler_vorhanden == 1 || boilerParameter.HEIZKREISPARAMETER.PUFFER_ARRAY[3].Puffer_vorhanden == 1)
                {
                    data.Add(new SingleValue("Heizkreismodul4_vorhanden", language, 1, 11));
                }
                else
                {
                    data.Add(new SingleValue("Heizkreismodul4_vorhanden", language, 0, 11));
                }
            }
            else
            {
                if (boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[2].Heizkreis_vorhanden == 1 || boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[3].Heizkreis_vorhanden == 1 || boilerParameter.HEIZKREISPARAMETER.BOILER_ARRAY[1].Boiler_vorhanden == 1 || boilerParameter.HEIZKREISPARAMETER.PUFFER_ARRAY[1].Puffer_vorhanden == 1)
                {
                    data.Add(new SingleValue("Heizkreismodul1_vorhanden", language, 1, 11));
                }
                else
                {
                    data.Add(new SingleValue("Heizkreismodul1_vorhanden", language, 0, 11));
                }

                if (boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[4].Heizkreis_vorhanden == 1 || boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[5].Heizkreis_vorhanden == 1 || boilerParameter.HEIZKREISPARAMETER.BOILER_ARRAY[2].Boiler_vorhanden == 1 || boilerParameter.HEIZKREISPARAMETER.PUFFER_ARRAY[2].Puffer_vorhanden == 1)
                {
                    data.Add(new SingleValue("Heizkreismodul2_vorhanden", language, 1, 11));
                }
                else
                {
                    data.Add(new SingleValue("Heizkreismodul2_vorhanden", language, 0, 11));
                }

                if (boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[6].Heizkreis_vorhanden == 1 || boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[7].Heizkreis_vorhanden == 1 || boilerParameter.HEIZKREISPARAMETER.BOILER_ARRAY[3].Boiler_vorhanden == 1 || boilerParameter.HEIZKREISPARAMETER.PUFFER_ARRAY[3].Puffer_vorhanden == 1)
                {
                    data.Add(new SingleValue("Heizkreismodul3_vorhanden", language, 1, 11));
                }
                else
                {
                    data.Add(new SingleValue("Heizkreismodul3_vorhanden", language, 0, 11));
                }
                data.Add(new SingleValue("Heizkreismodul4_vorhanden", language, 0, 11));
            }

            for (int i = 0; i < 4; i++)
            {
                data.Add(new SingleValue("FWM_vorhanden_" + (i + 1), language, boilerParameter.HEIZKREISPARAMETER.FWM_ARRAY[i].FWM_vorhanden, 11));
            }
            for (int i = 0; i < 4; i++)
            {
                data.Add(new SingleValue("Solarkreis_vorhanden_" + (i + 1), language, boilerParameter.HEIZKREISPARAMETER._SOLAR_ARRAY[i].Solarkreis_vorhanden, 11));
            }
            data.Add(new SingleValue("Umschalteinheit_EinAus", language, boilerParameter.SAUGAUSTRAGUNG.Umschalteinheit, 11));
            for (int i = 0; i < 4; i++)
            {
                data.Add(new SingleValue("Differenzregelkreis_vorhanden_" + (i + 1), language, boilerParameter._DIFFERENZREGLER_ARRAY[i].Differenzregelkreis_vorhanden, 11));
            }
            data.Add(new SingleValue("Raumluft_Modul_vorhanden", language, boilerParameter.BRENNER.Raumluft_Modul_vorhanden, 11));


            return data;
        }

        #endregion

        #region Maximus

        static public IList<SingleValue> serviceMenuMaximusRLA()
        {

            IList<SingleValue> data = new List<SingleValue>();

            //RLA - allgemeine Einstellungen
            data.Add(new SingleValue("KBA", language, maximusParameter.ALLGEMEINE_EINSTELLUNGEN.KBA, 0));
            data.Add(new SingleValue("Ausschalttemp_RLA_Differenz", language, boilerParameter.RLA.udRLAAusschalttemperatur, 0));
            data.Add(new SingleValue("udRLAStartdifferenz", language, boilerParameter.RLA.udRLAStartdifferenz, 0));
            data.Add(new SingleValue("udRLADiff_KT_RLF_PU", language, boilerParameter.RLA.udRLADiff_KT_RLF_PU, 0));
            data.Add(new SingleValue("udRLADiff_KT_PO", language, boilerParameter.RLA.udRLADiff_KT_PO, 0));
            data.Add(new SingleValue("udRLA_Hysterese", language, boilerParameter.RLA.udRLA_Hysterese, 0));
            data.Add(new SingleValue("RLA_Nachlaufzeit", language, boilerParameter.RLA_MISCHER.RLA_Nachlaufzeit, 0));
            data.Add(new SingleValue("PufferpumpeTyp", language, boilerParameter.HEIZKREISPARAMETER._HeizkreisKesselregler.PufferpumpeTyp, 0));
           

            //RLA - Mixer(X13)
            data.Add(new SingleValue("min_KT_Pellets", language, boilerParameter.RLA_MISCHER.min_KT_Pellets, 1));
            data.Add(new SingleValue("uRLA_Solltemperatur_Min", language, boilerParameter.RLA.uRLA_Solltemperatur_Min, 1));
            data.Add(new SingleValue("uRLA_Solltemperatur", language, boilerParameter.RLA.uRLA_Solltemperatur, 1)); //Rücklauf Solltemp. Max
            data.Add(new SingleValue("uminPause", language, boilerParameter.RLA.uminPause, 1));
            data.Add(new SingleValue("umaxPause", language, boilerParameter.RLA.umaxPause, 1));
            data.Add(new SingleValue("uMischerlaufzeit", language, boilerParameter.RLA.uMischerlaufzeit, 1));
            data.Add(new SingleValue("uPBereich", language, boilerParameter.RLA.uPBereich, 1));
            data.Add(new SingleValue("udPBereich_Plus", language, boilerParameter.RLA_MISCHER.udPBereich_Plus, 1));
            data.Add(new SingleValue("uPulsdauer", language, boilerParameter.RLA.uPulsdauer, 1));
            data.Add(new SingleValue("uiPumpe_ohne_Puffer", language, boilerParameter.RLA.uiPumpe_ohne_Puffer, 1));
            data.Add(new SingleValue("udRLAAusschalttemperatur", language, boilerParameter.RLA.udRLAAusschalttemperatur, 1));
            data.Add(new SingleValue("Mischerfunktion", language, boilerParameter.RLA.Mischerfunktion, 1));
            data.Add(new SingleValue("PufferpumpeTyp", language, boilerParameter.HEIZKREISPARAMETER._HeizkreisKesselregler.PufferpumpeTyp, 1));

            return data;
        }
        static public IList<SingleValue> serviceMenuMaximusPZ()
        {
            IList<SingleValue> data = new List<SingleValue>();
            if (maximusParameter.ALLGEMEINE_EINSTELLUNGEN.KBA.ToString() == "_BA_PELLETS")
            {
                data.Add(new SingleValue("udESZKurzerErsterEinschubimpuls", language, boilerParameter._EINSCHUB_ZUENDPHASE.udESZKurzerErsterEinschubimpuls, 0));
                data.Add(new SingleValue("udESZMaxEinschubimpuls", language, boilerParameter._EINSCHUB_ZUENDPHASE.udESZMaxEinschubimpuls, 0));
                //data.Add(new SingleValue("udMinEinschubimpuls", language, boilerParameter._EINSCHUB_ZUENDPHASE.udMinEinschubimpuls, 0));
            }
            else
            {
                data.Add(new SingleValue("udESZKurzerErsterEinschubimpuls", language, maximusParameter._EINSCHUB_ZUENDPHASE.udESZKurzerErsterEinschubimpuls, 0));
                data.Add(new SingleValue("udESZMaxEinschubimpuls", language, maximusParameter._EINSCHUB_ZUENDPHASE.udESZMaxEinschubimpuls, 0));
                //data.Add(new SingleValue("udMinEinschubimpuls", language, maximusParameter._EINSCHUB_ZUENDPHASE.udMinEinschubimpuls, 0));
            }   

            return data;
        }

        static public IList<SingleValue> serviceMenuMaximusPZF()
        {
            IList<SingleValue> data = new List<SingleValue>();
            if (maximusParameter.ALLGEMEINE_EINSTELLUNGEN.KBA.ToString() == "_BA_PELLETS")
            {
                data.Add(new SingleValue("udESZKurzerErsterEinschubimpuls", language, boilerParameter._EINSCHUB_ZUENDPHASE.udESZKurzerErsterEinschubimpuls, 0));    //0 Einschub
                data.Add(new SingleValue("udESZMaxEinschubimpuls", language, boilerParameter._EINSCHUB_ZUENDPHASE.udESZMaxEinschubimpuls, 0));
                data.Add(new SingleValue("udMinPause", language, boilerParameter._EINSCHUB_ZUENDPHASE.udMinPause, 0));
                data.Add(new SingleValue("udMaxPause", language, boilerParameter._EINSCHUB_ZUENDPHASE.udMaxPause, 0));
                data.Add(new SingleValue("udESZRauchgasanstieg", language, boilerParameter._EINSCHUB_ZUENDPHASE.udESZRauchgasanstieg, 0));
                data.Add(new SingleValue("udRGTWarmStart", language, boilerParameter.KESSELVERWALTUNG.udRGTWarmStart, 0));
                data.Add(new SingleValue("LuftzahlGrenze", language, boilerParameter._ZUENDUNG.LuftzahlGrenze, 0));
                data.Add(new SingleValue("udPEAnheizdrehzahl", language, boilerParameter._SAUGZUGGEBLAESE_PELLETS.udPEAnheizdrehzahl, 0));
                data.Add(new SingleValue("unPEStartLuefterdrehzahl", language, boilerParameter._SAUGZUGGEBLAESE_PELLETS.unPEStartLuefterdrehzahl, 0));
               
                data.Add(new SingleValue("udMaxZuendzeit", language, boilerParameter._ZUENDUNG.udMaxZuendzeit, 1));    //1 Zündung
                data.Add(new SingleValue("udErsterZuendimpuls", language, boilerParameter._ZUENDUNG.udErsterZuendimpuls, 1));
                data.Add(new SingleValue("udZuendimpuls", language, boilerParameter._ZUENDUNG.udZuendimpuls, 1));
                data.Add(new SingleValue("udZuendpause", language, boilerParameter._ZUENDUNG.udZuendpause, 1));
                data.Add(new SingleValue("udLambdaZuendimpulsEin", language, boilerParameter._ZUENDUNG.udLambdaZuendimpulsEin, 1));
                data.Add(new SingleValue("udLambdaZuendimpulsAus", language, boilerParameter._ZUENDUNG.udLambdaZuendimpulsAus, 1));
                data.Add(new SingleValue("GeblaeseNachlauf", language, boilerParameter._ZUENDUNG.GeblaeseNachlauf, 1));
            }
            else
            {
                data.Add(new SingleValue("udESZKurzerErsterEinschubimpuls", language, maximusParameter._EINSCHUB_ZUENDPHASE.udESZKurzerErsterEinschubimpuls, 0));    //0 Einschub
                data.Add(new SingleValue("udESZMaxEinschubimpuls", language, maximusParameter._EINSCHUB_ZUENDPHASE.udESZMaxEinschubimpuls, 0));
                data.Add(new SingleValue("udMinPause", language, maximusParameter._EINSCHUB_ZUENDPHASE.udMinPause, 0));
                data.Add(new SingleValue("udMaxPause", language, maximusParameter._EINSCHUB_ZUENDPHASE.udMaxPause, 0));
                data.Add(new SingleValue("udESZRauchgasanstieg", language, maximusParameter._EINSCHUB_ZUENDPHASE.udESZRauchgasanstieg, 0));
                data.Add(new SingleValue("AGTWarmStart", language, maximusParameter.ALLGEMEINE_EINSTELLUNGEN.AGTWarmStart, 0));
                data.Add(new SingleValue("LuftzahlGrenze", language, maximusParameter._ZUENDUNG.LuftzahlGrenze, 0));
                data.Add(new SingleValue("udPEAnheizdrehzahl", language, maximusParameter._SAUGZUGGEBLAESE_HACKGUT.udPEAnheizdrehzahl, 0));
                data.Add(new SingleValue("unPEStartLuefterdrehzahl", language, maximusParameter._SAUGZUGGEBLAESE_HACKGUT.unPEStartLuefterdrehzahl, 0));

                data.Add(new SingleValue("udMaxZuendzeit", language, boilerParameter._ZUENDUNG.udMaxZuendzeit, 1));    //1 Zündung
                data.Add(new SingleValue("udErsterZuendimpuls", language, boilerParameter._ZUENDUNG.udErsterZuendimpuls, 1));
                data.Add(new SingleValue("udZuendimpuls", language, boilerParameter._ZUENDUNG.udZuendimpuls, 1));
                data.Add(new SingleValue("udZuendpause", language, boilerParameter._ZUENDUNG.udZuendpause, 1));
                data.Add(new SingleValue("udLambdaZuendimpulsEin", language, boilerParameter._ZUENDUNG.udLambdaZuendimpulsEin, 1));
                data.Add(new SingleValue("udLambdaZuendimpulsAus", language, boilerParameter._ZUENDUNG.udLambdaZuendimpulsAus, 1));
                data.Add(new SingleValue("GeblaeseNachlauf", language, boilerParameter._ZUENDUNG.GeblaeseNachlauf, 1));

               /* data.Add(new SingleValue("udMaxZuendzeit", language, maximusParameter._ZUENDUNG.udMaxZuendzeit, 1));    //1 Zündung
                data.Add(new SingleValue("udErsterZuendimpuls", language, maximusParameter._ZUENDUNG.udErsterZuendimpuls, 1));
                data.Add(new SingleValue("udZuendimpuls", language, maximusParameter._ZUENDUNG.udZuendimpuls, 1));
                data.Add(new SingleValue("udZuendpause", language, maximusParameter._ZUENDUNG.udZuendpause, 1));
                data.Add(new SingleValue("udLambdaZuendimpulsEin", language, maximusParameter._ZUENDUNG.udLambdaZuendimpulsEin, 1));
                data.Add(new SingleValue("udLambdaZuendimpulsAus", language, maximusParameter._ZUENDUNG.udLambdaZuendimpulsAus, 1));
                data.Add(new SingleValue("GeblaeseNachlauf", language, maximusParameter._ZUENDUNG.GeblaeseNachlauf, 1));*/
            }
            return data;
        }

        static public IList<SingleValue> serviceMenuMaximusPB()
        {

            IList<SingleValue> data = new List<SingleValue>();
            if (maximusParameter.ALLGEMEINE_EINSTELLUNGEN.KBA.ToString() == "_BA_PELLETS")
            {
                data.Add(new SingleValue("udESEinschubimpuls100", language, boilerParameter._EINSCHUB_BETRIEB.udESEinschubimpuls100, 0));    //0 Pellets Betrieb
                // data.Add(new SingleValue("udESPause", language, boilerParameter._EINSCHUB_BETRIEB.udESPause, 0));
            }
            else
            {
                data.Add(new SingleValue("udESEinschubimpuls100", language, maximusParameter._EINSCHUB_BETRIEB.udESEinschubimpuls100, 0));    //0 Pellets Betrieb
            }

            // StockerWerte[0,10,..]
            for (int i = 0; i <= 100; i += 10)
            {
                data.Add(new SingleValue("StockerWerte_" + i.ToString(), language, boilerParameter.StockerWerte[i], 0));
            }
            
           
            return data;
        }

        static public IList<SingleValue> serviceMenuMaximusPBF()
        {
            IList<SingleValue> data = new List<SingleValue>();

            if (maximusParameter.ALLGEMEINE_EINSTELLUNGEN.KBA.ToString() == "_BA_PELLETS") {
                data.Add(new SingleValue("udESLambdaSollEinschub", language, boilerParameter._EINSCHUB_BETRIEB.udESLambdaSollEinschub, 0));    //0 Einschub Betrieb
                data.Add(new SingleValue("udESLbd_Tot_Bereich", language, boilerParameter._EINSCHUB_BETRIEB.udESLbd_Tot_Bereich, 0));
                data.Add(new SingleValue("udESLambdaOG", language, boilerParameter._EINSCHUB_BETRIEB.udESLambdaOG, 0));
                data.Add(new SingleValue("udESLambdaUG", language, boilerParameter._EINSCHUB_BETRIEB.udESLambdaUG, 0));
                data.Add(new SingleValue("udESMaxImpulsverlaengerung", language, boilerParameter._EINSCHUB_BETRIEB.udESMaxImpulsverlaengerung, 0));
                data.Add(new SingleValue("udESMaxImpulsverkuerzung", language, boilerParameter._EINSCHUB_BETRIEB.udESMaxImpulsverkuerzung, 0));
                data.Add(new SingleValue("udESReduzierimpuls", language, boilerParameter._EINSCHUB_BETRIEB.udESReduzierimpuls, 0));
                data.Add(new SingleValue("LambdaoffsetTeillast", language, boilerParameter._EINSCHUB_BETRIEB.LambdaoffsetTeillast, 0));
                data.Add(new SingleValue("SLK_ES_Verkuerzung", language, maximusParameter._LUFTKLAPPEN_PE.SLK_ES_Verkuerzung, 0));
               

                data.Add(new SingleValue("udESEinschubimpuls100", language, boilerParameter._EINSCHUB_BETRIEB.udESEinschubimpuls100, 1));    //1 Einschub Betrieb Optimierung
                data.Add(new SingleValue("udESMitterwertzeit", language, boilerParameter._EINSCHUB_BETRIEB.udESMitterwertzeit, 1));
                data.Add(new SingleValue("udESzulLDZAenderung", language, boilerParameter._EINSCHUB_BETRIEB.udESzulLDZAenderung, 1));
                data.Add(new SingleValue("udESFaktorImpAenderungLDZBereichMX", language, boilerParameter._EINSCHUB_BETRIEB.udESFaktorImpAenderungLDZBereich, 1));
                data.Add(new SingleValue("udESMinEinschubimpulsMx", language, boilerParameter._EINSCHUB_BETRIEB.udESMinEinschubimpuls, 1));
                data.Add(new SingleValue("udESMaxEinschubimpulsMx", language, boilerParameter._EINSCHUB_BETRIEB.udESMaxEinschubimpuls, 1));
                data.Add(new SingleValue("udESLbdMittelwert", language, boilerParameter._EINSCHUB_BETRIEB.udESLbdMittelwert, 1));
                data.Add(new SingleValue("MaxAnzahlBlockaden", language, boilerParameter._EINSCHUB_BETRIEB.MaxAnzahlBlockaden, 1));
                data.Add(new SingleValue("udES_LDZMittelwert", language, boilerParameter._EINSCHUB_BETRIEB.udES_LDZMittelwert, 1));
               

                data.Add(new SingleValue("udPEMin_Luefterdrehzahl", language, boilerParameter._SAUGZUGGEBLAESE_PELLETS.udPEMin_Luefterdrehzahl, 2));    //2 Saugzuggebläse
                data.Add(new SingleValue("udPEMax_Luefterdrehzahl", language, boilerParameter._SAUGZUGGEBLAESE_PELLETS.udPEMax_Luefterdrehzahl, 2));    
                data.Add(new SingleValue("udKesselsolltemperatur", language, boilerParameter.KESSELVERWALTUNG.udKesselsolltemperatur, 2));
                data.Add(new SingleValue("udPEKesseltemperaturPBereich", language, boilerParameter._SAUGZUGGEBLAESE_PELLETS.udPEKesseltemperaturPBereich, 2));
                data.Add(new SingleValue("udRGTSoll", language, boilerParameter.KESSELVERWALTUNG.udRGTSoll, 2));
                data.Add(new SingleValue("udMinRGT", language, boilerParameter.KESSELVERWALTUNG.udMinRGT, 2));
                data.Add(new SingleValue("udMaxRGT", language, boilerParameter.KESSELVERWALTUNG.udMaxRGT, 2));
                data.Add(new SingleValue("udPEMinLuefterrampePelletsbetrieb", language, boilerParameter._SAUGZUGGEBLAESE_PELLETS.udPEMinLuefterrampePelletsbetrieb, 2));
                data.Add(new SingleValue("udPEMaxLuefterrampePelletsbetrieb", language, boilerParameter._SAUGZUGGEBLAESE_PELLETS.udPEMaxLuefterrampePelletsbetrieb, 2));
                data.Add(new SingleValue("udPEMinLuefterrampePelletsbetrieb_schnell", language, boilerParameter._SAUGZUGGEBLAESE_PELLETS.udPEMinLuefterrampePelletsbetrieb_schnell, 2));
                data.Add(new SingleValue("udPEMaxLuefterrampePelletsbetrieb_schnell", language, boilerParameter._SAUGZUGGEBLAESE_PELLETS.udPEMaxLuefterrampePelletsbetrieb_schnell, 2));
                data.Add(new SingleValue("udPEAusbrenndrehzahl", language, boilerParameter._SAUGZUGGEBLAESE_PELLETS.udPEAusbrenndrehzahl, 2));
                data.Add(new SingleValue("Luefterdrehzahl_WTR", language, maximusParameter.ASCHEAUSTRAGUNG.Luefterdrehzahl_WTR, 2));
               
            }
            else
            {
                data.Add(new SingleValue("udESLambdaSollEinschub", language, maximusParameter._EINSCHUB_BETRIEB.udESLambdaSollEinschub, 0));    //0 Einschub Betrieb
                data.Add(new SingleValue("udESLbd_Tot_Bereich", language, maximusParameter._EINSCHUB_BETRIEB.udESLbd_Tot_Bereich, 0));
                data.Add(new SingleValue("udESLambdaOG", language, maximusParameter._EINSCHUB_BETRIEB.udESLambdaOG, 0));
                data.Add(new SingleValue("udESLambdaUG", language, maximusParameter._EINSCHUB_BETRIEB.udESLambdaUG, 0));
                data.Add(new SingleValue("udESMaxImpulsverlaengerung", language, maximusParameter._EINSCHUB_BETRIEB.udESMaxImpulsverlaengerung, 0));
                data.Add(new SingleValue("udESMaxImpulsverkuerzung", language, maximusParameter._EINSCHUB_BETRIEB.udESMaxImpulsverkuerzung, 0));
                data.Add(new SingleValue("udESReduzierimpuls", language, maximusParameter._EINSCHUB_BETRIEB.udESReduzierimpuls, 0));
                data.Add(new SingleValue("LambdaoffsetTeillast", language, maximusParameter._EINSCHUB_BETRIEB.LambdaoffsetTeillast, 0));
                data.Add(new SingleValue("SLK_ES_Verkuerzung", language, maximusParameter._LUFTKLAPPEN_HG.SLK_ES_Verkuerzung, 0));

                data.Add(new SingleValue("udESEinschubimpuls100", language, maximusParameter._EINSCHUB_BETRIEB.udESEinschubimpuls100, 1));    //1 Einschub Betrieb Optimierung
                data.Add(new SingleValue("udESMitterwertzeit", language, maximusParameter._EINSCHUB_BETRIEB.udESMitterwertzeit, 1));
                data.Add(new SingleValue("udESzulLDZAenderung", language, maximusParameter._EINSCHUB_BETRIEB.udESzulLDZAenderung, 1));
                data.Add(new SingleValue("udESFaktorImpAenderungLDZBereich", language, maximusParameter._EINSCHUB_BETRIEB.udESFaktorImpAenderungLDZBereich, 1));
                data.Add(new SingleValue("udESMinEinschubimpulsMx", language, maximusParameter._EINSCHUB_BETRIEB.udESMinEinschubimpuls, 1));
                data.Add(new SingleValue("udESMaxEinschubimpulsMx", language, maximusParameter._EINSCHUB_BETRIEB.udESMaxEinschubimpuls, 1));
                data.Add(new SingleValue("udESLbdMittelwert", language, maximusParameter._EINSCHUB_BETRIEB.udESLbdMittelwert, 1));
                data.Add(new SingleValue("MaxAnzahlBlockaden", language, maximusParameter._EINSCHUB_BETRIEB.MaxAnzahlBlockaden, 1));
                data.Add(new SingleValue("udES_LDZMittelwert", language, maximusParameter._EINSCHUB_BETRIEB.udES_LDZMittelwert, 1));
                
                data.Add(new SingleValue("udPEMin_Luefterdrehzahl", language, maximusParameter._SAUGZUGGEBLAESE_HACKGUT.udPEMin_Luefterdrehzahl, 2));    //2 Saugzuggebläse
                data.Add(new SingleValue("udPEMax_Luefterdrehzahl", language, maximusParameter._SAUGZUGGEBLAESE_HACKGUT.udPEMax_Luefterdrehzahl, 2));
                data.Add(new SingleValue("udKesselsolltemperatur", language, maximusParameter.ALLGEMEINE_EINSTELLUNGEN.Kesselsolltemperatur, 2));
                data.Add(new SingleValue("udPEKesseltemperaturPBereich", language, maximusParameter._SAUGZUGGEBLAESE_HACKGUT.udPEKesseltemperaturPBereich, 2));
                data.Add(new SingleValue("SollAGT", language, maximusParameter.ALLGEMEINE_EINSTELLUNGEN.SollAGT, 2));
                data.Add(new SingleValue("udMinRGT", language, maximusParameter.ALLGEMEINE_EINSTELLUNGEN.MinAGT, 2));
                data.Add(new SingleValue("udMaxRGT", language, maximusParameter.ALLGEMEINE_EINSTELLUNGEN.MaxAGT, 2));
                data.Add(new SingleValue("udPEMinLuefterrampePelletsbetrieb", language, maximusParameter._SAUGZUGGEBLAESE_HACKGUT.udPEMinLuefterrampePelletsbetrieb, 2));
                data.Add(new SingleValue("udPEMaxLuefterrampePelletsbetrieb", language, maximusParameter._SAUGZUGGEBLAESE_HACKGUT.udPEMaxLuefterrampePelletsbetrieb, 2));
                data.Add(new SingleValue("udPEMinLuefterrampePelletsbetrieb_schnell", language, maximusParameter._SAUGZUGGEBLAESE_HACKGUT.udPEMinLuefterrampePelletsbetrieb_schnell, 2));
                data.Add(new SingleValue("udPEMaxLuefterrampePelletsbetrieb_schnell", language, maximusParameter._SAUGZUGGEBLAESE_HACKGUT.udPEMaxLuefterrampePelletsbetrieb_schnell, 2));
                data.Add(new SingleValue("udPEAusbrenndrehzahl", language, maximusParameter._SAUGZUGGEBLAESE_HACKGUT.udPEAusbrenndrehzahl, 2));
                data.Add(new SingleValue("Luefterdrehzahl_WTR", language, maximusParameter.ASCHEAUSTRAGUNG.Luefterdrehzahl_WTR, 2));
            }
            
            return data;
        }

        static public IList<SingleValue> serviceMenuMaximusRTG()
        {
            IList<SingleValue> data = new List<SingleValue>();

            if (maximusParameter.ALLGEMEINE_EINSTELLUNGEN.KBA.ToString() == "_BA_PELLETS")
            {
                data.Add(new SingleValue("Sensorueberwachung", language, maximusParameter._RAUMAUSTRAGUNG_PE.Sensorueberwachung, 0));    //0 Raumaustragung 1
                data.Add(new SingleValue("Austragungsimpuls", language, maximusParameter._RAUMAUSTRAGUNG_PE.Austragungsimpuls, 0));
                data.Add(new SingleValue("Freq_Offset", language, maximusParameter._RAUMAUSTRAGUNG_PE.Freq_Offset, 0));
                data.Add(new SingleValue("Sensor_Einschublaufzeit", language, maximusParameter._RAUMAUSTRAGUNG_PE.Sensor_Einschublaufzeit, 0));
                data.Add(new SingleValue("Sensor_maxImpuls", language, maximusParameter._RAUMAUSTRAGUNG_PE.Sensor_maxEinschublaufzeit, 0));
                data.Add(new SingleValue("Sensor_maxEinschublaufzeit", language, maximusParameter._RAUMAUSTRAGUNG_PE.Sensor_maxImpuls, 0));
                data.Add(new SingleValue("Stromueberwachung_1", language, maximusParameter.RAUMAUSTRAGUNG.Stromueberwachung_1, 0));
                data.Add(new SingleValue("maxStrom_1", language, maximusParameter.RAUMAUSTRAGUNG.maxStrom_1, 0));
                data.Add(new SingleValue("maxZeit_Ueberstrom_1", language, maximusParameter.RAUMAUSTRAGUNG.maxZeit_Ueberstrom_1, 0));

                data.Add(new SingleValue("Sensorueberwachung_2", language, maximusParameter._RAUMAUSTRAGUNG_PE.Sensorueberwachung_2, 1));    //1 Raumaustragung 2
                data.Add(new SingleValue("Einschaltverzoegerung_Austragung_2", language, maximusParameter._RAUMAUSTRAGUNG_PE.Einschaltverzoegerung_Austragung_2, 1));
                data.Add(new SingleValue("maxLaufzeit_Austragung_2", language, maximusParameter._RAUMAUSTRAGUNG_PE.maxLaufzeit_Austragung_2, 1));
                data.Add(new SingleValue("Sensor_Einschublaufzeit_2", language, maximusParameter._RAUMAUSTRAGUNG_PE.Sensor_Einschublaufzeit_2, 1));
                data.Add(new SingleValue("Sensor_maxEinschublaufzeit_2", language, maximusParameter._RAUMAUSTRAGUNG_PE.Sensor_maxImpuls_2, 1));
                data.Add(new SingleValue("Sensor_maxImpuls_2", language, maximusParameter._RAUMAUSTRAGUNG_PE.Sensor_maxEinschublaufzeit_2, 1));
                data.Add(new SingleValue("Stromueberwachung_2", language, maximusParameter.RAUMAUSTRAGUNG.Stromueberwachung_2, 1));
                data.Add(new SingleValue("maxStrom_2", language, maximusParameter.RAUMAUSTRAGUNG.maxStrom_2, 1));
                data.Add(new SingleValue("maxZeit_Ueberstrom_2", language, maximusParameter.RAUMAUSTRAGUNG.maxZeit_Ueberstrom_2, 1));
            }
            else
            {
                data.Add(new SingleValue("Sensorueberwachung", language, maximusParameter._RAUMAUSTRAGUNG_HG.Sensorueberwachung, 0));    //0 Raumaustragung 1
                data.Add(new SingleValue("Austragungsimpuls", language, maximusParameter._RAUMAUSTRAGUNG_HG.Austragungsimpuls, 0));
                data.Add(new SingleValue("Freq_Offset", language, maximusParameter._RAUMAUSTRAGUNG_HG.Freq_Offset, 0));
                data.Add(new SingleValue("Sensor_Einschublaufzeit", language, maximusParameter._RAUMAUSTRAGUNG_HG.Sensor_Einschublaufzeit, 0));
                data.Add(new SingleValue("Sensor_maxEinschublaufzeit", language, maximusParameter._RAUMAUSTRAGUNG_HG.Sensor_maxEinschublaufzeit, 0));
                data.Add(new SingleValue("Sensor_maxImpuls", language, maximusParameter._RAUMAUSTRAGUNG_HG.Sensor_maxImpuls, 0));
                data.Add(new SingleValue("Stromueberwachung_1", language, maximusParameter.RAUMAUSTRAGUNG.Stromueberwachung_1, 0));
                data.Add(new SingleValue("maxStrom_1", language, maximusParameter.RAUMAUSTRAGUNG.maxStrom_1, 0));
                data.Add(new SingleValue("maxZeit_Ueberstrom_1", language, maximusParameter.RAUMAUSTRAGUNG.maxZeit_Ueberstrom_1, 0));

                data.Add(new SingleValue("Sensorueberwachung_2", language, maximusParameter._RAUMAUSTRAGUNG_HG.Sensorueberwachung_2, 1));    //1 Raumaustragung 2
                data.Add(new SingleValue("Einschaltverzoegerung_Austragung_2", language, maximusParameter._RAUMAUSTRAGUNG_HG.Einschaltverzoegerung_Austragung_2, 1));
                data.Add(new SingleValue("maxLaufzeit_Austragung_2", language, maximusParameter._RAUMAUSTRAGUNG_HG.maxLaufzeit_Austragung_2, 1));
                data.Add(new SingleValue("Sensor_Einschublaufzeit_2", language, maximusParameter._RAUMAUSTRAGUNG_HG.Sensor_Einschublaufzeit_2, 1));
                data.Add(new SingleValue("Sensor_maxEinschublaufzeit_2", language, maximusParameter._RAUMAUSTRAGUNG_HG.Sensor_maxEinschublaufzeit_2, 1));
                data.Add(new SingleValue("Sensor_maxImpuls_2", language, maximusParameter._RAUMAUSTRAGUNG_HG.Sensor_maxEinschublaufzeit_2, 1));
                data.Add(new SingleValue("Stromueberwachung_2", language, maximusParameter.RAUMAUSTRAGUNG.Stromueberwachung_2, 1));
                data.Add(new SingleValue("maxStrom_2", language, maximusParameter.RAUMAUSTRAGUNG.maxStrom_2, 1));
                data.Add(new SingleValue("maxZeit_Ueberstrom_2", language, maximusParameter.RAUMAUSTRAGUNG.maxZeit_Ueberstrom_2, 1));
            }

            data.Add(new SingleValue("Sensorueberwachung_3", language, maximusParameter.RAUMAUSTRAGUNG_3.Sensorueberwachung_3, 2));    //2 Raumaustragung 3
            data.Add(new SingleValue("Einschaltverzoegerung_Austragung_3", language, maximusParameter.RAUMAUSTRAGUNG_3.Einschaltverzoegerung_Austragung_3, 2));
            data.Add(new SingleValue("maxLaufzeit_Austragung_3", language, maximusParameter.RAUMAUSTRAGUNG_3.maxLaufzeit_Austragung_3, 2));
            data.Add(new SingleValue("Sensor_Einschublaufzeit_3", language, maximusParameter.RAUMAUSTRAGUNG_3.Sensor_Einschublaufzeit_3, 2));
            data.Add(new SingleValue("Sensor_maxEinschublaufzeit_3", language, maximusParameter.RAUMAUSTRAGUNG_3.Sensor_Ausschaltverz_3, 2));
            data.Add(new SingleValue("Sensor_Ausschaltverz_3", language, maximusParameter.RAUMAUSTRAGUNG_3.Sensor_maxEinschublaufzeit_3, 2));
            data.Add(new SingleValue("Stromueberwachung_3", language, maximusParameter.RAUMAUSTRAGUNG.Stromueberwachung_3, 2));
            data.Add(new SingleValue("maxStrom_3", language, maximusParameter.RAUMAUSTRAGUNG.maxStrom_3, 2));
            data.Add(new SingleValue("maxZeit_Ueberstrom_3", language, maximusParameter.RAUMAUSTRAGUNG.maxZeit_Ueberstrom_3, 2));

            return data;
        }

        static public IList<SingleValue> serviceMenuMaximusSA()
        {
            IList<SingleValue> data = new List<SingleValue>();

            data.Add(new SingleValue("udMaxSauglaufzeit", language, boilerParameter.SAUGAUSTRAGUNG.udMaxSauglaufzeit, 0));    //0 Saugaustragung
           
            return data;
        }

        static public IList<SingleValue> serviceMenuMaximusSAF()
        {

            IList<SingleValue> data = new List<SingleValue>();
            
            data.Add(new SingleValue("udMaxSauglaufzeit", language, boilerParameter.SAUGAUSTRAGUNG.udMaxSauglaufzeit, 0));    //0 Saugaustragung
            data.Add(new SingleValue("udMaxEinschublaufzeit", language, boilerParameter.SAUGAUSTRAGUNG.udMaxEinschublaufzeit, 0));
            data.Add(new SingleValue("SGA_Abschalttemperatur", language, boilerParameter.SAUGAUSTRAGUNG.SGA_Abschalttemperatur, 0));

            data.Add(new SingleValue("udEinschaltverzoegerung", language, boilerParameter.SAUGAUSTRAGUNG.udEinschaltverzoegerung, 1));    //1 Raumaustragung
            data.Add(new SingleValue("udAusschaltverzoegerung", language, boilerParameter.SAUGAUSTRAGUNG.udAusschaltverzoegerung, 1));
            data.Add(new SingleValue("udAustragungImpuls_Min", language, boilerParameter.SAUGAUSTRAGUNG.udAustragungImpuls_Min, 1));
            data.Add(new SingleValue("udAustragungImpuls_Sek", language, boilerParameter.SAUGAUSTRAGUNG.udAustragungImpuls_Sek, 1));
            data.Add(new SingleValue("udAustragungPause_Sek", language, boilerParameter.SAUGAUSTRAGUNG.udAustragungPause_Sek, 1));
            data.Add(new SingleValue("udSensorUeberwachungRA", language, boilerParameter.SAUGAUSTRAGUNG.udSensorUeberwachungRA, 1));
            

            return data;
        }

        static public IList<SingleValue> serviceMenuMaximusLK()
        {
            IList<SingleValue> data = new List<SingleValue>();
            if (maximusParameter.ALLGEMEINE_EINSTELLUNGEN.KBA.ToString() == "_BA_PELLETS")
            {
                data.Add(new SingleValue("udPEMin_Luefterdrehzahl", language, boilerParameter._SAUGZUGGEBLAESE_PELLETS.udPEMin_Luefterdrehzahl, 0));    //0 Luftklappen
                data.Add(new SingleValue("udPEMax_Luefterdrehzahl", language, boilerParameter._SAUGZUGGEBLAESE_PELLETS.udPEMax_Luefterdrehzahl, 0));
                data.Add(new SingleValue("PLK_minPosition", language, maximusParameter._LUFTKLAPPEN_PE.PLK_minPosition, 0));
                data.Add(new SingleValue("PLK_maxPosition", language, maximusParameter._LUFTKLAPPEN_PE.PLK_maxPosition, 0));
                data.Add(new SingleValue("SLK_minPosition", language, maximusParameter._LUFTKLAPPEN_PE.SLK_minPosition, 0));
                data.Add(new SingleValue("SLK_maxPosition", language, maximusParameter._LUFTKLAPPEN_PE.SLK_maxPosition, 0));
                data.Add(new SingleValue("SLK_maxPosition", language, maximusParameter._LUFTKLAPPEN_PE.SLK_maxPosition, 0));
                data.Add(new SingleValue("REZI_Sollmin", language, maximusParameter._LUFTKLAPPEN_PE.REZI_Sollmin, 0));
                data.Add(new SingleValue("REZI_Sollmax", language, maximusParameter._LUFTKLAPPEN_PE.REZI_Sollmax, 0));
            }
            else
            {
                data.Add(new SingleValue("udPEMin_Luefterdrehzahl", language, maximusParameter._SAUGZUGGEBLAESE_HACKGUT.udPEMin_Luefterdrehzahl, 0));    //0 Luftklappen
                data.Add(new SingleValue("udPEMax_Luefterdrehzahl", language, maximusParameter._SAUGZUGGEBLAESE_HACKGUT.udPEMax_Luefterdrehzahl, 0));
                data.Add(new SingleValue("PLK_minPosition", language, maximusParameter._LUFTKLAPPEN_HG.PLK_minPosition, 0));
                data.Add(new SingleValue("PLK_maxPosition", language, maximusParameter._LUFTKLAPPEN_HG.PLK_maxPosition, 0));
                data.Add(new SingleValue("SLK_minPosition", language, maximusParameter._LUFTKLAPPEN_HG.SLK_minPosition, 0));
                data.Add(new SingleValue("SLK_maxPosition", language, maximusParameter._LUFTKLAPPEN_HG.SLK_maxPosition, 0));
                data.Add(new SingleValue("SLK_maxPosition", language, maximusParameter._LUFTKLAPPEN_HG.SLK_maxPosition, 0));
                data.Add(new SingleValue("REZI_Sollmin", language, maximusParameter._LUFTKLAPPEN_HG.REZI_Sollmin, 0));
                data.Add(new SingleValue("REZI_Sollmax", language, maximusParameter._LUFTKLAPPEN_HG.REZI_Sollmax, 0));
            }

            return data;
        }

        static public IList<SingleValue> serviceMenuMaximusLKRF()
        {
            IList<SingleValue> data = new List<SingleValue>();
          
            if (maximusParameter.ALLGEMEINE_EINSTELLUNGEN.KBA.ToString() == "_BA_PELLETS")
            {
                data.Add(new SingleValue("REZI_Automatik", language, maximusParameter._LUFTKLAPPEN_PE.REZI_Automatik, 0));    //0 Rezirkulationsluftklappe
                data.Add(new SingleValue("REZI_AGT", language, maximusParameter._LUFTKLAPPEN_PE.REZI_AGT, 0));
                data.Add(new SingleValue("REZI_Sollmin", language, maximusParameter._LUFTKLAPPEN_PE.REZI_Sollmin, 0));
                data.Add(new SingleValue("REZI_Sollmax", language, maximusParameter._LUFTKLAPPEN_PE.REZI_Sollmax, 0));
            }
            else
            {
                data.Add(new SingleValue("REZI_Automatik", language, maximusParameter._LUFTKLAPPEN_HG.REZI_Automatik, 0));    //0 Rezirkulationsluftklappe
                data.Add(new SingleValue("REZI_AGT", language, maximusParameter._LUFTKLAPPEN_HG.REZI_AGT, 0));
                data.Add(new SingleValue("REZI_Sollmin", language, maximusParameter._LUFTKLAPPEN_HG.REZI_Sollmin, 0));
                data.Add(new SingleValue("REZI_Sollmax", language, maximusParameter._LUFTKLAPPEN_HG.REZI_Sollmax, 0));
            }

            return data;
        }
        
        static public IList<SingleValue> serviceMenuMaximusVS()
        {
            IList<SingleValue> data = new List<SingleValue>();

            if (maximusParameter.ALLGEMEINE_EINSTELLUNGEN.KBA.ToString() == "_BA_PELLETS")
            {
                data.Add(new SingleValue("udPEMin_Luefterdrehzahl", language, boilerParameter._SAUGZUGGEBLAESE_PELLETS.udPEMin_Luefterdrehzahl, 0));    //0 Vorschubrost
                data.Add(new SingleValue("udPEMax_Luefterdrehzahl", language, boilerParameter._SAUGZUGGEBLAESE_PELLETS.udPEMax_Luefterdrehzahl, 0));    
                data.Add(new SingleValue("minPause", language, maximusParameter.STUFENROST.minPause, 0));    
                data.Add(new SingleValue("maxPause", language, maximusParameter.STUFENROST.maxPause, 0));    
                data.Add(new SingleValue("Impuls", language, maximusParameter.STUFENROST.Impuls, 0));    
                data.Add(new SingleValue("Stufenrost_Stromueberwachung", language, maximusParameter.ASCHEAUSTRAGUNG.Stufenrost_Stromueberwachung, 0));    
                data.Add(new SingleValue("Stufenrost_maxStrom", language, maximusParameter.ASCHEAUSTRAGUNG.Stufenrost_maxStrom, 0));    
            }
            else
            {
                data.Add(new SingleValue("udPEMin_Luefterdrehzahl", language, maximusParameter._SAUGZUGGEBLAESE_HACKGUT.udPEMin_Luefterdrehzahl, 0));    //0 Vorschubrost
                data.Add(new SingleValue("udPEMax_Luefterdrehzahl", language, maximusParameter._SAUGZUGGEBLAESE_HACKGUT.udPEMax_Luefterdrehzahl, 0));
                data.Add(new SingleValue("minPause", language, maximusParameter.STUFENROST.minPause, 0));
                data.Add(new SingleValue("maxPause", language, maximusParameter.STUFENROST.maxPause, 0));
                data.Add(new SingleValue("Impuls", language, maximusParameter.STUFENROST.Impuls, 0));
                data.Add(new SingleValue("Stufenrost_Stromueberwachung", language, maximusParameter.ASCHEAUSTRAGUNG.Stufenrost_Stromueberwachung, 0));
                data.Add(new SingleValue("Stufenrost_maxStrom", language, maximusParameter.ASCHEAUSTRAGUNG.Stufenrost_maxStrom, 0));
              
            }

            return data;
        }

        static public IList<SingleValue> serviceMenuMaximusSP()
        {
            IList<SingleValue> data = new List<SingleValue>();

            if (maximusParameter.ALLGEMEINE_EINSTELLUNGEN.KBA.ToString() == "_BA_PELLETS")
            {
                data.Add(new SingleValue("udMaximalerStromEinschub", language, boilerParameter._EINSCHUB_ZUENDPHASE.udMaximalerStromEinschub, 0));    //0 Systemparameter
            }
            else
            {
                data.Add(new SingleValue("udMaximalerStromEinschub", language, maximusParameter._EINSCHUB_ZUENDPHASE.udMaximalerStromEinschub, 0));    //0 Systemparameter
            }
            //Modulttyp (k_daten.csv)
            data.Add(new SingleValue("CAN_Erweiterung1.sHK_XL_MOD1_Typ", language, 1));
            data.Add(new SingleValue("CAN_Erweiterung1.sHK_MOD1_Typ", language, 1));
            data.Add(new SingleValue("CAN_Erweiterung1.sHK_MOD2_Typ", language, 1));
            data.Add(new SingleValue("CAN_Erweiterung1.sHK_MOD3_Typ", language, 1));
            data.Add(new SingleValue("CAN_Erweiterung1.sFW_MOD1_Typ", language, 1));
            data.Add(new SingleValue("CAN_Erweiterung1.sFW_MOD2_Typ", language, 1));
            data.Add(new SingleValue("CAN_Erweiterung1.sFW_MOD3_Typ", language, 1));
            data.Add(new SingleValue("CAN_Erweiterung1.sFW_MOD4_Typ", language, 1));
            data.Add(new SingleValue("CAN_Erweiterung1.sDIFF_MOD1_Typ", language, 1));
            data.Add(new SingleValue("CAN_Erweiterung1.sDIFF_MOD2_Typ", language, 1));
            data.Add(new SingleValue("CAN_Erweiterung1.sDIFF_MOD3_Typ", language, 1));
            data.Add(new SingleValue("CAN_Erweiterung1.sDIFF_MOD4_Typ", language, 1));
            data.Add(new SingleValue("CAN_Erweiterung1.sSolar_MOD1_Typ", language, 1));
            data.Add(new SingleValue("CAN_Erweiterung1.sSolar_MOD2_Typ", language, 1));
            data.Add(new SingleValue("CAN_Erweiterung1.sSolar_MOD3_Typ", language, 1));
            data.Add(new SingleValue("CAN_Erweiterung1.sSolar_MOD4_Typ", language, 1));
            data.Add(new SingleValue("CAN_Erweiterung1.sSSUE_MOD1_Typ", language, 1));

            return data;
        }

        static public IList<SingleValue> serviceMenuMaximusSPF()
        {
            IList<SingleValue> data = new List<SingleValue>();

            data.Add(new SingleValue("udESMaxTemperatur", language, boilerParameter.KESSELVERWALTUNG.udESMaxTemperatur, 0));    // Seite 1
            data.Add(new SingleValue("udKesselsollueberhoeung", language, boilerParameter.KESSELVERWALTUNG.udKesselsollueberhoeung, 0));
            data.Add(new SingleValue("udKesseltempPrimaerluftZu", language, boilerParameter.KESSELVERWALTUNG.udKesseltempPrimaerluftZu, 0));
            data.Add(new SingleValue("udLambdaueberwachung", language, boilerParameter.BRENNER.udLambdaueberwachung, 0));
            data.Add(new SingleValue("udZeitLambdaUeberwachung", language, boilerParameter.BRENNER.udZeitLambdaUeberwachung, 0));
            data.Add(new SingleValue("Stromueberwachung", language, maximusParameter.FU_EINSCHUB.Stromueberwachung, 0));
            
            if(maximusParameter.ALLGEMEINE_EINSTELLUNGEN.KBA.ToString() == "_BA_PELLETS")
            {
                data.Add(new SingleValue("udMaximalerStromEinschub", language, boilerParameter._EINSCHUB_ZUENDPHASE.udMaximalerStromEinschub, 0));
            }
            else
            {
                data.Add(new SingleValue("udMaximalerStromEinschub", language, maximusParameter._EINSCHUB_ZUENDPHASE.udMaximalerStromEinschub, 0));
            }

            data.Add(new SingleValue("RauchgasAbfallueberwachung", language, boilerParameter.BRENNER.RauchgasAbfallueberwachung, 1));    // Seite 2
            data.Add(new SingleValue("udRGTAbfall_keine_Verkuerzung", language, boilerParameter.BRENNER.udRGTAbfall_keine_Verkuerzung, 1));
            data.Add(new SingleValue("udRGTAbfall_max_Verkuerzung", language, boilerParameter.BRENNER.udRGTAbfall_max_Verkuerzung, 1));
            data.Add(new SingleValue("udRGTAbfall_Stop", language, boilerParameter.BRENNER.udRGTAbfall_Stop, 1));
            data.Add(new SingleValue("udRGT_maxImpulsverkuerzung", language, boilerParameter.BRENNER.udRGT_maxImpulsverkuerzung, 1));
            data.Add(new SingleValue("udLDZKonstantzeit", language, boilerParameter.BRENNER.udLDZKonstantzeit, 1));
            data.Add(new SingleValue("RauchgasfehlertempUeberw", language, boilerParameter.BRENNER.RauchgasfehlertempUeberw, 1));
            data.Add(new SingleValue("FehlerRGTmin", language, boilerParameter.BRENNER.FehlerRGTmin, 1));
            data.Add(new SingleValue("udFehlerRGTmax", language, boilerParameter.BRENNER.udFehlerRGTmax, 1));
            data.Add(new SingleValue("udFehlerRGTZeitStart", language, boilerParameter.BRENNER.udFehlerRGTZeitStart, 1));
            data.Add(new SingleValue("udFehlerRGTZeitBetrieb", language, boilerParameter.BRENNER.udFehlerRGTZeitBetrieb, 1));

            data.Add(new SingleValue("udDrehzahlrueckfuehrung", language, boilerParameter._SAUGZUGGEBLAESE_PELLETS.udDrehzahlrueckfuehrung, 2));    // Seite 3
            data.Add(new SingleValue("Umschaltung_Regler1.sReglerUmschaltung", language, 0));
            data.Add(new SingleValue("PAnteil", language, boilerParameter.PIDREGLER.KESSEL.PAnteil, 2));
            data.Add(new SingleValue("IAnteil", language, boilerParameter.PIDREGLER.KESSEL.IAnteil, 2));
            data.Add(new SingleValue("DAnteil", language, boilerParameter.PIDREGLER.KESSEL.DAnteil, 2));
            data.Add(new SingleValue("I_Max", language, boilerParameter.PIDREGLER.KESSEL.I_Max, 2));

            data.Add(new SingleValue("AbsMin_Frequenz", language, maximusParameter.FU_EINSCHUB.AbsMin_Frequenz.ToString(), 3));    // Seite 4
            data.Add(new SingleValue("AbsMax_Frequenz", language, maximusParameter.FU_EINSCHUB.AbsMax_Frequenz, 3));
            data.Add(new SingleValue("Hochlaufzeit", language, maximusParameter.FU_EINSCHUB.Hochlaufzeit, 3));
            data.Add(new SingleValue("Tieflaufzeit", language, maximusParameter.FU_EINSCHUB.Tieflaufzeit, 3));
            data.Add(new SingleValue("Motor_Nennleistung", language, maximusParameter.FU_EINSCHUB.Motor_Nennleistung, 3));
            data.Add(new SingleValue("Motor_Nennspannung", language, maximusParameter.FU_EINSCHUB.Motor_Nennspannung, 3));
            data.Add(new SingleValue("Motor_Nennstrom", language, maximusParameter.FU_EINSCHUB.Motor_Nennstrom, 3));
            data.Add(new SingleValue("Motor_Nenndrehzahl", language, maximusParameter.FU_EINSCHUB.Motor_Nenndrehzahl, 3));
            data.Add(new SingleValue("Motor_Nennfrequenz", language, maximusParameter.FU_EINSCHUB.Motor_Nennfrequenz, 3));
            data.Add(new SingleValue("Taktung_Periodendauer", language, maximusParameter.FU_EINSCHUB.Taktung_Periodendauer, 3));
            data.Add(new SingleValue("Motor_Ueberstromschutz", language, maximusParameter.FU_EINSCHUB.Motor_Ueberstromschutz, 3));
            data.Add(new SingleValue("Motor_Ueberstromgrenze", language, maximusParameter.FU_EINSCHUB.Motor_Ueberstromgrenze, 3));
            data.Add(new SingleValue("Umrichter_Ueberlast_Warnung", language, maximusParameter.FU_EINSCHUB.Umrichter_Ueberlast_Warnung, 3));
            data.Add(new SingleValue("Umrichter_Ueberlast_Alarm", language, maximusParameter.FU_EINSCHUB.Umrichter_Ueberlast_Alarm, 3));
            data.Add(new SingleValue("Motor_Ueberlast_Warnung", language, maximusParameter.FU_EINSCHUB.Motor_Ueberlast_Warnung, 3));
            data.Add(new SingleValue("Motor_Ueberlast_Alarm", language, maximusParameter.FU_EINSCHUB.Motor_Ueberlast_Alarm, 3));
         
            return data;
        }

        static public IList<SingleValue> serviceMenuMaximusSPA()
        {
            IList<SingleValue> data = new List<SingleValue>();

            //Allgemeine Einstellungen
            data.Add(new SingleValue("FBM_Server1.sServer_aktiv", language, 0));  
            data.Add(new SingleValue("FBM_Server1.sPort", language, 0));  
            data.Add(new SingleValue("Kessel1.sNurAdminEinstieg", language, 0));  
            data.Add(new SingleValue("Ram4.Data", language, 0));  
            data.Add(new SingleValue("udMaxKesseltemperatur", language, boilerParameter.KESSELVERWALTUNG.udMaxKesseltemperatur, 0));
            data.Add(new SingleValue("udMaxKesseltemperatur", language, boilerParameter.KESSELVERWALTUNG.udMaxKesseltemperatur, 0));
            
            if(maximusParameter.ALLGEMEINE_EINSTELLUNGEN.KBA.ToString() == "_BA_PELLETS")
            {
                data.Add(new SingleValue("udPELuefternachlauf_kurz", language, boilerParameter._SAUGZUGGEBLAESE_PELLETS.udPELuefternachlauf_kurz, 0));
                data.Add(new SingleValue("udPELuefternachlauf_normal", language, boilerParameter._SAUGZUGGEBLAESE_PELLETS.udPELuefternachlauf_normal, 0));
                data.Add(new SingleValue("udPELuefternachlauf_lang", language, boilerParameter._SAUGZUGGEBLAESE_PELLETS.udPELuefternachlauf_lang, 0));
            }
            else
            {
                data.Add(new SingleValue("udPELuefternachlauf_kurz", language, maximusParameter._SAUGZUGGEBLAESE_HACKGUT.udPELuefternachlauf_kurz, 0));
                data.Add(new SingleValue("udPELuefternachlauf_normal", language, maximusParameter._SAUGZUGGEBLAESE_HACKGUT.udPELuefternachlauf_normal, 0));
                data.Add(new SingleValue("udPELuefternachlauf_lang", language, maximusParameter._SAUGZUGGEBLAESE_HACKGUT.udPELuefternachlauf_lang, 0));
            }

            data.Add(new SingleValue("TuerKontrolleEINAUS", language, boilerParameter.SYSTEMPARAMETER.TuerKontrolleEINAUS, 0));
            data.Add(new SingleValue("Bildschirmschoner_nach", language, boilerParameter.ALLGEMEINE_EINSTELLUNGEN.Bildschirmschoner_nach, 0));
            data.Add(new SingleValue("iATFrostschutzTempKessel", language, boilerParameter.BRENNER.iATFrostschutzTempKessel, 0));
            data.Add(new SingleValue("FunktionX51", language, boilerParameter.SYSTEMPARAMETER.FunktionX51, 0));
            data.Add(new SingleValue("Standby_Ein", language, boilerParameter.SYSTEMPARAMETER.Standby_Ein, 0));
            data.Add(new SingleValue("Standby_ZeitbisZfg", language, boilerParameter.SYSTEMPARAMETER.Standby_ZeitbisZfg, 0));
            data.Add(new SingleValue("udStandby_Zeitnachtouch", language, boilerParameter.SYSTEMPARAMETER.udStandby_Zeitnachtouch, 0));
            data.Add(new SingleValue("HKXL_Modul", language, boilerParameter.ALLGEMEINE_EINSTELLUNGEN.HKXL_Modul, 0));

            //Wartungstermin
            data.Add(new SingleValue("Wartung_Monate", language, boilerParameter.SYSTEMPARAMETER.Wartung_Monate, 1));
            data.Add(new SingleValue("naechster_wartungstermin", language, 1));
            data.Add(new SingleValue("CPU_Info1.sDatumBatterieGetauscht", language, 1));

            //Fremdkessel
            data.Add(new SingleValue("Fremdkesselfreigabe", language, boilerParameter.KESSELVERWALTUNG.Fremdkesselfreigabe, 2));   
            data.Add(new SingleValue("udZeitverzoegerung_FK", language, boilerParameter.SYSTEMPARAMETER.udZeitverzoegerung_FK, 2));
            data.Add(new SingleValue("Funktion_Reserverelais", language, boilerParameter.SYSTEMPARAMETER.Funktion_Reserverelais, 2));
            data.Add(new SingleValue("Kesselbetrieb_Verzoegerung", language, boilerParameter.BRENNER.Kesselbetrieb_Verzoegerung, 2));

            //3-Wege-Motorventil
            data.Add(new SingleValue("VentStellung_stromlos", language, boilerParameter.DREI_WEGE_MOTVENT.VentStellung_stromlos, 3));   
            data.Add(new SingleValue("Schalttemp_Ventil", language, boilerParameter.DREI_WEGE_MOTVENT.Schalttemp_Ventil, 3));
            data.Add(new SingleValue("FK_Fuehler_Steckplatz", language, boilerParameter.DREI_WEGE_MOTVENT.FK_Fuehler_Steckplatz, 3));
            data.Add(new SingleValue("Zielpuffer", language, boilerParameter.KASKADE.Zielpuffer, 3));
            data.Add(new SingleValue("Uebertemperatur", language, boilerParameter.DREI_WEGE_MOTVENT.Uebertemperatur, 3));
            data.Add(new SingleValue("AusschalthystereseFK", language, boilerParameter.DREI_WEGE_MOTVENT.AusschalthystereseFK, 3));
            data.Add(new SingleValue("EinschalthystereseFK", language, boilerParameter.DREI_WEGE_MOTVENT.EinschalthystereseFK, 3));

            //Fuehlereinstellungen
            data.Add(new SingleValue("SensorTyp_Temp3", language, boilerParameter.SYSTEMPARAMETER.SensorTyp_Temp3, 4));
            data.Add(new SingleValue("SensorTyp_Temp4", language, boilerParameter.SYSTEMPARAMETER.SensorTyp_Temp4, 4));
            data.Add(new SingleValue("SensorTyp_Temp5", language, boilerParameter.SYSTEMPARAMETER.SensorTyp_Temp5, 4));
            data.Add(new SingleValue("SensorTyp_Temp6", language, boilerParameter.SYSTEMPARAMETER.SensorTyp_Temp6, 4));
            data.Add(new SingleValue("SensorTyp_Temp7", language, boilerParameter.SYSTEMPARAMETER.SensorTyp_Temp7, 4));
            data.Add(new SingleValue("SensorTyp_Temp8", language, boilerParameter.SYSTEMPARAMETER.SensorTyp_Temp8, 4));
            data.Add(new SingleValue("SensorTyp_Temp9", language, boilerParameter.SYSTEMPARAMETER.SensorTyp_Temp9, 4));
            data.Add(new SingleValue("SensorTyp_Temp10", language, boilerParameter.SYSTEMPARAMETER.SensorTyp_Temp10, 4));
            data.Add(new SingleValue("SensorTyp_Temp11", language, boilerParameter.SYSTEMPARAMETER.SensorTyp_Temp11, 4));
            data.Add(new SingleValue("SensorTyp_Temp12", language, boilerParameter.SYSTEMPARAMETER.SensorTyp_Temp12, 4));
            data.Add(new SingleValue("SensorTyp_Temp13", language, boilerParameter.SYSTEMPARAMETER.SensorTyp_Temp13, 4));
            data.Add(new SingleValue("SensorTyp_Temp14", language, boilerParameter.SYSTEMPARAMETER.SensorTyp_Temp14, 4));
            data.Add(new SingleValue("SensorTyp_Temp15", language, boilerParameter.SYSTEMPARAMETER.SensorTyp_Temp15, 4));
            data.Add(new SingleValue("SensorTypSol_Temp1", language, boilerParameter.SYSTEMPARAMETER.SensorTypSol_Temp1, 4));
            data.Add(new SingleValue("SensorTypSol_Temp2", language, boilerParameter.SYSTEMPARAMETER.SensorTypSol_Temp2, 4));
            data.Add(new SingleValue("SensorTypSol_Temp3", language, boilerParameter.SYSTEMPARAMETER.SensorTypSol_Temp3, 4));
            data.Add(new SingleValue("SensorTypSol_Temp4", language, boilerParameter.SYSTEMPARAMETER.SensorTypSol_Temp4, 4));
            data.Add(new SingleValue("SensorTypSol_Temp5", language, boilerParameter.SYSTEMPARAMETER.SensorTypSol_Temp5, 4));
            data.Add(new SingleValue("WiderstandsOffset_RF1", language, boilerParameter.SYSTEMPARAMETER.WiderstandsOffset_RF1, 4));
            data.Add(new SingleValue("WiderstandsOffset_RF2", language, boilerParameter.SYSTEMPARAMETER.WiderstandsOffset_RF2, 4));
            data.Add(new SingleValue("AT_Abgleich_X42", language, boilerParameter.SYSTEMPARAMETER.AT_Abgleich_X42, 4));

            //Wärmetaucherreinigung
            data.Add(new SingleValue("udMinimaleLaufzeitPellets", language, boilerParameter.WAERMETAUSCHERREINUNG.udMinimaleLaufzeitPellets, 5));
            data.Add(new SingleValue("udMaximaleLaufzeitPellets", language, boilerParameter.WAERMETAUSCHERREINUNG.udMaximaleLaufzeitPellets, 5));
            data.Add(new SingleValue("udGesamtlaufzeit", language, boilerParameter.WAERMETAUSCHERREINUNG.udGesamtlaufzeit, 5));   
            data.Add(new SingleValue("udPulsdauer", language, boilerParameter.WAERMETAUSCHERREINUNG.udPulsdauer, 5));
            data.Add(new SingleValue("udPulspause", language, boilerParameter.WAERMETAUSCHERREINUNG.udPulspause, 5));
            data.Add(new SingleValue("MaxAnzAbgebrochenerReinigungen", language, boilerParameter.WAERMETAUSCHERREINUNG.MaxAnzAbgebrochenerReinigungen, 5));
            data.Add(new SingleValue("Blockadestrom", language, boilerParameter.WAERMETAUSCHERREINUNG.Blockadestrom, 5));
            data.Add(new SingleValue("FreigabeRauchgastemperatur", language, boilerParameter.WAERMETAUSCHERREINUNG.FreigabeRauchgastemperatur, 5));

            //Ascheaustragung Brenner
            data.Add(new SingleValue("AscheBrenner_LzStart", language, maximusParameter.ASCHEAUSTRAGUNG.AscheBrenner_LzStart , 6));
            data.Add(new SingleValue("AscheBrenner_Impuls", language, maximusParameter.ASCHEAUSTRAGUNG.AscheBrenner_Impuls , 6));
            data.Add(new SingleValue("AscheBrenner_Pause", language, maximusParameter.ASCHEAUSTRAGUNG.AscheBrenner_Pause , 6));
            data.Add(new SingleValue("AscheBrenner_Tk_Ueberwachung", language, maximusParameter.ASCHEAUSTRAGUNG.AscheBrenner_Tk_Ueberwachung , 6));
            data.Add(new SingleValue("AscheBrenner_Stromueberwachung", language, maximusParameter.ASCHEAUSTRAGUNG.AscheBrenner_Stromueberwachung , 6));
            data.Add(new SingleValue("AscheBrenner_maxStrom", language, maximusParameter.ASCHEAUSTRAGUNG.AscheBrenner_maxStrom , 6));
            data.Add(new SingleValue("AscheBrenner_Impulse", language, maximusParameter.ASCHEAUSTRAGUNG.AscheBrenner_Impulse , 6));
                       
            //Aschenaustragung Staubabscheider
            data.Add(new SingleValue("AscheWTR_Impulse", language, maximusParameter.ASCHEAUSTRAGUNG.AscheWTR_Impulse, 7));
            data.Add(new SingleValue("AscheWTR_Stromueberwachung", language, maximusParameter.ASCHEAUSTRAGUNG.AscheWTR_Stromueberwachung, 7));
            data.Add(new SingleValue("AscheWTR_maxStrom", language, maximusParameter.ASCHEAUSTRAGUNG.AscheWTR_maxStrom, 7));

            //Ascheaustragung Box/Tonne
            data.Add(new SingleValue("udEntleerungIntervall", language, boilerParameter.ASCHENAUSTRAGUNG.udEntleerungIntervall, 8));
            data.Add(new SingleValue("Stromueberwachung", language, maximusParameter.ASCHETONNE.Stromueberwachung, 8));
            data.Add(new SingleValue("maxStrom", language, maximusParameter.ASCHETONNE.maxStrom, 8));

            //Elektrostatischer Staubabscheider
            data.Add(new SingleValue("max_Spannung", language, maximusParameter.PARTIKELFILLTER.max_Spannung, 9));
            data.Add(new SingleValue("max_Strom", language, maximusParameter.PARTIKELFILLTER.max_Strom, 9));
            data.Add(new SingleValue("Rampe", language, maximusParameter.PARTIKELFILLTER.Rampe, 9));
            data.Add(new SingleValue("AGT_Startfreigabe", language, maximusParameter.PARTIKELFILLTER.AGT_Startfreigabe, 9));

            //Anlagentyp & Nennleistung
            data.Add(new SingleValue("KESSELTYP", language, boilerParameter.KESSELTYP, 10));
            data.Add(new SingleValue("KESSELLEISTUNG", language, boilerParameter.KESSELLEISTUNG, 10));
            data.Add(new SingleValue("KBA", language, maximusParameter.ALLGEMEINE_EINSTELLUNGEN.KBA, 10));
            data.Add(new SingleValue("Drucksensor", language, maximusParameter.ALLGEMEINE_EINSTELLUNGEN.Drucksensor, 10));
            data.Add(new SingleValue("AGT_Anhebung_vorhanden", language, maximusParameter.ALLGEMEINE_EINSTELLUNGEN.AGT_Anhebung_vorhanden, 10));
            data.Add(new SingleValue("Brennraumfuehler_vorhanden", language, maximusParameter.ALLGEMEINE_EINSTELLUNGEN.Brennraumfuehler_vorhanden, 10));
            data.Add(new SingleValue("Aschetonne_vorhanden", language, maximusParameter.ASCHETONNE.Aschetonne_vorhanden, 10));
            data.Add(new SingleValue("Ascheboxschalter_vorhanden", language, maximusParameter.ASCHEAUSTRAGUNG.Ascheboxschalter_vorhanden, 10));
            data.Add(new SingleValue("ANLAGENTYP", language, boilerParameter.ANLAGENTYP, 10));

            //Saugaustragung Anlagentyp
            data.Add(new SingleValue("Umschalteinheit", language, boilerParameter.SAUGAUSTRAGUNG.Umschalteinheit, 11));      //10 Saugaustragung Anlagentyp
            data.Add(new SingleValue("Sondenumschaltung", language, boilerParameter.SAUGAUSTRAGUNG.Sondenumschaltung, 11));
            data.Add(new SingleValue("verwendete_Saugsonden", language, boilerParameter.SAUGAUSTRAGUNG.verwendete_Saugsonden, 11));
            data.Add(new SingleValue("Saugsonde_mit_Freisaugen_beginnen", language, boilerParameter.SAUGAUSTRAGUNG.Saugsonde_mit_Freisaugen_beginnen, 11));
            data.Add(new SingleValue("udSaugimpuls_SSUE", language, boilerParameter.SAUGAUSTRAGUNG.udSaugimpuls_SSUE, 11));


            return data;
        }

        #endregion

        #region Wärmepumpe

        static public IList<SingleValue> serviceMenuWP()
        {

            IList<SingleValue> data = new List<SingleValue>();

            //Heating
            data.Add(new SingleValue("Max_comp_speed_during_heating", language, boilerParameter._WP_PARA._RCC_SETTING[0].Max_comp_speed_during_heating, 0));  //0 Heating
            data.Add(new SingleValue("imaxCompdrz_minus5AT", language, boilerParameter._WP_PARA._WP_SYSREG_SET.imaxCompdrz_minus5AT, 0));
            data.Add(new SingleValue("Heating_Superheat_Setpoint", language, boilerParameter._WP_PARA._RCC_SETTING[0].Heating_Superheat_Setpoint, 0));
            data.Add(new SingleValue("Startup_SH_SP_Inc_heating_startup_Reg008", language, boilerParameter._WP_PARA._RCC_SETTING[0].Startup_SH_SP_Inc_heating_startup_Reg008, 0));
            data.Add(new SingleValue("Heating_startup_duration", language, boilerParameter._WP_PARA._RCC_SETTING[0].Heating_startup_duration, 0));
            data.Add(new SingleValue("Heating_startup_opening", language, boilerParameter._WP_PARA._RCC_SETTING[0].Heating_startup_opening, 0));
            data.Add(new SingleValue("Heating_startup_speed_duration", language, boilerParameter._WP_PARA._RCC_SETTING[0].Heating_startup_speed_duration, 0));
            data.Add(new SingleValue("Heating_startup_speed", language, boilerParameter._WP_PARA._RCC_SETTING[0].Heating_startup_speed, 0));
            data.Add(new SingleValue("Heating_control_mode", language, boilerParameter._WP_PARA._RCC_SETTING[0].Heating_control_mode, 0));
            data.Add(new SingleValue("Heating_EXV_Kp", language, boilerParameter._WP_PARA._RCC_SETTING[0].Heating_EXV_Kp, 0));
            data.Add(new SingleValue("Heating_EXV_Ki", language, boilerParameter._WP_PARA._RCC_SETTING[0].Heating_EXV_Ki, 0));
            data.Add(new SingleValue("Heating_EXV_Kd", language, boilerParameter._WP_PARA._RCC_SETTING[0].Heating_EXV_Kd, 0));
            data.Add(new SingleValue("Startup_SH_SP_Inc_after_defrost_Reg007", language, boilerParameter._WP_PARA._RCC_SETTING[0].Startup_SH_SP_Inc_after_defrost_Reg007, 0));
            data.Add(new SingleValue("Startup_SH_SP_Inc_timeout_Reg010", language, boilerParameter._WP_PARA._RCC_SETTING[0].Startup_SH_SP_Inc_timeout_Reg010, 0));
            data.Add(new SingleValue("Startup_SH_SP_Inc_speedlimit_Reg011", language, boilerParameter._WP_PARA._RCC_SETTING[0].Startup_SH_SP_Inc_speedlimit_Reg011, 0));
            data.Add(new SingleValue("Startup_SH_SP_Inc_speedduration_Reg012", language, boilerParameter._WP_PARA._RCC_SETTING[0].Startup_SH_SP_Inc_speedduration_Reg012, 0));
            data.Add(new SingleValue("Press_drop_sh_sp_shift_rate_Reg005", language, boilerParameter._WP_PARA._RCC_SETTING[0].Press_drop_sh_sp_shift_rate_Reg005, 0));


            //EVI - valve
            data.Add(new SingleValue("EVI_Superheat_Setpoint", language, boilerParameter._WP_PARA._RCC_SETTING[0].EVI_Superheat_Setpoint, 1));  //1 EVI - valve
            data.Add(new SingleValue("EVI_startup_duration", language, boilerParameter._WP_PARA._RCC_SETTING[0].EVI_startup_duration, 1));
            data.Add(new SingleValue("EVI_startup_opening", language, boilerParameter._WP_PARA._RCC_SETTING[0].EVI_startup_opening, 1));
            data.Add(new SingleValue("EVI_control_mode", language, boilerParameter._WP_PARA._RCC_SETTING[0].EVI_control_mode, 1));
            data.Add(new SingleValue("EVI_EXV_Kp", language, boilerParameter._WP_PARA._RCC_SETTING[0].EVI_EXV_Kp, 1));
            data.Add(new SingleValue("EVI_EXV_Ki", language, boilerParameter._WP_PARA._RCC_SETTING[0].EVI_EXV_Ki, 1));
            data.Add(new SingleValue("EVI_EXV_Kd", language, boilerParameter._WP_PARA._RCC_SETTING[0].EVI_EXV_Kd, 1));


            //Cooling
            data.Add(new SingleValue("Cooling_superheat_setpoint", language, boilerParameter._WP_PARA._RCC_SETTING[0].Cooling_superheat_setpoint, 2));  //2 Cooling
            data.Add(new SingleValue("Startup_SH_SP_Inc_cooling_startup_Reg009", language, boilerParameter._WP_PARA._RCC_SETTING[0].Startup_SH_SP_Inc_cooling_startup_Reg009, 2));
            data.Add(new SingleValue("Cooling_startup_duration", language, boilerParameter._WP_PARA._RCC_SETTING[0].Cooling_startup_duration, 2));
            data.Add(new SingleValue("Cooling_startup_opening", language, boilerParameter._WP_PARA._RCC_SETTING[0].Cooling_startup_opening, 2));
            data.Add(new SingleValue("Cooling_start_up_speed_duration", language, boilerParameter._WP_PARA._RCC_SETTING[0].Cooling_start_up_speed_duration, 2));
            data.Add(new SingleValue("Cooling_startup_speed", language, boilerParameter._WP_PARA._RCC_SETTING[0].Cooling_startup_speed, 2));
            data.Add(new SingleValue("Cooling_control_mode", language, boilerParameter._WP_PARA._RCC_SETTING[0].Cooling_control_mode, 2));
            data.Add(new SingleValue("Cooling_EXV_Kp", language, boilerParameter._WP_PARA._RCC_SETTING[0].Cooling_EXV_Kp, 2));
            data.Add(new SingleValue("Cooling_EXV_Ki", language, boilerParameter._WP_PARA._RCC_SETTING[0].Cooling_EXV_Ki, 2));
            data.Add(new SingleValue("Cooling_EXV_Kd", language, boilerParameter._WP_PARA._RCC_SETTING[0].Cooling_EXV_Kd, 2));
            data.Add(new SingleValue("maxUnterschreitungszeit", language, boilerParameter._WP_PARA._WP_SYSREG_SET.maxUnterschreitungszeit, 2));


            //Defrost
            data.Add(new SingleValue("Defrost_startup_duration", language, boilerParameter._WP_PARA._RCC_SETTING[0].Defrost_startup_duration, 3));  //3 Defrost
            data.Add(new SingleValue("Defrost_statup_opening", language, boilerParameter._WP_PARA._RCC_SETTING[0].Defrost_statup_opening, 3));
            data.Add(new SingleValue("Defrost_startup_speed_duration", language, boilerParameter._WP_PARA._RCC_SETTING[0].Defrost_startup_speed_duration, 3));
            data.Add(new SingleValue("Defrost_startup_speed", language, boilerParameter._WP_PARA._RCC_SETTING[0].Defrost_startup_speed, 3));
            data.Add(new SingleValue("Defrost_interval", language, boilerParameter._WP_PARA._RCC_SETTING[0].Defrost_interval, 3));
            data.Add(new SingleValue("Defrost_duration", language, boilerParameter._WP_PARA._RCC_SETTING[0].Defrost_duration, 3));
            data.Add(new SingleValue("Defrost_endtemp", language, boilerParameter._WP_PARA._RCC_SETTING[0].Defrost_endtemp, 3));
            data.Add(new SingleValue("CompStopDelayAfterDefrost", language, boilerParameter._WP_PARA._RCC_SETTING[0].CompStopDelayAfterDefrost, 3));
            data.Add(new SingleValue("Defrost_delta_threshold", language, boilerParameter._WP_PARA._RCC_SETTING[0].Defrost_delta_threshold, 3));
            data.Add(new SingleValue("Defrost_delta_absolute_threshold", language, boilerParameter._WP_PARA._RCC_SETTING[0].Defrost_delta_absolute_threshold, 3));
            data.Add(new SingleValue("Defrost_slowdown_condensing_temp", language, boilerParameter._WP_PARA._RCC_SETTING[0].Defrost_slowdown_condensing_temp, 3));
            data.Add(new SingleValue("Defrost_slowdown_speed", language, boilerParameter._WP_PARA._RCC_SETTING[0].Defrost_slowdown_speed, 3));
            data.Add(new SingleValue("Compressor_stop_delay_before_defrost", language, boilerParameter._WP_PARA._RCC_SETTING[0].Compressor_stop_delay_before_defrost, 3));
            data.Add(new SingleValue("Max_comp_speed_during_defrost", language, boilerParameter._WP_PARA._RCC_SETTING[0].Max_comp_speed_during_defrost, 3));
            data.Add(new SingleValue("Min_comp_speed_during_defrost", language, boilerParameter._WP_PARA._RCC_SETTING[0].Min_comp_speed_during_defrost, 3));


            //System Parameter
            data.Add(new SingleValue("Compressor_Model", language, boilerParameter._WP_PARA._RCC_SETTING[0].Compressor_Model, 4));  //4 System Parameter
            data.Add(new SingleValue("ProduktRevison", language, boilerParameter.ALLGEMEINE_EINSTELLUNGEN.ProduktRevison, 4));
            data.Add(new SingleValue("Suction_Sensor_Type_P1", language, boilerParameter._WP_PARA._RCC_SETTING[0].Suction_Sensor_Type_P1, 4));
            data.Add(new SingleValue("Discharge_Sensor_Type_P2", language, boilerParameter._WP_PARA._RCC_SETTING[0].Discharge_Sensor_Type_P2, 4));
            data.Add(new SingleValue("Intermediate_Sensor_Type_P3", language, boilerParameter._WP_PARA._RCC_SETTING[0].Intermediate_Sensor_Type_P3, 4));
            data.Add(new SingleValue("Statorheater", language, boilerParameter._WP_PARA._RCC_SETTING[0].Statorheater, 4));
            data.Add(new SingleValue("Accessory_power_inital_value", language, boilerParameter._WP_PARA._RCC_SETTING[0].Accessory_power_inital_value, 4));
            data.Add(new SingleValue("Compressor_current_protection", language, boilerParameter._WP_PARA._RCC_SETTING[0].Compressor_current_protection, 4));
            data.Add(new SingleValue("FREMAR_frequency_band", language, boilerParameter._WP_PARA._RCC_SETTING[0].FREMAR_frequency_band, 4));
            data.Add(new SingleValue("FREMAR_frequency_1", language, boilerParameter._WP_PARA._RCC_SETTING[0].FREMAR_frequency_1, 4));
            data.Add(new SingleValue("FREMAR_frequency_2", language, boilerParameter._WP_PARA._RCC_SETTING[0].FREMAR_frequency_2, 4));
            data.Add(new SingleValue("FREMAR_frequency_3", language, boilerParameter._WP_PARA._RCC_SETTING[0].FREMAR_frequency_3, 4));
            data.Add(new SingleValue("Comp_minSpeed", language, boilerParameter._WP_PARA._RCC_SETTING[0].Comp_minSpeed, 4));
            data.Add(new SingleValue("Statorheater_control_Source", language, boilerParameter._WP_RCC_SETTING_2[0].Statorheater_control_Source, 4));
            data.Add(new SingleValue("fourway_switching_speed", language, boilerParameter._WP_PARA._RCC_SETTING[0].fourway_switching_speed, 4));
            data.Add(new SingleValue("fourway_switching_delta_pressure", language, boilerParameter._WP_PARA._RCC_SETTING[0].fourway_switching_delta_pressure, 4));
            data.Add(new SingleValue("max_Leistung_Boilerladung", language, boilerParameter._WP_PARA._WP_SYSREG_SET.max_Leistung_Boilerladung, 4));
            data.Add(new SingleValue("WP_udMaxKesseltemperatur", language, boilerParameter.KESSELVERWALTUNG.udMaxKesseltemperatur, 4));
            data.Add(new SingleValue("fourway_switching_delta_pressure", language, boilerParameter.KESSELVERWALTUNG.udKesselsolltemperatur, 4));
            // data.Add(new SingleValue("fourway_switching_delta_pressure", language, boilerParameter._WP_PARA._RCC_SETTING[0]., 4));      //demand offset - in k_daten.csv
            data.Add(new SingleValue("VltSoll_Anhebung_vor_EVUSperre", language, boilerParameter._WP_PARA._WP_SYSREG_SET.VltSoll_Anhebung_vor_EVUSperre, 4));
            data.Add(new SingleValue("Capacity_input_mode", language, boilerParameter._WP_PARA._RCC_SETTING[0].Capacity_input_mode, 4));
            data.Add(new SingleValue("Digital_input_1", language, boilerParameter._WP_PARA._RCC_SETTING[0].Digital_input_1, 4));
            data.Add(new SingleValue("Digital_input_2", language, boilerParameter._WP_PARA._RCC_SETTING[0].Digital_input_2, 4));
            data.Add(new SingleValue("Digital_output_2", language, boilerParameter._WP_PARA._RCC_SETTING[0].Digital_output_2, 4));
            data.Add(new SingleValue("Digital_output_3", language, boilerParameter._WP_PARA._RCC_SETTING[0].Digital_output_3, 4));
            data.Add(new SingleValue("udWP_Periodendauer", language, boilerParameter._WP_PARA._WP_SYSREG_SET.udWP_Periodendauer, 4));
            data.Add(new SingleValue("Aussentemp_Sperre_WP", language, boilerParameter._WP_PARA._WP_SYSREG_SET.Aussentemp_Sperre_WP, 4));
            data.Add(new SingleValue("iPowerWP_P_Faktor", language, boilerParameter._WP_PARA._WP_SYSREG_SET.iPowerWP_P_Faktor, 4));
            data.Add(new SingleValue("Demand_AnalogIn_EA", language, boilerParameter._WP_PARA._WP_SYSREG_SET.Demand_AnalogIn_EA, 4));
            data.Add(new SingleValue("Pre_opening_phase_duration_Reg078", language, boilerParameter._WP_PARA._RCC_SETTING[0].Pre_opening_phase_duration_Reg078, 4));
            data.Add(new SingleValue("Pre_opening_temp_Reg103", language, boilerParameter._WP_PARA._RCC_SETTING[0].Pre_opening_temp_Reg103, 4));  //Früher: Pre-opening pressure
            data.Add(new SingleValue("DriveSpeedRate_Reg108", language, boilerParameter._WP_PARA._RCC_SETTING[0].DriveSpeedRate_Reg108, 4));
            data.Add(new SingleValue("ControlSpeedRate_Reg109", language, boilerParameter._WP_RCC_SETTING_2[0].ControlSpeedRate_Reg109, 4));
            data.Add(new SingleValue("Compressor_Speed_delta_sensitivity_Reg110", language, boilerParameter._WP_RCC_SETTING_2[0].Compressor_Speed_delta_sensitivity_Reg110, 4));
            data.Add(new SingleValue("Compressor_Speed_delta_scaling_Reg111", language, boilerParameter._WP_RCC_SETTING_2[0].Compressor_Speed_delta_scaling_Reg111, 4));
            data.Add(new SingleValue("Valvescaling_threshold_Reg112", language, boilerParameter._WP_RCC_SETTING_2[0].Valvescaling_threshold_Reg112, 4));
            data.Add(new SingleValue("Valvescaling_minimum_Reg113", language, boilerParameter._WP_RCC_SETTING_2[0].Valvescaling_minimum_Reg113, 4));
            
            //Modultyp (k_daten.csv) 
            data.Add(new SingleValue("CAN_Erweiterung1.sHK_XL_MOD1_Typ", language, 4));
            data.Add(new SingleValue("CAN_Erweiterung1.sHK_MOD1_Typ", language, 4));
            data.Add(new SingleValue("CAN_Erweiterung1.sHK_MOD2_Typ", language, 4));
            data.Add(new SingleValue("CAN_Erweiterung1.sHK_MOD3_Typ", language, 4));
            data.Add(new SingleValue("CAN_Erweiterung1.sFW_MOD1_Typ", language, 4));
            data.Add(new SingleValue("CAN_Erweiterung1.sFW_MOD2_Typ", language, 4));
            data.Add(new SingleValue("CAN_Erweiterung1.sFW_MOD3_Typ", language, 4));
            data.Add(new SingleValue("CAN_Erweiterung1.sFW_MOD4_Typ", language, 4));
            data.Add(new SingleValue("CAN_Erweiterung1.sDIFF_MOD1_Typ", language, 4));
            data.Add(new SingleValue("CAN_Erweiterung1.sDIFF_MOD2_Typ", language, 4));
            data.Add(new SingleValue("CAN_Erweiterung1.sDIFF_MOD3_Typ", language, 4));
            data.Add(new SingleValue("CAN_Erweiterung1.sDIFF_MOD4_Typ", language, 4));
            data.Add(new SingleValue("CAN_Erweiterung1.sSolar_MOD1_Typ", language, 4));
            data.Add(new SingleValue("CAN_Erweiterung1.sSolar_MOD2_Typ", language, 4));
            data.Add(new SingleValue("CAN_Erweiterung1.sSolar_MOD3_Typ", language, 4));
            data.Add(new SingleValue("CAN_Erweiterung1.sSolar_MOD4_Typ", language, 4));
            data.Add(new SingleValue("CAN_Erweiterung1.sSSUE_MOD1_Typ", language, 4));
           

            //Fan
            data.Add(new SingleValue("Fan_control_min_speed", language, boilerParameter._WP_PARA._RCC_SETTING[0].Fan_control_min_speed, 5));   //5 Fan
            data.Add(new SingleValue("Fan_control_max_speed", language, boilerParameter._WP_PARA._RCC_SETTING[0].Fan_control_max_speed, 5));
            data.Add(new SingleValue("Fan_ctrl_min_speed_temp_heat", language, boilerParameter._WP_PARA._RCC_SETTING[0].Fan_ctrl_min_speed_temp_heat, 5));
            data.Add(new SingleValue("Fan_ctrl_max_speed_temp_heat", language, boilerParameter._WP_PARA._RCC_SETTING[0].Fan_ctrl_max_speed_temp_heat, 5));
            data.Add(new SingleValue("uiCompSpeed_m20_minFan", language, boilerParameter._WP_PARA._WP_LUEFTER_STRUCT[0].uiCompSpeed_m20_minFan, 5));
            data.Add(new SingleValue("uiCompSpeed_m20_maxFan", language, boilerParameter._WP_PARA._WP_LUEFTER_STRUCT[0].uiCompSpeed_m20_maxFan, 5));
            data.Add(new SingleValue("uiCompSpeed_p15_minFan", language, boilerParameter._WP_PARA._WP_LUEFTER_STRUCT[0].uiCompSpeed_p15_minFan, 5));
            data.Add(new SingleValue("uiCompSpeed_p15_maxFan", language, boilerParameter._WP_PARA._WP_LUEFTER_STRUCT[0].uiCompSpeed_p15_maxFan, 5));
            data.Add(new SingleValue("FanCtrl_minSpeed_Cooling", language, boilerParameter._WP_PARA._RCC_SETTING[0].FanCtrl_minSpeed_Cooling, 5));
            data.Add(new SingleValue("FanCtrl_maxSpeed_Cooling", language, boilerParameter._WP_PARA._RCC_SETTING[0].FanCtrl_maxSpeed_Cooling, 5));
            data.Add(new SingleValue("Fan_ctrl_min_speed_temp_cooling", language, boilerParameter._WP_PARA._RCC_SETTING[0].Fan_ctrl_min_speed_temp_cooling, 5));
            data.Add(new SingleValue("Fan_ctrl_max_speed_temp_cooling", language, boilerParameter._WP_PARA._RCC_SETTING[0].Fan_ctrl_max_speed_temp_cooling, 5));
            data.Add(new SingleValue("Fan_delay_after_defrost", language, boilerParameter._WP_PARA._RCC_SETTING[0].Fan_delay_after_defrost, 5));
            data.Add(new SingleValue("At_NachlaufE_A", language, boilerParameter._WP_PARA._WP_LUEFTER_STRUCT[0].At_NachlaufE_A, 5));
            data.Add(new SingleValue("At_Freigabetemp", language, boilerParameter._WP_PARA._WP_LUEFTER_STRUCT[0].At_Freigabetemp, 5));
            data.Add(new SingleValue("At_max_Zeit", language, boilerParameter._WP_PARA._WP_LUEFTER_STRUCT[0].At_max_Zeit, 5));
            data.Add(new SingleValue("At_Abschaltoffset", language, boilerParameter._WP_PARA._WP_LUEFTER_STRUCT[0].At_Abschaltoffset, 5));
            data.Add(new SingleValue("At_Drehzahl", language, boilerParameter._WP_PARA._WP_LUEFTER_STRUCT[0].At_Drehzahl, 5));


            //Primary circuit pump
            data.Add(new SingleValue("Pumpentyp", language, boilerParameter._WP_PARA._WP_PK_PUMPE_PARA[0].Pumpentyp, 6));   //6 Primary circuit pump
            data.Add(new SingleValue("Frostschutztemp", language, boilerParameter._WP_PARA._WP_PK_PUMPE_PARA[0].Frostschutztemp, 6));
            data.Add(new SingleValue("Nachlaufzeit", language, boilerParameter._WP_PARA._WP_PK_PUMPE_PARA[0].Nachlaufzeit, 6));
            data.Add(new SingleValue("Frostschutz_Df_Soll", language, boilerParameter._WP_PARA._WP_PK_PUMPE_PARA[0].Frostschutz_Df_Soll, 6));
            data.Add(new SingleValue("Betrieb_min_Durchf", language, boilerParameter._WP_PARA._WP_PK_PUMPE_PARA[0].Betrieb_min_Durchf, 6));
            data.Add(new SingleValue("Betrieb_max_Durchf", language, boilerParameter._WP_PARA._WP_PK_PUMPE_PARA[0].Betrieb_max_Durchf, 6));
            data.Add(new SingleValue("Betrieb_max_Drehzahl", language, boilerParameter._WP_PARA._WP_PK_PUMPE_PARA[0].Betrieb_max_Drehzahl, 6));
            data.Add(new SingleValue("FlowCtrl_Defrost", language, boilerParameter._WP_PARA._WP_PK_PUMPE_PARA[0].FlowCtrl_Defrost, 6));
            data.Add(new SingleValue("Soll_Spreizung_Heizbetrieb", language, boilerParameter._WP_PARA._WP_PK_PUMPE_PARA[0].Soll_Spreizung_Heizbetrieb, 6));
            data.Add(new SingleValue("Soll_Spreizung_Boilerladung", language, boilerParameter._WP_PARA._WP_PK_PUMPE_PARA[0].Soll_Spreizung_Boilerladung, 6));
            data.Add(new SingleValue("Soll_Spreizung_Kuehlen", language, boilerParameter._WP_PARA._WP_PK_PUMPE_PARA[0].Soll_Spreizung_Kuehlen, 6));

            //Alarm
            data.Add(new SingleValue("Low_superheat_alarm_threshold", language, boilerParameter._WP_PARA._RCC_SETTING[0].Low_superheat_alarm_threshold, 7));   //7 Alarm
            data.Add(new SingleValue("Low_superheat_alarm_delay", language, boilerParameter._WP_PARA._RCC_SETTING[0].Low_superheat_alarm_delay, 7));
            data.Add(new SingleValue("Low_pressure_alarm_threshold", language, boilerParameter._WP_PARA._RCC_SETTING[0].Low_pressure_alarm_threshold, 7));
            data.Add(new SingleValue("Low_pressure_alarm_delay", language, boilerParameter._WP_PARA._RCC_SETTING[0].Low_pressure_alarm_delay, 7));
            data.Add(new SingleValue("High_Condensing_pressure_alarm_threshold", language, boilerParameter._WP_PARA._RCC_SETTING[0].High_Condensing_pressure_alarm_threshold, 7));
            data.Add(new SingleValue("High_condensing_pressure_alarm_delay", language, boilerParameter._WP_PARA._RCC_SETTING[0].High_condensing_pressure_alarm_delay, 7));
            data.Add(new SingleValue("Alarm_pause", language, boilerParameter._WP_PARA._RCC_SETTING[0].Alarm_pause, 7));
            data.Add(new SingleValue("Envelope_alarm_delay", language, boilerParameter._WP_PARA._RCC_SETTING[0].Envelope_alarm_delay, 7));
            data.Add(new SingleValue("MOP_threshold", language, boilerParameter._WP_PARA._RCC_SETTING[0].MOP_threshold, 7));
            data.Add(new SingleValue("High_superheat_alarm_threshold", language, boilerParameter._WP_PARA._RCC_SETTING[0].High_superheat_alarm_threshold, 7));
            data.Add(new SingleValue("High_superheat_alarm_delay", language, boilerParameter._WP_PARA._RCC_SETTING[0].High_superheat_alarm_delay, 7));

            //Oil return
            data.Add(new SingleValue("Oilreturn_speed", language, boilerParameter._WP_PARA._RCC_SETTING[0].Oilreturn_speed, 8));   //8 Oil return
            data.Add(new SingleValue("Oilreturn_threshold", language, boilerParameter._WP_PARA._RCC_SETTING[0].Oilreturn_threshold, 8));
            data.Add(new SingleValue("Oilreturn_duration", language, boilerParameter._WP_PARA._RCC_SETTING[0].Oilreturn_duration, 8));
            data.Add(new SingleValue("Oilreturn_threshold_duration_heating", language, boilerParameter._WP_PARA._RCC_SETTING[0].Oilreturn_threshold_duration, 8));
            data.Add(new SingleValue("Oilreturn_ts_duration_Cooling", language, boilerParameter._WP_RCC_SETTING_2[0].Oilreturn_ts_duration_Cooling, 8));

            return data;
        }

        static public IList<SingleValue> serviceMenuWPHeatingRod()
        {

            IList<SingleValue> data = new List<SingleValue>();

            data.Add(new SingleValue("Aussen_Freigabe_temp_ZH", language, boilerParameter._WP_PARA._WP_SYSREG_SET.Aussen_Freigabe_temp_ZH, 0));     //0 Elektroheizstab
            data.Add(new SingleValue("E_Stab_Zeitverzoegerung_TWS", language, boilerParameter._WP_PARA._WP_SYSREG_SET.E_Stab_Zeitverzoegerung_TWS, 0));
            data.Add(new SingleValue("E_Stab_min_ED", language, boilerParameter._WP_PARA._WP_SYSREG_SET.E_Stab_min_ED, 0));
            data.Add(new SingleValue("E_Stab_Periodendauer", language, boilerParameter._WP_PARA._WP_SYSREG_SET.E_Stab_Periodendauer, 0));
            data.Add(new SingleValue("E_StabLeistung", language, boilerParameter._WP_PARA._WP_SYSREG_SET.E_StabLeistung, 0));
            data.Add(new SingleValue("EStab_Kompensation", language, boilerParameter._WP_PARA._WP_SYSREG_SET.EStab_Kompensation, 0));
            data.Add(new SingleValue("E_StabAnforderung", language, boilerParameter._WP_PARA._WP_SYSREG_SET.uiZuheizen_bei_Boilerladung, 0));

            return data;
        }

        static public IList<SingleValue> serviceMenuWPExternalBoiler()
        {

            IList<SingleValue> data = new List<SingleValue>();

            //Kessel Sollwerte
            data.Add(new SingleValue("Fremdkesselfreigabe", language, boilerParameter.KESSELVERWALTUNG.Fremdkesselfreigabe, 0));     //0 Kessel Sollwerte
            data.Add(new SingleValue("WP_udZeitverzoegerung_FK", language, boilerParameter.SYSTEMPARAMETER.udZeitverzoegerung_FK, 0));
            data.Add(new SingleValue("AussenFreigabeTemp", language, boilerParameter.HEIZKREISPARAMETER._HeizkreisKesselregler.AussenFreigabeTemp, 0));
            data.Add(new SingleValue("AusschalthystereseFK", language, boilerParameter.DREI_WEGE_MOTVENT.AusschalthystereseFK, 0));
            data.Add(new SingleValue("EinschalthystereseFK", language, boilerParameter.DREI_WEGE_MOTVENT.EinschalthystereseFK, 0));
            data.Add(new SingleValue("udmin_Kessellaufzeit", language, boilerParameter.HEIZKREISPARAMETER._HeizkreisKesselregler.udmin_Kessellaufzeit, 0));
            data.Add(new SingleValue("Uebertemperatur", language, boilerParameter.DREI_WEGE_MOTVENT.Uebertemperatur, 0));
            data.Add(new SingleValue("KesselreglerTyp", language, boilerParameter.HEIZKREISPARAMETER._HeizkreisKesselregler.KesselreglerTyp, 0));
            data.Add(new SingleValue("WP_udPEMax_Luefterdrehzahl", language, boilerParameter._SAUGZUGGEBLAESE_PELLETS.udPEMax_Luefterdrehzahl, 0));
            data.Add(new SingleValue("WP_udPEMin_Luefterdrehzahl", language, boilerParameter._SAUGZUGGEBLAESE_PELLETS.udPEMin_Luefterdrehzahl, 0));
            data.Add(new SingleValue("Moulation_plus_VltSoll", language, boilerParameter.HEIZKREISPARAMETER._HeizkreisKesselregler.Moulation_plus_VltSoll, 0));
            data.Add(new SingleValue("Moulation_minus_VltSoll", language, boilerParameter.HEIZKREISPARAMETER._HeizkreisKesselregler.Moulation_minus_VltSoll, 0));
            data.Add(new SingleValue("FK_Fuehler_Steckplatz", language, boilerParameter.DREI_WEGE_MOTVENT.FK_Fuehler_Steckplatz, 0));
            data.Add(new SingleValue("FKSolltemperatur", language, boilerParameter.HEIZKREISPARAMETER._HeizkreisKesselregler.FKSolltemperatur, 0));
            data.Add(new SingleValue("udPEKesseltemperaturPBereich", language, boilerParameter._SAUGZUGGEBLAESE_PELLETS.udPEKesseltemperaturPBereich, 0));
            data.Add(new SingleValue("Modus_Analogausgang", language, boilerParameter.HEIZKREISPARAMETER._HeizkreisKesselregler.Modus_Analogausgang, 0));
            data.Add(new SingleValue("Temp_10V", language, boilerParameter.HEIZKREISPARAMETER._HeizkreisKesselregler.Temp_10V, 0));
            data.Add(new SingleValue("Temp_0V", language, boilerParameter.HEIZKREISPARAMETER._HeizkreisKesselregler.Temp_0V, 0));

            //RLA - Allgemeine Einstellungen
            data.Add(new SingleValue("WP_udRLAAusschalttemperatur", language, boilerParameter.RLA.udRLAAusschalttemperatur, 1));     //1 RLA - Allgemeine Einstellungen
            data.Add(new SingleValue("udRLAStartdifferenz", language, boilerParameter.RLA.udRLAStartdifferenz, 1));
            data.Add(new SingleValue("udRLADiff_KT_RLF_PU", language, boilerParameter.RLA.udRLADiff_KT_RLF_PU, 1));
            data.Add(new SingleValue("udRLADiff_KT_PO", language, boilerParameter.RLA.udRLADiff_KT_PO, 1));
            data.Add(new SingleValue("udRLA_Hysterese", language, boilerParameter.RLA.udRLA_Hysterese, 1));
            data.Add(new SingleValue("RLA_Nachlaufzeit", language, boilerParameter.RLA_MISCHER.RLA_Nachlaufzeit, 1));

            //RLA - Mischer(X13)
            data.Add(new SingleValue("Mischerfunktion", language, boilerParameter.RLA.Mischerfunktion, 2));     //2 RLA - Mischer(X13)
            data.Add(new SingleValue("uRLA_Solltemperatur_Min", language, boilerParameter.RLA.uRLA_Solltemperatur_Min, 2));
            data.Add(new SingleValue("uRLA_Solltemperatur", language, boilerParameter.RLA.uRLA_Solltemperatur, 2));
            data.Add(new SingleValue("uminPause", language, boilerParameter.RLA.uminPause, 2));
            data.Add(new SingleValue("umaxPause", language, boilerParameter.RLA.umaxPause, 2));
            data.Add(new SingleValue("uMischerlaufzeit", language, boilerParameter.RLA.uMischerlaufzeit, 2));
            data.Add(new SingleValue("WP_uPBereich_Minus", language, boilerParameter.RLA.uPBereich, 2));
            data.Add(new SingleValue("WP_uPBereich_Plus", language, boilerParameter.RLA_MISCHER.udPBereich_Plus, 2));
            data.Add(new SingleValue("uPulsdauer", language, boilerParameter.RLA.uPulsdauer, 2));
            data.Add(new SingleValue("uiPumpe_ohne_Puffer", language, boilerParameter.RLA.uiPumpe_ohne_Puffer, 2));


            return data;
        }

        static public IList<SingleValue> serviceMenuWPSmartGrid()
        {

            IList<SingleValue> data = new List<SingleValue>();

            data.Add(new SingleValue("WPBZS4_Freigabe", language, boilerParameter._SG_Ready_Para.WPBZS4_Freigabe, 0));     //0 Smart Grid
            data.Add(new SingleValue("WPSGR_BA4_TWS_UEBERLADUNG_1", language, boilerParameter._SG_Ready_Para.WPSGR_BA4_TWS_UEBERLADUNG[0], 0));
            data.Add(new SingleValue("WPSGR_BA4_TWS_UEBERLADUNG_2", language, boilerParameter._SG_Ready_Para.WPSGR_BA4_TWS_UEBERLADUNG[1], 0));
            data.Add(new SingleValue("WPSGR_BA4_TWS_UEBERLADUNG_3", language, boilerParameter._SG_Ready_Para.WPSGR_BA4_TWS_UEBERLADUNG[2], 0));
            data.Add(new SingleValue("WPSGR_BA4_TWS_UEBERLADUNG_4", language, boilerParameter._SG_Ready_Para.WPSGR_BA4_TWS_UEBERLADUNG[3], 0));
            data.Add(new SingleValue("WPSGR_BA3_VLTSOLL_AH_1", language, boilerParameter._SG_Ready_Para.WPSGR_BA3_VLTSOLL_AH[0], 0));
            data.Add(new SingleValue("WPSGR_BA3_VLTSOLL_AH_2", language, boilerParameter._SG_Ready_Para.WPSGR_BA3_VLTSOLL_AH[1], 0));
            data.Add(new SingleValue("WPSGR_BA3_VLTSOLL_AH_3", language, boilerParameter._SG_Ready_Para.WPSGR_BA3_VLTSOLL_AH[2], 0));
            data.Add(new SingleValue("WPSGR_BA3_VLTSOLL_AH_4", language, boilerParameter._SG_Ready_Para.WPSGR_BA3_VLTSOLL_AH[3], 0));
            data.Add(new SingleValue("WPSGR_BA3_VLTSOLL_AH_5", language, boilerParameter._SG_Ready_Para.WPSGR_BA3_VLTSOLL_AH[4], 0));
            data.Add(new SingleValue("WPSGR_BA3_VLTSOLL_AH_6", language, boilerParameter._SG_Ready_Para.WPSGR_BA3_VLTSOLL_AH[5], 0));
            data.Add(new SingleValue("WPSGR_BA3_VLTSOLL_AH_7", language, boilerParameter._SG_Ready_Para.WPSGR_BA3_VLTSOLL_AH[6], 0));
            data.Add(new SingleValue("WPSGR_BA3_VLTSOLL_AH_8", language, boilerParameter._SG_Ready_Para.WPSGR_BA3_VLTSOLL_AH[7], 0));
            data.Add(new SingleValue("WPSGR_BA4_RTSOLL_AH_1", language, boilerParameter._SG_Ready_Para.WPSGR_BA4_RTSOLL_AH[0], 0));
            data.Add(new SingleValue("WPSGR_BA4_RTSOLL_AH_2", language, boilerParameter._SG_Ready_Para.WPSGR_BA4_RTSOLL_AH[1], 0));
            data.Add(new SingleValue("WPSGR_BA4_RTSOLL_AH_3", language, boilerParameter._SG_Ready_Para.WPSGR_BA4_RTSOLL_AH[2], 0));
            data.Add(new SingleValue("WPSGR_BA4_RTSOLL_AH_4", language, boilerParameter._SG_Ready_Para.WPSGR_BA4_RTSOLL_AH[3], 0));
            data.Add(new SingleValue("WPSGR_BA4_RTSOLL_AH_5", language, boilerParameter._SG_Ready_Para.WPSGR_BA4_RTSOLL_AH[4], 0));
            data.Add(new SingleValue("WPSGR_BA4_RTSOLL_AH_6", language, boilerParameter._SG_Ready_Para.WPSGR_BA4_RTSOLL_AH[5], 0));
            data.Add(new SingleValue("WPSGR_BA4_RTSOLL_AH_7", language, boilerParameter._SG_Ready_Para.WPSGR_BA4_RTSOLL_AH[6], 0));
            data.Add(new SingleValue("WPSGR_BA4_RTSOLL_AH_8", language, boilerParameter._SG_Ready_Para.WPSGR_BA4_RTSOLL_AH[7], 0));
            data.Add(new SingleValue("WPSGR_BA4_PUFFER_1", language, boilerParameter._SG_Ready_Para.WPSGR_BA4_PUFFER[0], 0));
            data.Add(new SingleValue("WPSGR_BA4_PUFFER_2", language, boilerParameter._SG_Ready_Para.WPSGR_BA4_PUFFER[1], 0));
            data.Add(new SingleValue("WPSGR_BA4_PUFFER_3", language, boilerParameter._SG_Ready_Para.WPSGR_BA4_PUFFER[2], 0));
            data.Add(new SingleValue("WPSGR_BA4_PUFFER_4", language, boilerParameter._SG_Ready_Para.WPSGR_BA4_PUFFER[3], 0));


            data.Add(new SingleValue("Photovoltaik", language, boilerParameter.ALLGEMEINE_EINSTELLUNGEN.Photovoltaik, 1));     //1 PV Überschussnutzung
            data.Add(new SingleValue("PV_Einschaltschwelle", language, boilerParameter.ALLGEMEINE_EINSTELLUNGEN.PV_Einschaltschwelle, 1));
            data.Add(new SingleValue("PV_Ausschaltschwelle", language, boilerParameter.ALLGEMEINE_EINSTELLUNGEN.PV_Ausschaltschwelle, 1));
            data.Add(new SingleValue("Solarertag_beruecksichtigen", language, boilerParameter.ALLGEMEINE_EINSTELLUNGEN.Solarertag_beruecksichtigen, 1));
            data.Add(new SingleValue("PV_Regelung1.sTCP_Port", language, 1)); //(k_daten.csv)
            return data;
        }

        static public IList<SingleValue> serviceMenuWPPriority()
        {

            IList<SingleValue> data = new List<SingleValue>();

            data.Add(new SingleValue("BZM_Aktiv", language, boilerParameter._WP_BZM_KASKADE.BZM_Aktiv, 0));              //0 Priority
            data.Add(new SingleValue("BZM_TWS_Laufzeit", language, boilerParameter._WP_BZM_KASKADE.BZM_TWS_Laufzeit, 0));
            data.Add(new SingleValue("BZM_HK_Laufzeit", language, boilerParameter._WP_BZM_KASKADE.BZM_HK_Laufzeit, 0));
            data.Add(new SingleValue("BZM_Kuehlen_Laufzeit", language, boilerParameter._WP_BZM_KASKADE.BZM_Kuehlen_Laufzeit, 0));
            data.Add(new SingleValue("BZM_Pool_Laufzeit", language, boilerParameter._WP_BZM_KASKADE.BZM_Pool_Laufzeit, 0));
            return data;
        }

        static public IList<SingleValue> serviceMenuWPParameter()
        {

            IList<SingleValue> data = new List<SingleValue>();

            data.Add(new SingleValue("AT_Abgleich_X42", language, boilerParameter.SYSTEMPARAMETER.AT_Abgleich_X42, 0));              //0 Allgemeine Einstellungen
            //Nur Administator Einstieg in k_daten.csv
            data.Add(new SingleValue("Bildschirmschoner_nach", language, boilerParameter.ALLGEMEINE_EINSTELLUNGEN.Bildschirmschoner_nach, 0));
            data.Add(new SingleValue("FunktionX28", language, boilerParameter.HEIZKREISPARAMETER._HeizkreisKesselregler.FunktionX28, 0));
            data.Add(new SingleValue("Kuehlventil_invers", language, boilerParameter.ALLGEMEINE_EINSTELLUNGEN.Kuehlventil_invers, 0));
            data.Add(new SingleValue("FunktionAusgangX7", language, boilerParameter.ALLGEMEINE_EINSTELLUNGEN.Funktion_Ausgang_X7, 0));
            data.Add(new SingleValue("FunktionAusgangX5", language, boilerParameter.ALLGEMEINE_EINSTELLUNGEN2.FunktionAusgangX5, 0));
            data.Add(new SingleValue("FunktionAusgangH1X7", language, boilerParameter.ALLGEMEINE_EINSTELLUNGEN2.FunktionAusgangH1X7, 0));
            data.Add(new SingleValue("FunktionAusgangH1X5", language, boilerParameter.ALLGEMEINE_EINSTELLUNGEN2.FunktionAusgangH1X5, 0));
            //Servicemenü (k_daten.csv)
            data.Add(new SingleValue("FBM_Server1.sServer_aktiv", language, 0));
            data.Add(new SingleValue("FBM_Server1.sPort", language, 0));
            data.Add(new SingleValue("Kessel1.sNurAdminEinstieg", language, 0));

            //2 Wartungstermin k_daten.csv
            data.Add(new SingleValue("WartungsterminEINAUS", language, boilerParameter.SYSTEMPARAMETER.WartungsterminEINAUS, 2));    
            data.Add(new SingleValue("Wartung_Monate", language, boilerParameter.SYSTEMPARAMETER.Wartung_Monate, 2));
            data.Add(new SingleValue("naechster_wartungstermin", language, 2)); 

           
            return data;
        }

        #endregion

        #region Regelzentrale
        static public IList<SingleValue> serviceMenuRZSollwerte()
        {

            IList<SingleValue> data = new List<SingleValue>();

            data.Add(new SingleValue("KesselreglerTyp", language, boilerParameter.HEIZKREISPARAMETER._HeizkreisKesselregler.KesselreglerTyp, 0));   //0 Kesselsollwerte
            data.Add(new SingleValue("udPEKesseltemperaturPBereich", language, boilerParameter._SAUGZUGGEBLAESE_PELLETS.udPEKesseltemperaturPBereich, 0));
            data.Add(new SingleValue("RZ_udPEMax_Luefterdrehzahl", language, boilerParameter._SAUGZUGGEBLAESE_PELLETS.udPEMax_Luefterdrehzahl, 0));
            data.Add(new SingleValue("RZ_udPEMin_Luefterdrehzahl", language, boilerParameter._SAUGZUGGEBLAESE_PELLETS.udPEMin_Luefterdrehzahl, 0));
            data.Add(new SingleValue("RZ_AusschalthystereseFK", language, boilerParameter.DREI_WEGE_MOTVENT.AusschalthystereseFK, 0));
            data.Add(new SingleValue("RZ_EinschalthystereseFK", language, boilerParameter.DREI_WEGE_MOTVENT.EinschalthystereseFK, 0));
            data.Add(new SingleValue("Moulation_plus_VltSoll", language, boilerParameter.HEIZKREISPARAMETER._HeizkreisKesselregler.Moulation_plus_VltSoll, 0));
            data.Add(new SingleValue("Moulation_minus_VltSoll", language, boilerParameter.HEIZKREISPARAMETER._HeizkreisKesselregler.Moulation_minus_VltSoll, 0));
            data.Add(new SingleValue("udmin_Kessellaufzeit", language, boilerParameter.HEIZKREISPARAMETER._HeizkreisKesselregler.udmin_Kessellaufzeit, 0));
            data.Add(new SingleValue("udMaxKesseltemperatur", language, boilerParameter.KESSELVERWALTUNG.udMaxKesseltemperatur, 0));
            data.Add(new SingleValue("Zweiter_Autokessel", language, boilerParameter.HEIZKREISPARAMETER._HeizkreisKesselregler.Zweiter_Autokessel, 0));
            data.Add(new SingleValue("Modus_Analogausgang", language, boilerParameter.HEIZKREISPARAMETER._HeizkreisKesselregler.Modus_Analogausgang, 0));
            data.Add(new SingleValue("Temp_10V", language, boilerParameter.HEIZKREISPARAMETER._HeizkreisKesselregler.Temp_10V, 0));
            data.Add(new SingleValue("Temp_0V", language, boilerParameter.HEIZKREISPARAMETER._HeizkreisKesselregler.Temp_0V, 0));
            data.Add(new SingleValue("RZ_udZeitverzoegerung_FK", language, boilerParameter.SYSTEMPARAMETER.udZeitverzoegerung_FK, 0));
            data.Add(new SingleValue("EnergiequellenTyp", language, boilerParameter.HEIZKREISPARAMETER._HeizkreisKesselregler.EnergiequellenTyp, 0));
            
            return data;
        }
        static public IList<SingleValue> serviceMenuRZErweiterungsmodul()
        {

            IList<SingleValue> data = new List<SingleValue>();

            //Erweiterungsmodul
            //Modultyp (k_daten.csv)
            data.Add(new SingleValue("CAN_Erweiterung1.sHK_XL_MOD1_Typ", language, 0));
            data.Add(new SingleValue("CAN_Erweiterung1.sHK_MOD1_Typ", language, 0));
            data.Add(new SingleValue("CAN_Erweiterung1.sHK_MOD2_Typ", language, 0));
            data.Add(new SingleValue("CAN_Erweiterung1.sHK_MOD3_Typ", language, 0));
            data.Add(new SingleValue("CAN_Erweiterung1.sFW_MOD1_Typ", language, 0));
            data.Add(new SingleValue("CAN_Erweiterung1.sFW_MOD2_Typ", language, 0));
            data.Add(new SingleValue("CAN_Erweiterung1.sFW_MOD3_Typ", language, 0));
            data.Add(new SingleValue("CAN_Erweiterung1.sFW_MOD4_Typ", language, 0));
            data.Add(new SingleValue("CAN_Erweiterung1.sDIFF_MOD1_Typ", language, 0));
            data.Add(new SingleValue("CAN_Erweiterung1.sDIFF_MOD2_Typ", language, 0));
            data.Add(new SingleValue("CAN_Erweiterung1.sDIFF_MOD3_Typ", language, 0));
            data.Add(new SingleValue("CAN_Erweiterung1.sDIFF_MOD4_Typ", language, 0));
            data.Add(new SingleValue("CAN_Erweiterung1.sSolar_MOD1_Typ", language, 0));
            data.Add(new SingleValue("CAN_Erweiterung1.sSolar_MOD2_Typ", language, 0));
            data.Add(new SingleValue("CAN_Erweiterung1.sSolar_MOD3_Typ", language, 0));
            data.Add(new SingleValue("CAN_Erweiterung1.sSolar_MOD4_Typ", language, 0));
            data.Add(new SingleValue("CAN_Erweiterung1.sSSUE_MOD1_Typ", language, 0));

            //Heizkreismodule?
            if (boilerParameter.KESSELTYP == _KESSELPARAMETER._KESSELTYP.HK_Regelung || boilerParameter.KESSELTYP == _KESSELPARAMETER._KESSELTYP.Maximus)
            {
                if (boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[0].Heizkreis_vorhanden == 1 || boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[1].Heizkreis_vorhanden == 1 || boilerParameter.HEIZKREISPARAMETER.BOILER_ARRAY[0].Boiler_vorhanden == 1 || boilerParameter.HEIZKREISPARAMETER.PUFFER_ARRAY[0].Puffer_vorhanden == 1)
                {
                    data.Add(new SingleValue("Heizkreismodul1_vorhanden", language, 1, 0));
                }
                else
                {
                    data.Add(new SingleValue("Heizkreismodul1_vorhanden", language, 0, 0));
                }

                if (boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[2].Heizkreis_vorhanden == 1 || boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[3].Heizkreis_vorhanden == 1 || boilerParameter.HEIZKREISPARAMETER.BOILER_ARRAY[1].Boiler_vorhanden == 1 || boilerParameter.HEIZKREISPARAMETER.PUFFER_ARRAY[1].Puffer_vorhanden == 1)
                {
                    data.Add(new SingleValue("Heizkreismodul2_vorhanden", language, 1, 0));
                }
                else
                {
                    data.Add(new SingleValue("Heizkreismodul2_vorhanden", language, 0, 0));
                }

                if (boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[4].Heizkreis_vorhanden == 1 || boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[5].Heizkreis_vorhanden == 1 || boilerParameter.HEIZKREISPARAMETER.BOILER_ARRAY[2].Boiler_vorhanden == 1 || boilerParameter.HEIZKREISPARAMETER.PUFFER_ARRAY[2].Puffer_vorhanden == 1)
                {
                    data.Add(new SingleValue("Heizkreismodul3_vorhanden", language, 1, 0));
                }
                else
                {
                    data.Add(new SingleValue("Heizkreismodul3_vorhanden", language, 0, 0));
                }

                if (boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[6].Heizkreis_vorhanden == 1 || boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[7].Heizkreis_vorhanden == 1 || boilerParameter.HEIZKREISPARAMETER.BOILER_ARRAY[3].Boiler_vorhanden == 1 || boilerParameter.HEIZKREISPARAMETER.PUFFER_ARRAY[3].Puffer_vorhanden == 1)
                {
                    data.Add(new SingleValue("Heizkreismodul4_vorhanden", language, 1, 0));
                }
                else
                {
                    data.Add(new SingleValue("Heizkreismodul4_vorhanden", language, 0, 0));
                }
            }
            else
            {
                if (boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[2].Heizkreis_vorhanden == 1 || boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[3].Heizkreis_vorhanden == 1 || boilerParameter.HEIZKREISPARAMETER.BOILER_ARRAY[1].Boiler_vorhanden == 1 || boilerParameter.HEIZKREISPARAMETER.PUFFER_ARRAY[1].Puffer_vorhanden == 1)
                {
                    data.Add(new SingleValue("Heizkreismodul1_vorhanden", language, 1, 0));
                }
                else
                {
                    data.Add(new SingleValue("Heizkreismodul1_vorhanden", language, 0, 0));
                }

                if (boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[4].Heizkreis_vorhanden == 1 || boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[5].Heizkreis_vorhanden == 1 || boilerParameter.HEIZKREISPARAMETER.BOILER_ARRAY[2].Boiler_vorhanden == 1 || boilerParameter.HEIZKREISPARAMETER.PUFFER_ARRAY[2].Puffer_vorhanden == 1)
                {
                    data.Add(new SingleValue("Heizkreismodul2_vorhanden", language, 1, 0));
                }
                else
                {
                    data.Add(new SingleValue("Heizkreismodul2_vorhanden", language, 0, 0));
                }

                if (boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[6].Heizkreis_vorhanden == 1 || boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[7].Heizkreis_vorhanden == 1 || boilerParameter.HEIZKREISPARAMETER.BOILER_ARRAY[3].Boiler_vorhanden == 1 || boilerParameter.HEIZKREISPARAMETER.PUFFER_ARRAY[3].Puffer_vorhanden == 1)
                {
                    data.Add(new SingleValue("Heizkreismodul3_vorhanden", language, 1, 0));
                }
                else
                {
                    data.Add(new SingleValue("Heizkreismodul3_vorhanden", language, 0, 0));
                }
                data.Add(new SingleValue("Heizkreismodul4_vorhanden", language, 0, 0));
            }

            for (int i = 0; i < 4; i++)
            {
                data.Add(new SingleValue("FWM_vorhanden_" + (i + 1), language, boilerParameter.HEIZKREISPARAMETER.FWM_ARRAY[i].FWM_vorhanden, 0));
            }
            for (int i = 0; i < 4; i++)
            {
                data.Add(new SingleValue("Solarkreis_vorhanden_" + (i + 1), language, boilerParameter.HEIZKREISPARAMETER._SOLAR_ARRAY[i].Solarkreis_vorhanden, 0));
            }
            data.Add(new SingleValue("Umschalteinheit_EinAus", language, boilerParameter.SAUGAUSTRAGUNG.Umschalteinheit, 0));
            for (int i = 0; i < 4; i++)
            {
                data.Add(new SingleValue("Differenzregelkreis_vorhanden_" + (i + 1), language, boilerParameter._DIFFERENZREGLER_ARRAY[i].Differenzregelkreis_vorhanden, 0));
            }
            data.Add(new SingleValue("Raumluft_Modul_vorhanden", language, boilerParameter.BRENNER.Raumluft_Modul_vorhanden, 0));

            data.Add(new SingleValue("FBM_Server1.sServer_aktiv", language, 1));
            data.Add(new SingleValue("FBM_Server1.sPort", language, 1));
            data.Add(new SingleValue("Kessel1.sNurAdminEinstieg", language, 1));
            data.Add(new SingleValue("CPU_Info1.sDatumBatterieGetauscht", language, 1));
           

            return data;
        }

        #endregion


        #endregion
        
    
        #region HeatingCircuits
        static public IList<MultipleValues> heatingCircuitsOperationMode()
        {

            IList<MultipleValues> data = new List<MultipleValues>();

            string[][] heatingcircuits = new string[2][];      //string[Betriebsart, Modus][heizkreis 1, 2,...]
            for (int i = 0; i < heatingcircuits.Length; i++)
            {
                heatingcircuits[i] = new string[boilerParameter.HEIZKREISPARAMETER.HK_ARRAY.Length];
            }

            for (int i = 0; i < boilerParameter.HEIZKREISPARAMETER.HK_ARRAY.Length; i++)
            {
                if (boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i].Heizkreis_vorhanden == 1)
                {
                    heatingcircuits[0][i] = boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i].HKModus.ToString();
                    heatingcircuits[1][i] = boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i].Zeitschaltung.ToString();
                }
            }

            data.Add(new MultipleValues("HKModus", language, heatingcircuits[0], 0));
            data.Add(new MultipleValues("HKBetriebsart", language, heatingcircuits[1], 0));

            return data;
        }

        static public IList<MultipleValues> ReadHeatingCircuits()
        {

            IList<MultipleValues> data = new List<MultipleValues>();

            data.Add(new MultipleValues("Heizkreis", "Heizkreis 1", "Heizkreis 2", "Heizkreis 3", "Heizkreis 4", "Heizkreis 5", "Heizkreis 6", "Heizkreis 7", "Heizkreis 8"));
            
            return data;
        }

        static public IList<MultipleValues> heatingCircuitsDaily()
        {

            IList<MultipleValues> data = new List<MultipleValues>();

            string[][] heatingcircuits = new string[5][];      //string[freigabezeit 1, 2,...][heizkreis 1, 2,...]
            for (int i = 0; i < heatingcircuits.Length; i++)
            {
                heatingcircuits[i] = new string[boilerParameter.HEIZKREISPARAMETER.HK_ARRAY.Length];
            }


            #region Einzelzeit Sonntag

            for (int i = 0; i < boilerParameter.HEIZKREISPARAMETER.HK_ARRAY.Length; i++)
            {
                if (boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i].Heizkreis_vorhanden == 1)
                {
                    heatingcircuits[0][i] = boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i]._EinzelZeiten.Sonntag.hdStart1.ToString() + boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i]._EinzelZeiten.Sonntag.hdEnde1.ToString();
                    heatingcircuits[1][i] = boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i]._EinzelZeiten.Sonntag.hdStart2.ToString() + boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i]._EinzelZeiten.Sonntag.hdEnde2.ToString();
                    heatingcircuits[2][i] = boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i]._EinzelZeiten.Sonntag.hdStart3.ToString() + boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i]._EinzelZeiten.Sonntag.hdEnde3.ToString();
                    heatingcircuits[3][i] = boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i]._EinzelZeiten.Sonntag.hdStart4.ToString() + boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i]._EinzelZeiten.Sonntag.hdEnde4.ToString();
                    heatingcircuits[4][i] = boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i]._EinzelZeiten.Sonntag.hdStart5.ToString() + boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i]._EinzelZeiten.Sonntag.hdEnde5.ToString();
                }
            }
            data.Add(new MultipleValues("hdStart1", language, heatingcircuits[0], 0));  //0 Sonntag
            data.Add(new MultipleValues("hdStart2", language, heatingcircuits[1], 0));
            data.Add(new MultipleValues("hdStart3", language, heatingcircuits[2], 0));
            data.Add(new MultipleValues("hdStart4", language, heatingcircuits[3], 0));
            data.Add(new MultipleValues("hdStart5", language, heatingcircuits[4], 0));

            for (int i = 0; i < heatingcircuits.Length; i++)
            {
                Array.Clear(heatingcircuits[i], 0, heatingcircuits[i].Length);
            }

            #endregion

            #region Einzelzeit Montag

            for (int i = 0; i < boilerParameter.HEIZKREISPARAMETER.HK_ARRAY.Length; i++)
            {
                if (boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i].Heizkreis_vorhanden == 1)
                {
                    heatingcircuits[0][i] = boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i]._EinzelZeiten.Montag.hdStart1.ToString() + boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i]._EinzelZeiten.Montag.hdEnde1.ToString();
                    heatingcircuits[1][i] = boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i]._EinzelZeiten.Montag.hdStart2.ToString() + boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i]._EinzelZeiten.Montag.hdEnde2.ToString();
                    heatingcircuits[2][i] = boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i]._EinzelZeiten.Montag.hdStart3.ToString() + boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i]._EinzelZeiten.Montag.hdEnde3.ToString();
                    heatingcircuits[3][i] = boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i]._EinzelZeiten.Montag.hdStart4.ToString() + boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i]._EinzelZeiten.Montag.hdEnde4.ToString();
                    heatingcircuits[4][i] = boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i]._EinzelZeiten.Montag.hdStart5.ToString() + boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i]._EinzelZeiten.Montag.hdEnde5.ToString();
                }

            }
            data.Add(new MultipleValues("hdStart1", language, heatingcircuits[0], 1));      //1 Montag
            data.Add(new MultipleValues("hdStart2", language, heatingcircuits[1], 1));
            data.Add(new MultipleValues("hdStart3", language, heatingcircuits[2], 1));
            data.Add(new MultipleValues("hdStart4", language, heatingcircuits[3], 1));
            data.Add(new MultipleValues("hdStart5", language, heatingcircuits[4], 1));

            for (int i = 0; i < heatingcircuits.Length; i++)
            {
                Array.Clear(heatingcircuits[i], 0, heatingcircuits[i].Length);
            }

            #endregion

            #region Einzelzeit Dienstag

            for (int i = 0; i < boilerParameter.HEIZKREISPARAMETER.HK_ARRAY.Length; i++)
            {
                if (boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i].Heizkreis_vorhanden == 1)
                {
                    heatingcircuits[0][i] = boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i]._EinzelZeiten.Dienstag.hdStart1.ToString() + boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i]._EinzelZeiten.Dienstag.hdEnde1.ToString();
                    heatingcircuits[1][i] = boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i]._EinzelZeiten.Dienstag.hdStart2.ToString() + boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i]._EinzelZeiten.Dienstag.hdEnde2.ToString();
                    heatingcircuits[2][i] = boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i]._EinzelZeiten.Dienstag.hdStart3.ToString() + boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i]._EinzelZeiten.Dienstag.hdEnde3.ToString();
                    heatingcircuits[3][i] = boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i]._EinzelZeiten.Dienstag.hdStart4.ToString() + boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i]._EinzelZeiten.Dienstag.hdEnde4.ToString();
                    heatingcircuits[4][i] = boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i]._EinzelZeiten.Dienstag.hdStart5.ToString() + boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i]._EinzelZeiten.Dienstag.hdEnde5.ToString();
                }

            }
            data.Add(new MultipleValues("hdStart1", language, heatingcircuits[0], 2));  //2 Dienstag
            data.Add(new MultipleValues("hdStart2", language, heatingcircuits[1], 2));
            data.Add(new MultipleValues("hdStart3", language, heatingcircuits[2], 2));
            data.Add(new MultipleValues("hdStart4", language, heatingcircuits[3], 2));
            data.Add(new MultipleValues("hdStart5", language, heatingcircuits[4], 2));

            for (int i = 0; i < heatingcircuits.Length; i++)
            {
                Array.Clear(heatingcircuits[i], 0, heatingcircuits[i].Length);
            }

            #endregion

            #region Einzelzeit Mittwoch

            for (int i = 0; i < boilerParameter.HEIZKREISPARAMETER.HK_ARRAY.Length; i++)
            {
                if (boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i].Heizkreis_vorhanden == 1)
                {
                    heatingcircuits[0][i] = boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i]._EinzelZeiten.Mittwoch.hdStart1.ToString() + boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i]._EinzelZeiten.Mittwoch.hdEnde1.ToString();
                    heatingcircuits[1][i] = boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i]._EinzelZeiten.Mittwoch.hdStart2.ToString() + boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i]._EinzelZeiten.Mittwoch.hdEnde2.ToString();
                    heatingcircuits[2][i] = boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i]._EinzelZeiten.Mittwoch.hdStart3.ToString() + boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i]._EinzelZeiten.Mittwoch.hdEnde3.ToString();
                    heatingcircuits[3][i] = boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i]._EinzelZeiten.Mittwoch.hdStart4.ToString() + boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i]._EinzelZeiten.Mittwoch.hdEnde4.ToString();
                    heatingcircuits[4][i] = boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i]._EinzelZeiten.Mittwoch.hdStart5.ToString() + boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i]._EinzelZeiten.Mittwoch.hdEnde5.ToString();
                }

            }
            data.Add(new MultipleValues("hdStart1", language, heatingcircuits[0], 3));  //3 Mittwoch
            data.Add(new MultipleValues("hdStart2", language, heatingcircuits[1], 3));
            data.Add(new MultipleValues("hdStart3", language, heatingcircuits[2], 3));
            data.Add(new MultipleValues("hdStart4", language, heatingcircuits[3], 3));
            data.Add(new MultipleValues("hdStart5", language, heatingcircuits[4], 3));

            for (int i = 0; i < heatingcircuits.Length; i++)
            {
                Array.Clear(heatingcircuits[i], 0, heatingcircuits[i].Length);
            }

            #endregion

            #region Einzelzeit Donnerstag

            for (int i = 0; i < boilerParameter.HEIZKREISPARAMETER.HK_ARRAY.Length; i++)
            {
                if (boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i].Heizkreis_vorhanden == 1)
                {
                    heatingcircuits[0][i] = boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i]._EinzelZeiten.Donnerstag.hdStart1.ToString() + boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i]._EinzelZeiten.Donnerstag.hdEnde1.ToString();
                    heatingcircuits[1][i] = boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i]._EinzelZeiten.Donnerstag.hdStart2.ToString() + boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i]._EinzelZeiten.Donnerstag.hdEnde2.ToString();
                    heatingcircuits[2][i] = boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i]._EinzelZeiten.Donnerstag.hdStart3.ToString() + boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i]._EinzelZeiten.Donnerstag.hdEnde3.ToString();
                    heatingcircuits[3][i] = boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i]._EinzelZeiten.Donnerstag.hdStart4.ToString() + boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i]._EinzelZeiten.Donnerstag.hdEnde4.ToString();
                    heatingcircuits[4][i] = boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i]._EinzelZeiten.Donnerstag.hdStart5.ToString() + boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i]._EinzelZeiten.Donnerstag.hdEnde5.ToString();
                }

            }
            data.Add(new MultipleValues("hdStart1", language, heatingcircuits[0], 4));  //4 Donnerstag
            data.Add(new MultipleValues("hdStart2", language, heatingcircuits[1], 4));
            data.Add(new MultipleValues("hdStart3", language, heatingcircuits[2], 4));
            data.Add(new MultipleValues("hdStart4", language, heatingcircuits[3], 4));
            data.Add(new MultipleValues("hdStart5", language, heatingcircuits[4], 4));

            for (int i = 0; i < heatingcircuits.Length; i++)
            {
                Array.Clear(heatingcircuits[i], 0, heatingcircuits[i].Length);
            }

            #endregion

            #region Einzelzeit Freitag

            for (int i = 0; i < boilerParameter.HEIZKREISPARAMETER.HK_ARRAY.Length; i++)
            {
                if (boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i].Heizkreis_vorhanden == 1)
                {
                    heatingcircuits[0][i] = boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i]._EinzelZeiten.Freitag.hdStart1.ToString() + boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i]._EinzelZeiten.Freitag.hdEnde1.ToString();
                    heatingcircuits[1][i] = boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i]._EinzelZeiten.Freitag.hdStart2.ToString() + boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i]._EinzelZeiten.Freitag.hdEnde2.ToString();
                    heatingcircuits[2][i] = boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i]._EinzelZeiten.Freitag.hdStart3.ToString() + boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i]._EinzelZeiten.Freitag.hdEnde3.ToString();
                    heatingcircuits[3][i] = boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i]._EinzelZeiten.Freitag.hdStart4.ToString() + boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i]._EinzelZeiten.Freitag.hdEnde4.ToString();
                    heatingcircuits[4][i] = boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i]._EinzelZeiten.Freitag.hdStart5.ToString() + boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i]._EinzelZeiten.Freitag.hdEnde5.ToString();
                }

            }
            data.Add(new MultipleValues("hdStart1", language, heatingcircuits[0], 5));  //5 Freitag
            data.Add(new MultipleValues("hdStart2", language, heatingcircuits[1], 5));
            data.Add(new MultipleValues("hdStart3", language, heatingcircuits[2], 5));
            data.Add(new MultipleValues("hdStart4", language, heatingcircuits[3], 5));
            data.Add(new MultipleValues("hdStart5", language, heatingcircuits[4], 5));

            for (int i = 0; i < heatingcircuits.Length; i++)
            {
                Array.Clear(heatingcircuits[i], 0, heatingcircuits[i].Length);
            }

            #endregion

            #region Einzelzeit Samstag

            for (int i = 0; i < boilerParameter.HEIZKREISPARAMETER.HK_ARRAY.Length; i++)
            {
                if (boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i].Heizkreis_vorhanden == 1)
                {
                    heatingcircuits[0][i] = boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i]._EinzelZeiten.Samstag.hdStart1.ToString() + boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i]._EinzelZeiten.Samstag.hdEnde1.ToString();
                    heatingcircuits[1][i] = boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i]._EinzelZeiten.Samstag.hdStart2.ToString() + boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i]._EinzelZeiten.Samstag.hdEnde2.ToString();
                    heatingcircuits[2][i] = boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i]._EinzelZeiten.Samstag.hdStart3.ToString() + boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i]._EinzelZeiten.Samstag.hdEnde3.ToString();
                    heatingcircuits[3][i] = boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i]._EinzelZeiten.Samstag.hdStart4.ToString() + boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i]._EinzelZeiten.Samstag.hdEnde4.ToString();
                    heatingcircuits[4][i] = boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i]._EinzelZeiten.Samstag.hdStart5.ToString() + boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i]._EinzelZeiten.Samstag.hdEnde5.ToString();
                }

            }
            data.Add(new MultipleValues("hdStart1", language, heatingcircuits[0], 6));  //6 Samstag
            data.Add(new MultipleValues("hdStart2", language, heatingcircuits[1], 6));
            data.Add(new MultipleValues("hdStart3", language, heatingcircuits[2], 6));
            data.Add(new MultipleValues("hdStart4", language, heatingcircuits[3], 6));
            data.Add(new MultipleValues("hdStart5", language, heatingcircuits[4], 6));

            #endregion

            return data;
        }

        static public IList<MultipleValues> heatingCircuitsDailyCooling()
        {

            IList<MultipleValues> data = new List<MultipleValues>();

            string[][] heatingcircuits = new string[5][];      //string[freigabezeit 1, 2,...][heizkreis 1, 2,...]
            for (int i = 0; i < heatingcircuits.Length; i++)
            {
                heatingcircuits[i] = new string[boilerParameter.HEIZKREISPARAMETER.HK_ARRAY.Length];
            }


            #region Einzelzeit Sonntag

            for (int i = 0; i < boilerParameter.HEIZKREISPARAMETER.HK_ARRAY.Length; i++)
            {
                if (boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i].Heizkreis_vorhanden == 1)
                {
                    heatingcircuits[0][i] = boilerParameter._HK_KUEHLEN[i]._Einzelzeiten.Sonntag.hdStart1.ToString() + boilerParameter._HK_KUEHLEN[i]._Einzelzeiten.Sonntag.hdEnde1.ToString();
                    heatingcircuits[1][i] = boilerParameter._HK_KUEHLEN[i]._Einzelzeiten.Sonntag.hdStart2.ToString() + boilerParameter._HK_KUEHLEN[i]._Einzelzeiten.Sonntag.hdEnde2.ToString();
                    heatingcircuits[2][i] = boilerParameter._HK_KUEHLEN[i]._Einzelzeiten.Sonntag.hdStart3.ToString() + boilerParameter._HK_KUEHLEN[i]._Einzelzeiten.Sonntag.hdEnde3.ToString();
                    heatingcircuits[3][i] = boilerParameter._HK_KUEHLEN[i]._Einzelzeiten.Sonntag.hdStart4.ToString() + boilerParameter._HK_KUEHLEN[i]._Einzelzeiten.Sonntag.hdEnde4.ToString();
                    heatingcircuits[4][i] = boilerParameter._HK_KUEHLEN[i]._Einzelzeiten.Sonntag.hdStart5.ToString() + boilerParameter._HK_KUEHLEN[i]._Einzelzeiten.Sonntag.hdEnde5.ToString();
                }

            }
            data.Add(new MultipleValues("hdStart1", language, heatingcircuits[0], 0));  //0 Sonntag
            data.Add(new MultipleValues("hdStart2", language, heatingcircuits[1], 0));
            data.Add(new MultipleValues("hdStart3", language, heatingcircuits[2], 0));
            data.Add(new MultipleValues("hdStart4", language, heatingcircuits[3], 0));
            data.Add(new MultipleValues("hdStart5", language, heatingcircuits[4], 0));

            for (int i = 0; i < heatingcircuits.Length; i++)
            {
                Array.Clear(heatingcircuits[i], 0, heatingcircuits[i].Length);
            }

            #endregion

            #region Einzelzeit Montag

            for (int i = 0; i < boilerParameter.HEIZKREISPARAMETER.HK_ARRAY.Length; i++)
            {
                if (boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i].Heizkreis_vorhanden == 1)
                {
                    heatingcircuits[0][i] = boilerParameter._HK_KUEHLEN[i]._Einzelzeiten.Montag.hdStart1.ToString() + boilerParameter._HK_KUEHLEN[i]._Einzelzeiten.Montag.hdEnde1.ToString();
                    heatingcircuits[1][i] = boilerParameter._HK_KUEHLEN[i]._Einzelzeiten.Montag.hdStart2.ToString() + boilerParameter._HK_KUEHLEN[i]._Einzelzeiten.Montag.hdEnde2.ToString();
                    heatingcircuits[2][i] = boilerParameter._HK_KUEHLEN[i]._Einzelzeiten.Montag.hdStart3.ToString() + boilerParameter._HK_KUEHLEN[i]._Einzelzeiten.Montag.hdEnde3.ToString();
                    heatingcircuits[3][i] = boilerParameter._HK_KUEHLEN[i]._Einzelzeiten.Montag.hdStart4.ToString() + boilerParameter._HK_KUEHLEN[i]._Einzelzeiten.Montag.hdEnde4.ToString();
                    heatingcircuits[4][i] = boilerParameter._HK_KUEHLEN[i]._Einzelzeiten.Montag.hdStart5.ToString() + boilerParameter._HK_KUEHLEN[i]._Einzelzeiten.Montag.hdEnde5.ToString();
                }

            }
            data.Add(new MultipleValues("hdStart1", language, heatingcircuits[0], 1));      //1 Montag
            data.Add(new MultipleValues("hdStart2", language, heatingcircuits[1], 1));
            data.Add(new MultipleValues("hdStart3", language, heatingcircuits[2], 1));
            data.Add(new MultipleValues("hdStart4", language, heatingcircuits[3], 1));
            data.Add(new MultipleValues("hdStart5", language, heatingcircuits[4], 1));

            for (int i = 0; i < heatingcircuits.Length; i++)
            {
                Array.Clear(heatingcircuits[i], 0, heatingcircuits[i].Length);
            }

            #endregion

            #region Einzelzeit Dienstag

            for (int i = 0; i < boilerParameter.HEIZKREISPARAMETER.HK_ARRAY.Length; i++)
            {
                if (boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i].Heizkreis_vorhanden == 1)
                {
                    heatingcircuits[0][i] = boilerParameter._HK_KUEHLEN[i]._Einzelzeiten.Dienstag.hdStart1.ToString() + boilerParameter._HK_KUEHLEN[i]._Einzelzeiten.Dienstag.hdEnde1.ToString();
                    heatingcircuits[1][i] = boilerParameter._HK_KUEHLEN[i]._Einzelzeiten.Dienstag.hdStart2.ToString() + boilerParameter._HK_KUEHLEN[i]._Einzelzeiten.Dienstag.hdEnde2.ToString();
                    heatingcircuits[2][i] = boilerParameter._HK_KUEHLEN[i]._Einzelzeiten.Dienstag.hdStart3.ToString() + boilerParameter._HK_KUEHLEN[i]._Einzelzeiten.Dienstag.hdEnde3.ToString();
                    heatingcircuits[3][i] = boilerParameter._HK_KUEHLEN[i]._Einzelzeiten.Dienstag.hdStart4.ToString() + boilerParameter._HK_KUEHLEN[i]._Einzelzeiten.Dienstag.hdEnde4.ToString();
                    heatingcircuits[4][i] = boilerParameter._HK_KUEHLEN[i]._Einzelzeiten.Dienstag.hdStart5.ToString() + boilerParameter._HK_KUEHLEN[i]._Einzelzeiten.Dienstag.hdEnde5.ToString();
                }

            }
            data.Add(new MultipleValues("hdStart1", language, heatingcircuits[0], 2));  //2 Dienstag
            data.Add(new MultipleValues("hdStart2", language, heatingcircuits[1], 2));
            data.Add(new MultipleValues("hdStart3", language, heatingcircuits[2], 2));
            data.Add(new MultipleValues("hdStart4", language, heatingcircuits[3], 2));
            data.Add(new MultipleValues("hdStart5", language, heatingcircuits[4], 2));

            for (int i = 0; i < heatingcircuits.Length; i++)
            {
                Array.Clear(heatingcircuits[i], 0, heatingcircuits[i].Length);
            }

            #endregion

            #region Einzelzeit Mittwoch

            for (int i = 0; i < boilerParameter.HEIZKREISPARAMETER.HK_ARRAY.Length; i++)
            {
                if (boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i].Heizkreis_vorhanden == 1)
                {
                    heatingcircuits[0][i] = boilerParameter._HK_KUEHLEN[i]._Einzelzeiten.Mittwoch.hdStart1.ToString() + boilerParameter._HK_KUEHLEN[i]._Einzelzeiten.Mittwoch.hdEnde1.ToString();
                    heatingcircuits[1][i] = boilerParameter._HK_KUEHLEN[i]._Einzelzeiten.Mittwoch.hdStart2.ToString() + boilerParameter._HK_KUEHLEN[i]._Einzelzeiten.Mittwoch.hdEnde2.ToString();
                    heatingcircuits[2][i] = boilerParameter._HK_KUEHLEN[i]._Einzelzeiten.Mittwoch.hdStart3.ToString() + boilerParameter._HK_KUEHLEN[i]._Einzelzeiten.Mittwoch.hdEnde3.ToString();
                    heatingcircuits[3][i] = boilerParameter._HK_KUEHLEN[i]._Einzelzeiten.Mittwoch.hdStart4.ToString() + boilerParameter._HK_KUEHLEN[i]._Einzelzeiten.Mittwoch.hdEnde4.ToString();
                    heatingcircuits[4][i] = boilerParameter._HK_KUEHLEN[i]._Einzelzeiten.Mittwoch.hdStart5.ToString() + boilerParameter._HK_KUEHLEN[i]._Einzelzeiten.Mittwoch.hdEnde5.ToString();
                }

            }
            data.Add(new MultipleValues("hdStart1", language, heatingcircuits[0], 3));  //3 Mittwoch
            data.Add(new MultipleValues("hdStart2", language, heatingcircuits[1], 3));
            data.Add(new MultipleValues("hdStart3", language, heatingcircuits[2], 3));
            data.Add(new MultipleValues("hdStart4", language, heatingcircuits[3], 3));
            data.Add(new MultipleValues("hdStart5", language, heatingcircuits[4], 3));

            for (int i = 0; i < heatingcircuits.Length; i++)
            {
                Array.Clear(heatingcircuits[i], 0, heatingcircuits[i].Length);
            }

            #endregion

            #region Einzelzeit Donnerstag

            for (int i = 0; i < boilerParameter.HEIZKREISPARAMETER.HK_ARRAY.Length; i++)
            {
                if (boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i].Heizkreis_vorhanden == 1)
                {
                    heatingcircuits[0][i] = boilerParameter._HK_KUEHLEN[i]._Einzelzeiten.Donnerstag.hdStart1.ToString() + boilerParameter._HK_KUEHLEN[i]._Einzelzeiten.Donnerstag.hdEnde1.ToString();
                    heatingcircuits[1][i] = boilerParameter._HK_KUEHLEN[i]._Einzelzeiten.Donnerstag.hdStart2.ToString() + boilerParameter._HK_KUEHLEN[i]._Einzelzeiten.Donnerstag.hdEnde2.ToString();
                    heatingcircuits[2][i] = boilerParameter._HK_KUEHLEN[i]._Einzelzeiten.Donnerstag.hdStart3.ToString() + boilerParameter._HK_KUEHLEN[i]._Einzelzeiten.Donnerstag.hdEnde3.ToString();
                    heatingcircuits[3][i] = boilerParameter._HK_KUEHLEN[i]._Einzelzeiten.Donnerstag.hdStart4.ToString() + boilerParameter._HK_KUEHLEN[i]._Einzelzeiten.Donnerstag.hdEnde4.ToString();
                    heatingcircuits[4][i] = boilerParameter._HK_KUEHLEN[i]._Einzelzeiten.Donnerstag.hdStart5.ToString() + boilerParameter._HK_KUEHLEN[i]._Einzelzeiten.Donnerstag.hdEnde5.ToString();
                }

            }
            data.Add(new MultipleValues("hdStart1", language, heatingcircuits[0], 4));  //4 Donnerstag
            data.Add(new MultipleValues("hdStart2", language, heatingcircuits[1], 4));
            data.Add(new MultipleValues("hdStart3", language, heatingcircuits[2], 4));
            data.Add(new MultipleValues("hdStart4", language, heatingcircuits[3], 4));
            data.Add(new MultipleValues("hdStart5", language, heatingcircuits[4], 4));

            for (int i = 0; i < heatingcircuits.Length; i++)
            {
                Array.Clear(heatingcircuits[i], 0, heatingcircuits[i].Length);
            }

            #endregion

            #region Einzelzeit Freitag

            for (int i = 0; i < boilerParameter.HEIZKREISPARAMETER.HK_ARRAY.Length; i++)
            {
                if (boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i].Heizkreis_vorhanden == 1)
                {
                    heatingcircuits[0][i] = boilerParameter._HK_KUEHLEN[i]._Einzelzeiten.Freitag.hdStart1.ToString() + boilerParameter._HK_KUEHLEN[i]._Einzelzeiten.Freitag.hdEnde1.ToString();
                    heatingcircuits[1][i] = boilerParameter._HK_KUEHLEN[i]._Einzelzeiten.Freitag.hdStart2.ToString() + boilerParameter._HK_KUEHLEN[i]._Einzelzeiten.Freitag.hdEnde2.ToString();
                    heatingcircuits[2][i] = boilerParameter._HK_KUEHLEN[i]._Einzelzeiten.Freitag.hdStart3.ToString() + boilerParameter._HK_KUEHLEN[i]._Einzelzeiten.Freitag.hdEnde3.ToString();
                    heatingcircuits[3][i] = boilerParameter._HK_KUEHLEN[i]._Einzelzeiten.Freitag.hdStart4.ToString() + boilerParameter._HK_KUEHLEN[i]._Einzelzeiten.Freitag.hdEnde4.ToString();
                    heatingcircuits[4][i] = boilerParameter._HK_KUEHLEN[i]._Einzelzeiten.Freitag.hdStart5.ToString() + boilerParameter._HK_KUEHLEN[i]._Einzelzeiten.Freitag.hdEnde5.ToString();
                }

            }
            data.Add(new MultipleValues("hdStart1", language, heatingcircuits[0], 5));  //5 Freitag
            data.Add(new MultipleValues("hdStart2", language, heatingcircuits[1], 5));
            data.Add(new MultipleValues("hdStart3", language, heatingcircuits[2], 5));
            data.Add(new MultipleValues("hdStart4", language, heatingcircuits[3], 5));
            data.Add(new MultipleValues("hdStart5", language, heatingcircuits[4], 5));

            for (int i = 0; i < heatingcircuits.Length; i++)
            {
                Array.Clear(heatingcircuits[i], 0, heatingcircuits[i].Length);
            }

            #endregion

            #region Einzelzeit Samstag

            for (int i = 0; i < boilerParameter.HEIZKREISPARAMETER.HK_ARRAY.Length; i++)
            {
                if (boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i].Heizkreis_vorhanden == 1)
                {
                    heatingcircuits[0][i] = boilerParameter._HK_KUEHLEN[i]._Einzelzeiten.Samstag.hdStart1.ToString() + boilerParameter._HK_KUEHLEN[i]._Einzelzeiten.Samstag.hdEnde1.ToString();
                    heatingcircuits[1][i] = boilerParameter._HK_KUEHLEN[i]._Einzelzeiten.Samstag.hdStart2.ToString() + boilerParameter._HK_KUEHLEN[i]._Einzelzeiten.Samstag.hdEnde2.ToString();
                    heatingcircuits[2][i] = boilerParameter._HK_KUEHLEN[i]._Einzelzeiten.Samstag.hdStart3.ToString() + boilerParameter._HK_KUEHLEN[i]._Einzelzeiten.Samstag.hdEnde3.ToString();
                    heatingcircuits[3][i] = boilerParameter._HK_KUEHLEN[i]._Einzelzeiten.Samstag.hdStart4.ToString() + boilerParameter._HK_KUEHLEN[i]._Einzelzeiten.Samstag.hdEnde4.ToString();
                    heatingcircuits[4][i] = boilerParameter._HK_KUEHLEN[i]._Einzelzeiten.Samstag.hdStart5.ToString() + boilerParameter._HK_KUEHLEN[i]._Einzelzeiten.Samstag.hdEnde5.ToString();
                }

            }
            data.Add(new MultipleValues("hdStart1", language, heatingcircuits[0], 6));  //6 Samstag
            data.Add(new MultipleValues("hdStart2", language, heatingcircuits[1], 6));
            data.Add(new MultipleValues("hdStart3", language, heatingcircuits[2], 6));
            data.Add(new MultipleValues("hdStart4", language, heatingcircuits[3], 6));
            data.Add(new MultipleValues("hdStart5", language, heatingcircuits[4], 6));

            #endregion

            return data;
        }

        static public IList<MultipleValues> heatingCircuitsBlock()
        {

            IList<MultipleValues> data = new List<MultipleValues>();

            string[][] heatingcircuits = new string[5][];      //string[freigabezeit 1, 2,...][heizkreis 1, 2,...]
            for (int i = 0; i < heatingcircuits.Length; i++)
            {
                heatingcircuits[i] = new string[boilerParameter.HEIZKREISPARAMETER.HK_ARRAY.Length];
            }

            #region Montag - Freitag

            for (int i = 0; i < boilerParameter.HEIZKREISPARAMETER.HK_ARRAY.Length; i++)
            {
                if (boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i].Heizkreis_vorhanden == 1)
                {
                    heatingcircuits[0][i] = boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i]._Blockzeiten.Mo_FrBlock.hdStart1.ToString() + boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i]._Blockzeiten.Mo_FrBlock.hdEnde1.ToString();
                    heatingcircuits[1][i] = boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i]._Blockzeiten.Mo_FrBlock.hdStart2.ToString() + boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i]._Blockzeiten.Mo_FrBlock.hdEnde2.ToString();
                    heatingcircuits[2][i] = boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i]._Blockzeiten.Mo_FrBlock.hdStart3.ToString() + boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i]._Blockzeiten.Mo_FrBlock.hdEnde3.ToString();
                    heatingcircuits[3][i] = boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i]._Blockzeiten.Mo_FrBlock.hdStart4.ToString() + boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i]._Blockzeiten.Mo_FrBlock.hdEnde4.ToString();
                    heatingcircuits[4][i] = boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i]._Blockzeiten.Mo_FrBlock.hdStart5.ToString() + boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i]._Blockzeiten.Mo_FrBlock.hdEnde5.ToString();
                }
            }
            //0 Montag - Freitag
            data.Add(new MultipleValues("hdStart1", language, heatingcircuits[0], 0));  
            data.Add(new MultipleValues("hdStart2", language, heatingcircuits[1], 0));
            data.Add(new MultipleValues("hdStart3", language, heatingcircuits[2], 0));
            data.Add(new MultipleValues("hdStart4", language, heatingcircuits[3], 0));
            data.Add(new MultipleValues("hdStart5", language, heatingcircuits[4], 0));

            for (int i = 0; i < heatingcircuits.Length; i++)
            {
                Array.Clear(heatingcircuits[i], 0, heatingcircuits[i].Length);
            }

            #endregion

            #region Samstag - Sontag

            for (int i = 0; i < boilerParameter.HEIZKREISPARAMETER.HK_ARRAY.Length; i++)
            {
                if (boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i].Heizkreis_vorhanden == 1)
                {
                    heatingcircuits[0][i] = boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i]._Blockzeiten.Sa_SoBlock.hdStart1.ToString() + boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i]._Blockzeiten.Sa_SoBlock.hdEnde1.ToString();
                    heatingcircuits[1][i] = boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i]._Blockzeiten.Sa_SoBlock.hdStart2.ToString() + boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i]._Blockzeiten.Sa_SoBlock.hdEnde2.ToString();
                    heatingcircuits[2][i] = boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i]._Blockzeiten.Sa_SoBlock.hdStart3.ToString() + boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i]._Blockzeiten.Sa_SoBlock.hdEnde3.ToString();
                    heatingcircuits[3][i] = boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i]._Blockzeiten.Sa_SoBlock.hdStart4.ToString() + boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i]._Blockzeiten.Sa_SoBlock.hdEnde4.ToString();
                    heatingcircuits[4][i] = boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i]._Blockzeiten.Sa_SoBlock.hdStart5.ToString() + boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i]._Blockzeiten.Sa_SoBlock.hdEnde5.ToString();
                }
            }
            //1 Samstag - Sonntag
            data.Add(new MultipleValues("hdStart1", language, heatingcircuits[0], 1));  
            data.Add(new MultipleValues("hdStart2", language, heatingcircuits[1], 1));
            data.Add(new MultipleValues("hdStart3", language, heatingcircuits[2], 1));
            data.Add(new MultipleValues("hdStart4", language, heatingcircuits[3], 1));
            data.Add(new MultipleValues("hdStart5", language, heatingcircuits[4], 1));

            #endregion


            return data;
        }

        static public IList<MultipleValues> heatingCircuitsBlockCooling()
        {

            IList<MultipleValues> data = new List<MultipleValues>();

            string[][] heatingcircuits = new string[5][];      //string[freigabezeit 1, 2,...][heizkreis 1, 2,...]
            for (int i = 0; i < heatingcircuits.Length; i++)
            {
                heatingcircuits[i] = new string[boilerParameter.HEIZKREISPARAMETER.HK_ARRAY.Length];
            }

            #region Montag - Freitag

            for (int i = 0; i < boilerParameter.HEIZKREISPARAMETER.HK_ARRAY.Length; i++)
            {
                if (boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i].Heizkreis_vorhanden == 1)
                {
                    heatingcircuits[0][i] = boilerParameter._HK_KUEHLEN[i]._Blockzeiten.Mo_Fr_Zeiten.hdStart1.ToString() + boilerParameter._HK_KUEHLEN[i]._Blockzeiten.Mo_Fr_Zeiten.hdEnde1.ToString();
                    heatingcircuits[1][i] = boilerParameter._HK_KUEHLEN[i]._Blockzeiten.Mo_Fr_Zeiten.hdStart2.ToString() + boilerParameter._HK_KUEHLEN[i]._Blockzeiten.Mo_Fr_Zeiten.hdEnde2.ToString();
                    heatingcircuits[2][i] = boilerParameter._HK_KUEHLEN[i]._Blockzeiten.Mo_Fr_Zeiten.hdStart3.ToString() + boilerParameter._HK_KUEHLEN[i]._Blockzeiten.Mo_Fr_Zeiten.hdEnde3.ToString();
                    heatingcircuits[3][i] = boilerParameter._HK_KUEHLEN[i]._Blockzeiten.Mo_Fr_Zeiten.hdStart4.ToString() + boilerParameter._HK_KUEHLEN[i]._Blockzeiten.Mo_Fr_Zeiten.hdEnde4.ToString();
                    heatingcircuits[4][i] = boilerParameter._HK_KUEHLEN[i]._Blockzeiten.Mo_Fr_Zeiten.hdStart5.ToString() + boilerParameter._HK_KUEHLEN[i]._Blockzeiten.Mo_Fr_Zeiten.hdEnde5.ToString();
                }
            }

            data.Add(new MultipleValues("hdStart1", language, heatingcircuits[0], 0));  //0 Montag - Freitag
            data.Add(new MultipleValues("hdStart2", language, heatingcircuits[1], 0));
            data.Add(new MultipleValues("hdStart3", language, heatingcircuits[2], 0));
            data.Add(new MultipleValues("hdStart4", language, heatingcircuits[3], 0));
            data.Add(new MultipleValues("hdStart5", language, heatingcircuits[4], 0));

            for (int i = 0; i < heatingcircuits.Length; i++)
            {
                Array.Clear(heatingcircuits[i], 0, heatingcircuits[i].Length);
            }

            #endregion

            #region Samstag - Sontag

            for (int i = 0; i < boilerParameter.HEIZKREISPARAMETER.HK_ARRAY.Length; i++)
            {
                if (boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i].Heizkreis_vorhanden == 1)
                {
                    heatingcircuits[0][i] = boilerParameter._HK_KUEHLEN[i]._Blockzeiten.Sa_So_Zeiten.hdStart1.ToString() + boilerParameter._HK_KUEHLEN[i]._Blockzeiten.Sa_So_Zeiten.hdEnde1.ToString();
                    heatingcircuits[1][i] = boilerParameter._HK_KUEHLEN[i]._Blockzeiten.Sa_So_Zeiten.hdStart2.ToString() + boilerParameter._HK_KUEHLEN[i]._Blockzeiten.Sa_So_Zeiten.hdEnde2.ToString();
                    heatingcircuits[2][i] = boilerParameter._HK_KUEHLEN[i]._Blockzeiten.Sa_So_Zeiten.hdStart3.ToString() + boilerParameter._HK_KUEHLEN[i]._Blockzeiten.Sa_So_Zeiten.hdEnde3.ToString();
                    heatingcircuits[3][i] = boilerParameter._HK_KUEHLEN[i]._Blockzeiten.Sa_So_Zeiten.hdStart4.ToString() + boilerParameter._HK_KUEHLEN[i]._Blockzeiten.Sa_So_Zeiten.hdEnde4.ToString();
                    heatingcircuits[4][i] = boilerParameter._HK_KUEHLEN[i]._Blockzeiten.Sa_So_Zeiten.hdStart5.ToString() + boilerParameter._HK_KUEHLEN[i]._Blockzeiten.Sa_So_Zeiten.hdEnde5.ToString();
                }
            }

            data.Add(new MultipleValues("hdStart1", language, heatingcircuits[0], 1));  //1 Samstag - Sonntag
            data.Add(new MultipleValues("hdStart2", language, heatingcircuits[1], 1));
            data.Add(new MultipleValues("hdStart3", language, heatingcircuits[2], 1));
            data.Add(new MultipleValues("hdStart4", language, heatingcircuits[3], 1));
            data.Add(new MultipleValues("hdStart5", language, heatingcircuits[4], 1));

            #endregion


            return data;
        }

        static public IList<MultipleValues> heatingCircuitsSettings()
        {
            //Sollte Wert hinzugefügt werden heatingcircuits[][] um 1 erhöhen, wert hinten im array einfügen und dann in richtigen Reihenfolge zum Model hinzufügen (nicht alle indexes ändern!)


            IList<MultipleValues> data = new List<MultipleValues>();

            string[][] heatingcircuits = new string[100][];
            for (int i = 0; i < heatingcircuits.Length; i++)
            {
                heatingcircuits[i] = new string[boilerParameter.HEIZKREISPARAMETER.HK_ARRAY.Length];
            }


            for (int i = 0; i < boilerParameter.HEIZKREISPARAMETER.HK_ARRAY.Length; i++)
            {
                if (boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i].Heizkreis_vorhanden == 1)
                {
                    #region Arrays Allgemeine Einstellungen
                    heatingcircuits[0][i] = boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i].SommerTemperaturHeizen.ToString();
                    heatingcircuits[1][i] = boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i].SommerTemperaturAbsenken.ToString();
                    heatingcircuits[2][i] = boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i].Frostschutztemperatur.ToString();
                    heatingcircuits[3][i] = boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i].Pufferdifferenz.ToString();
                    heatingcircuits[4][i] = boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i].udMittelwertBildungsdauer.ToString();
                    #endregion 

                    #region Arrays Heizkurve
                    heatingcircuits[5][i] = boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i].VorlaufMaximum.ToString();
                    heatingcircuits[6][i] = boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i].Min_Vorlauftemp.ToString();
                    heatingcircuits[7][i] = boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i].AngelpunktMinus15.ToString();
                    heatingcircuits[8][i] = boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i].Angelpunkt_Mitte.ToString();
                    heatingcircuits[9][i] = boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i].AngelpunktPlus15.ToString();
                    heatingcircuits[10][i] = boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i].Vorlaufabsenkung.ToString();
                    heatingcircuits[11][i] = boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i].Heizkurve_3Punkt.ToString();
                    #endregion

                    #region Arrays Heizkurve Fachpersonal
                    heatingcircuits[12][i] = boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i].VltAenderungHeizzeit1.ToString();
                    heatingcircuits[13][i] = boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i].VltAenderungHeizzeit2.ToString();
                    heatingcircuits[14][i] = boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i].VltAenderungHeizzeit3.ToString();
                    heatingcircuits[15][i] = boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i].VltAenderungHeizzeit4.ToString();
                    heatingcircuits[16][i] = boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i].VltAenderungHeizzeit5.ToString();
                    #endregion

                    #region Arrays Raumeinstellungen
                    heatingcircuits[17][i] = boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i].RaumSollHeizbetrieb1.ToString();
                    heatingcircuits[18][i] = boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i].RaumSollAbsenkbetrieb.ToString();
                    heatingcircuits[19][i] = boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i].Raumabweichungsfaktor.ToString();
                    heatingcircuits[20][i] = boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i].SchaltHystereseRT.ToString();
                    heatingcircuits[21][i] = boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i].RaumTempAbgleich.ToString();
                    #endregion

                    #region Arrays Raumeinstellungen Fachpersonal
                    heatingcircuits[22][i] = boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i].RaumSollHeizbetrieb1.ToString();
                    heatingcircuits[23][i] = boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i].RaumSollHeizbetrieb2.ToString();
                    heatingcircuits[24][i] = boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i].RaumSollHeizbetrieb3.ToString();
                    heatingcircuits[25][i] = boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i].RaumSollHeizbetrieb4.ToString();
                    heatingcircuits[26][i] = boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i].RaumSollHeizbetrieb5.ToString();
                    #endregion

                    #region Arrays Mischereinstellungen
                    heatingcircuits[27][i] = boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i].udMischerlaufzeit.ToString();
                    heatingcircuits[28][i] = boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i].udPulsdauer.ToString();
                    heatingcircuits[29][i] = boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i].udPausendauerMin.ToString();
                    heatingcircuits[30][i] = boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i].udPausendauerMax.ToString();
                    heatingcircuits[31][i] = boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i].MischerP_Bereich.ToString();
                    heatingcircuits[32][i] = boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i].AnfahrEntlastung.ToString();
                    #endregion

                    #region Arrays Estrichprogramm
                    heatingcircuits[33][i] = boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i]._Estricheinstellungen.StartTemperatur.ToString();
                    heatingcircuits[34][i] = boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i]._Estricheinstellungen.Temperaturanstieg.ToString();
                    heatingcircuits[35][i] = boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i]._Estricheinstellungen.MaximalTemperatur.ToString();
                    heatingcircuits[36][i] = boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i]._Estricheinstellungen.Temperaturabsenkung.ToString();
                    heatingcircuits[37][i] = boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i]._Estricheinstellungen.EndTemperatur.ToString();
                    heatingcircuits[38][i] = boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i]._Estricheinstellungen.udAufheizdauer.ToString();
                    heatingcircuits[39][i] = boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i]._Estricheinstellungen.udVerweilzeit.ToString();
                    heatingcircuits[40][i] = boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i]._Estricheinstellungen.udAbsenkzeit.ToString();
                    #endregion

                    #region Arrays Estrichprogramm
                    heatingcircuits[33][i] = boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i]._Estricheinstellungen.StartTemperatur.ToString();
                    heatingcircuits[34][i] = boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i]._Estricheinstellungen.Temperaturanstieg.ToString();
                    heatingcircuits[35][i] = boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i]._Estricheinstellungen.MaximalTemperatur.ToString();
                    heatingcircuits[36][i] = boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i]._Estricheinstellungen.Temperaturabsenkung.ToString();
                    heatingcircuits[37][i] = boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i]._Estricheinstellungen.EndTemperatur.ToString();
                    heatingcircuits[38][i] = boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i]._Estricheinstellungen.udAufheizdauer.ToString();
                    heatingcircuits[39][i] = boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i]._Estricheinstellungen.udVerweilzeit.ToString();
                    heatingcircuits[40][i] = boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i]._Estricheinstellungen.udAbsenkzeit.ToString();
                    #endregion

                    #region Arrays Systemparameter
                    heatingcircuits[41][i] = boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i].Kesseltemp_Min.ToString();
                    heatingcircuits[42][i] = boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i].Mischer_Anf_Starttemp.ToString();
                    heatingcircuits[43][i] = boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i].Energiequelle.ToString();
                    heatingcircuits[44][i] = boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i].Raumeinfluss.ToString();
                    heatingcircuits[45][i] = boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i].AnforderungPufferAus.ToString();
                    heatingcircuits[46][i] = boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i].Fremdkesselanforderung.ToString();
                    #endregion

                    #region Arrays Wärmeableitung
                    heatingcircuits[47][i] = boilerParameter._HK_ARRAY_ERWEITERUNG[i].Ueberwaermeabfuhr_permanent.ToString();
                    heatingcircuits[48][i] = boilerParameter._HK_ARRAY_ERWEITERUNG[i].Ueberwaermeabfuhr_Solarertrag.ToString();
                    #endregion
                }
            }

            #region Allgemeine Einstellungen
            data.Add(new MultipleValues("RaumSollAbsenkbetrieb", language, heatingcircuits[0], 0));
            data.Add(new MultipleValues("SommerTemperaturAbsenken", language, heatingcircuits[1], 0));
            data.Add(new MultipleValues("Frostschutztemperatur", language, heatingcircuits[2], 0));
            data.Add(new MultipleValues("Pufferdifferenz", language, heatingcircuits[3], 0));
            data.Add(new MultipleValues("udMittelwertBildungsdauer", language, heatingcircuits[4], 0));
            data.Add(new MultipleValues("Visu_HKParameter1.sNiedrigkuehlen_ok", language, 0));
            #endregion

            #region Heizkurve
            data.Add(new MultipleValues("VorlaufMaximum", language, heatingcircuits[5], 1));
            data.Add(new MultipleValues("Min_Vorlauftemp", language, heatingcircuits[6], 1));
            data.Add(new MultipleValues("AngelpunktMinus15", language, heatingcircuits[7], 1));
            data.Add(new MultipleValues("Angelpunkt_Mitte", language, heatingcircuits[8], 1));
            data.Add(new MultipleValues("AngelpunktPlus15", language, heatingcircuits[9], 1));
            data.Add(new MultipleValues("Vorlaufabsenkung", language, heatingcircuits[10], 1));
            data.Add(new MultipleValues("Heizkurve_3Punkt", language, heatingcircuits[11], 1));

            #endregion

            #region Heizkurve Fachpersonal
            data.Add(new MultipleValues("VltAenderungHeizzeit1", language, heatingcircuits[12], 2));
            data.Add(new MultipleValues("VltAenderungHeizzeit2", language, heatingcircuits[13], 2));
            data.Add(new MultipleValues("VltAenderungHeizzeit3", language, heatingcircuits[14], 2));
            data.Add(new MultipleValues("VltAenderungHeizzeit4", language, heatingcircuits[15], 2));
            data.Add(new MultipleValues("VltAenderungHeizzeit5", language, heatingcircuits[16], 2));
            #endregion

            if(boilerParameter.HEIZKREISPARAMETER.HK_ARRAY.Length > 0) {
                for(int i=0; i< boilerParameter.HEIZKREISPARAMETER.HK_ARRAY.Length; i++)
                {
                    if(boilerParameter.HEIZKREISPARAMETER.HK_ARRAY[i].Raumeinfluss.ToString() == "1")
                    {
                        #region Raumeinstellungen
                        data.Add(new MultipleValues("RaumSollHeizbetrieb", language, heatingcircuits[17], 3));
                        data.Add(new MultipleValues("RaumSollAbsenkbetrieb", language, heatingcircuits[18], 3));
                        data.Add(new MultipleValues("Raumabweichungsfaktor", language, heatingcircuits[19], 3));
                        data.Add(new MultipleValues("SchaltHystereseRT", language, heatingcircuits[20], 3));
                        data.Add(new MultipleValues("RaumTempAbgleich", language, heatingcircuits[21], 3));
                        #endregion

                        #region Raumeinstellungen Fachpersonal
                        data.Add(new MultipleValues("RaumSollHeizbetrieb1", language, heatingcircuits[22], 4));
                        data.Add(new MultipleValues("RaumSollHeizbetrieb2", language, heatingcircuits[23], 4));
                        data.Add(new MultipleValues("RaumSollHeizbetrieb3", language, heatingcircuits[24], 4));
                        data.Add(new MultipleValues("RaumSollHeizbetrieb4", language, heatingcircuits[25], 4));
                        data.Add(new MultipleValues("RaumSollHeizbetrieb5", language, heatingcircuits[26], 4));
                        #endregion
                    }
                }
            
            }


            #region Mischereinstellungen
            data.Add(new MultipleValues("udMischerlaufzeit", language, heatingcircuits[27], 5));
            data.Add(new MultipleValues("udPulsdauer", language, heatingcircuits[28], 5));
            data.Add(new MultipleValues("udPausendauerMin", language, heatingcircuits[29], 5));
            data.Add(new MultipleValues("udPausendauerMax", language, heatingcircuits[30], 5));
            data.Add(new MultipleValues("MischerP_Bereich", language, heatingcircuits[31], 5));
            data.Add(new MultipleValues("AnfahrEntlastung", language, heatingcircuits[32], 5));
            #endregion

            #region Estrichprogramm
            data.Add(new MultipleValues("StartTemperatur", language, heatingcircuits[33], 6));
            data.Add(new MultipleValues("Temperaturanstieg", language, heatingcircuits[34], 6));
            data.Add(new MultipleValues("MaximalTemperatur", language, heatingcircuits[35], 6));
            data.Add(new MultipleValues("Temperaturabsenkung", language, heatingcircuits[36], 6));
            data.Add(new MultipleValues("EndTemperatur", language, heatingcircuits[37], 6));
            data.Add(new MultipleValues("udAufheizdauer", language, heatingcircuits[38], 6));
            data.Add(new MultipleValues("udVerweilzeit", language, heatingcircuits[39], 6));
            data.Add(new MultipleValues("udAbsenkzeit", language, heatingcircuits[40], 6));
            #endregion

            #region Systemparameter
            data.Add(new MultipleValues("Kesseltemp_Min", language, heatingcircuits[41], 7));
            data.Add(new MultipleValues("Mischer_Anf_Starttemp", language, heatingcircuits[42], 7));
            data.Add(new MultipleValues("Energiequelle", language, heatingcircuits[43], 7));
            data.Add(new MultipleValues("Raumeinfluss", language, heatingcircuits[44], 7));
            data.Add(new MultipleValues("AnforderungPufferAus", language, heatingcircuits[45], 7));
            data.Add(new MultipleValues("Fremdkesselanforderung", language, heatingcircuits[46], 7));
            #endregion

            #region Wärmeableitung
            data.Add(new MultipleValues("Ueberwaermeabfuhr_permanent", language, heatingcircuits[47], 8));
            data.Add(new MultipleValues("Ueberwaermeabfuhr_Solarertrag", language, heatingcircuits[48], 8));
            #endregion

            return data;
        }
        #endregion

        #region DrinkwaterTanks
        static public IList<MultipleValues> drinkWaterTanksOperationMode()
        {

            IList<MultipleValues> data = new List<MultipleValues>();

            string[] drinkwatertanks = new string[boilerParameter.HEIZKREISPARAMETER.BOILER_ARRAY.Length];

            for (int i = 0; i < boilerParameter.HEIZKREISPARAMETER.BOILER_ARRAY.Length; i++)
            {
                if (boilerParameter.HEIZKREISPARAMETER.BOILER_ARRAY[i].Boiler_vorhanden == 1)
                {
                    drinkwatertanks[i] = boilerParameter.HEIZKREISPARAMETER.BOILER_ARRAY[i].FreigabeArt.ToString();
                }
            }

            data.Add(new MultipleValues("DT_FreigabeArt", language, drinkwatertanks, 0));


            return data;
        }

        static public IList<MultipleValues> drinkWaterTanksDaily()
        {

            IList<MultipleValues> data = new List<MultipleValues>();

            string[][] drinkwatertanks = new string[12][];
            for (int i = 0; i < drinkwatertanks.Length; i++)
            {
                drinkwatertanks[i] = new string[boilerParameter.HEIZKREISPARAMETER.BOILER_ARRAY.Length];
            }

            #region Sonntag

            for (int i = 0; i < boilerParameter.HEIZKREISPARAMETER.BOILER_ARRAY.Length; i++)
            {
                if (boilerParameter.HEIZKREISPARAMETER.BOILER_ARRAY[i].Boiler_vorhanden == 1)
                {
                    drinkwatertanks[0][i] = boilerParameter._BOILER_FREIGABEZEITEN[i].So_Freigabezeit.hdStart1.ToString() + boilerParameter._BOILER_FREIGABEZEITEN[i].So_Freigabezeit.hdEnde1.ToString();
                    drinkwatertanks[1][i] = boilerParameter._BOILER_FREIGABEZEITEN[i].So_Freigabezeit.hdStart2.ToString() + boilerParameter._BOILER_FREIGABEZEITEN[i].So_Freigabezeit.hdEnde2.ToString();
                    drinkwatertanks[2][i] = boilerParameter._BOILER_FREIGABEZEITEN[i].So_Freigabezeit.hdStart3.ToString() + boilerParameter._BOILER_FREIGABEZEITEN[i].So_Freigabezeit.hdEnde3.ToString();
                    drinkwatertanks[3][i] = boilerParameter._BOILER_FREIGABEZEITEN[i].So_Freigabezeit.hdStart4.ToString() + boilerParameter._BOILER_FREIGABEZEITEN[i].So_Freigabezeit.hdEnde4.ToString();
                    drinkwatertanks[4][i] = boilerParameter._BOILER_FREIGABEZEITEN[i].So_Freigabezeit.hdStart5.ToString() + boilerParameter._BOILER_FREIGABEZEITEN[i].So_Freigabezeit.hdEnde5.ToString();
                    drinkwatertanks[5][i] = boilerParameter._BOILER_FREIGABEZEITEN[i].So_Temperaturen.Solltemperatur_1.ToString();
                    drinkwatertanks[6][i] = boilerParameter._BOILER_FREIGABEZEITEN[i].So_Temperaturen.Solltemperatur_2.ToString();
                    drinkwatertanks[7][i] = boilerParameter._BOILER_FREIGABEZEITEN[i].So_Temperaturen.Solltemperatur_3.ToString();
                    drinkwatertanks[8][i] = boilerParameter._BOILER_FREIGABEZEITEN[i].So_Temperaturen.Solltemperatur_4.ToString();
                    drinkwatertanks[9][i] = boilerParameter._BOILER_FREIGABEZEITEN[i].So_Temperaturen.Solltemperatur_5.ToString();
                    drinkwatertanks[10][i] = boilerParameter._BOILER_FREIGABEZEITEN[i].So_Temperaturen.Hysterese.ToString();
                    drinkwatertanks[11][i] = boilerParameter._BOILER_FREIGABEZEITEN[i].So_Temperaturen.MinimaleTemperatur.ToString();
                }
            }

            data.Add(new MultipleValues("hdStart1", language, drinkwatertanks[0], 0));  //0 Sonntag
            data.Add(new MultipleValues("hdStart2", language, drinkwatertanks[1], 0));
            data.Add(new MultipleValues("hdStart3", language, drinkwatertanks[2], 0));
            data.Add(new MultipleValues("hdStart4", language, drinkwatertanks[3], 0));
            data.Add(new MultipleValues("hdStart5", language, drinkwatertanks[4], 0));
            data.Add(new MultipleValues("Solltemperatur_1", language, drinkwatertanks[5], 0));
            data.Add(new MultipleValues("Solltemperatur_2", language, drinkwatertanks[6], 0));
            data.Add(new MultipleValues("Solltemperatur_3", language, drinkwatertanks[7], 0));
            data.Add(new MultipleValues("Solltemperatur_4", language, drinkwatertanks[8], 0));
            data.Add(new MultipleValues("Solltemperatur_5", language, drinkwatertanks[9], 0));
            data.Add(new MultipleValues("Boiler_Hysterese", language, drinkwatertanks[10], 0));
            data.Add(new MultipleValues("Boiler_MinimaleTemperatur", language, drinkwatertanks[11], 0));

            for (int i = 0; i < drinkwatertanks.Length; i++)
            {
                Array.Clear(drinkwatertanks[i], 0, drinkwatertanks[i].Length);
            }

            #endregion

            #region Montag

            for (int i = 0; i < boilerParameter.HEIZKREISPARAMETER.BOILER_ARRAY.Length; i++)
            {
                if (boilerParameter.HEIZKREISPARAMETER.BOILER_ARRAY[i].Boiler_vorhanden == 1)
                {
                    drinkwatertanks[0][i] = boilerParameter._BOILER_FREIGABEZEITEN[i].Mo_Freigabezeit.hdStart1.ToString() + boilerParameter._BOILER_FREIGABEZEITEN[i].Mo_Freigabezeit.hdEnde1.ToString();
                    drinkwatertanks[1][i] = boilerParameter._BOILER_FREIGABEZEITEN[i].Mo_Freigabezeit.hdStart2.ToString() + boilerParameter._BOILER_FREIGABEZEITEN[i].Mo_Freigabezeit.hdEnde2.ToString();
                    drinkwatertanks[2][i] = boilerParameter._BOILER_FREIGABEZEITEN[i].Mo_Freigabezeit.hdStart3.ToString() + boilerParameter._BOILER_FREIGABEZEITEN[i].Mo_Freigabezeit.hdEnde3.ToString();
                    drinkwatertanks[3][i] = boilerParameter._BOILER_FREIGABEZEITEN[i].Mo_Freigabezeit.hdStart4.ToString() + boilerParameter._BOILER_FREIGABEZEITEN[i].Mo_Freigabezeit.hdEnde4.ToString();
                    drinkwatertanks[4][i] = boilerParameter._BOILER_FREIGABEZEITEN[i].Mo_Freigabezeit.hdStart5.ToString() + boilerParameter._BOILER_FREIGABEZEITEN[i].Mo_Freigabezeit.hdEnde5.ToString();
                    drinkwatertanks[5][i] = boilerParameter._BOILER_FREIGABEZEITEN[i].Mo_Temperaturen.Solltemperatur_1.ToString();
                    drinkwatertanks[6][i] = boilerParameter._BOILER_FREIGABEZEITEN[i].Mo_Temperaturen.Solltemperatur_2.ToString();
                    drinkwatertanks[7][i] = boilerParameter._BOILER_FREIGABEZEITEN[i].Mo_Temperaturen.Solltemperatur_3.ToString();
                    drinkwatertanks[8][i] = boilerParameter._BOILER_FREIGABEZEITEN[i].Mo_Temperaturen.Solltemperatur_4.ToString();
                    drinkwatertanks[9][i] = boilerParameter._BOILER_FREIGABEZEITEN[i].Mo_Temperaturen.Solltemperatur_5.ToString();
                    drinkwatertanks[10][i] = boilerParameter._BOILER_FREIGABEZEITEN[i].Mo_Temperaturen.Hysterese.ToString();
                    drinkwatertanks[11][i] = boilerParameter._BOILER_FREIGABEZEITEN[i].Mo_Temperaturen.MinimaleTemperatur.ToString();
                }
            }

            data.Add(new MultipleValues("hdStart1", language, drinkwatertanks[0], 1));  //1 Montag
            data.Add(new MultipleValues("hdStart2", language, drinkwatertanks[1], 1));
            data.Add(new MultipleValues("hdStart3", language, drinkwatertanks[2], 1));
            data.Add(new MultipleValues("hdStart4", language, drinkwatertanks[3], 1));
            data.Add(new MultipleValues("hdStart5", language, drinkwatertanks[4], 1));
            data.Add(new MultipleValues("Solltemperatur_1", language, drinkwatertanks[5], 1));
            data.Add(new MultipleValues("Solltemperatur_2", language, drinkwatertanks[6], 1));
            data.Add(new MultipleValues("Solltemperatur_3", language, drinkwatertanks[7], 1));
            data.Add(new MultipleValues("Solltemperatur_4", language, drinkwatertanks[8], 1));
            data.Add(new MultipleValues("Solltemperatur_5", language, drinkwatertanks[9], 1));
            data.Add(new MultipleValues("Boiler_Hysterese", language, drinkwatertanks[10], 1));
            data.Add(new MultipleValues("Boiler_MinimaleTemperatur", language, drinkwatertanks[11], 1));

            for (int i = 0; i < drinkwatertanks.Length; i++)
            {
                Array.Clear(drinkwatertanks[i], 0, drinkwatertanks[i].Length);
            }

            #endregion

            #region Dienstag

            for (int i = 0; i < boilerParameter.HEIZKREISPARAMETER.BOILER_ARRAY.Length; i++)
            {
                if (boilerParameter.HEIZKREISPARAMETER.BOILER_ARRAY[i].Boiler_vorhanden == 1)
                {
                    drinkwatertanks[0][i] = boilerParameter._BOILER_FREIGABEZEITEN[i].Di_Freigabezeit.hdStart1.ToString() + boilerParameter._BOILER_FREIGABEZEITEN[i].Di_Freigabezeit.hdEnde1.ToString();
                    drinkwatertanks[1][i] = boilerParameter._BOILER_FREIGABEZEITEN[i].Di_Freigabezeit.hdStart2.ToString() + boilerParameter._BOILER_FREIGABEZEITEN[i].Di_Freigabezeit.hdEnde2.ToString();
                    drinkwatertanks[2][i] = boilerParameter._BOILER_FREIGABEZEITEN[i].Di_Freigabezeit.hdStart3.ToString() + boilerParameter._BOILER_FREIGABEZEITEN[i].Di_Freigabezeit.hdEnde3.ToString();
                    drinkwatertanks[3][i] = boilerParameter._BOILER_FREIGABEZEITEN[i].Di_Freigabezeit.hdStart4.ToString() + boilerParameter._BOILER_FREIGABEZEITEN[i].Di_Freigabezeit.hdEnde4.ToString();
                    drinkwatertanks[4][i] = boilerParameter._BOILER_FREIGABEZEITEN[i].Di_Freigabezeit.hdStart5.ToString() + boilerParameter._BOILER_FREIGABEZEITEN[i].Di_Freigabezeit.hdEnde5.ToString();
                    drinkwatertanks[5][i] = boilerParameter._BOILER_FREIGABEZEITEN[i].Di_Temperaturen.Solltemperatur_1.ToString();
                    drinkwatertanks[6][i] = boilerParameter._BOILER_FREIGABEZEITEN[i].Di_Temperaturen.Solltemperatur_2.ToString();
                    drinkwatertanks[7][i] = boilerParameter._BOILER_FREIGABEZEITEN[i].Di_Temperaturen.Solltemperatur_3.ToString();
                    drinkwatertanks[8][i] = boilerParameter._BOILER_FREIGABEZEITEN[i].Di_Temperaturen.Solltemperatur_4.ToString();
                    drinkwatertanks[9][i] = boilerParameter._BOILER_FREIGABEZEITEN[i].Di_Temperaturen.Solltemperatur_5.ToString();
                    drinkwatertanks[10][i] = boilerParameter._BOILER_FREIGABEZEITEN[i].Di_Temperaturen.Hysterese.ToString();
                    drinkwatertanks[11][i] = boilerParameter._BOILER_FREIGABEZEITEN[i].Di_Temperaturen.MinimaleTemperatur.ToString();
                }
            }

            data.Add(new MultipleValues("hdStart1", language, drinkwatertanks[0], 2));  //2 Dienstag
            data.Add(new MultipleValues("hdStart2", language, drinkwatertanks[1], 2));
            data.Add(new MultipleValues("hdStart3", language, drinkwatertanks[2], 2));
            data.Add(new MultipleValues("hdStart4", language, drinkwatertanks[3], 2));
            data.Add(new MultipleValues("hdStart5", language, drinkwatertanks[4], 2));
            data.Add(new MultipleValues("Solltemperatur_1", language, drinkwatertanks[5], 2));
            data.Add(new MultipleValues("Solltemperatur_2", language, drinkwatertanks[6], 2));
            data.Add(new MultipleValues("Solltemperatur_3", language, drinkwatertanks[7], 2));
            data.Add(new MultipleValues("Solltemperatur_4", language, drinkwatertanks[8], 2));
            data.Add(new MultipleValues("Solltemperatur_5", language, drinkwatertanks[9], 2));
            data.Add(new MultipleValues("Boiler_Hysterese", language, drinkwatertanks[10], 2));
            data.Add(new MultipleValues("Boiler_MinimaleTemperatur", language, drinkwatertanks[11], 2));

            for (int i = 0; i < drinkwatertanks.Length; i++)
            {
                Array.Clear(drinkwatertanks[i], 0, drinkwatertanks[i].Length);
            }

            #endregion

            #region Mittwoch

            for (int i = 0; i < boilerParameter.HEIZKREISPARAMETER.BOILER_ARRAY.Length; i++)
            {
                if (boilerParameter.HEIZKREISPARAMETER.BOILER_ARRAY[i].Boiler_vorhanden == 1)
                {
                    drinkwatertanks[0][i] = boilerParameter._BOILER_FREIGABEZEITEN[i].Mi_Freigabezeit.hdStart1.ToString() + boilerParameter._BOILER_FREIGABEZEITEN[i].Mi_Freigabezeit.hdEnde1.ToString();
                    drinkwatertanks[1][i] = boilerParameter._BOILER_FREIGABEZEITEN[i].Mi_Freigabezeit.hdStart2.ToString() + boilerParameter._BOILER_FREIGABEZEITEN[i].Mi_Freigabezeit.hdEnde2.ToString();
                    drinkwatertanks[2][i] = boilerParameter._BOILER_FREIGABEZEITEN[i].Mi_Freigabezeit.hdStart3.ToString() + boilerParameter._BOILER_FREIGABEZEITEN[i].Mi_Freigabezeit.hdEnde3.ToString();
                    drinkwatertanks[3][i] = boilerParameter._BOILER_FREIGABEZEITEN[i].Mi_Freigabezeit.hdStart4.ToString() + boilerParameter._BOILER_FREIGABEZEITEN[i].Mi_Freigabezeit.hdEnde4.ToString();
                    drinkwatertanks[4][i] = boilerParameter._BOILER_FREIGABEZEITEN[i].Mi_Freigabezeit.hdStart5.ToString() + boilerParameter._BOILER_FREIGABEZEITEN[i].Mi_Freigabezeit.hdEnde5.ToString();
                    drinkwatertanks[5][i] = boilerParameter._BOILER_FREIGABEZEITEN[i].Mi_Temperaturen.Solltemperatur_1.ToString();
                    drinkwatertanks[6][i] = boilerParameter._BOILER_FREIGABEZEITEN[i].Mi_Temperaturen.Solltemperatur_2.ToString();
                    drinkwatertanks[7][i] = boilerParameter._BOILER_FREIGABEZEITEN[i].Mi_Temperaturen.Solltemperatur_3.ToString();
                    drinkwatertanks[8][i] = boilerParameter._BOILER_FREIGABEZEITEN[i].Mi_Temperaturen.Solltemperatur_4.ToString();
                    drinkwatertanks[9][i] = boilerParameter._BOILER_FREIGABEZEITEN[i].Mi_Temperaturen.Solltemperatur_5.ToString();
                    drinkwatertanks[10][i] = boilerParameter._BOILER_FREIGABEZEITEN[i].Mi_Temperaturen.Hysterese.ToString();
                    drinkwatertanks[11][i] = boilerParameter._BOILER_FREIGABEZEITEN[i].Mi_Temperaturen.MinimaleTemperatur.ToString();
                }
            }

            data.Add(new MultipleValues("hdStart1", language, drinkwatertanks[0], 3));  //3 Mittwoch
            data.Add(new MultipleValues("hdStart2", language, drinkwatertanks[1], 3));
            data.Add(new MultipleValues("hdStart3", language, drinkwatertanks[2], 3));
            data.Add(new MultipleValues("hdStart4", language, drinkwatertanks[3], 3));
            data.Add(new MultipleValues("hdStart5", language, drinkwatertanks[4], 3));
            data.Add(new MultipleValues("Solltemperatur_1", language, drinkwatertanks[5], 3));
            data.Add(new MultipleValues("Solltemperatur_2", language, drinkwatertanks[6], 3));
            data.Add(new MultipleValues("Solltemperatur_3", language, drinkwatertanks[7], 3));
            data.Add(new MultipleValues("Solltemperatur_4", language, drinkwatertanks[8], 3));
            data.Add(new MultipleValues("Solltemperatur_5", language, drinkwatertanks[9], 3));
            data.Add(new MultipleValues("Boiler_Hysterese", language, drinkwatertanks[10], 3));
            data.Add(new MultipleValues("Boiler_MinimaleTemperatur", language, drinkwatertanks[11], 3));

            for (int i = 0; i < drinkwatertanks.Length; i++)
            {
                Array.Clear(drinkwatertanks[i], 0, drinkwatertanks[i].Length);
            }

            #endregion

            #region Donnerstag

            for (int i = 0; i < boilerParameter.HEIZKREISPARAMETER.BOILER_ARRAY.Length; i++)
            {
                if (boilerParameter.HEIZKREISPARAMETER.BOILER_ARRAY[i].Boiler_vorhanden == 1)
                {
                    drinkwatertanks[0][i] = boilerParameter._BOILER_FREIGABEZEITEN[i].Do_Freigabezeit.hdStart1.ToString() + boilerParameter._BOILER_FREIGABEZEITEN[i].Do_Freigabezeit.hdEnde1.ToString();
                    drinkwatertanks[1][i] = boilerParameter._BOILER_FREIGABEZEITEN[i].Do_Freigabezeit.hdStart2.ToString() + boilerParameter._BOILER_FREIGABEZEITEN[i].Do_Freigabezeit.hdEnde2.ToString();
                    drinkwatertanks[2][i] = boilerParameter._BOILER_FREIGABEZEITEN[i].Do_Freigabezeit.hdStart3.ToString() + boilerParameter._BOILER_FREIGABEZEITEN[i].Do_Freigabezeit.hdEnde3.ToString();
                    drinkwatertanks[3][i] = boilerParameter._BOILER_FREIGABEZEITEN[i].Do_Freigabezeit.hdStart4.ToString() + boilerParameter._BOILER_FREIGABEZEITEN[i].Do_Freigabezeit.hdEnde4.ToString();
                    drinkwatertanks[4][i] = boilerParameter._BOILER_FREIGABEZEITEN[i].Do_Freigabezeit.hdStart5.ToString() + boilerParameter._BOILER_FREIGABEZEITEN[i].Do_Freigabezeit.hdEnde5.ToString();
                    drinkwatertanks[5][i] = boilerParameter._BOILER_FREIGABEZEITEN[i].Do_Temperaturen.Solltemperatur_1.ToString();
                    drinkwatertanks[6][i] = boilerParameter._BOILER_FREIGABEZEITEN[i].Do_Temperaturen.Solltemperatur_2.ToString();
                    drinkwatertanks[7][i] = boilerParameter._BOILER_FREIGABEZEITEN[i].Do_Temperaturen.Solltemperatur_3.ToString();
                    drinkwatertanks[8][i] = boilerParameter._BOILER_FREIGABEZEITEN[i].Do_Temperaturen.Solltemperatur_4.ToString();
                    drinkwatertanks[9][i] = boilerParameter._BOILER_FREIGABEZEITEN[i].Do_Temperaturen.Solltemperatur_5.ToString();
                    drinkwatertanks[10][i] = boilerParameter._BOILER_FREIGABEZEITEN[i].Do_Temperaturen.Hysterese.ToString();
                    drinkwatertanks[11][i] = boilerParameter._BOILER_FREIGABEZEITEN[i].Do_Temperaturen.MinimaleTemperatur.ToString();
                }
            }

            data.Add(new MultipleValues("hdStart1", language, drinkwatertanks[0], 4));  //4 Donnerstag
            data.Add(new MultipleValues("hdStart2", language, drinkwatertanks[1], 4));
            data.Add(new MultipleValues("hdStart3", language, drinkwatertanks[2], 4));
            data.Add(new MultipleValues("hdStart4", language, drinkwatertanks[3], 4));
            data.Add(new MultipleValues("hdStart5", language, drinkwatertanks[4], 4));
            data.Add(new MultipleValues("Solltemperatur_1", language, drinkwatertanks[5], 4));
            data.Add(new MultipleValues("Solltemperatur_2", language, drinkwatertanks[6], 4));
            data.Add(new MultipleValues("Solltemperatur_3", language, drinkwatertanks[7], 4));
            data.Add(new MultipleValues("Solltemperatur_4", language, drinkwatertanks[8], 4));
            data.Add(new MultipleValues("Solltemperatur_5", language, drinkwatertanks[9], 4));
            data.Add(new MultipleValues("Boiler_Hysterese", language, drinkwatertanks[10], 4));
            data.Add(new MultipleValues("Boiler_MinimaleTemperatur", language, drinkwatertanks[11], 4));

            for (int i = 0; i < drinkwatertanks.Length; i++)
            {
                Array.Clear(drinkwatertanks[i], 0, drinkwatertanks[i].Length);
            }

            #endregion

            #region Freitag

            for (int i = 0; i < boilerParameter.HEIZKREISPARAMETER.BOILER_ARRAY.Length; i++)
            {
                if (boilerParameter.HEIZKREISPARAMETER.BOILER_ARRAY[i].Boiler_vorhanden == 1)
                {
                    drinkwatertanks[0][i] = boilerParameter._BOILER_FREIGABEZEITEN[i].Fr_Freigabezeit.hdStart1.ToString() + boilerParameter._BOILER_FREIGABEZEITEN[i].Fr_Freigabezeit.hdEnde1.ToString();
                    drinkwatertanks[1][i] = boilerParameter._BOILER_FREIGABEZEITEN[i].Fr_Freigabezeit.hdStart2.ToString() + boilerParameter._BOILER_FREIGABEZEITEN[i].Fr_Freigabezeit.hdEnde2.ToString();
                    drinkwatertanks[2][i] = boilerParameter._BOILER_FREIGABEZEITEN[i].Fr_Freigabezeit.hdStart3.ToString() + boilerParameter._BOILER_FREIGABEZEITEN[i].Fr_Freigabezeit.hdEnde3.ToString();
                    drinkwatertanks[3][i] = boilerParameter._BOILER_FREIGABEZEITEN[i].Fr_Freigabezeit.hdStart4.ToString() + boilerParameter._BOILER_FREIGABEZEITEN[i].Fr_Freigabezeit.hdEnde4.ToString();
                    drinkwatertanks[4][i] = boilerParameter._BOILER_FREIGABEZEITEN[i].Fr_Freigabezeit.hdStart5.ToString() + boilerParameter._BOILER_FREIGABEZEITEN[i].Fr_Freigabezeit.hdEnde5.ToString();
                    drinkwatertanks[5][i] = boilerParameter._BOILER_FREIGABEZEITEN[i].Fr_Temperaturen.Solltemperatur_1.ToString();
                    drinkwatertanks[6][i] = boilerParameter._BOILER_FREIGABEZEITEN[i].Fr_Temperaturen.Solltemperatur_2.ToString();
                    drinkwatertanks[7][i] = boilerParameter._BOILER_FREIGABEZEITEN[i].Fr_Temperaturen.Solltemperatur_3.ToString();
                    drinkwatertanks[8][i] = boilerParameter._BOILER_FREIGABEZEITEN[i].Fr_Temperaturen.Solltemperatur_4.ToString();
                    drinkwatertanks[9][i] = boilerParameter._BOILER_FREIGABEZEITEN[i].Fr_Temperaturen.Solltemperatur_5.ToString();
                    drinkwatertanks[10][i] = boilerParameter._BOILER_FREIGABEZEITEN[i].Fr_Temperaturen.Hysterese.ToString();
                    drinkwatertanks[11][i] = boilerParameter._BOILER_FREIGABEZEITEN[i].Fr_Temperaturen.MinimaleTemperatur.ToString();
                }
            }

            data.Add(new MultipleValues("hdStart1", language, drinkwatertanks[0], 5));  //5 Freitag
            data.Add(new MultipleValues("hdStart2", language, drinkwatertanks[1], 5));
            data.Add(new MultipleValues("hdStart3", language, drinkwatertanks[2], 5));
            data.Add(new MultipleValues("hdStart4", language, drinkwatertanks[3], 5));
            data.Add(new MultipleValues("hdStart5", language, drinkwatertanks[4], 5));
            data.Add(new MultipleValues("Solltemperatur_1", language, drinkwatertanks[5], 5));
            data.Add(new MultipleValues("Solltemperatur_2", language, drinkwatertanks[6], 5));
            data.Add(new MultipleValues("Solltemperatur_3", language, drinkwatertanks[7], 5));
            data.Add(new MultipleValues("Solltemperatur_4", language, drinkwatertanks[8], 5));
            data.Add(new MultipleValues("Solltemperatur_5", language, drinkwatertanks[9], 5));
            data.Add(new MultipleValues("Boiler_Hysterese", language, drinkwatertanks[10], 5));
            data.Add(new MultipleValues("Boiler_MinimaleTemperatur", language, drinkwatertanks[11], 5));

            for (int i = 0; i < drinkwatertanks.Length; i++)
            {
                Array.Clear(drinkwatertanks[i], 0, drinkwatertanks[i].Length);
            }

            #endregion

            #region Samstag

            for (int i = 0; i < boilerParameter.HEIZKREISPARAMETER.BOILER_ARRAY.Length; i++)
            {
                if (boilerParameter.HEIZKREISPARAMETER.BOILER_ARRAY[i].Boiler_vorhanden == 1)
                {
                    drinkwatertanks[0][i] = boilerParameter._BOILER_FREIGABEZEITEN[i].Sa_Freigabezeit.hdStart1.ToString() + boilerParameter._BOILER_FREIGABEZEITEN[i].Sa_Freigabezeit.hdEnde1.ToString();
                    drinkwatertanks[1][i] = boilerParameter._BOILER_FREIGABEZEITEN[i].Sa_Freigabezeit.hdStart2.ToString() + boilerParameter._BOILER_FREIGABEZEITEN[i].Sa_Freigabezeit.hdEnde2.ToString();
                    drinkwatertanks[2][i] = boilerParameter._BOILER_FREIGABEZEITEN[i].Sa_Freigabezeit.hdStart3.ToString() + boilerParameter._BOILER_FREIGABEZEITEN[i].Sa_Freigabezeit.hdEnde3.ToString();
                    drinkwatertanks[3][i] = boilerParameter._BOILER_FREIGABEZEITEN[i].Sa_Freigabezeit.hdStart4.ToString() + boilerParameter._BOILER_FREIGABEZEITEN[i].Sa_Freigabezeit.hdEnde4.ToString();
                    drinkwatertanks[4][i] = boilerParameter._BOILER_FREIGABEZEITEN[i].Sa_Freigabezeit.hdStart5.ToString() + boilerParameter._BOILER_FREIGABEZEITEN[i].Sa_Freigabezeit.hdEnde5.ToString();
                    drinkwatertanks[5][i] = boilerParameter._BOILER_FREIGABEZEITEN[i].Sa_Temperaturen.Solltemperatur_1.ToString();
                    drinkwatertanks[6][i] = boilerParameter._BOILER_FREIGABEZEITEN[i].Sa_Temperaturen.Solltemperatur_2.ToString();
                    drinkwatertanks[7][i] = boilerParameter._BOILER_FREIGABEZEITEN[i].Sa_Temperaturen.Solltemperatur_3.ToString();
                    drinkwatertanks[8][i] = boilerParameter._BOILER_FREIGABEZEITEN[i].Sa_Temperaturen.Solltemperatur_4.ToString();
                    drinkwatertanks[9][i] = boilerParameter._BOILER_FREIGABEZEITEN[i].Sa_Temperaturen.Solltemperatur_5.ToString();
                    drinkwatertanks[10][i] = boilerParameter._BOILER_FREIGABEZEITEN[i].Sa_Temperaturen.Hysterese.ToString();
                    drinkwatertanks[11][i] = boilerParameter._BOILER_FREIGABEZEITEN[i].Sa_Temperaturen.MinimaleTemperatur.ToString();
                }
            }

            data.Add(new MultipleValues("hdStart1", language, drinkwatertanks[0], 6));  //6 Samstag
            data.Add(new MultipleValues("hdStart2", language, drinkwatertanks[1], 6));
            data.Add(new MultipleValues("hdStart3", language, drinkwatertanks[2], 6));
            data.Add(new MultipleValues("hdStart4", language, drinkwatertanks[3], 6));
            data.Add(new MultipleValues("hdStart5", language, drinkwatertanks[4], 6));
            data.Add(new MultipleValues("Solltemperatur_1", language, drinkwatertanks[5], 6));
            data.Add(new MultipleValues("Solltemperatur_2", language, drinkwatertanks[6], 6));
            data.Add(new MultipleValues("Solltemperatur_3", language, drinkwatertanks[7], 6));
            data.Add(new MultipleValues("Solltemperatur_4", language, drinkwatertanks[8], 6));
            data.Add(new MultipleValues("Solltemperatur_5", language, drinkwatertanks[9], 6));
            data.Add(new MultipleValues("Boiler_Hysterese", language, drinkwatertanks[10], 6));
            data.Add(new MultipleValues("Boiler_MinimaleTemperatur", language, drinkwatertanks[11], 6));

            return data;
            #endregion
        }

        static public IList<MultipleValues> drinkWaterTanksBlock()
        {

            IList<MultipleValues> data = new List<MultipleValues>();

            string[][] drinkwatertanks = new string[12][];
            for (int i = 0; i < drinkwatertanks.Length; i++)
            {
                drinkwatertanks[i] = new string[boilerParameter.HEIZKREISPARAMETER.BOILER_ARRAY.Length];
            }

            #region Montag-Freitag

            for (int i = 0; i < boilerParameter.HEIZKREISPARAMETER.BOILER_ARRAY.Length; i++)
            {
                if (boilerParameter.HEIZKREISPARAMETER.BOILER_ARRAY[i].Boiler_vorhanden == 1)
                {
                    drinkwatertanks[0][i] = boilerParameter._BOILER_FREIGABEZEITEN[i].Mo_bis_Fr_Freigabezeit.hdStart1.ToString() + boilerParameter._BOILER_FREIGABEZEITEN[i].Mo_bis_Fr_Freigabezeit.hdEnde1.ToString();
                    drinkwatertanks[1][i] = boilerParameter._BOILER_FREIGABEZEITEN[i].Mo_bis_Fr_Freigabezeit.hdStart2.ToString() + boilerParameter._BOILER_FREIGABEZEITEN[i].Mo_bis_Fr_Freigabezeit.hdEnde2.ToString();
                    drinkwatertanks[2][i] = boilerParameter._BOILER_FREIGABEZEITEN[i].Mo_bis_Fr_Freigabezeit.hdStart3.ToString() + boilerParameter._BOILER_FREIGABEZEITEN[i].Mo_bis_Fr_Freigabezeit.hdEnde3.ToString();
                    drinkwatertanks[3][i] = boilerParameter._BOILER_FREIGABEZEITEN[i].Mo_bis_Fr_Freigabezeit.hdStart4.ToString() + boilerParameter._BOILER_FREIGABEZEITEN[i].Mo_bis_Fr_Freigabezeit.hdEnde4.ToString();
                    drinkwatertanks[4][i] = boilerParameter._BOILER_FREIGABEZEITEN[i].Mo_bis_Fr_Freigabezeit.hdStart5.ToString() + boilerParameter._BOILER_FREIGABEZEITEN[i].Mo_bis_Fr_Freigabezeit.hdEnde5.ToString();
                    drinkwatertanks[5][i] = boilerParameter._BOILER_FREIGABEZEITEN[i].Mo_bis_Fr_Temperaturen.Solltemperatur_1.ToString();
                    drinkwatertanks[6][i] = boilerParameter._BOILER_FREIGABEZEITEN[i].Mo_bis_Fr_Temperaturen.Solltemperatur_2.ToString();
                    drinkwatertanks[7][i] = boilerParameter._BOILER_FREIGABEZEITEN[i].Mo_bis_Fr_Temperaturen.Solltemperatur_3.ToString();
                    drinkwatertanks[8][i] = boilerParameter._BOILER_FREIGABEZEITEN[i].Mo_bis_Fr_Temperaturen.Solltemperatur_4.ToString();
                    drinkwatertanks[9][i] = boilerParameter._BOILER_FREIGABEZEITEN[i].Mo_bis_Fr_Temperaturen.Solltemperatur_5.ToString();
                    drinkwatertanks[10][i] = boilerParameter._BOILER_FREIGABEZEITEN[i].Mo_bis_Fr_Temperaturen.Hysterese.ToString();
                    drinkwatertanks[11][i] = boilerParameter._BOILER_FREIGABEZEITEN[i].Mo_bis_Fr_Temperaturen.MinimaleTemperatur.ToString();
                }
            }

            data.Add(new MultipleValues("hdStart1", language, drinkwatertanks[0], 0));  //0 Montag - Freitag
            data.Add(new MultipleValues("hdStart2", language, drinkwatertanks[1], 0));
            data.Add(new MultipleValues("hdStart3", language, drinkwatertanks[2], 0));
            data.Add(new MultipleValues("hdStart4", language, drinkwatertanks[3], 0));
            data.Add(new MultipleValues("hdStart5", language, drinkwatertanks[4], 0));
            data.Add(new MultipleValues("Solltemperatur_1", language, drinkwatertanks[5], 0));
            data.Add(new MultipleValues("Solltemperatur_2", language, drinkwatertanks[6], 0));
            data.Add(new MultipleValues("Solltemperatur_3", language, drinkwatertanks[7], 0));
            data.Add(new MultipleValues("Solltemperatur_4", language, drinkwatertanks[8], 0));
            data.Add(new MultipleValues("Solltemperatur_5", language, drinkwatertanks[9], 0));
            data.Add(new MultipleValues("Boiler_Hysterese", language, drinkwatertanks[10], 0));
            data.Add(new MultipleValues("Boiler_MinimaleTemperatur", language, drinkwatertanks[11], 0));

            for (int i = 0; i < drinkwatertanks.Length; i++)
            {
                Array.Clear(drinkwatertanks[i], 0, drinkwatertanks[i].Length);
            }

            #endregion

            #region Samstag-Sonntag

            for (int i = 0; i < boilerParameter.HEIZKREISPARAMETER.BOILER_ARRAY.Length; i++)
            {
                if (boilerParameter.HEIZKREISPARAMETER.BOILER_ARRAY[i].Boiler_vorhanden == 1)
                {
                    drinkwatertanks[0][i] = boilerParameter._BOILER_FREIGABEZEITEN[i].Sa_bis_So_Freigabezeit.hdStart1.ToString() + boilerParameter._BOILER_FREIGABEZEITEN[i].Sa_bis_So_Freigabezeit.hdEnde1.ToString();
                    drinkwatertanks[1][i] = boilerParameter._BOILER_FREIGABEZEITEN[i].Sa_bis_So_Freigabezeit.hdStart2.ToString() + boilerParameter._BOILER_FREIGABEZEITEN[i].Sa_bis_So_Freigabezeit.hdEnde2.ToString();
                    drinkwatertanks[2][i] = boilerParameter._BOILER_FREIGABEZEITEN[i].Sa_bis_So_Freigabezeit.hdStart3.ToString() + boilerParameter._BOILER_FREIGABEZEITEN[i].Sa_bis_So_Freigabezeit.hdEnde3.ToString();
                    drinkwatertanks[3][i] = boilerParameter._BOILER_FREIGABEZEITEN[i].Sa_bis_So_Freigabezeit.hdStart4.ToString() + boilerParameter._BOILER_FREIGABEZEITEN[i].Sa_bis_So_Freigabezeit.hdEnde4.ToString();
                    drinkwatertanks[4][i] = boilerParameter._BOILER_FREIGABEZEITEN[i].Sa_bis_So_Freigabezeit.hdStart5.ToString() + boilerParameter._BOILER_FREIGABEZEITEN[i].Sa_bis_So_Freigabezeit.hdEnde5.ToString();
                    drinkwatertanks[5][i] = boilerParameter._BOILER_FREIGABEZEITEN[i].Sa_bis_So_Temperaturen.Solltemperatur_1.ToString();
                    drinkwatertanks[6][i] = boilerParameter._BOILER_FREIGABEZEITEN[i].Sa_bis_So_Temperaturen.Solltemperatur_2.ToString();
                    drinkwatertanks[7][i] = boilerParameter._BOILER_FREIGABEZEITEN[i].Sa_bis_So_Temperaturen.Solltemperatur_3.ToString();
                    drinkwatertanks[8][i] = boilerParameter._BOILER_FREIGABEZEITEN[i].Sa_bis_So_Temperaturen.Solltemperatur_4.ToString();
                    drinkwatertanks[9][i] = boilerParameter._BOILER_FREIGABEZEITEN[i].Sa_bis_So_Temperaturen.Solltemperatur_5.ToString();
                    drinkwatertanks[10][i] = boilerParameter._BOILER_FREIGABEZEITEN[i].Sa_bis_So_Temperaturen.Hysterese.ToString();
                    drinkwatertanks[11][i] = boilerParameter._BOILER_FREIGABEZEITEN[i].Sa_bis_So_Temperaturen.MinimaleTemperatur.ToString();
                }
            }

            data.Add(new MultipleValues("hdStart1", language, drinkwatertanks[0], 1));  //1 Samstag - Sonntag
            data.Add(new MultipleValues("hdStart2", language, drinkwatertanks[1], 1));
            data.Add(new MultipleValues("hdStart3", language, drinkwatertanks[2], 1));
            data.Add(new MultipleValues("hdStart4", language, drinkwatertanks[3], 1));
            data.Add(new MultipleValues("hdStart5", language, drinkwatertanks[4], 1));
            data.Add(new MultipleValues("Solltemperatur_1", language, drinkwatertanks[5], 1));
            data.Add(new MultipleValues("Solltemperatur_2", language, drinkwatertanks[6], 1));
            data.Add(new MultipleValues("Solltemperatur_3", language, drinkwatertanks[7], 1));
            data.Add(new MultipleValues("Solltemperatur_4", language, drinkwatertanks[8], 1));
            data.Add(new MultipleValues("Solltemperatur_5", language, drinkwatertanks[9], 1));
            data.Add(new MultipleValues("Boiler_Hysterese", language, drinkwatertanks[10], 1));
            data.Add(new MultipleValues("Boiler_MinimaleTemperatur", language, drinkwatertanks[11], 1));

            for (int i = 0; i < drinkwatertanks.Length; i++)
            {
                Array.Clear(drinkwatertanks[i], 0, drinkwatertanks[i].Length);
            }

            #endregion

            #region Samstag-Sonntag

            for (int i = 0; i < boilerParameter.HEIZKREISPARAMETER.BOILER_ARRAY.Length; i++)
            {
                if (boilerParameter.HEIZKREISPARAMETER.BOILER_ARRAY[i].Boiler_vorhanden == 1)
                {
                    drinkwatertanks[0][i] = boilerParameter.HEIZKREISPARAMETER.BOILER_ARRAY[i].FreigabeZeit.hdStart1.ToString() + boilerParameter.HEIZKREISPARAMETER.BOILER_ARRAY[i].FreigabeZeit.hdEnde1.ToString();
                    drinkwatertanks[1][i] = boilerParameter.HEIZKREISPARAMETER.BOILER_ARRAY[i].FreigabeZeit.hdStart2.ToString() + boilerParameter.HEIZKREISPARAMETER.BOILER_ARRAY[i].FreigabeZeit.hdEnde2.ToString();
                    drinkwatertanks[2][i] = boilerParameter.HEIZKREISPARAMETER.BOILER_ARRAY[i].FreigabeZeit.hdStart3.ToString() + boilerParameter.HEIZKREISPARAMETER.BOILER_ARRAY[i].FreigabeZeit.hdEnde3.ToString();
                    drinkwatertanks[3][i] = boilerParameter.HEIZKREISPARAMETER.BOILER_ARRAY[i].FreigabeZeit.hdStart4.ToString() + boilerParameter.HEIZKREISPARAMETER.BOILER_ARRAY[i].FreigabeZeit.hdEnde4.ToString();
                    drinkwatertanks[4][i] = boilerParameter.HEIZKREISPARAMETER.BOILER_ARRAY[i].FreigabeZeit.hdStart5.ToString() + boilerParameter.HEIZKREISPARAMETER.BOILER_ARRAY[i].FreigabeZeit.hdEnde5.ToString();
                    drinkwatertanks[5][i] = boilerParameter.HEIZKREISPARAMETER.BOILER_ARRAY[i].Solltemp1.ToString();
                    drinkwatertanks[6][i] = boilerParameter.HEIZKREISPARAMETER.BOILER_ARRAY[i].Solltemp2.ToString();
                    drinkwatertanks[7][i] = boilerParameter.HEIZKREISPARAMETER.BOILER_ARRAY[i].Solltemp3.ToString();
                    drinkwatertanks[8][i] = boilerParameter.HEIZKREISPARAMETER.BOILER_ARRAY[i].Solltemp4.ToString();
                    drinkwatertanks[9][i] = boilerParameter.HEIZKREISPARAMETER.BOILER_ARRAY[i].Solltemp5.ToString();
                    drinkwatertanks[10][i] = boilerParameter.HEIZKREISPARAMETER.BOILER_ARRAY[i].Hysterese.ToString();
                    drinkwatertanks[11][i] = boilerParameter.HEIZKREISPARAMETER.BOILER_ARRAY[i].MinimalTemp.ToString();
                }
            }

            data.Add(new MultipleValues("hdStart1", language, drinkwatertanks[0], 2));  //2 Montag - Sonntag
            data.Add(new MultipleValues("hdStart2", language, drinkwatertanks[1], 2));
            data.Add(new MultipleValues("hdStart3", language, drinkwatertanks[2], 2));
            data.Add(new MultipleValues("hdStart4", language, drinkwatertanks[3], 2));
            data.Add(new MultipleValues("hdStart5", language, drinkwatertanks[4], 2));
            data.Add(new MultipleValues("Solltemperatur_1", language, drinkwatertanks[5], 2));
            data.Add(new MultipleValues("Solltemperatur_2", language, drinkwatertanks[6], 2));
            data.Add(new MultipleValues("Solltemperatur_3", language, drinkwatertanks[7], 2));
            data.Add(new MultipleValues("Solltemperatur_4", language, drinkwatertanks[8], 2));
            data.Add(new MultipleValues("Solltemperatur_5", language, drinkwatertanks[9], 2));
            data.Add(new MultipleValues("Boiler_Hysterese", language, drinkwatertanks[10], 2));
            data.Add(new MultipleValues("Boiler_MinimaleTemperatur", language, drinkwatertanks[11], 2));

            #endregion

            return data;
        }

        static public IList<MultipleValues> drinkWaterTanksSettings()
        {

            IList<MultipleValues> data = new List<MultipleValues>();

            string[][] drinkwatertanks = new string[17][];
            for (int i = 0; i < drinkwatertanks.Length; i++)
            {
                drinkwatertanks[i] = new string[boilerParameter.HEIZKREISPARAMETER.BOILER_ARRAY.Length];
            }

            for (int i = 0; i < boilerParameter.HEIZKREISPARAMETER.BOILER_ARRAY.Length; i++)
            {
                if (boilerParameter.HEIZKREISPARAMETER.BOILER_ARRAY[i].Boiler_vorhanden == 1)
                {
                    drinkwatertanks[0][i] = boilerParameter.HEIZKREISPARAMETER.BOILER_ARRAY[i].Energiequelle.ToString();
                    drinkwatertanks[1][i] = boilerParameter.HEIZKREISPARAMETER.BOILER_ARRAY[i].TempErhoehung.ToString();
                    drinkwatertanks[2][i] = boilerParameter.HEIZKREISPARAMETER.BOILER_ARRAY[i].Ausschaltdifferenz_Anforderung.ToString();
                    drinkwatertanks[3][i] = boilerParameter.HEIZKREISPARAMETER.BOILER_ARRAY[i].KT_Min.ToString();
                    drinkwatertanks[4][i] = boilerParameter.HEIZKREISPARAMETER.BOILER_ARRAY[i].Legionellenbetrieb.ToString();
                    drinkwatertanks[5][i] = boilerParameter.HEIZKREISPARAMETER.BOILER_ARRAY[i].Legionellentemperatur.ToString();
                    drinkwatertanks[6][i] = boilerParameter.HEIZKREISPARAMETER.BOILER_ARRAY[i].TWS_Eigenschaft.ToString();
                    drinkwatertanks[7][i] = boilerParameter.HEIZKREISPARAMETER.BOILER_ARRAY[i].Laden_bei_Kesselbetrieb.ToString();
                    drinkwatertanks[8][i] = boilerParameter.HEIZKREISPARAMETER.BOILER_ARRAY[i].Pumpendifferenz.ToString();
                    drinkwatertanks[9][i] = boilerParameter.HEIZKREISPARAMETER.BOILER_ARRAY[i].AnforderungsDifferenz.ToString();
                    drinkwatertanks[10][i] = boilerParameter.HEIZKREISPARAMETER.BOILER_ARRAY[i].Fremdkesselfreigabe.ToString();
                    drinkwatertanks[11][i] = boilerParameter.HEIZKREISPARAMETER.BOILER_ARRAY[i].uiBoilerladung_Ventil.ToString();
                    drinkwatertanks[12][i] = boilerParameter.HEIZKREISPARAMETER.BOILER_ARRAY[i].Vorrang.ToString();
                    drinkwatertanks[13][i] = boilerParameter.HEIZKREISPARAMETER.BOILER_ARRAY[i].uiVentilstellung_Invers.ToString();
                    drinkwatertanks[14][i] = boilerParameter.HEIZKREISPARAMETER.BOILER_ARRAY[i].uiVentilBereitschaftTWS.ToString();
                    drinkwatertanks[15][i] = boilerParameter.HEIZKREISPARAMETER.BOILER_ARRAY[i].Ueberwaermeabfuhr_permanent.ToString();
                    drinkwatertanks[16][i] = boilerParameter.HEIZKREISPARAMETER.BOILER_ARRAY[i].Ueberwaermeabfuhr_Solarertrag.ToString();
                }
            }

            data.Add(new MultipleValues("Energiequelle", language, drinkwatertanks[0], 0));  //0 Systemparameter
            data.Add(new MultipleValues("TempErhoehung", language, drinkwatertanks[1], 0));
            data.Add(new MultipleValues("Ausschaltdifferenz_Anforderung", language, drinkwatertanks[2], 0));
            data.Add(new MultipleValues("KT_Min", language, drinkwatertanks[3], 0));
            data.Add(new MultipleValues("Legionellenbetrieb", language, drinkwatertanks[4], 0));
            data.Add(new MultipleValues("Legionellentemperatur", language, drinkwatertanks[5], 0));
            data.Add(new MultipleValues("TWS_Eigenschaft", language, drinkwatertanks[6], 0));
            data.Add(new MultipleValues("Laden_bei_Kesselbetrieb", language, drinkwatertanks[7], 0));
            data.Add(new MultipleValues("Pumpendifferenz", language, drinkwatertanks[8], 0));
            data.Add(new MultipleValues("AnforderungsDifferenz", language, drinkwatertanks[9], 0));
            data.Add(new MultipleValues("Fremdkesselfreigabe", language, drinkwatertanks[10], 0));
            data.Add(new MultipleValues("uiBoilerladung_Ventil", language, drinkwatertanks[11], 0));
            data.Add(new MultipleValues("Vorrang", language, drinkwatertanks[12], 0));
            if (boilerParameter.KESSELTYP == _KESSELPARAMETER._KESSELTYP.WP)
            {
                data.Add(new MultipleValues("uiVentilstellung_Invers", language, drinkwatertanks[13], 0));
                data.Add(new MultipleValues("uiVentilBereitschaftTWS", language, drinkwatertanks[14], 0));
            }


            data.Add(new MultipleValues("Ueberwaermeabfuhr_permanent", language, drinkwatertanks[15], 1));  //1 Wärmeableitung
            data.Add(new MultipleValues("Ueberwaermeabfuhr_Solarertrag", language, drinkwatertanks[16], 1));



            return data;
        }

        #endregion

        #region FreshWaterModuls
        static public IList<MultipleValues> freshWaterModulesSettings()
        {

            IList<MultipleValues> data = new List<MultipleValues>();

            string[][] freshwatermodules = new string[4][];
            for (int i = 0; i < freshwatermodules.Length; i++)
            {
                freshwatermodules[i] = new string[boilerParameter.HEIZKREISPARAMETER.FWM_ARRAY.Length];
            }

            for (int i = 0; i < boilerParameter.HEIZKREISPARAMETER.FWM_ARRAY.Length; i++)
            {
                if (boilerParameter.HEIZKREISPARAMETER.FWM_ARRAY[i].FWM_vorhanden == 1)
                {
                    freshwatermodules[0][i] = boilerParameter.HEIZKREISPARAMETER.FWM_ARRAY[i].Pumpenbetriebsart.ToString();
                    freshwatermodules[1][i] = boilerParameter.HEIZKREISPARAMETER.FWM_ARRAY[i].Pumpe_Man_Vorgabe.ToString();
                    freshwatermodules[2][i] = boilerParameter.HEIZKREISPARAMETER.FWM_ARRAY[i].Solltemperatur.ToString();          //?
                    freshwatermodules[3][i] = boilerParameter.HEIZKREISPARAMETER.FWM_ARRAY[i].uiZirkAccess.ToString();

                }
            }

            data.Add(new MultipleValues("Pumpenbetriebsart", language, freshwatermodules[0], 0));  //0 Systemparameter
            data.Add(new MultipleValues("Pumpe_Man_Vorgabe", language, freshwatermodules[1], 0));
            data.Add(new MultipleValues("FWM_Solltemperatur", language, freshwatermodules[2], 0));
            data.Add(new MultipleValues("uiZirkAccess", language, freshwatermodules[3], 0));



            return data;
        }

        static public IList<MultipleValues> freshWaterModulesParameters()
        {

            IList<MultipleValues> data = new List<MultipleValues>();

            string[][] freshwatermodules = new string[18][];
            for (int i = 0; i < freshwatermodules.Length; i++)
            {
                freshwatermodules[i] = new string[boilerParameter.HEIZKREISPARAMETER.FWM_ARRAY.Length];
            }

            for (int i = 0; i < boilerParameter.HEIZKREISPARAMETER.FWM_ARRAY.Length; i++)
            {
                if (boilerParameter.HEIZKREISPARAMETER.FWM_ARRAY[i].FWM_vorhanden == 1)
                {
                    //Seite 1
                    freshwatermodules[0][i] = boilerParameter.HEIZKREISPARAMETER.FWM_ARRAY[i].Hocheffizenzpumpe.ToString();     //Pumpentyp
                    if (freshwatermodules[0][i] == "0") //Standardpumpe
                    {
                        freshwatermodules[1][i] = "s" + boilerParameter.HEIZKREISPARAMETER.FWM_ARRAY[i].Wellenpaket.ToString();                       //format schierig
                        freshwatermodules[2][i] = boilerParameter.HEIZKREISPARAMETER.FWM_ARRAY[i].Bezugsfuehler_SpeicherOben.ToString();
                        // freshwatermodules[3][i] PWM_Invers bei Hocheffizienzpumpe
                        freshwatermodules[4][i] = boilerParameter.HEIZKREISPARAMETER.FWM_ARRAY[i].PID_Zeitintervall_Standartpumpe.ToString();
                        freshwatermodules[5][i] = boilerParameter.HEIZKREISPARAMETER.FWM_ARRAY[i].PID_P_Standartpumpe.ToString();
                        freshwatermodules[6][i] = boilerParameter.HEIZKREISPARAMETER.FWM_ARRAY[i].PID_I_Standartpumpe.ToString();
                        freshwatermodules[7][i] = boilerParameter.HEIZKREISPARAMETER.FWM_ARRAY[i].PID_D_Standartpumpe.ToString();
                        freshwatermodules[8][i] = boilerParameter.HEIZKREISPARAMETER.FWM_ARRAY[i].PID_DIV_P.ToString();
                        freshwatermodules[9][i] = boilerParameter.HEIZKREISPARAMETER.FWM_ARRAY[i].PID_DIV_I.ToString();
                        freshwatermodules[10][i] = boilerParameter.HEIZKREISPARAMETER.FWM_ARRAY[i].PID_DIV_D.ToString();
                        freshwatermodules[11][i] = boilerParameter.HEIZKREISPARAMETER.FWM_ARRAY[i].PID_ABS_I.ToString();
                        freshwatermodules[12][i] = boilerParameter.HEIZKREISPARAMETER.FWM_ARRAY[i].PID_Min_Standartpumpe.ToString();
                        freshwatermodules[13][i] = boilerParameter.HEIZKREISPARAMETER.FWM_ARRAY[i].PID_Max_Standartpumpe.ToString();
                        freshwatermodules[14][i] = boilerParameter.HEIZKREISPARAMETER.FWM_ARRAY[i].PID_ext_Regler.ToString();


                    }
                    else        //Hocheffizienzpumpe
                    {
                        freshwatermodules[1][i] = "h" + boilerParameter.HEIZKREISPARAMETER.FWM_ARRAY[i].PWM_Signal.ToString();
                        freshwatermodules[2][i] = boilerParameter.HEIZKREISPARAMETER.FWM_ARRAY[i].Bezugsfuehler_SpeicherOben.ToString();
                        freshwatermodules[3][i] = boilerParameter.HEIZKREISPARAMETER.FWM_ARRAY[i].PWM_Invers.ToString();
                        freshwatermodules[4][i] = boilerParameter.HEIZKREISPARAMETER.FWM_ARRAY[i].PID_Zeitintervall_HE.ToString();
                        freshwatermodules[5][i] = boilerParameter.HEIZKREISPARAMETER.FWM_ARRAY[i].PID_P_HE.ToString();
                        freshwatermodules[6][i] = boilerParameter.HEIZKREISPARAMETER.FWM_ARRAY[i].PID_I_HE.ToString();
                        freshwatermodules[7][i] = boilerParameter.HEIZKREISPARAMETER.FWM_ARRAY[i].PID_D_HE.ToString();
                        freshwatermodules[8][i] = boilerParameter.HEIZKREISPARAMETER.FWM_ARRAY[i].PID_DIV_P.ToString();
                        freshwatermodules[9][i] = boilerParameter.HEIZKREISPARAMETER.FWM_ARRAY[i].PID_DIV_I.ToString();
                        freshwatermodules[10][i] = boilerParameter.HEIZKREISPARAMETER.FWM_ARRAY[i].PID_DIV_D.ToString();
                        freshwatermodules[11][i] = boilerParameter.HEIZKREISPARAMETER.FWM_ARRAY[i].PID_ABS_I.ToString();
                        freshwatermodules[12][i] = boilerParameter.HEIZKREISPARAMETER.FWM_ARRAY[i].PID_Min_HE.ToString();
                        freshwatermodules[13][i] = boilerParameter.HEIZKREISPARAMETER.FWM_ARRAY[i].PID_Max_HE.ToString();
                        freshwatermodules[14][i] = boilerParameter.HEIZKREISPARAMETER.FWM_ARRAY[i].PID_ext_Regler.ToString();
                    }

                    //Seite 2
                    freshwatermodules[15][i] = boilerParameter.HEIZKREISPARAMETER.FWM_ARRAY[i].uiFWM_konvent.ToString();
                    freshwatermodules[16][i] = boilerParameter.HEIZKREISPARAMETER.FWM_ARRAY[i].uiMaxDF_Faktor.ToString();
                    freshwatermodules[17][i] = boilerParameter.HEIZKREISPARAMETER.FWM_ARRAY[i].uiWT_Steigung.ToString();
                }
            }

            data.Add(new MultipleValues("Hocheffizenzpumpe", language, freshwatermodules[0], 0));  //0 Seite 1
            data.Add(new MultipleValues("Ansteuerung", language, freshwatermodules[1], 0));
            data.Add(new MultipleValues("Bezugsfuehler_SpeicherOben", language, freshwatermodules[2], 0));
            data.Add(new MultipleValues("PWM_Invers", language, freshwatermodules[3], 0));
            data.Add(new MultipleValues("PID_Zeitintervall_HE", language, freshwatermodules[4], 0));
            data.Add(new MultipleValues("PID_P", language, freshwatermodules[5], 0));
            data.Add(new MultipleValues("PID_I", language, freshwatermodules[6], 0));
            data.Add(new MultipleValues("PID_D", language, freshwatermodules[7], 0));
            data.Add(new MultipleValues("PID_DIV_P", language, freshwatermodules[8], 0));
            data.Add(new MultipleValues("PID_DIV_I", language, freshwatermodules[9], 0));
            data.Add(new MultipleValues("PID_DIV_D", language, freshwatermodules[10], 0));
            data.Add(new MultipleValues("", language, freshwatermodules[11], 0));
            data.Add(new MultipleValues("PID_Min", language, freshwatermodules[12], 0));
            data.Add(new MultipleValues("PID_Max", language, freshwatermodules[13], 0));
            data.Add(new MultipleValues("PID_ext_Regler", language, freshwatermodules[14], 0));
            data.Add(new MultipleValues("uiFWM_konvent", language, freshwatermodules[15], 1));
            data.Add(new MultipleValues("uiMaxDF_Faktor", language, freshwatermodules[16], 1));
            data.Add(new MultipleValues("uiWT_Steigung", language, freshwatermodules[17], 1));



            return data;
        }

        #endregion

        #region Circulation
        static public IList<MultipleValues> circulationsSettings()
        {

            IList<MultipleValues> data = new List<MultipleValues>();

            string[][] circulations = new string[5][];
            for (int i = 0; i < circulations.Length; i++)
            {
                circulations[i] = new string[boilerParameter._ZIRKULATIONS_ARRAY.Length];
            }

            for (int i = 0; i < boilerParameter._ZIRKULATIONS_ARRAY.Length; i++)
            {
                if (boilerParameter._ZIRKULATIONS_ARRAY[i].Zirkulation_Vorhanden == 1)
                {
                    circulations[0][i] = boilerParameter._ZIRKULATIONS_ARRAY[i].FreigabeArt.ToString();
                    circulations[1][i] = boilerParameter._ZIRKULATIONS_ARRAY[i].udPumpenImpuls.ToString();            //einschaltdauer ?
                    circulations[2][i] = boilerParameter._ZIRKULATIONS_ARRAY[i].udPumpenPause.ToString();            //wartezeit ?
                    circulations[3][i] = boilerParameter._ZIRKULATIONS_ARRAY[i].WarmwasserSollTemp.ToString();
                    circulations[4][i] = boilerParameter._ZIRKULATIONS_ARRAY[i].Kontrollimpuls.ToString();

                }
            }

            data.Add(new MultipleValues("Zk_FreigabeArt", language, circulations[0], 0));
            data.Add(new MultipleValues("udPumpenImpuls", language, circulations[1], 0));
            data.Add(new MultipleValues("udPumpenPause", language, circulations[2], 0));
            data.Add(new MultipleValues("WarmwasserSollTemp", language, circulations[3], 0));
            data.Add(new MultipleValues("Kontrollimpuls", language, circulations[4], 0));



            return data;
        }
        static public IList<MultipleValues> circulationsDaily()
        {

            IList<MultipleValues> data = new List<MultipleValues>();

            string[][] circulations = new string[5][];
            for (int i = 0; i < circulations.Length; i++)
            {
                circulations[i] = new string[boilerParameter._ZIRKULATIONS_ARRAY.Length];
            }

            #region Sonntag

            for (int i = 0; i < boilerParameter._ZIRKULATIONS_ARRAY.Length; i++)
            {
                if (boilerParameter._ZIRKULATIONS_ARRAY[i].Zirkulation_Vorhanden == 1)
                {
                    circulations[0][i] = boilerParameter._ZIRKULATION_FREIGABEZEITEN[i].So_Freigabezeit.hdStart1.ToString() + boilerParameter._ZIRKULATION_FREIGABEZEITEN[i].So_Freigabezeit.hdEnde1.ToString();
                    circulations[1][i] = boilerParameter._ZIRKULATION_FREIGABEZEITEN[i].So_Freigabezeit.hdStart2.ToString() + boilerParameter._ZIRKULATION_FREIGABEZEITEN[i].So_Freigabezeit.hdEnde2.ToString();
                    circulations[2][i] = boilerParameter._ZIRKULATION_FREIGABEZEITEN[i].So_Freigabezeit.hdStart3.ToString() + boilerParameter._ZIRKULATION_FREIGABEZEITEN[i].So_Freigabezeit.hdEnde3.ToString();
                    circulations[3][i] = boilerParameter._ZIRKULATION_FREIGABEZEITEN[i].So_Freigabezeit.hdStart4.ToString() + boilerParameter._ZIRKULATION_FREIGABEZEITEN[i].So_Freigabezeit.hdEnde4.ToString();
                    circulations[4][i] = boilerParameter._ZIRKULATION_FREIGABEZEITEN[i].So_Freigabezeit.hdStart5.ToString() + boilerParameter._ZIRKULATION_FREIGABEZEITEN[i].So_Freigabezeit.hdEnde5.ToString();

                }
            }

            data.Add(new MultipleValues("hdStart1", language, circulations[0], 0));     //0 Sonntag
            data.Add(new MultipleValues("hdStart2", language, circulations[1], 0));
            data.Add(new MultipleValues("hdStart3", language, circulations[2], 0));
            data.Add(new MultipleValues("hdStart4", language, circulations[3], 0));
            data.Add(new MultipleValues("hdStart5", language, circulations[3], 0));

            for (int i = 0; i < circulations.Length; i++)
            {
                Array.Clear(circulations[i], 0, circulations[i].Length);
            }

            #endregion

            #region Montag

            for (int i = 0; i < boilerParameter._ZIRKULATIONS_ARRAY.Length; i++)
            {
                if (boilerParameter._ZIRKULATIONS_ARRAY[i].Zirkulation_Vorhanden == 1)
                {
                    circulations[0][i] = boilerParameter._ZIRKULATION_FREIGABEZEITEN[i].Mo_Freigabezeit.hdStart1.ToString() + boilerParameter._ZIRKULATION_FREIGABEZEITEN[i].Mo_Freigabezeit.hdEnde1.ToString();
                    circulations[1][i] = boilerParameter._ZIRKULATION_FREIGABEZEITEN[i].Mo_Freigabezeit.hdStart2.ToString() + boilerParameter._ZIRKULATION_FREIGABEZEITEN[i].Mo_Freigabezeit.hdEnde2.ToString();
                    circulations[2][i] = boilerParameter._ZIRKULATION_FREIGABEZEITEN[i].Mo_Freigabezeit.hdStart3.ToString() + boilerParameter._ZIRKULATION_FREIGABEZEITEN[i].Mo_Freigabezeit.hdEnde3.ToString();
                    circulations[3][i] = boilerParameter._ZIRKULATION_FREIGABEZEITEN[i].Mo_Freigabezeit.hdStart4.ToString() + boilerParameter._ZIRKULATION_FREIGABEZEITEN[i].Mo_Freigabezeit.hdEnde4.ToString();
                    circulations[4][i] = boilerParameter._ZIRKULATION_FREIGABEZEITEN[i].Mo_Freigabezeit.hdStart5.ToString() + boilerParameter._ZIRKULATION_FREIGABEZEITEN[i].Mo_Freigabezeit.hdEnde5.ToString();

                }
            }

            data.Add(new MultipleValues("hdStart1", language, circulations[0], 1));     //1 Montag
            data.Add(new MultipleValues("hdStart2", language, circulations[1], 1));
            data.Add(new MultipleValues("hdStart3", language, circulations[2], 1));
            data.Add(new MultipleValues("hdStart4", language, circulations[3], 1));
            data.Add(new MultipleValues("hdStart5", language, circulations[3], 1));

            for (int i = 0; i < circulations.Length; i++)
            {
                Array.Clear(circulations[i], 0, circulations[i].Length);
            }

            #endregion

            #region Dienstag

            for (int i = 0; i < boilerParameter._ZIRKULATIONS_ARRAY.Length; i++)
            {
                if (boilerParameter._ZIRKULATIONS_ARRAY[i].Zirkulation_Vorhanden == 1)
                {
                    circulations[0][i] = boilerParameter._ZIRKULATION_FREIGABEZEITEN[i].Di_Freigabezeit.hdStart1.ToString() + boilerParameter._ZIRKULATION_FREIGABEZEITEN[i].Di_Freigabezeit.hdEnde1.ToString();
                    circulations[1][i] = boilerParameter._ZIRKULATION_FREIGABEZEITEN[i].Di_Freigabezeit.hdStart2.ToString() + boilerParameter._ZIRKULATION_FREIGABEZEITEN[i].Di_Freigabezeit.hdEnde2.ToString();
                    circulations[2][i] = boilerParameter._ZIRKULATION_FREIGABEZEITEN[i].Di_Freigabezeit.hdStart3.ToString() + boilerParameter._ZIRKULATION_FREIGABEZEITEN[i].Di_Freigabezeit.hdEnde3.ToString();
                    circulations[3][i] = boilerParameter._ZIRKULATION_FREIGABEZEITEN[i].Di_Freigabezeit.hdStart4.ToString() + boilerParameter._ZIRKULATION_FREIGABEZEITEN[i].Di_Freigabezeit.hdEnde4.ToString();
                    circulations[4][i] = boilerParameter._ZIRKULATION_FREIGABEZEITEN[i].Di_Freigabezeit.hdStart5.ToString() + boilerParameter._ZIRKULATION_FREIGABEZEITEN[i].Di_Freigabezeit.hdEnde5.ToString();

                }
            }

            data.Add(new MultipleValues("hdStart1", language, circulations[0], 2));     //2 Dienstag
            data.Add(new MultipleValues("hdStart2", language, circulations[1], 2));
            data.Add(new MultipleValues("hdStart3", language, circulations[2], 2));
            data.Add(new MultipleValues("hdStart4", language, circulations[3], 2));
            data.Add(new MultipleValues("hdStart5", language, circulations[3], 2));

            for (int i = 0; i < circulations.Length; i++)
            {
                Array.Clear(circulations[i], 0, circulations[i].Length);
            }

            #endregion

            #region Mittwoch

            for (int i = 0; i < boilerParameter._ZIRKULATIONS_ARRAY.Length; i++)
            {
                if (boilerParameter._ZIRKULATIONS_ARRAY[i].Zirkulation_Vorhanden == 1)
                {
                    circulations[0][i] = boilerParameter._ZIRKULATION_FREIGABEZEITEN[i].Mi_Freigabezeit.hdStart1.ToString() + boilerParameter._ZIRKULATION_FREIGABEZEITEN[i].Mi_Freigabezeit.hdEnde1.ToString();
                    circulations[1][i] = boilerParameter._ZIRKULATION_FREIGABEZEITEN[i].Mi_Freigabezeit.hdStart2.ToString() + boilerParameter._ZIRKULATION_FREIGABEZEITEN[i].Mi_Freigabezeit.hdEnde2.ToString();
                    circulations[2][i] = boilerParameter._ZIRKULATION_FREIGABEZEITEN[i].Mi_Freigabezeit.hdStart3.ToString() + boilerParameter._ZIRKULATION_FREIGABEZEITEN[i].Mi_Freigabezeit.hdEnde3.ToString();
                    circulations[3][i] = boilerParameter._ZIRKULATION_FREIGABEZEITEN[i].Mi_Freigabezeit.hdStart4.ToString() + boilerParameter._ZIRKULATION_FREIGABEZEITEN[i].Mi_Freigabezeit.hdEnde4.ToString();
                    circulations[4][i] = boilerParameter._ZIRKULATION_FREIGABEZEITEN[i].Mi_Freigabezeit.hdStart5.ToString() + boilerParameter._ZIRKULATION_FREIGABEZEITEN[i].Mi_Freigabezeit.hdEnde5.ToString();

                }
            }

            data.Add(new MultipleValues("hdStart1", language, circulations[0], 3));     //3 Mittwoch
            data.Add(new MultipleValues("hdStart2", language, circulations[1], 3));
            data.Add(new MultipleValues("hdStart3", language, circulations[2], 3));
            data.Add(new MultipleValues("hdStart4", language, circulations[3], 3));
            data.Add(new MultipleValues("hdStart5", language, circulations[3], 3));

            for (int i = 0; i < circulations.Length; i++)
            {
                Array.Clear(circulations[i], 0, circulations[i].Length);
            }

            #endregion

            #region Donnerstag

            for (int i = 0; i < boilerParameter._ZIRKULATIONS_ARRAY.Length; i++)
            {
                if (boilerParameter._ZIRKULATIONS_ARRAY[i].Zirkulation_Vorhanden == 1)
                {
                    circulations[0][i] = boilerParameter._ZIRKULATION_FREIGABEZEITEN[i].Do_Freigabezeit.hdStart1.ToString() + boilerParameter._ZIRKULATION_FREIGABEZEITEN[i].Do_Freigabezeit.hdEnde1.ToString();
                    circulations[1][i] = boilerParameter._ZIRKULATION_FREIGABEZEITEN[i].Do_Freigabezeit.hdStart2.ToString() + boilerParameter._ZIRKULATION_FREIGABEZEITEN[i].Do_Freigabezeit.hdEnde2.ToString();
                    circulations[2][i] = boilerParameter._ZIRKULATION_FREIGABEZEITEN[i].Do_Freigabezeit.hdStart3.ToString() + boilerParameter._ZIRKULATION_FREIGABEZEITEN[i].Do_Freigabezeit.hdEnde3.ToString();
                    circulations[3][i] = boilerParameter._ZIRKULATION_FREIGABEZEITEN[i].Do_Freigabezeit.hdStart4.ToString() + boilerParameter._ZIRKULATION_FREIGABEZEITEN[i].Do_Freigabezeit.hdEnde4.ToString();
                    circulations[4][i] = boilerParameter._ZIRKULATION_FREIGABEZEITEN[i].Do_Freigabezeit.hdStart5.ToString() + boilerParameter._ZIRKULATION_FREIGABEZEITEN[i].Do_Freigabezeit.hdEnde5.ToString();

                }
            }

            data.Add(new MultipleValues("hdStart1", language, circulations[0], 4));     //4 Donnerstag
            data.Add(new MultipleValues("hdStart2", language, circulations[1], 4));
            data.Add(new MultipleValues("hdStart3", language, circulations[2], 4));
            data.Add(new MultipleValues("hdStart4", language, circulations[3], 4));
            data.Add(new MultipleValues("hdStart5", language, circulations[3], 4));

            for (int i = 0; i < circulations.Length; i++)
            {
                Array.Clear(circulations[i], 0, circulations[i].Length);
            }

            #endregion

            #region Freitag

            for (int i = 0; i < boilerParameter._ZIRKULATIONS_ARRAY.Length; i++)
            {
                if (boilerParameter._ZIRKULATIONS_ARRAY[i].Zirkulation_Vorhanden == 1)
                {
                    circulations[0][i] = boilerParameter._ZIRKULATION_FREIGABEZEITEN[i].Fr_Freigabezeit.hdStart1.ToString() + boilerParameter._ZIRKULATION_FREIGABEZEITEN[i].Fr_Freigabezeit.hdEnde1.ToString();
                    circulations[1][i] = boilerParameter._ZIRKULATION_FREIGABEZEITEN[i].Fr_Freigabezeit.hdStart2.ToString() + boilerParameter._ZIRKULATION_FREIGABEZEITEN[i].Fr_Freigabezeit.hdEnde2.ToString();
                    circulations[2][i] = boilerParameter._ZIRKULATION_FREIGABEZEITEN[i].Fr_Freigabezeit.hdStart3.ToString() + boilerParameter._ZIRKULATION_FREIGABEZEITEN[i].Fr_Freigabezeit.hdEnde3.ToString();
                    circulations[3][i] = boilerParameter._ZIRKULATION_FREIGABEZEITEN[i].Fr_Freigabezeit.hdStart4.ToString() + boilerParameter._ZIRKULATION_FREIGABEZEITEN[i].Fr_Freigabezeit.hdEnde4.ToString();
                    circulations[4][i] = boilerParameter._ZIRKULATION_FREIGABEZEITEN[i].Fr_Freigabezeit.hdStart5.ToString() + boilerParameter._ZIRKULATION_FREIGABEZEITEN[i].Fr_Freigabezeit.hdEnde5.ToString();

                }
            }

            data.Add(new MultipleValues("hdStart1", language, circulations[0], 5));     //5 Freitag
            data.Add(new MultipleValues("hdStart2", language, circulations[1], 5));
            data.Add(new MultipleValues("hdStart3", language, circulations[2], 5));
            data.Add(new MultipleValues("hdStart4", language, circulations[3], 5));
            data.Add(new MultipleValues("hdStart5", language, circulations[3], 5));

            for (int i = 0; i < circulations.Length; i++)
            {
                Array.Clear(circulations[i], 0, circulations[i].Length);
            }

            #endregion

            #region Samstag

            for (int i = 0; i < boilerParameter._ZIRKULATIONS_ARRAY.Length; i++)
            {
                if (boilerParameter._ZIRKULATIONS_ARRAY[i].Zirkulation_Vorhanden == 1)
                {
                    circulations[0][i] = boilerParameter._ZIRKULATION_FREIGABEZEITEN[i].Sa_Freigabezeit.hdStart1.ToString() + boilerParameter._ZIRKULATION_FREIGABEZEITEN[i].Sa_Freigabezeit.hdEnde1.ToString();
                    circulations[1][i] = boilerParameter._ZIRKULATION_FREIGABEZEITEN[i].Sa_Freigabezeit.hdStart2.ToString() + boilerParameter._ZIRKULATION_FREIGABEZEITEN[i].Sa_Freigabezeit.hdEnde2.ToString();
                    circulations[2][i] = boilerParameter._ZIRKULATION_FREIGABEZEITEN[i].Sa_Freigabezeit.hdStart3.ToString() + boilerParameter._ZIRKULATION_FREIGABEZEITEN[i].Sa_Freigabezeit.hdEnde3.ToString();
                    circulations[3][i] = boilerParameter._ZIRKULATION_FREIGABEZEITEN[i].Sa_Freigabezeit.hdStart4.ToString() + boilerParameter._ZIRKULATION_FREIGABEZEITEN[i].Sa_Freigabezeit.hdEnde4.ToString();
                    circulations[4][i] = boilerParameter._ZIRKULATION_FREIGABEZEITEN[i].Sa_Freigabezeit.hdStart5.ToString() + boilerParameter._ZIRKULATION_FREIGABEZEITEN[i].Sa_Freigabezeit.hdEnde5.ToString();

                }
            }

            data.Add(new MultipleValues("hdStart1", language, circulations[0], 6));     //6 Samstag
            data.Add(new MultipleValues("hdStart2", language, circulations[1], 6));
            data.Add(new MultipleValues("hdStart3", language, circulations[2], 6));
            data.Add(new MultipleValues("hdStart4", language, circulations[3], 6));
            data.Add(new MultipleValues("hdStart5", language, circulations[3], 6));

            #endregion

            return data;
        }
        static public IList<MultipleValues> circulationsBlock()
        {

            IList<MultipleValues> data = new List<MultipleValues>();

            string[][] circulations = new string[5][];
            for (int i = 0; i < circulations.Length; i++)
            {
                circulations[i] = new string[boilerParameter._ZIRKULATIONS_ARRAY.Length];
            }

            #region Montag - Freitag

            for (int i = 0; i < boilerParameter._ZIRKULATIONS_ARRAY.Length; i++)
            {
                if (boilerParameter._ZIRKULATIONS_ARRAY[i].Zirkulation_Vorhanden == 1)
                {
                    circulations[0][i] = boilerParameter._ZIRKULATION_FREIGABEZEITEN[i].Mo_bis_Fr_Freigabezeit.hdStart1.ToString() + boilerParameter._ZIRKULATION_FREIGABEZEITEN[i].Mo_bis_Fr_Freigabezeit.hdEnde1.ToString();
                    circulations[1][i] = boilerParameter._ZIRKULATION_FREIGABEZEITEN[i].Mo_bis_Fr_Freigabezeit.hdStart2.ToString() + boilerParameter._ZIRKULATION_FREIGABEZEITEN[i].Mo_bis_Fr_Freigabezeit.hdEnde2.ToString();
                    circulations[2][i] = boilerParameter._ZIRKULATION_FREIGABEZEITEN[i].Mo_bis_Fr_Freigabezeit.hdStart3.ToString() + boilerParameter._ZIRKULATION_FREIGABEZEITEN[i].Mo_bis_Fr_Freigabezeit.hdEnde3.ToString();
                    circulations[3][i] = boilerParameter._ZIRKULATION_FREIGABEZEITEN[i].Mo_bis_Fr_Freigabezeit.hdStart4.ToString() + boilerParameter._ZIRKULATION_FREIGABEZEITEN[i].Mo_bis_Fr_Freigabezeit.hdEnde4.ToString();
                    circulations[4][i] = boilerParameter._ZIRKULATION_FREIGABEZEITEN[i].Mo_bis_Fr_Freigabezeit.hdStart5.ToString() + boilerParameter._ZIRKULATION_FREIGABEZEITEN[i].Mo_bis_Fr_Freigabezeit.hdEnde5.ToString();

                }
            }

            data.Add(new MultipleValues("hdStart1", language, circulations[0], 0));     //0 Montag - Freitag
            data.Add(new MultipleValues("hdStart2", language, circulations[1], 0));
            data.Add(new MultipleValues("hdStart3", language, circulations[2], 0));
            data.Add(new MultipleValues("hdStart4", language, circulations[3], 0));
            data.Add(new MultipleValues("hdStart5", language, circulations[3], 0));

            for (int i = 0; i < circulations.Length; i++)
            {
                Array.Clear(circulations[i], 0, circulations[i].Length);
            }

            #endregion

            #region Samstag - Sonntag

            for (int i = 0; i < boilerParameter._ZIRKULATIONS_ARRAY.Length; i++)
            {
                if (boilerParameter._ZIRKULATIONS_ARRAY[i].Zirkulation_Vorhanden == 1)
                {
                    circulations[0][i] = boilerParameter._ZIRKULATION_FREIGABEZEITEN[i].Sa_bis_So_Freigabezeit.hdStart1.ToString() + boilerParameter._ZIRKULATION_FREIGABEZEITEN[i].Sa_bis_So_Freigabezeit.hdEnde1.ToString();
                    circulations[1][i] = boilerParameter._ZIRKULATION_FREIGABEZEITEN[i].Sa_bis_So_Freigabezeit.hdStart2.ToString() + boilerParameter._ZIRKULATION_FREIGABEZEITEN[i].Sa_bis_So_Freigabezeit.hdEnde2.ToString();
                    circulations[2][i] = boilerParameter._ZIRKULATION_FREIGABEZEITEN[i].Sa_bis_So_Freigabezeit.hdStart3.ToString() + boilerParameter._ZIRKULATION_FREIGABEZEITEN[i].Sa_bis_So_Freigabezeit.hdEnde3.ToString();
                    circulations[3][i] = boilerParameter._ZIRKULATION_FREIGABEZEITEN[i].Sa_bis_So_Freigabezeit.hdStart4.ToString() + boilerParameter._ZIRKULATION_FREIGABEZEITEN[i].Sa_bis_So_Freigabezeit.hdEnde4.ToString();
                    circulations[4][i] = boilerParameter._ZIRKULATION_FREIGABEZEITEN[i].Sa_bis_So_Freigabezeit.hdStart5.ToString() + boilerParameter._ZIRKULATION_FREIGABEZEITEN[i].Sa_bis_So_Freigabezeit.hdEnde5.ToString();

                }
            }

            data.Add(new MultipleValues("hdStart1", language, circulations[0], 1));     //1 Samstag - Sonntag
            data.Add(new MultipleValues("hdStart2", language, circulations[1], 1));
            data.Add(new MultipleValues("hdStart3", language, circulations[2], 1));
            data.Add(new MultipleValues("hdStart4", language, circulations[3], 1));
            data.Add(new MultipleValues("hdStart5", language, circulations[3], 1));

            for (int i = 0; i < circulations.Length; i++)
            {
                Array.Clear(circulations[i], 0, circulations[i].Length);
            }

            #endregion

            #region Montag - Sonntag

            for (int i = 0; i < boilerParameter._ZIRKULATIONS_ARRAY.Length; i++)
            {
                if (boilerParameter._ZIRKULATIONS_ARRAY[i].Zirkulation_Vorhanden == 1)
                {
                    circulations[0][i] = boilerParameter._ZIRKULATIONS_ARRAY[i].hdFG1_StartZeit.ToString() + boilerParameter._ZIRKULATIONS_ARRAY[i].hdFG1_EndZeit.ToString();
                    circulations[1][i] = boilerParameter._ZIRKULATIONS_ARRAY[i].hdFG2_StartZeit.ToString() + boilerParameter._ZIRKULATIONS_ARRAY[i].hdFG2_EndZeit.ToString();
                    circulations[2][i] = boilerParameter._ZIRKULATIONS_ARRAY[i].hdFG3_StartZeit.ToString() + boilerParameter._ZIRKULATIONS_ARRAY[i].hdFG3_EndZeit.ToString();
                    circulations[3][i] = boilerParameter._ZIRKULATIONS_ARRAY[i].hdFG4_StartZeit.ToString() + boilerParameter._ZIRKULATIONS_ARRAY[i].hdFG4_EndZeit.ToString();
                    circulations[4][i] = boilerParameter._ZIRKULATIONS_ARRAY[i].hdFG5_StartZeit.ToString() + boilerParameter._ZIRKULATIONS_ARRAY[i].hdFG5_EndZeit.ToString();

                }
            }

            data.Add(new MultipleValues("hdStart1", language, circulations[0], 2));     //2 Dienstag
            data.Add(new MultipleValues("hdStart2", language, circulations[1], 2));
            data.Add(new MultipleValues("hdStart3", language, circulations[2], 2));
            data.Add(new MultipleValues("hdStart4", language, circulations[3], 2));
            data.Add(new MultipleValues("hdStart5", language, circulations[3], 2));

            #endregion

            return data;
        }

        #endregion

        #region Buffers
        static public IList<MultipleValues> buffers()
        {

            IList<MultipleValues> data = new List<MultipleValues>();

            string[][] buffers = new string[21][];
            for (int i = 0; i < buffers.Length; i++)
            {
                buffers[i] = new string[boilerParameter.HEIZKREISPARAMETER.PUFFER_ARRAY.Length];
            }

            for (int i = 0; i < boilerParameter.HEIZKREISPARAMETER.PUFFER_ARRAY.Length; i++)
            {
                if (boilerParameter.HEIZKREISPARAMETER.PUFFER_ARRAY[i].Puffer_vorhanden == 1)
                {
                    //Betriebsart
                    buffers[0][i] = boilerParameter.HEIZKREISPARAMETER.PUFFER_ARRAY[i].Freigabeart.ToString();

                    //Immer Aus
                    buffers[1][i] = boilerParameter.HEIZKREISPARAMETER.PUFFER_ARRAY[i].min_Oben.ToString();

                    //Immer Ein
                    buffers[2][i] = boilerParameter.HEIZKREISPARAMETER.PUFFER_ARRAY[i].max_Unten.ToString();

                    //Zeitschaltung
                    buffers[3][i] = boilerParameter.HEIZKREISPARAMETER.PUFFER_ARRAY[i].max_Unten_2.ToString();
                    buffers[4][i] = boilerParameter.HEIZKREISPARAMETER.PUFFER_ARRAY[i].max_Unten_3.ToString();
                    buffers[5][i] = boilerParameter.HEIZKREISPARAMETER.PUFFER_ARRAY[i].Freigabezeit.hdStart1.ToString() + boilerParameter.HEIZKREISPARAMETER.PUFFER_ARRAY[i].Freigabezeit.hdEnde1.ToString();
                    buffers[6][i] = boilerParameter.HEIZKREISPARAMETER.PUFFER_ARRAY[i].Freigabezeit.hdStart2.ToString() + boilerParameter.HEIZKREISPARAMETER.PUFFER_ARRAY[i].Freigabezeit.hdEnde2.ToString();
                    buffers[7][i] = boilerParameter.HEIZKREISPARAMETER.PUFFER_ARRAY[i].Freigabezeit.hdStart3.ToString() + boilerParameter.HEIZKREISPARAMETER.PUFFER_ARRAY[i].Freigabezeit.hdEnde3.ToString();
                    buffers[8][i] = boilerParameter.HEIZKREISPARAMETER.PUFFER_ARRAY[i].Freigabezeit.hdStart4.ToString() + boilerParameter.HEIZKREISPARAMETER.PUFFER_ARRAY[i].Freigabezeit.hdEnde4.ToString();
                    buffers[9][i] = boilerParameter.HEIZKREISPARAMETER.PUFFER_ARRAY[i].Freigabezeit.hdStart5.ToString() + boilerParameter.HEIZKREISPARAMETER.PUFFER_ARRAY[i].Freigabezeit.hdEnde5.ToString();
                    buffers[10][i] = boilerParameter.HEIZKREISPARAMETER.PUFFER_ARRAY[i].max_Unten_FK.ToString();


                    //Systemparameter
                    buffers[11][i] = boilerParameter.HEIZKREISPARAMETER.PUFFER_ARRAY[i].KT_Min.ToString();
                    buffers[12][i] = boilerParameter.HEIZKREISPARAMETER.PUFFER_ARRAY[i].ENERGIEQUELLE.ToString();
                    buffers[13][i] = boilerParameter.HEIZKREISPARAMETER.PUFFER_ARRAY[i].Anforderungsdifferenz.ToString();
                    buffers[14][i] = boilerParameter.HEIZKREISPARAMETER.PUFFER_ARRAY[i].Pumpendiff_oben.ToString();
                    buffers[15][i] = boilerParameter.HEIZKREISPARAMETER.PUFFER_ARRAY[i].Pumpendiff_unten.ToString();
                    buffers[16][i] = boilerParameter.HEIZKREISPARAMETER.PUFFER_ARRAY[i].Durchladen_Bei_Boileranforderung.ToString();
                    buffers[17][i] = boilerParameter.HEIZKREISPARAMETER.PUFFER_ARRAY[i].Fremdkesselfreigabe.ToString();
                    buffers[18][i] = boilerParameter.HEIZKREISPARAMETER.PUFFER_ARRAY[i].VltSoll_Max_unten.ToString();

                    //Wärmeableitung
                    buffers[19][i] = boilerParameter.HEIZKREISPARAMETER.PUFFER_ARRAY[i].Ueberwaermeabfuhr_permanent.ToString();
                    buffers[20][i] = boilerParameter.HEIZKREISPARAMETER.PUFFER_ARRAY[i].Ueberwaermeabfuhr_Solarertrag.ToString();
                }
            }

            data.Add(new MultipleValues("Puffer_Betriebsart", language, buffers[0], 0));    //0 Betriebsart

            data.Add(new MultipleValues("min_Oben", language, buffers[1], 1));      //1 Immer Aus

            data.Add(new MultipleValues("min_Oben", language, buffers[1], 2));      //2 Immer Ein
            data.Add(new MultipleValues("max_Unten", language, buffers[2], 2));

            data.Add(new MultipleValues("min_Oben", language, buffers[1], 3));      //3 Zeitschaltung
            data.Add(new MultipleValues("max_Unten_1", language, buffers[2], 3));
            data.Add(new MultipleValues("max_Unten_2", language, buffers[3], 3));
            data.Add(new MultipleValues("max_Unten_3", language, buffers[4], 3));
            data.Add(new MultipleValues("hdStart1", language, buffers[5], 3));
            data.Add(new MultipleValues("hdStart2", language, buffers[6], 3));
            data.Add(new MultipleValues("hdStart3", language, buffers[7], 3));
            data.Add(new MultipleValues("hdStart4", language, buffers[8], 3));
            data.Add(new MultipleValues("hdStart5", language, buffers[9], 3));
            data.Add(new MultipleValues("max_Unten_FK", language, buffers[10], 3));

            data.Add(new MultipleValues("KT_Min", language, buffers[11], 4));     //4 Systemparameter
            data.Add(new MultipleValues("Puffer_ENERGIEQUELLE", language, buffers[12], 4));
            data.Add(new MultipleValues("AnforderungsDifferenz", language, buffers[13], 4));
            data.Add(new MultipleValues("Pumpendiff_oben", language, buffers[14], 4));
            data.Add(new MultipleValues("Pumpendiff_unten", language, buffers[15], 4));
            data.Add(new MultipleValues("Durchladen_Bei_Boileranforderung", language, buffers[16], 4));
            data.Add(new MultipleValues("Fremdkesselfreigabe", language, buffers[17], 4));
            data.Add(new MultipleValues("VltSoll_Max_unten", language, buffers[18], 4));

            data.Add(new MultipleValues("Ueberwaermeabfuhr_permanent", language, buffers[19], 5));     //5 Wärmeableitung
            data.Add(new MultipleValues("Ueberwaermeabfuhr_Solarertrag", language, buffers[20], 5));
            return data;
        }
        #endregion

        #region Buffers Maximus
        static public IList<MultipleValues> buffersMaximus()
        {
            IList<MultipleValues> data = new List<MultipleValues>();

            string[][] buffers = new string[22][];
            string[][] buffersArray = new string[5][];

            for (int i = 0; i < buffers.Length; i++)
            {
                buffers[i] = new string[boilerParameter.HEIZKREISPARAMETER.PUFFER_ARRAY.Length];
            }

            for(int i=0; i< buffersArray.Length; i++)
            {
                buffersArray[i] = new string[maximusParameter._PUFFER_ARRAY.Length];
            }

            for (int i = 0; i < boilerParameter.HEIZKREISPARAMETER.PUFFER_ARRAY.Length; i++)
            {
                if (boilerParameter.HEIZKREISPARAMETER.PUFFER_ARRAY[i].Puffer_vorhanden == 1)
                {
                    //Betriebsart
                    buffers[0][i] = boilerParameter.HEIZKREISPARAMETER.PUFFER_ARRAY[i].Freigabeart.ToString();

                    //Immer Aus
                    buffers[1][i] = boilerParameter.HEIZKREISPARAMETER.PUFFER_ARRAY[i].min_Oben.ToString();

                    //Immer Ein
                    buffers[2][i] = boilerParameter.HEIZKREISPARAMETER.PUFFER_ARRAY[i].max_Unten.ToString();

                    //Zeitschaltung
                    buffers[3][i] = boilerParameter.HEIZKREISPARAMETER.PUFFER_ARRAY[i].max_Unten_2.ToString();
                    buffers[4][i] = boilerParameter.HEIZKREISPARAMETER.PUFFER_ARRAY[i].max_Unten_3.ToString();
                    buffers[5][i] = boilerParameter.HEIZKREISPARAMETER.PUFFER_ARRAY[i].Freigabezeit.hdStart1.ToString() + boilerParameter.HEIZKREISPARAMETER.PUFFER_ARRAY[i].Freigabezeit.hdEnde1.ToString();
                    buffers[6][i] = boilerParameter.HEIZKREISPARAMETER.PUFFER_ARRAY[i].Freigabezeit.hdStart2.ToString() + boilerParameter.HEIZKREISPARAMETER.PUFFER_ARRAY[i].Freigabezeit.hdEnde2.ToString();
                    buffers[7][i] = boilerParameter.HEIZKREISPARAMETER.PUFFER_ARRAY[i].Freigabezeit.hdStart3.ToString() + boilerParameter.HEIZKREISPARAMETER.PUFFER_ARRAY[i].Freigabezeit.hdEnde3.ToString();
                    buffers[8][i] = boilerParameter.HEIZKREISPARAMETER.PUFFER_ARRAY[i].Freigabezeit.hdStart4.ToString() + boilerParameter.HEIZKREISPARAMETER.PUFFER_ARRAY[i].Freigabezeit.hdEnde4.ToString();
                    buffers[9][i] = boilerParameter.HEIZKREISPARAMETER.PUFFER_ARRAY[i].Freigabezeit.hdStart5.ToString() + boilerParameter.HEIZKREISPARAMETER.PUFFER_ARRAY[i].Freigabezeit.hdEnde5.ToString();
                    buffers[10][i] = boilerParameter.HEIZKREISPARAMETER.PUFFER_ARRAY[i].max_Unten_FK.ToString();

                    //Systemparameter
                    buffers[11][i] = boilerParameter.HEIZKREISPARAMETER.PUFFER_ARRAY[i].KT_Min.ToString();
                    buffers[12][i] = boilerParameter.HEIZKREISPARAMETER.PUFFER_ARRAY[i].ENERGIEQUELLE.ToString();
                    buffers[13][i] = boilerParameter.HEIZKREISPARAMETER.PUFFER_ARRAY[i].Anforderungsdifferenz.ToString();
                    buffers[14][i] = boilerParameter.HEIZKREISPARAMETER.PUFFER_ARRAY[i].Pumpendiff_oben.ToString();
                    buffers[15][i] = boilerParameter.HEIZKREISPARAMETER.PUFFER_ARRAY[i].Pumpendiff_unten.ToString();
                    buffers[16][i] = boilerParameter.HEIZKREISPARAMETER.PUFFER_ARRAY[i].Durchladen_Bei_Boileranforderung.ToString();
                    buffers[17][i] = boilerParameter.HEIZKREISPARAMETER.PUFFER_ARRAY[i].Fremdkesselfreigabe.ToString();
                    buffers[18][i] = boilerParameter.HEIZKREISPARAMETER.PUFFER_ARRAY[i].VltSoll_Max_unten.ToString();

                    //Wärmeableitung
                    buffers[19][i] = boilerParameter.HEIZKREISPARAMETER.PUFFER_ARRAY[i].Ueberwaermeabfuhr_permanent.ToString();
                    buffers[20][i] = boilerParameter.HEIZKREISPARAMETER.PUFFER_ARRAY[i].Ueberwaermeabfuhr_Solarertrag.ToString();
                    buffers[21][i] = boilerParameter.PUFFER_ERWEITERUNG.Durchladen_bei_extAnf.ToString();
                    
                }
            }

            for(int j=0; j < maximusParameter._PUFFER_ARRAY.Length; j++)
            {
                buffersArray[0][j] = maximusParameter._PUFFER_ARRAY[j].FK_PTO_Soll.ToString();
                buffersArray[1][j] = maximusParameter._PUFFER_ARRAY[j].FK_PTO_Hysterese.ToString();
                buffersArray[2][j] = maximusParameter._PUFFER_ARRAY[j].Durchladen_X35_aktiv.ToString();
                buffersArray[3][j] = maximusParameter._PUFFER_ARRAY[j].max_Unten_X35.ToString();
                buffersArray[4][j] = maximusParameter._PUFFER_ARRAY[j].Durchladen_X35_bei_AT.ToString();
            }

            data.Add(new MultipleValues("KT_Min", language, buffers[11], 0));     //0 Systemparameter
            data.Add(new MultipleValues("Puffer_ENERGIEQUELLE", language, buffers[12], 0));
            data.Add(new MultipleValues("AnforderungsDifferenz", language, buffers[13], 0));
            data.Add(new MultipleValues("Pumpendiff_oben", language, buffers[14], 0));
            data.Add(new MultipleValues("Pumpendiff_unten", language, buffers[15], 0));
            data.Add(new MultipleValues("Durchladen_Bei_Boileranforderung", language, buffers[16], 0));
            data.Add(new MultipleValues("Fremdkesselfreigabe", language, buffers[17], 0));
            data.Add(new MultipleValues("VltSoll_Max_unten", language, buffers[18], 0));
            data.Add(new MultipleValues("FK_PTO_Soll", language, buffersArray[0], 0));
            data.Add(new MultipleValues("FK_PTO_Hysterese", language, buffersArray[1], 0));
            data.Add(new MultipleValues("Durchladen_X35_aktiv", language, buffersArray[2], 0));

            data.Add(new MultipleValues("min_Oben", language, buffers[1], 1));      //1 Immer Ein
            data.Add(new MultipleValues("max_Unten", language, buffers[2], 1));

            data.Add(new MultipleValues("min_Oben", language, buffers[1], 2));      //2 Zeitschaltung
            data.Add(new MultipleValues("max_Unten_1", language, buffers[2], 2));
            data.Add(new MultipleValues("max_Unten_2", language, buffers[3], 2));
            data.Add(new MultipleValues("max_Unten_3", language, buffers[4], 2));
            data.Add(new MultipleValues("hdStart1", language, buffers[5], 2));
            data.Add(new MultipleValues("hdStart2", language, buffers[6], 2));
            data.Add(new MultipleValues("hdStart3", language, buffers[7], 2));
            data.Add(new MultipleValues("hdStart4", language, buffers[8], 2));
            data.Add(new MultipleValues("hdStart5", language, buffers[9], 2));
            data.Add(new MultipleValues("max_Unten_FK", language, buffers[10], 2));
            data.Add(new MultipleValues("max_Unten_X35", language, buffersArray[3], 2));
            data.Add(new MultipleValues("Durchladen_X35_bei_AT", language, buffersArray[4], 2));

            return data;
        }
        #endregion

        #region Solar
        static public IList<MultipleValues> solarTargetValues()
        {

            IList<MultipleValues> data = new List<MultipleValues>();

            string[][] solar = new string[51][];
            for (int i = 0; i < solar.Length; i++)
            {
                solar[i] = new string[boilerParameter.HEIZKREISPARAMETER._SOLAR_ARRAY.Length];
            }

            for (int i = 0; i < boilerParameter.HEIZKREISPARAMETER._SOLAR_ARRAY.Length; i++)
            {
                if (boilerParameter.HEIZKREISPARAMETER._SOLAR_ARRAY[i].Solarkreis_vorhanden == 1)
                {
                    //1. Speicher
                    solar[0][i] = boilerParameter.HEIZKREISPARAMETER._SOLAR_ARRAY[i].SOLARKREISSOLLWERTE.udPumpensteuerung.ToString();
                    solar[1][i] = boilerParameter.HEIZKREISPARAMETER._SOLAR_ARRAY[i].SOLARKREISSOLLWERTE.hdFG1_StartZeit.ToString() + boilerParameter.HEIZKREISPARAMETER._SOLAR_ARRAY[i].SOLARKREISSOLLWERTE.hdFG1_EndZeit.ToString();
                    solar[2][i] = boilerParameter.HEIZKREISPARAMETER._SOLAR_ARRAY[i].SOLARKREISSOLLWERTE.udSolar_Eindifferenz1.ToString();
                    solar[3][i] = boilerParameter.HEIZKREISPARAMETER._SOLAR_ARRAY[i].SOLARKREISSOLLWERTE.udSolar_Ausdifferenz1.ToString();
                    solar[4][i] = boilerParameter.HEIZKREISPARAMETER._SOLAR_ARRAY[i].SOLARKREISSOLLWERTE.udMax_Speicher1Temp_unten.ToString();
                    solar[5][i] = boilerParameter.HEIZKREISPARAMETER._SOLAR_ARRAY[i].SOLARKREISSOLLWERTE.Kollektor1MinTemp.ToString();
                    solar[6][i] = boilerParameter.HEIZKREISPARAMETER._SOLAR_ARRAY[i].SOLARKREISREGLER.PumpenNachlaufzeit.ToString();
                    solar[7][i] = boilerParameter.HEIZKREISPARAMETER._SOLAR_ARRAY[i].SOLARKREISREGLER.iSollDifferenz1.ToString();
                    solar[8][i] = boilerParameter.HEIZKREISPARAMETER._SOLAR_ARRAY[i].SOLARKREISSOLLWERTE.udKollektor1_SollTemp.ToString();

                    //2.Speicher
                    solar[9][i] = boilerParameter.HEIZKREISPARAMETER._SOLAR_ARRAY[i].SOLARKREISREGLER.dPumpensteuerung2.ToString();
                    solar[10][i] = boilerParameter.HEIZKREISPARAMETER._SOLAR_ARRAY[i].STARTFUNKTION.hdFG2_StartZeit.ToString() + boilerParameter.HEIZKREISPARAMETER._SOLAR_ARRAY[i].STARTFUNKTION.hdFG2_EndZeit.ToString();
                    solar[11][i] = boilerParameter.HEIZKREISPARAMETER._SOLAR_ARRAY[i].SOLARKREISSOLLWERTE.udSolar_Eindifferenz2.ToString();
                    solar[12][i] = boilerParameter.HEIZKREISPARAMETER._SOLAR_ARRAY[i].SOLARKREISSOLLWERTE.udSolar_Ausdifferenz2.ToString();
                    solar[13][i] = boilerParameter.HEIZKREISPARAMETER._SOLAR_ARRAY[i].SOLARKREISSOLLWERTE.udMax_Speicher2Temp_unten.ToString();
                    solar[14][i] = boilerParameter.HEIZKREISPARAMETER._SOLAR_ARRAY[i].SOLARKREISSOLLWERTE.Kollektor2MinTemp.ToString();
                    solar[15][i] = boilerParameter.HEIZKREISPARAMETER._SOLAR_ARRAY[i].SOLARKREISSOLLWERTE.iPumpennachlauf2.ToString();
                    solar[16][i] = boilerParameter.HEIZKREISPARAMETER._SOLAR_ARRAY[i].SOLARKREISREGLER.iSollDifferenz2.ToString();
                    solar[17][i] = boilerParameter.HEIZKREISPARAMETER._SOLAR_ARRAY[i].SOLARKREISSOLLWERTE.udKollektor2_SollTemp.ToString();

                    //Startfunktion
                    solar[18][i] = boilerParameter.HEIZKREISPARAMETER._SOLAR_ARRAY[i].STARTFUNKTION.udStartfunktionEINAUS.ToString();
                    solar[19][i] = boilerParameter.HEIZKREISPARAMETER._SOLAR_ARRAY[i].STARTFUNKTION.udMaxPause.ToString();
                    solar[20][i] = boilerParameter.HEIZKREISPARAMETER._SOLAR_ARRAY[i].STARTFUNKTION.udMessspuelImpuls.ToString();
                    solar[21][i] = boilerParameter.HEIZKREISPARAMETER._SOLAR_ARRAY[i].STARTFUNKTION.udMesszeit.ToString();
                    solar[22][i] = boilerParameter.HEIZKREISPARAMETER._SOLAR_ARRAY[i].STARTFUNKTION.ud1Messanstieg.ToString();
                    solar[23][i] = boilerParameter.HEIZKREISPARAMETER._SOLAR_ARRAY[i].STARTFUNKTION.ud2SpuelImpuls.ToString();

                    // 2/3 - Kreis Einstellung
                    solar[24][i] = boilerParameter.HEIZKREISPARAMETER._SOLAR_ARRAY[i].Solarschema.ToString();


                    //Help variable
                    solar[50][i] = boilerParameter.HEIZKREISPARAMETER._SOLAR_ARRAY[i].Regelkreise.ToString();
                    if (boilerParameter.HEIZKREISPARAMETER._SOLAR_ARRAY[i].Regelkreise == 1)                    //Wenn nur ein Regelkreis vorhanden => Solarvorrang = nicht verwendt
                    {
                        solar[25][i] = "-1";
                    }
                    else                                                                                            //Sonst je nach Wert
                    {
                        solar[25][i] = boilerParameter.HEIZKREISPARAMETER._SOLAR_ARRAY[i].Speichervorrang.ToString();
                    }

                    solar[26][i] = boilerParameter.HEIZKREISPARAMETER._SOLAR_ARRAY[i].Gesamtwartezeit_tw.ToString();
                    solar[27][i] = boilerParameter.HEIZKREISPARAMETER._SOLAR_ARRAY[i].Laufzeit_tL.ToString();
                    solar[28][i] = boilerParameter.HEIZKREISPARAMETER._SOLAR_ARRAY[i].Ventilruhestellung.ToString();
                    solar[29][i] = boilerParameter.HEIZKREISPARAMETER._SOLAR_ARRAY[i].byVentilruhestellung2.ToString();
                    solar[30][i] = boilerParameter.HEIZKREISPARAMETER._SOLAR_ARRAY[i].iDifferenzAktivierung2terKreis.ToString();
                    solar[31][i] = boilerParameter.HEIZKREISPARAMETER._SOLAR_ARRAY[i].PumpenEinteilung.ToString();                                   // false

                    // Wärmemengenzähler
                    solar[32][i] = boilerParameter.HEIZKREISPARAMETER._WMZ_ARRAY[i].Durchflussrate.ToString();
                    solar[33][i] = boilerParameter.HEIZKREISPARAMETER._WMZ_ARRAY[i].Frostschutzkonzentration.ToString();
                    solar[34][i] = boilerParameter.HEIZKREISPARAMETER._WMZ_ARRAY[i].DurchflussmengengeberTyp.ToString();

                    // Werte aus k-daten file fehlen hier noch (35-40)



                    //3. Speicher
                    if (boilerParameter.HEIZKREISPARAMETER._SOLAR_ARRAY[i].Regelkreise == 3)
                    {
                        i++;
                        solar[41][i] = boilerParameter.HEIZKREISPARAMETER._SOLAR_ARRAY[i].SOLARKREISSOLLWERTE.udPumpensteuerung.ToString();
                        solar[42][i] = boilerParameter.HEIZKREISPARAMETER._SOLAR_ARRAY[i].SOLARKREISSOLLWERTE.hdFG1_StartZeit.ToString() + boilerParameter.HEIZKREISPARAMETER._SOLAR_ARRAY[i].SOLARKREISSOLLWERTE.hdFG1_EndZeit.ToString();
                        solar[43][i] = boilerParameter.HEIZKREISPARAMETER._SOLAR_ARRAY[i].SOLARKREISSOLLWERTE.udSolar_Eindifferenz1.ToString();
                        solar[44][i] = boilerParameter.HEIZKREISPARAMETER._SOLAR_ARRAY[i].SOLARKREISSOLLWERTE.udSolar_Ausdifferenz1.ToString();
                        solar[45][i] = boilerParameter.HEIZKREISPARAMETER._SOLAR_ARRAY[i].SOLARKREISSOLLWERTE.udMax_Speicher1Temp_unten.ToString();
                        solar[46][i] = boilerParameter.HEIZKREISPARAMETER._SOLAR_ARRAY[i].SOLARKREISSOLLWERTE.Kollektor1MinTemp.ToString();
                        solar[47][i] = boilerParameter.HEIZKREISPARAMETER._SOLAR_ARRAY[i].SOLARKREISREGLER.PumpenNachlaufzeit.ToString();
                        solar[48][i] = boilerParameter.HEIZKREISPARAMETER._SOLAR_ARRAY[i].SOLARKREISREGLER.iSollDifferenz1.ToString();
                        solar[49][i] = boilerParameter.HEIZKREISPARAMETER._SOLAR_ARRAY[i].SOLARKREISSOLLWERTE.udKollektor1_SollTemp.ToString();
                    }

                }
            }
            data.Add(new MultipleValues("Solar_udPumpensteuerung", language, solar[0], 0));    //0 Speicher 1
            data.Add(new MultipleValues("hdFG_StartZeit", language, solar[1], 0));
            data.Add(new MultipleValues("udSolar_Eindifferenz", language, solar[2], 0));
            data.Add(new MultipleValues("udSolar_Ausdifferenz", language, solar[3], 0));
            data.Add(new MultipleValues("udMax_SpeicherTemp_unten", language, solar[4], 0));
            data.Add(new MultipleValues("KollektorMinTemp", language, solar[5], 0));
            data.Add(new MultipleValues("PumpenNachlaufzeit", language, solar[6], 0));
            data.Add(new MultipleValues("iSollDifferenz", language, solar[7], 0));
            data.Add(new MultipleValues("udKollektor_SollTemp", language, solar[8], 0));

            data.Add(new MultipleValues("Solar_udPumpensteuerung", language, solar[9], 1));    //1 Speicher 2
            data.Add(new MultipleValues("hdFG_StartZeit", language, solar[10], 1));
            data.Add(new MultipleValues("udSolar_Eindifferenz", language, solar[11], 1));
            data.Add(new MultipleValues("udSolar_Ausdifferenz", language, solar[12], 1));
            data.Add(new MultipleValues("udMax_SpeicherTemp_unten", language, solar[13], 1));
            data.Add(new MultipleValues("KollektorMinTemp", language, solar[14], 1));
            data.Add(new MultipleValues("PumpenNachlaufzeit", language, solar[15], 1));
            data.Add(new MultipleValues("iSollDifferenz", language, solar[16], 1));
            data.Add(new MultipleValues("udKollektor_SollTemp", language, solar[17], 1));

            data.Add(new MultipleValues("Solar_udPumpensteuerung", language, solar[41], 2));    //2 Speicher 3
            data.Add(new MultipleValues("hdFG_StartZeit", language, solar[42], 2));
            data.Add(new MultipleValues("udSolar_Eindifferenz", language, solar[43], 2));
            data.Add(new MultipleValues("udSolar_Ausdifferenz", language, solar[44], 2));
            data.Add(new MultipleValues("udMax_SpeicherTemp_unten", language, solar[45], 2));
            data.Add(new MultipleValues("KollektorMinTemp", language, solar[46], 2));
            data.Add(new MultipleValues("PumpenNachlaufzeit", language, solar[47], 2));
            data.Add(new MultipleValues("iSollDifferenz", language, solar[48], 2));
            data.Add(new MultipleValues("udKollektor_SollTemp", language, solar[49], 2));

            data.Add(new MultipleValues("udStartfunktionEINAUS", language, solar[18], 3));    //3 Startfunktion
            data.Add(new MultipleValues("udMaxPause", language, solar[19], 3));
            data.Add(new MultipleValues("udMessspuelImpuls", language, solar[20], 3));
            data.Add(new MultipleValues("udMesszeit", language, solar[21], 3));
            data.Add(new MultipleValues("ud1Messanstieg", language, solar[22], 3));
            data.Add(new MultipleValues("ud2SpuelImpuls", language, solar[23], 3));

            data.Add(new MultipleValues("Solarschema", language, solar[24], 4));    //4 2/3 - Kreis Einstellung
            data.Add(new MultipleValues("Speichervorrang", language, solar[25], 4));
            data.Add(new MultipleValues("Gesamtwartezeit_tw", language, solar[26], 4));
            data.Add(new MultipleValues("Laufzeit_tL", language, solar[27], 4));
            data.Add(new MultipleValues("Ventilruhestellung", language, solar[28], 4));
            data.Add(new MultipleValues("byVentilruhestellung2", language, solar[29], 4));
            data.Add(new MultipleValues("iDifferenzAktivierung2terKreis", language, solar[30], 4));
            data.Add(new MultipleValues("PumpenEinteilung", language, solar[31], 4));

            data.Add(new MultipleValues("Durchflussrate", language, solar[32], 5));    //5 Wärmemengenzähler
            data.Add(new MultipleValues("Frostschutzkonzentration", language, solar[33], 5));
            data.Add(new MultipleValues("DurchflussmengengeberTyp", language, solar[34], 5));
            // Werte aus k-daten file fehlen hier noch (35-40)


            return data;
        }

        static public IList<MultipleValues> solarAdditionalFunctions()
        {

            IList<MultipleValues> data = new List<MultipleValues>();

            string[][] solar = new string[12][];
            for (int i = 0; i < solar.Length; i++)
            {
                solar[i] = new string[boilerParameter.HEIZKREISPARAMETER._SOLAR_ARRAY.Length];
            }

            for (int i = 0; i < boilerParameter.HEIZKREISPARAMETER._SOLAR_ARRAY.Length; i++)
            {
                if (boilerParameter.HEIZKREISPARAMETER._SOLAR_ARRAY[i].Solarkreis_vorhanden == 1)
                {
                    // Übertemperaturbegrenzung
                    solar[0][i] = boilerParameter.HEIZKREISPARAMETER._SOLAR_ARRAY[i].SOLARKREISREGLER.ud_UebertempBegrenzung.ToString();
                    solar[1][i] = boilerParameter.HEIZKREISPARAMETER._SOLAR_ARRAY[i].SOLARKREISREGLER.udSolarAbschaltTemp.ToString();
                    solar[2][i] = boilerParameter.HEIZKREISPARAMETER._SOLAR_ARRAY[i].SOLARKREISREGLER.udSolarFreigabeTemp.ToString();

                    // Frostschutz
                    solar[3][i] = boilerParameter.HEIZKREISPARAMETER._SOLAR_ARRAY[i].SOLARKREISSOLLWERTE.iFrostschutz_E_A.ToString();
                    solar[4][i] = boilerParameter.HEIZKREISPARAMETER._SOLAR_ARRAY[i].SOLARKREISSOLLWERTE.iFrostschutz_Einschalttemp.ToString();
                    solar[5][i] = boilerParameter.HEIZKREISPARAMETER._SOLAR_ARRAY[i].SOLARKREISSOLLWERTE.iFrostschutz_Ausschalttemp.ToString();

                    // Wärmeableitung
                    solar[6][i] = boilerParameter.HEIZKREISPARAMETER._SOLAR_ARRAY[i].SOLARKREISSOLLWERTE.iUeberwaermeabfuhr_E_A.ToString();
                    solar[7][i] = boilerParameter.HEIZKREISPARAMETER._SOLAR_ARRAY[i].SOLARKREISSOLLWERTE.iKollTemp_WaermeabfuhrEin.ToString();
                    solar[8][i] = boilerParameter.HEIZKREISPARAMETER._SOLAR_ARRAY[i].SOLARKREISSOLLWERTE.iKollTemp_WaermeabfuhrAus.ToString();

                    // Kühlfunktion
                    solar[9][i] = boilerParameter.HEIZKREISPARAMETER._SOLAR_ARRAY[i].SOLARKREISREGLER.iKuehlfunktionEA.ToString();
                    solar[10][i] = boilerParameter.HEIZKREISPARAMETER._SOLAR_ARRAY[i].SOLARKREISREGLER.hdKF_StartZeit.ToString() + boilerParameter.HEIZKREISPARAMETER._SOLAR_ARRAY[i].SOLARKREISREGLER.hdKF_EndZeit.ToString();
                    solar[11][i] = boilerParameter.HEIZKREISPARAMETER._SOLAR_ARRAY[i].SOLARKREISREGLER.iKuehlfunktionmaxSpeicherUnten.ToString();

                    if (boilerParameter.HEIZKREISPARAMETER._SOLAR_ARRAY[i].Regelkreise == 3) i++; //Solarkreis 2 Überspringen, wenn 3 Regelkreise vorhanden
                }
            }

            data.Add(new MultipleValues("ud_UebertempBegrenzung", language, solar[0], 0));    //0 Übertemperaturbegrenzung
            data.Add(new MultipleValues("udSolarAbschaltTemp", language, solar[1], 0));
            data.Add(new MultipleValues("udSolarFreigabeTemp", language, solar[2], 0));

            data.Add(new MultipleValues("iFrostschutz_E_A", language, solar[3], 1));    //1 Frostschutz
            data.Add(new MultipleValues("iFrostschutz_Einschalttemp", language, solar[4], 1));
            data.Add(new MultipleValues("iFrostschutz_Ausschalttemp", language, solar[5], 1));

            data.Add(new MultipleValues("iUeberwaermeabfuhr_E_A", language, solar[6], 2));    //2 Wärmeableitung
            data.Add(new MultipleValues("iKollTemp_WaermeabfuhrEin", language, solar[7], 2));
            data.Add(new MultipleValues("iKollTemp_WaermeabfuhrAus", language, solar[8], 2));

            data.Add(new MultipleValues("iKuehlfunktionEA", language, solar[9], 3));    //3 Kühlfunktion
            data.Add(new MultipleValues("hdKF_StartZeit", language, solar[10], 3));
            data.Add(new MultipleValues("iKuehlfunktionmaxSpeicherUnten", language, solar[11], 3));


            return data;
        }

        static public IList<MultipleValues> solarParameters()
        {

            IList<MultipleValues> data = new List<MultipleValues>();

            string[][] solar = new string[40][];
            for (int i = 0; i < solar.Length; i++)
            {
                solar[i] = new string[boilerParameter.HEIZKREISPARAMETER._SOLAR_ARRAY.Length];
            }

            for (int i = 0; i < boilerParameter.HEIZKREISPARAMETER._SOLAR_ARRAY.Length; i++)
            {
                if (boilerParameter.HEIZKREISPARAMETER._SOLAR_ARRAY[i].Solarkreis_vorhanden == 1)
                {
                    // Fühlereinstellungen Modul
                    solar[0][i] = boilerParameter.HEIZKREISPARAMETER._SOLAR_ARRAY[i].Kollektorfuehlertyp.ToString();
                    solar[1][i] = boilerParameter.HEIZKREISPARAMETER._SOLAR_ARRAY[i].Speicherfuehler1Typ.ToString();
                    solar[2][i] = boilerParameter.HEIZKREISPARAMETER._SOLAR_ARRAY[i].Speicherfuehler2Typ.ToString();
                    solar[3][i] = (boilerParameter.HEIZKREISPARAMETER._SOLAR_ARRAY[i].bsiFuehlertyp.value & 1).ToString();                       //Fühlertyp I3
                    solar[4][i] = (boilerParameter.HEIZKREISPARAMETER._SOLAR_ARRAY[i].bsiFuehlertyp.value & 4).ToString();                       //Fühlertyp I5                                                                            //Fühlertyp I5
                    solar[5][i] = (boilerParameter.HEIZKREISPARAMETER._SOLAR_ARRAY[i].bsiFuehlertyp.value & 16).ToString();                      //Fühlertyp I10                                                                                                                                                                                               //Fühlertyp I10

                    //Regelkreis 1
                    solar[6][i] = boilerParameter.HEIZKREISPARAMETER._SOLAR_ARRAY[i].Regelkreise.ToString();
                    solar[7][i] = boilerParameter.HEIZKREISPARAMETER._SOLAR_ARRAY[i].Wellenpaket.ToString();
                    solar[8][i] = boilerParameter.HEIZKREISPARAMETER._SOLAR_ARRAY[i].Signaltyp_AnOut1.ToString();
                    solar[9][i] = boilerParameter.HEIZKREISPARAMETER._SOLAR_ARRAY[i].PWM_AnOut1_Invers.ToString();
                    solar[10][i] = boilerParameter.HEIZKREISPARAMETER._SOLAR_ARRAY[i].SOLARKREISSOLLWERTE.udDrehzahlregelung.ToString();
                    solar[11][i] = boilerParameter.HEIZKREISPARAMETER._SOLAR_ARRAY[i].SOLARKREISREGLER.ud_PI_Intervall.ToString();
                    solar[12][i] = boilerParameter.HEIZKREISPARAMETER._SOLAR_ARRAY[i].SOLARKREISREGLER.ud_DIV_P.ToString();
                    solar[13][i] = boilerParameter.HEIZKREISPARAMETER._SOLAR_ARRAY[i].SOLARKREISREGLER.ud_DIV_I.ToString();
                    solar[14][i] = boilerParameter.HEIZKREISPARAMETER._SOLAR_ARRAY[i].SOLARKREISREGLER.ud_DIV_D.ToString();
                    solar[15][i] = boilerParameter.HEIZKREISPARAMETER._SOLAR_ARRAY[i].SOLARKREISREGLER.ud_PI_P_Faktor.ToString();
                    solar[16][i] = boilerParameter.HEIZKREISPARAMETER._SOLAR_ARRAY[i].SOLARKREISREGLER.ud_PI_I_Faktor.ToString();
                    solar[17][i] = boilerParameter.HEIZKREISPARAMETER._SOLAR_ARRAY[i].SOLARKREISREGLER.ud_PID_D_Faktor.ToString();
                    solar[18][i] = boilerParameter.HEIZKREISPARAMETER._SOLAR_ARRAY[i].SOLARKREISREGLER.ud_Max_I_Anteil.ToString();
                    solar[19][i] = boilerParameter.HEIZKREISPARAMETER._SOLAR_ARRAY[i].SOLARKREISSOLLWERTE.iMaxDrehzahl1.ToString();
                    solar[20][i] = boilerParameter.HEIZKREISPARAMETER._SOLAR_ARRAY[i].SOLARKREISSOLLWERTE.udMinDrehzahl1.ToString();
                    solar[21][i] = boilerParameter.HEIZKREISPARAMETER._SOLAR_ARRAY[i].Hep_min_Spannung1.ToString();
                    solar[22][i] = boilerParameter.HEIZKREISPARAMETER._SOLAR_ARRAY[i].iVerzoegerungSekPumpe_SLM.ToString();

                    //Regelkreis 2
                    solar[23][i] = boilerParameter.HEIZKREISPARAMETER._SOLAR_ARRAY[i].Signaltyp_AnOut1.ToString();
                    solar[24][i] = boilerParameter.HEIZKREISPARAMETER._SOLAR_ARRAY[i].PWM_AnOut2_Invers.ToString();
                    solar[25][i] = boilerParameter.HEIZKREISPARAMETER._SOLAR_ARRAY[i].SOLARKREISSOLLWERTE.iMaxDrehzahl2.ToString();
                    solar[26][i] = boilerParameter.HEIZKREISPARAMETER._SOLAR_ARRAY[i].SOLARKREISSOLLWERTE.iMinDrehzahl2.ToString();
                    solar[27][i] = boilerParameter.HEIZKREISPARAMETER._SOLAR_ARRAY[i].iHep_min_Spannung2.ToString();

                    if (boilerParameter.HEIZKREISPARAMETER._SOLAR_ARRAY[i].Regelkreise == 3)
                    {
                        i++;
                        //Fühlermodul 2
                        solar[28][i] = boilerParameter.HEIZKREISPARAMETER._SOLAR_ARRAY[i].Kollektorfuehlertyp.ToString();
                        solar[29][i] = boilerParameter.HEIZKREISPARAMETER._SOLAR_ARRAY[i].Speicherfuehler1Typ.ToString();
                        solar[30][i] = boilerParameter.HEIZKREISPARAMETER._SOLAR_ARRAY[i].Speicherfuehler2Typ.ToString();
                        solar[31][i] = (boilerParameter.HEIZKREISPARAMETER._SOLAR_ARRAY[i].bsiFuehlertyp.value & 1).ToString();                       //Fühlertyp I3
                        solar[32][i] = (boilerParameter.HEIZKREISPARAMETER._SOLAR_ARRAY[i].bsiFuehlertyp.value & 4).ToString();                       //Fühlertyp I5                                                                            //Fühlertyp I5
                        solar[33][i] = (boilerParameter.HEIZKREISPARAMETER._SOLAR_ARRAY[i].bsiFuehlertyp.value & 16).ToString();                      //Fühlertyp I10  

                        //Regelkreis 3
                        solar[34][i] = boilerParameter.HEIZKREISPARAMETER._SOLAR_ARRAY[i].Wellenpaket.ToString();
                        solar[35][i] = boilerParameter.HEIZKREISPARAMETER._SOLAR_ARRAY[i].Signaltyp_AnOut1.ToString();
                        solar[36][i] = boilerParameter.HEIZKREISPARAMETER._SOLAR_ARRAY[i].PWM_AnOut1_Invers.ToString();
                        solar[37][i] = boilerParameter.HEIZKREISPARAMETER._SOLAR_ARRAY[i].SOLARKREISSOLLWERTE.iMaxDrehzahl1.ToString();
                        solar[38][i] = boilerParameter.HEIZKREISPARAMETER._SOLAR_ARRAY[i].SOLARKREISSOLLWERTE.udMinDrehzahl1.ToString();
                        solar[39][i] = boilerParameter.HEIZKREISPARAMETER._SOLAR_ARRAY[i].Hep_min_Spannung1.ToString();
                    }

                }
            }


            data.Add(new MultipleValues("Kollektorfuehlertyp", language, solar[0], 0));    //0 Fühlereinstellungen Modul
            data.Add(new MultipleValues("Speicherfuehler1Typ", language, solar[1], 0));
            data.Add(new MultipleValues("Speicherfuehler2Typ", language, solar[2], 0));
            data.Add(new MultipleValues("bsiFuehlertyp_I3", language, solar[3], 0));
            data.Add(new MultipleValues("bsiFuehlertyp_I5", language, solar[4], 0));
            data.Add(new MultipleValues("bsiFuehlertyp_I10", language, solar[5], 0));

            data.Add(new MultipleValues("Regelkreise", language, solar[6], 1));   //1 Regelkreis 1
            data.Add(new MultipleValues("Wellenpaket", language, solar[7], 1));
            data.Add(new MultipleValues("Signaltyp_AnOut", language, solar[8], 1));
            data.Add(new MultipleValues("PWM_AnOut_Invers", language, solar[9], 1));
            data.Add(new MultipleValues("udDrehzahlregelung", language, solar[10], 1));
            data.Add(new MultipleValues("ud_PI_Intervall", language, solar[11], 1));
            data.Add(new MultipleValues("ud_DIV_P", language, solar[12], 1));
            data.Add(new MultipleValues("ud_DIV_I", language, solar[13], 1));
            data.Add(new MultipleValues("ud_DIV_D", language, solar[14], 1));
            data.Add(new MultipleValues("ud_PI_P_Faktor", language, solar[15], 1));
            data.Add(new MultipleValues("ud_PI_I_Faktor", language, solar[16], 1));
            data.Add(new MultipleValues("ud_PID_D_Faktor", language, solar[17], 1));
            data.Add(new MultipleValues("ud_Max_I_Anteil", language, solar[18], 1));
            data.Add(new MultipleValues("iMaxDrehzahl", language, solar[19], 1));
            data.Add(new MultipleValues("iMinDrehzahl", language, solar[20], 1));
            data.Add(new MultipleValues("Hep_min_Spannung", language, solar[21], 1));
            data.Add(new MultipleValues("iVerzoegerungSekPumpe_SLM", language, solar[22], 1));

            data.Add(new MultipleValues("Signaltyp_AnOut", language, solar[23], 2));    //2 Regelkreis 2
            data.Add(new MultipleValues("PWM_AnOut_Invers", language, solar[24], 2));
            data.Add(new MultipleValues("iMaxDrehzahl", language, solar[25], 2));
            data.Add(new MultipleValues("iMinDrehzahl", language, solar[26], 2));
            data.Add(new MultipleValues("Hep_min_Spannung", language, solar[27], 2));

            data.Add(new MultipleValues("Kollektorfuehlertyp", language, solar[28], 3));    //3 Fühlereinstellungen Modul 2
            data.Add(new MultipleValues("Speicherfuehler1Typ", language, solar[29], 3));
            data.Add(new MultipleValues("Speicherfuehler2Typ", language, solar[30], 3));
            data.Add(new MultipleValues("bsiFuehlertyp_I3", language, solar[31], 3));
            data.Add(new MultipleValues("bsiFuehlertyp_I5", language, solar[32], 3));
            data.Add(new MultipleValues("bsiFuehlertyp_I10", language, solar[33], 3));

            data.Add(new MultipleValues("Wellenpaket", language, solar[34], 4));
            data.Add(new MultipleValues("Signaltyp_AnOut", language, solar[35], 4));    //4 Regelkreis 3
            data.Add(new MultipleValues("PWM_AnOut_Invers", language, solar[36], 4));
            data.Add(new MultipleValues("iMaxDrehzahl", language, solar[37], 4));
            data.Add(new MultipleValues("iMinDrehzahl", language, solar[38], 4));
            data.Add(new MultipleValues("Hep_min_Spannung", language, solar[39], 4));

            return data;
        }



        #endregion

        #region DifferenceModuls
        static public IList<MultipleValues> differenceModuls1()
        {

            IList<MultipleValues> data = new List<MultipleValues>();

            string[][] differenceModuls = new string[13][];
            for (int i = 0; i < differenceModuls.Length; i++)
            {
                differenceModuls[i] = new string[boilerParameter._DIFFERENZREGLER_ARRAY.Length];
            }

            for (int i = 0; i < boilerParameter._DIFFERENZREGLER_ARRAY.Length; i++)
            {
                if (boilerParameter._DIFFERENZREGLER_ARRAY[i].Differenzregelkreis_vorhanden == 1)
                {
                    //Betriebsart
                    differenceModuls[0][i] = boilerParameter._DIFFERENZREGLER_ARRAY[i].DIFF_BETRIEBSART_1.ToString();

                    //Systemparameter
                    differenceModuls[1][i] = boilerParameter._DIFFERENZREGLER_ARRAY[i].Diff_Fuehlereingang1.ToString();
                    differenceModuls[2][i] = boilerParameter._DIFFERENZREGLER_ARRAY[i].Diff_Fuehlereingang2.ToString();
                    differenceModuls[3][i] = boilerParameter._DIFFERENZREGLER_ARRAY[i].Diff_Einschaltdifferenz12.ToString();
                    differenceModuls[4][i] = boilerParameter._DIFFERENZREGLER_ARRAY[i].Diff_Ausschaltdifferenz12.ToString();
                    differenceModuls[5][i] = boilerParameter._DIFFERENZREGLER_ARRAY[i].Diff_Min_Temp_1_ein.ToString();
                    differenceModuls[6][i] = boilerParameter._DIFFERENZREGLER_ARRAY[i].Diff_Min_Temp_1_aus.ToString();
                    differenceModuls[7][i] = boilerParameter._DIFFERENZREGLER_ARRAY[i].Diff_Max_Temp_2_ein.ToString();
                    differenceModuls[8][i] = boilerParameter._DIFFERENZREGLER_ARRAY[i].Diff_Max_Temp_2_aus.ToString();
                    differenceModuls[9][i] = boilerParameter._DIFFERENZREGLER_ARRAY[i].Diff_Ausg1_invers.ToString();
                    differenceModuls[10][i] = boilerParameter._DIFFERENZREGLER_ARRAY[i].Diff_Fachmannebene_immer_sichtbar.ToString();
                    differenceModuls[11][i] = boilerParameter._DIFFERENZREGLER_ARRAY[i].Diff_Kreis1_zusaetzlicheEinschaltbedingung.ToString();
                    differenceModuls[12][i] = boilerParameter._DIFFERENZREGLER_ARRAY[i].Blockadeschutz1.ToString();
                }
            }

            data.Add(new MultipleValues("DIFF_BETRIEBSART", language, differenceModuls[0], 0));    //0 Betriebsart



            if (boilerParameter.KESSELTYP == _KESSELPARAMETER._KESSELTYP.WP || boilerParameter.KESSELTYP == _KESSELPARAMETER._KESSELTYP.HK_Regelung)
            {
                data.Add(new MultipleValues("WP_Diff_Fuehlereingang1", language, differenceModuls[1], 1));    //1 Systemparameter
                data.Add(new MultipleValues("WP_Diff_Fuehlereingang2", language, differenceModuls[2], 1));
            }
            else
            {
                data.Add(new MultipleValues("Kessel_Diff_Fuehlereingang1", language, differenceModuls[1], 1));
                data.Add(new MultipleValues("Kessel_Diff_Fuehlereingang2", language, differenceModuls[2], 1));
            }

            data.Add(new MultipleValues("Diff_Einschaltdifferenz", language, differenceModuls[3], 1));
            data.Add(new MultipleValues("Diff_Ausschaltdifferenz", language, differenceModuls[4], 1));
            data.Add(new MultipleValues("Diff_Min_Temp_1_ein", language, differenceModuls[5], 1));
            data.Add(new MultipleValues("Diff_Min_Temp_1_aus", language, differenceModuls[6], 1));
            data.Add(new MultipleValues("Diff_Max_Temp_2_ein", language, differenceModuls[7], 1));
            data.Add(new MultipleValues("Diff_Max_Temp_2_aus", language, differenceModuls[8], 1));
            data.Add(new MultipleValues("Diff_Ausg_invers", language, differenceModuls[9], 1));
            data.Add(new MultipleValues("Diff_Fachmannebene_immer_sichtbar", language, differenceModuls[10], 1));
            data.Add(new MultipleValues("Diff_Kreis_zusaetzlicheEinschaltbedingung", language, differenceModuls[11], 1));
            data.Add(new MultipleValues("Blockadeschutz", language, differenceModuls[12], 1));

            return data;
        }
        static public IList<MultipleValues> differenceModuls2()
        {

            IList<MultipleValues> data = new List<MultipleValues>();

            string[][] differenceModuls = new string[13][];
            for (int i = 0; i < differenceModuls.Length; i++)
            {
                differenceModuls[i] = new string[boilerParameter._DIFFERENZREGLER_ARRAY.Length];
            }

            for (int i = 0; i < boilerParameter._DIFFERENZREGLER_ARRAY.Length; i++)
            {
                if (boilerParameter._DIFFERENZREGLER_ARRAY[i].Differenzregelkreis_vorhanden == 1)
                {
                    //Betriebsart
                    differenceModuls[0][i] = boilerParameter._DIFFERENZREGLER_ARRAY[i].DIFF_BETRIEBSART_2.ToString();

                    //Systemparameter
                    differenceModuls[1][i] = boilerParameter._DIFFERENZREGLER_ARRAY[i].Diff_Fuehlereingang3.ToString();
                    differenceModuls[2][i] = boilerParameter._DIFFERENZREGLER_ARRAY[i].Diff_Fuehlereingang4.ToString();
                    differenceModuls[3][i] = boilerParameter._DIFFERENZREGLER_ARRAY[i].Diff_Einschaltdifferenz34.ToString();
                    differenceModuls[4][i] = boilerParameter._DIFFERENZREGLER_ARRAY[i].Diff_Ausschaltdifferenz34.ToString();
                    differenceModuls[5][i] = boilerParameter._DIFFERENZREGLER_ARRAY[i].Diff_Min_Temp_3_ein.ToString();
                    differenceModuls[6][i] = boilerParameter._DIFFERENZREGLER_ARRAY[i].Diff_Min_Temp_3_aus.ToString();
                    differenceModuls[7][i] = boilerParameter._DIFFERENZREGLER_ARRAY[i].Diff_Max_Temp_4_ein.ToString();
                    differenceModuls[8][i] = boilerParameter._DIFFERENZREGLER_ARRAY[i].Diff_Max_Temp_4_aus.ToString();
                    differenceModuls[9][i] = boilerParameter._DIFFERENZREGLER_ARRAY[i].Diff_Ausg2_invers.ToString();
                    differenceModuls[10][i] = boilerParameter._DIFFERENZREGLER_ARRAY[i].Diff_Fachmannebene_immer_sichtbar.ToString();
                    differenceModuls[11][i] = boilerParameter._DIFFERENZREGLER_ARRAY[i].Diff_Kreis2_zusaetzlicheEinschaltbedingung.ToString();
                    differenceModuls[12][i] = boilerParameter._DIFFERENZREGLER_ARRAY[i].Blockadeschutz2.ToString();
                }
            }

            data.Add(new MultipleValues("DIFF_BETRIEBSART", language, differenceModuls[0], 0));    //0 Betriebsart



            if (boilerParameter.KESSELTYP == _KESSELPARAMETER._KESSELTYP.WP || boilerParameter.KESSELTYP == _KESSELPARAMETER._KESSELTYP.HK_Regelung)
            {
                data.Add(new MultipleValues("WP_Diff_Fuehlereingang1", language, differenceModuls[1], 1));    //1 Systemparameter
                data.Add(new MultipleValues("WP_Diff_Fuehlereingang2", language, differenceModuls[2], 1));
            }
            else
            {
                data.Add(new MultipleValues("Kessel_Diff_Fuehlereingang1", language, differenceModuls[1], 1));
                data.Add(new MultipleValues("Kessel_Diff_Fuehlereingang2", language, differenceModuls[2], 1));
            }

            data.Add(new MultipleValues("Diff_Einschaltdifferenz", language, differenceModuls[3], 1));
            data.Add(new MultipleValues("Diff_Ausschaltdifferenz", language, differenceModuls[4], 1));
            data.Add(new MultipleValues("Diff_Min_Temp_1_ein", language, differenceModuls[5], 1));
            data.Add(new MultipleValues("Diff_Min_Temp_1_aus", language, differenceModuls[6], 1));
            data.Add(new MultipleValues("Diff_Max_Temp_2_ein", language, differenceModuls[7], 1));
            data.Add(new MultipleValues("Diff_Max_Temp_2_aus", language, differenceModuls[8], 1));
            data.Add(new MultipleValues("Diff_Ausg_invers", language, differenceModuls[9], 1));
            data.Add(new MultipleValues("Diff_Fachmannebene_immer_sichtbar", language, differenceModuls[10], 1));
            data.Add(new MultipleValues("Diff_Kreis_zusaetzlicheEinschaltbedingung", language, differenceModuls[11], 1));
            data.Add(new MultipleValues("Blockadeschutz", language, differenceModuls[12], 1));

            return data;
        }

        #endregion

        #region Octoplus
        static public IList<SingleValue> octoplusDifferenceLoadingCircle()
        {

            IList<SingleValue> data = new List<SingleValue>();

            data.Add(new SingleValue("DIFF_BETRIEBSART", language, boilerParameter._DIFF_REGLER_OCTOPLUS.DIFF_BETRIEBSART, 0)); //0 Betriebsmodus

            data.Add(new SingleValue("Kessel_Diff_Fuehlereingang1", language, boilerParameter._DIFF_REGLER_OCTOPLUS.Diff_Fuehlereingang1, 1)); //1 Systemparameter
            data.Add(new SingleValue("Kessel_Diff_Fuehlereingang2", language, boilerParameter._DIFF_REGLER_OCTOPLUS.Diff_Fuehlereingang2, 1));
            data.Add(new SingleValue("Diff_Einschaltdifferenz", language, boilerParameter._DIFF_REGLER_OCTOPLUS.Diff_Einschaltdifferenz, 1));
            data.Add(new SingleValue("Diff_Ausschaltdifferenz", language, boilerParameter._DIFF_REGLER_OCTOPLUS.Diff_Ausschaltdifferenz, 1));
            data.Add(new SingleValue("Diff_Min_Temp_1_ein", language, boilerParameter._DIFF_REGLER_OCTOPLUS.Diff_Min_Temp_1_ein, 1));
            data.Add(new SingleValue("Diff_Min_Temp_1_aus", language, boilerParameter._DIFF_REGLER_OCTOPLUS.Diff_Min_Temp_1_aus, 1));
            data.Add(new SingleValue("Diff_Max_Temp_2_ein", language, boilerParameter._DIFF_REGLER_OCTOPLUS.Diff_Max_Temp_2_ein, 1));
            data.Add(new SingleValue("Diff_Max_Temp_2_aus", language, boilerParameter._DIFF_REGLER_OCTOPLUS.Diff_Max_Temp_2_aus, 1));
            data.Add(new SingleValue("Diff_Ausg_invers", language, boilerParameter._DIFF_REGLER_OCTOPLUS.Diff_Ausg_invers, 1));
            data.Add(new SingleValue("Diff_Fachmannebene_immer_sichtbar", language, boilerParameter._DIFF_REGLER_OCTOPLUS.Diff_Fachmannebene_immer_sichtbar, 1));
            data.Add(new SingleValue("Diff_Kreis_zusaetzlicheEinschaltbedingung", language, boilerParameter._DIFF_REGLER_OCTOPLUS.Diff_Kreis_zusaetzlicheEinschaltbedingung, 1));
            data.Add(new SingleValue("Blockadeschutz", language, boilerParameter._DIFF_REGLER_OCTOPLUS.Blockadeschutz, 1));

            return data;
        }

        static public IList<SingleValue> octoplusDrinkingWater()
        {

            IList<SingleValue> data = new List<SingleValue>();

            data.Add(new SingleValue("TWR_VORRANG", language, boilerParameter.TRINKWASSERBEREICH.TWR_VORRANG, 0));       //0 Allgemeine Einsetllungen
            data.Add(new SingleValue("TWR_Solltemperatur", language, boilerParameter.TRINKWASSERBEREICH.Solltemperatur, 0));
            data.Add(new SingleValue("TWR_Hysterese", language, boilerParameter.TRINKWASSERBEREICH.Hysterese, 0));
            data.Add(new SingleValue("hdStart1", language, boilerParameter.TRINKWASSERBEREICH.hdFreigabe1Start.ToString() + boilerParameter.TRINKWASSERBEREICH.hdFreigabe1Ende.ToString(), 0));
            data.Add(new SingleValue("hdStart2", language, boilerParameter.TRINKWASSERBEREICH.hdFreigabe2Start.ToString() + boilerParameter.TRINKWASSERBEREICH.hdFreigabe2Ende.ToString(), 0));



            return data;
        }
        #endregion

        #region Wetterfrosch
        static public IList<SingleValue> wetterfrosch()
        {

            IList<SingleValue> data = new List<SingleValue>();

            data.Add(new SingleValue("Wettereinfluss_aktiv", language, boilerParameter.WETTEREINSTELLPARAMETER.Wettereinfluss_aktiv, 0)); //0 Wetterfroschfunktion

            if (boilerParameter.WETTEREINSTELLPARAMETER.Wettereinfluss_aktiv == 1)
            {
                data.Add(new SingleValue("HK_Wettereinfluss 1", language, boilerParameter.WETTEREINSTELLPARAMETER.HK_Wettereinfluss[0], 1));                                    //1 Heizkreis
                data.Add(new SingleValue("HK_Wettereinfluss_Glasflaeche 1", language, boilerParameter.WETTEREINSTELLPARAMETER.HK_Wettereinfluss_Glasflaeche[0], 1));
                data.Add(new SingleValue("HK_Wettereinfluss_Glasflaeche_EA 1", language, boilerParameter.WETTEREINSTELLPARAMETER.HK_Wettereinfluss_Glasflaeche_EA[0], 1));

                data.Add(new SingleValue("HK_Wettereinfluss 2", language, boilerParameter.WETTEREINSTELLPARAMETER.HK_Wettereinfluss[1], 1));
                data.Add(new SingleValue("HK_Wettereinfluss_Glasflaeche 2", language, boilerParameter.WETTEREINSTELLPARAMETER.HK_Wettereinfluss_Glasflaeche[1], 1));
                data.Add(new SingleValue("HK_Wettereinfluss_Glasflaeche_EA 2", language, boilerParameter.WETTEREINSTELLPARAMETER.HK_Wettereinfluss_Glasflaeche_EA[1], 1));

                data.Add(new SingleValue("HK_Wettereinfluss 3", language, boilerParameter.WETTEREINSTELLPARAMETER.HK_Wettereinfluss[2], 1));
                data.Add(new SingleValue("HK_Wettereinfluss_Glasflaeche 3", language, boilerParameter.WETTEREINSTELLPARAMETER.HK_Wettereinfluss_Glasflaeche[2], 1));
                data.Add(new SingleValue("HK_Wettereinfluss_Glasflaeche_EA 3", language, boilerParameter.WETTEREINSTELLPARAMETER.HK_Wettereinfluss_Glasflaeche_EA[2], 1));

                data.Add(new SingleValue("HK_Wettereinfluss 4", language, boilerParameter.WETTEREINSTELLPARAMETER.HK_Wettereinfluss[3], 1));
                data.Add(new SingleValue("HK_Wettereinfluss_Glasflaeche 4", language, boilerParameter.WETTEREINSTELLPARAMETER.HK_Wettereinfluss_Glasflaeche[3], 1));
                data.Add(new SingleValue("HK_Wettereinfluss_Glasflaeche_EA 4", language, boilerParameter.WETTEREINSTELLPARAMETER.HK_Wettereinfluss_Glasflaeche_EA[3], 1));

                data.Add(new SingleValue("HK_Wettereinfluss 5", language, boilerParameter.WETTEREINSTELLPARAMETER.HK_Wettereinfluss[4], 1));
                data.Add(new SingleValue("HK_Wettereinfluss_Glasflaeche 5", language, boilerParameter.WETTEREINSTELLPARAMETER.HK_Wettereinfluss_Glasflaeche[4], 1));
                data.Add(new SingleValue("HK_Wettereinfluss_Glasflaeche_EA 5", language, boilerParameter.WETTEREINSTELLPARAMETER.HK_Wettereinfluss_Glasflaeche_EA[4], 1));

                data.Add(new SingleValue("HK_Wettereinfluss 6", language, boilerParameter.WETTEREINSTELLPARAMETER.HK_Wettereinfluss[5], 1));
                data.Add(new SingleValue("HK_Wettereinfluss_Glasflaeche 6", language, boilerParameter.WETTEREINSTELLPARAMETER.HK_Wettereinfluss_Glasflaeche[5], 1));
                data.Add(new SingleValue("HK_Wettereinfluss_Glasflaeche_EA 6", language, boilerParameter.WETTEREINSTELLPARAMETER.HK_Wettereinfluss_Glasflaeche_EA[5], 1));

                data.Add(new SingleValue("HK_Wettereinfluss 7", language, boilerParameter.WETTEREINSTELLPARAMETER.HK_Wettereinfluss[6], 1));
                data.Add(new SingleValue("HK_Wettereinfluss_Glasflaeche 7", language, boilerParameter.WETTEREINSTELLPARAMETER.HK_Wettereinfluss_Glasflaeche[6], 1));
                data.Add(new SingleValue("HK_Wettereinfluss_Glasflaeche_EA 7", language, boilerParameter.WETTEREINSTELLPARAMETER.HK_Wettereinfluss_Glasflaeche_EA[6], 1));

                data.Add(new SingleValue("HK_Wettereinfluss 8", language, boilerParameter.WETTEREINSTELLPARAMETER.HK_Wettereinfluss[7], 1));
                data.Add(new SingleValue("HK_Wettereinfluss_Glasflaeche 8", language, boilerParameter.WETTEREINSTELLPARAMETER.HK_Wettereinfluss_Glasflaeche[7], 1));
                data.Add(new SingleValue("HK_Wettereinfluss_Glasflaeche_EA 8", language, boilerParameter.WETTEREINSTELLPARAMETER.HK_Wettereinfluss_Glasflaeche_EA[7], 1));

                data.Add(new SingleValue("TWS_Wettereinfluss 1", language, boilerParameter.WETTEREINSTELLPARAMETER.TWS_Wettereinfluss[0], 2));       //2 Trinkwasser
                data.Add(new SingleValue("TWS_Wettereinfluss 2", language, boilerParameter.WETTEREINSTELLPARAMETER.TWS_Wettereinfluss[1], 2));
                data.Add(new SingleValue("TWS_Wettereinfluss 3", language, boilerParameter.WETTEREINSTELLPARAMETER.TWS_Wettereinfluss[2], 2));
                data.Add(new SingleValue("TWS_Wettereinfluss 4", language, boilerParameter.WETTEREINSTELLPARAMETER.TWS_Wettereinfluss[3], 2));
                data.Add(new SingleValue("TW_Bereich_Wettereinfluss", language, boilerParameter.WETTEREINSTELLPARAMETER.TW_Bereich_Wettereinfluss, 2));


                data.Add(new SingleValue("Puffer_Wettereinfluss 1", language, boilerParameter.WETTEREINSTELLPARAMETER.Puffer_Wettereinfluss[0], 3));       //3 Puffer
                data.Add(new SingleValue("Puffer_Wettereinfluss 2", language, boilerParameter.WETTEREINSTELLPARAMETER.Puffer_Wettereinfluss[1], 3));
                data.Add(new SingleValue("Puffer_Wettereinfluss 3", language, boilerParameter.WETTEREINSTELLPARAMETER.Puffer_Wettereinfluss[2], 3));
                data.Add(new SingleValue("Puffer_Wettereinfluss 4", language, boilerParameter.WETTEREINSTELLPARAMETER.Puffer_Wettereinfluss[3], 3));

                data.Add(new SingleValue("HK_Wettereinfluss_max_KelvinMinuten", language, boilerParameter.WETTEREINSTELLPARAMETER.HK_Wettereinfluss_max_KelvinMinuten, 4));       //4 Systemparameter
                data.Add(new SingleValue("TWS_max_KelvinMinuten", language, boilerParameter.WETTEREINSTELLPARAMETER.TWS_max_KelvinMinuten, 4));
                data.Add(new SingleValue("TW_Bereich_max_KelvinMinuten", language, boilerParameter.WETTEREINSTELLPARAMETER.TW_Bereich_max_KelvinMinuten, 4));
                data.Add(new SingleValue("Brennersperre_Wetter", language, boilerParameter.WETTER_BRENNERSPERRE.Brennersperre_Wetter, 4));
                data.Add(new SingleValue("Brennersperre_Prognose", language, boilerParameter.WETTER_BRENNERSPERRE.Brennersperre_Prognose, 4));
                data.Add(new SingleValue("Brennersperre_AT", language, boilerParameter.WETTER_BRENNERSPERRE.Brennersperre_AT, 4));
            }

            return data;
        }



        #endregion

    }
}