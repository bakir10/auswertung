﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using LogdataReader.LineWrappers;

namespace LogdataReader
{

    #region Enums

    /// <summary>
    /// Alle möglichen Arten von Zeilen
    /// </summary>
    public enum LineType
    {
        Variable = 0,
        StructHeader = 1,
        StructFooter = 2,
        EnumHeader = 3,
        EnumMember = 4,
        EnumFooter = 5,
        BDINTMember = 6,
        BDINTFooter = 7,
        None = 8,
        BSINTFooter = 9,
        Undefined = 10,
        Array = 11
    }

    public enum DataType
    {
        HDINT = 0,
        SINT = 1,
        INT = 2,
        DINT = 3,
        USINT = 4,
        UINT = 5,
        UDINT = 6,
        BDINT = 7,
        REAL = 8,
        LREAL = 9,
        BOOL = 10,
        CHAR = 11,
        BYTE = 12,
        STRUCT = 13,
        BSINT = 14,
        ENUM = 15,
        UNDEF = 16
    }

    #endregion

    #region Klassen

    /// <summary>
    /// Basisklasse für alle Zeilenumwandler.
    /// Zeilenumwandler dienen dazu, aus der Zeile Informationen zu bekommen und
    /// diese zu formatieren.
    /// Falls eine neue Art Zeile hinzugefügt werden soll, muss eine Klasse erstellt werden,
    /// die von dieser erbt und die enstprechende Zeilen Formattierung implementiert
    /// </summary>
    public class LineWrapper
    {
        public virtual string content
        {
            get;
            set;
        }

        public virtual string getFormattedLine()
        {
            return "Not Implemented";
        }

        public LineWrapper(string line)
        {
            content = line;
        }

        /// <summary>
        /// Fügt gegebenen string in anderen an der Stelle des ersten Buchstabens ein.
        /// </summary>
        /// <param name="line">string, in den eingefügt werden soll</param>
        /// <param name="insert">Einzufügender string</param>
        /// <returns></returns>
        protected string insertInLine(string line, string insert)
        {
            int firstLetterPos = searchFirstLetter(line);
            if (firstLetterPos >= 0)
            {
                line = line.Insert(firstLetterPos, insert);
            }
            return line;
        }

        /// <summary>
        /// Sucht in einem String nach dem ersten Buchstaben oder _
        /// Kommt der Buchstabe nicht vor, wird -1 zurückgegeben
        /// </summary>
        /// <param name="search">String, in dem gesucht werden soll</param>
        /// <returns>Gefundene Position</returns>
        protected int searchFirstLetter(string search, int start = 0)
        {
            int result = -1, i;

            for (i = start; i < search.Length; i++)
            {
                if (char.IsLetter(search[i]) || search[i] == '_')
                {
                    result = i;
                    break;
                }
            }

            return result;
        }
    }

    /// <summary>
    /// Representäiert eine Zeile
    /// </summary>
    public class Line
    {

        #region Public Variables
        public string line
        {
            get;
            private set;
        }

        public LineType lineType
        {
            get;
            private set;
        }

        /// <summary>
        /// Zeilenmwandler der Zeile.
        /// Um welche Art es sich handlet lässt sich vom lineType ableiten
        /// </summary>
        public LineWrapper lineWrapper
        {
            get;
            private set;
        }
        #endregion

        // Konstanten
        protected const string typedef = "typedef";
        protected const string structHeader = "struct";
        protected const string structFooter = "}";
        protected const string variable = ";";
        protected const string array = "[";
        protected const string enumMember = "#define";

        /// <summary>
        /// Konstruktor für Zeilenklasse.
        /// Initialisiert Zeile und ermittelt Zeilentypen 
        /// Bei Implementation neuer Zeile muss die Logik zur Ermittlung des Zeilentyps heir eingebaut werden.
        /// </summary>
        /// <param name="_line">Zeileninhalt</param>
        /// <param name="isInsideStruct">Befindet sich die Zeile in einem Struct?</param>
        /// <param name="prevLineType">Typ vorhergehender Zeile</param>
        /// <param name="nextLineType">Typ nachgehender Zeile</param>
        public Line(string _line, bool isInsideStruct = false, LineType prevLineType = LineType.Undefined, LineType nextLineType = LineType.Undefined)
        {
            line = _line;

            if (line.Contains(structHeader))
            {
                string[] lineSplit = line.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                if (lineSplit.Length <= 1) // BDINTs haben keinen Namen
                {
                    lineWrapper = new BDINTMember(line);
                    lineType = LineType.BDINTMember;
                }
                else
                {
                    lineWrapper = new StructHeader(line, isInsideStruct);
                    lineType = LineType.StructHeader;
                }

            }
            else if (line.Contains(typedef))
            {
                if (nextLineType == LineType.EnumMember)
                {
                    lineWrapper = new EnumHeader(line, isInsideStruct);
                    lineType = LineType.EnumHeader;
                }
                else
                {
                    // typedefs, die kein Enum starten werden ignoriert
                    lineType = LineType.Undefined;
                }
            }
            else if (line.Contains(structFooter))
            {
                if (prevLineType == LineType.Undefined)
                {
                    lineWrapper = new BSINTFooter(line);
                    lineType = LineType.BSINTFooter;
                }
                else if (prevLineType == LineType.BDINTMember)
                {
                    lineWrapper = new BDINTFooter(line);
                    lineType = LineType.BDINTFooter;
                }
                else
                {
                    lineWrapper = new StructFooter(line);
                    lineType = LineType.StructFooter;
                }
            }
            else if (line.Contains("#define"))
            {
                lineWrapper = new EnumMember(line);
                lineType = LineType.EnumMember;
            }
            else if (line.Contains(":") && !line.Contains("//")) // Zeilen, die : enthalten gehören einem BDINT an!
            {
                lineWrapper = new BDINTMember(line);
                lineType = LineType.BDINTMember;
            }
            else if (line.Contains(variable))
            {
                if (nextLineType == LineType.EnumMember)
                {
                    lineWrapper = new EnumHeader(line);
                    lineType = LineType.EnumHeader;
                }
                else
                {
                    lineWrapper = new Variable(line);
                    lineType = LineType.Variable;
                }
            }
            else if (prevLineType == LineType.BDINTMember)
            {
                lineType = LineType.Undefined;
            }
            else
            {
                lineType = LineType.None;
            }

        }

        public Line(string _line, LineType lineType)
        {
            line = _line;

            this.lineType = lineType;

            switch (lineType)
            {
                case LineType.Variable: lineWrapper = new Variable(line); break;
                case LineType.EnumHeader: lineWrapper = new EnumHeader(line); break;
                case LineType.EnumFooter: lineWrapper = new EnumFooter(line); break;
                case LineType.EnumMember: lineWrapper = new EnumMember(line); break;
                case LineType.StructFooter: lineWrapper = new StructFooter(line); break;
                case LineType.StructHeader: lineWrapper = new StructHeader(line, true); break;
                default: break;
            }
        }

        public string getFormattedLine()
        {
            string result = string.Empty;

            if (lineWrapper != null)
            {
                result = lineWrapper.getFormattedLine();
            }
            else if (!line.Contains("#pragma") && lineType != LineType.Undefined) // #pragma Zeilen werden weggelassen
            {
                result = line + "\n";
            }

            return result;
        }
    }

    #endregion

}
