﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
//using LogdataReader.LineWrappers;

namespace LogdataReader
{

    /// <summary>
    /// Representiert ein Enum (=Enumerator)
    /// </summary>
    public class Enum
    {

        // Statische Liste, um zu vermeidnen, dass Enums doppelt geschrieben werden
        public static List<string> enumsWritten = new List<string>();

        public string name;
        public bool isInsideStruct;
        public int length;

        public List<Line> lines = new List<Line>();

        /// <summary>
        /// Fängt bei gegebener Start-Position an, File zu durchlaufen, bis das Enum endet
        /// </summary>
        /// <param name="rawLines">Gesamtes File, Zeilenweise</param>
        /// <param name="start">Startzeile</param>
        /// <param name="isInsideStruct">Befindet sich das Enum in einem Struct?</param>
        public Enum(List<string> rawLines, int start, bool isInsideStruct)
        {
            this.isInsideStruct = isInsideStruct;

            LineType prevType = LineType.Undefined, nextType = LineType.Undefined;
            Line line, nextLine;

            int i;
            for (i = start; i < rawLines.Count; i++)
            {
                if (i + 1 < rawLines.Count)
                {
                    nextLine = new Line(rawLines[i + 1], isInsideStruct);
                    nextType = nextLine.lineType;
                }

                line = new Line(rawLines[i], isInsideStruct, prevType, nextType);
                if (line.lineType == LineType.EnumHeader)
                {
                    if (i == start)
                    {
                        EnumHeader enumHeader = (EnumHeader)line.lineWrapper;
                        name = enumHeader.enumName;
                    }
                    lines.Add(line);
                }
                else if (line.lineType == LineType.EnumMember)
                {
                    prevType = line.lineType;
                    lines.Add(line);
                }
                else
                {
                    break;
                }
            }
        }

        /// <summary>
        /// Schreibt Enum in Stream
        /// </summary>
        /// <param name="writer"></param>
        public void writeAll(StreamWriter writer)
        {
            if (!enumsWritten.Contains(this.name)) // Keine doppelten Enums!
            {
                enumsWritten.Add(this.name);
                int i;
                for (i = 0; i < lines.Count; i++)
                {
                    string toWrite = lines[i].getFormattedLine();
                    // Enumende wird an letzte Zeile angehängt.
                    if (i + 1 >= lines.Count)
                    {
                        toWrite += "}\n";
                    }
                    writer.Write(toWrite);
                }
            }
        }

    }
}
