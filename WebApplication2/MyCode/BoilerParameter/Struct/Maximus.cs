﻿using System.Runtime.InteropServices;
using SINT = System.Byte;
using INT = System.Int16;
using DINT = System.Int32;
using USINT = System.Byte;
using UINT = System.UInt16;
using UDINT = System.UInt32;
using REAL = System.Single;
using LREAL = System.Double;
using BOOL = System.Byte;
using CHAR = System.SByte;
using BYTE = System.SByte;

namespace LogdataReader
{
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public unsafe struct _MAXIMUS
    {
        public _Struct_EinschubBetrieb _EINSCHUB_BETRIEB;
        public _Struct_EinschubZuendung _EINSCHUB_ZUENDPHASE;
        public _Struct_Saugzuggeblaese _SAUGZUGGEBLAESE_HACKGUT;
        public _ALLGEMEINE_EINSTELLUNGEN ALLGEMEINE_EINSTELLUNGEN;
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public unsafe struct _ALLGEMEINE_EINSTELLUNGEN
        {
            public DINT Hohe_Drehzahl_1_Zuendimpuls;
            public DINT MinAGT;
            public DINT MaxAGT;
            public DINT Schaltschranktemp;
            public DINT AGT_Hysterese;
            public DINT AGTWarmStart;
            public DINT Kesselsolltemperatur;
            public DINT SollAGT;
            public DINT MinDZ_SGT;
            public DINT StructItem5;
            public DINT Drucksensor;
            public DINT AGT_Anhebung_vorhanden;
            public UDINT LDZ_Nachtabsenkung;
            public HDINT NachtabsenkungStart;
            public HDINT NachtabsenkungEnde;
            public DINT Brennraumfuehler_vorhanden;
            public _KBA KBA;
            public enum _KBA : UDINT
            {
                _BA_PELLETS = 0,
                _BA_HACKGUT = 1,
            }
        }
        public _FU_EINSCHUB FU_EINSCHUB;
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public unsafe struct _FU_EINSCHUB
        {
            public UDINT AbsMax_Frequenz;
            public UDINT AbsMin_Frequenz;
            public UDINT Taktung_Periodendauer;
            public DINT Stromueberwachung;
            public DINT Hochlaufzeit;
            public DINT Tieflaufzeit;
            public DINT Motor_Nennleistung;
            public DINT Motor_Nennspannung;
            public DINT Motor_Nennstrom;
            public DINT Motor_Nenndrehzahl;
            public DINT Motor_Nennfrequenz;
            public DINT Umrichter_Ueberlast_Warnung;
            public DINT Umrichter_Ueberlast_Alarm;
            public DINT Motor_Ueberlast_Warnung;
            public DINT Motor_Ueberlast_Alarm;
            public DINT Motor_Ueberstromschutz;
            public DINT Motor_Ueberstromgrenze;
            public DINT StructItem1;
            public DINT StructItem2;
            public DINT StructItem3;
            public DINT StructItem4;
            public DINT StructItem5;
            public DINT StructItem6;
            public DINT StructItem7;
            public DINT StructItem8;
            public DINT StructItem9;
            public DINT StructItem10;
            public DINT StructItem11;
        }
        public _Struct_Luftklappen _LUFTKLAPPEN_PE;
        public _Struct_Luftklappen _LUFTKLAPPEN_HG;
        public _Struct_Zuendung _ZUENDUNG;
        public _Struct_Raumaustragung _RAUMAUSTRAGUNG_HG;
        public _Struct_Raumaustragung _RAUMAUSTRAGUNG_PE;
        public _RAUMAUSTRAGUNG RAUMAUSTRAGUNG;
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public unsafe struct _RAUMAUSTRAGUNG
        {
            public DINT Stromueberwachung_1;
            public DINT maxStrom_1;
            public DINT maxZeit_Ueberstrom_1;
            public DINT Stromueberwachung_2;
            public DINT maxStrom_2;
            public DINT maxZeit_Ueberstrom_2;
            public DINT Stromueberwachung_3;
            public DINT maxStrom_3;
            public DINT maxZeit_Ueberstrom_3;
            public DINT StructItem2;
            public DINT StructItem3;
            public DINT StructItem4;
            public DINT StructItem5;
            public DINT StructItem6;
            public DINT StructItem7;
            public DINT StructItem8;
            public DINT StructItem9;
            public DINT StructItem10;
            public DINT StructItem11;
            public DINT StructItem12;
            public DINT StructItem13;
            public DINT StructItem14;
        }
        public _ASCHEAUSTRAGUNG ASCHEAUSTRAGUNG;
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public unsafe struct _ASCHEAUSTRAGUNG
        {
            public UDINT AscheBrenner_LzStart;
            public UDINT AscheBrenner_Impuls;
            public DINT AscheWTR_maxLaufzeit;
            public UDINT AscheWTR_Lz_sek_pro_h;
            public DINT AscheWTR_Stromueberwachung;
            public DINT AscheWTR_maxStrom;
            public UDINT AscheBrenner_Pause;
            public DINT Stufenrost_Stromueberwachung;
            public DINT Stufenrost_maxStrom;
            public UDINT Stufenrost_BMIN_Stoerung;
            public DINT AscheBrenner_Stromueberwachung;
            public DINT AscheBrenner_maxStrom;
            public DINT AscheBrenner_Tk_Ueberwachung;
            public DINT Luefterdrehzahl_WTR;
            public DINT Ascheboxschalter_vorhanden;
            public DINT AscheBrenner_Impulse;
            public DINT AscheWTR_Impulse;
        }
        public _STUFENROST STUFENROST;
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public unsafe struct _STUFENROST
        {
            public UDINT Laufzeit_sek_pro_h;
            public DINT minPause;
            public DINT maxPause;
            public DINT Impuls;
            public DINT StructItem;
            public DINT StructItem0;
            public DINT StructItem1;
            public DINT StructItem2;
            public DINT StructItem3;
            public DINT StructItem4;
            public DINT StructItem5;
            public DINT StructItem6;
            public DINT StructItem7;
        }
        public _PARTIKELFILLTER PARTIKELFILLTER;
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public unsafe struct _PARTIKELFILLTER
        {
            public DINT AGT_Startfreigabe;
            public DINT max_Spannung;
            public DINT max_Strom;
            public UDINT Rampe;
            public DINT Ueberwachung_Leistung;
            public UDINT Leistung_Konstant;
            public DINT Leistung_Stopp;
            public DINT StructItem2;
            public DINT StructItem3;
            public DINT StructItem4;
            public DINT StructItem5;
            public DINT StructItem6;
        }
        public _FREIGABEZEITEN _ASCHE_FREIGABEZEIT;
        public _ASCHETONNE ASCHETONNE;
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public unsafe struct _ASCHETONNE
        {
            public DINT Aschetonne_vorhanden;
            public DINT Stromueberwachung;
            public DINT maxStrom;
            public DINT StructItem;
            public DINT StructItem0;
            public DINT StructItem1;
            public DINT StructItem2;
            public DINT StructItem3;
            public DINT StructItem4;
            public DINT StructItem5;
            public DINT StructItem6;
            public DINT StructItem7;
            public DINT StructItem8;
        }
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4 + 1 - 1)]
        public _Mx_Puffer_Struct[] _PUFFER_ARRAY;
        public _RAUMAUSTRAGUNG_3 RAUMAUSTRAGUNG_3;
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public unsafe struct _RAUMAUSTRAGUNG_3
        {
            public UDINT Einschaltverzoegerung_Austragung_3;
            public UDINT maxLaufzeit_Austragung_3;
            public DINT Sensorueberwachung_3;
            public DINT Sensor_Einschaltverz_3;
            public DINT Sensor_Einschublaufzeit_3;
            public DINT Sensor_maxEinschublaufzeit_3;
            public UDINT Sensor_Ausschaltverz_3;
            public UDINT Sensor_maxImpuls_3;
            public DINT StructItem6;
            public DINT StructItem7;
            public DINT StructItem8;
            public DINT StructItem9;
            public DINT StructItem10;
            public DINT StructItem11;
            public DINT StructItem12;
            public DINT StructItem13;
            public DINT StructItem14;
            public DINT StructItem15;
            public DINT StructItem16;
            public DINT StructItem17;
            public DINT StructItem18;
        }
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 2136 + 1 - 0)]
        public DINT[] MAXIMUS_Reserve;
    }
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public unsafe struct _Struct_Luftklappen
    {
        public DINT SLK_minPosition;
        public DINT SLK_maxPosition;
        public DINT PLK_minPosition;
        public DINT PLK_maxPosition;
        public DINT SLK_ES_Verkuerzung;
        public DINT REZI_Automatik;
        public DINT REZI_AGT;
        public DINT REZI_BRT;
        public DINT REZI_Sollmin;
        public DINT REZI_Sollmax;
        public DINT StructItem7;
        public DINT StructItem8;
        public DINT StructItem;
        public DINT StructItem0;
        public DINT StructItem9;
        public DINT StructItem10;
        public DINT StructItem11;
    }
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public unsafe struct _Struct_Raumaustragung
    {
        public DINT Sensorueberwachung;
        public DINT Austragungsimpuls;
        public DINT Austragungspause;
        public DINT Sensor_Einschaltverz;
        public DINT Sensor_Einschublaufzeit;
        public DINT Sensor_maxEinschublaufzeit;
        public UDINT Sensor_Ausschaltverz;
        public UDINT Sensor_maxImpuls;
        public UDINT Einschaltverzoegerung_Austragung_2;
        public UDINT maxLaufzeit_Austragung_2;
        public DINT Sensorueberwachung_2;
        public DINT Sensor_Einschaltverz_2;
        public DINT Sensor_Einschublaufzeit_2;
        public DINT Sensor_maxEinschublaufzeit_2;
        public UDINT Sensor_Ausschaltverz_2;
        public UDINT Sensor_maxImpuls_2;
        public DINT Freq_Offset;
        public DINT StructItem6;
        public DINT StructItem7;
        public DINT StructItem8;
    }
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public unsafe struct _Mx_Puffer_Struct
    {
        public DINT max_Unten_X35;
        public DINT Durchladen_X35_bei_AT;
        public DINT Durchladen_X35_aktiv;
        public DINT FK_PTO_Soll;
        public DINT FK_PTO_Hysterese;
        public DINT StructItem;
        public DINT StructItem0;
        public DINT StructItem1;
        public DINT StructItem2;
        public DINT StructItem3;
    }

}