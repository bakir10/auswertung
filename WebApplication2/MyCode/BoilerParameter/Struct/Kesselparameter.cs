﻿using System.Runtime.InteropServices;
using SINT = System.Byte;
using INT = System.Int16;
using DINT = System.Int32;
using USINT = System.Byte;
using UINT = System.UInt16;
using UDINT = System.UInt32;
using REAL = System.Single;
using LREAL = System.Double;
using BOOL = System.Byte;
using CHAR = System.SByte;
using BYTE = System.SByte;

namespace LogdataReader
{
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public unsafe struct _KESSELPARAMETER
    {
        public _WAERMETAUSCHERREINUNG WAERMETAUSCHERREINUNG;
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public unsafe struct _WAERMETAUSCHERREINUNG
        {
            public HDINT hdFg1_StartZeit;
            public HDINT hdFg1_EndZeit;
            public HDINT hdFg2_StartZeit;
            public HDINT hdFg2_EndZeit;
            public HDINT hdFg3_StartZeit;
            public HDINT hdFg3_EndZeit;
            public DINT Zwangsreinigung;  //! <Type Comment="nicht verwendet!" Name="_KESSELPARAMETER._WAERMETAUSCHERREINUNG.Zwangsreinigung"/>
            public UDINT udGesamtlaufzeit;
            public UDINT udPulsdauer;
            public UDINT udPulspause;
            public DINT Blockadestrom;
            public UDINT udMinimaleLaufzeitStueckgut;
            public UDINT udMaximaleLaufzeitStueckgut;
            public UDINT udMinimaleLaufzeitPellets;
            public UDINT udMaximaleLaufzeitPellets;
            public DINT FreigabeKesselTemperatur;
            public DINT StrommessungAktiv;
            public DINT MaxAnzAbgebrochenerReinigungen;
            public DINT FreigabeRauchgastemperatur;
            public UDINT udESLZ_bis_Abbrandimpuls;
            public DINT Impuls_bei_Start;
            public DINT StructItem2;
            public DINT StructItem3;
            public DINT StructItem4;
            public DINT StructItem5;
        }
        public _SAUGAUSTRAGUNG SAUGAUSTRAGUNG;
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public unsafe struct _SAUGAUSTRAGUNG
        {
            public UDINT udEinschaltverzoegerung;
            public UDINT udAusschaltverzoegerung;
            public UDINT udMaxSauglaufzeit;
            public UDINT udMaxEinschublaufzeit;
            public UDINT udStartEinschubProzente;
            public UDINT udMinEinschubProzente;
            public UDINT udAustragungImpuls_Min;
            public UDINT udAustragungImpuls_Sek;
            public UDINT udAustragungPause_Sek;
            public HDINT hdFG1_StartZeit;
            public HDINT hdFG1_EndZeit;
            public HDINT hdFG2_StartZeit;
            public HDINT hdFG2_EndZeit;
            public HDINT hdFG3_StartZeit;
            public HDINT hdFG3_EndZeit;
            public UDINT udSGA_Anz_Zwangsbefuellung;
            public DINT SGA_Abschalttemperatur;
            public UDINT udMaxAustragungsImpulsRA;
            public UDINT udSensorUeberwachungRA;
            public UDINT udMaxEinschubImpulsRA;
            public DINT udThermokontaktUeberwachung;
            public _Umschalteinheit Umschalteinheit;
            public enum _Umschalteinheit : UDINT
            {
                NEIN = 0,
                MIT_3_SONDEN = 1,
                MIT_4_SONDEN = 2,
                MIT_5_SONDEN = 3,
                MIT_6_SONDEN = 4,
                MIT_7_SONDEN = 5,
                MIT_8_SONDEN = 6,
                MIT_9_SONDEN = 7,
                MIT_10_SONDEN = 8,
                MIT_12_SONDEN = 10,
                KASKADE_6_SONDEN = 30,
            }
            public _Sondenumschaltung Sondenumschaltung;
            public enum _Sondenumschaltung : UDINT
            {
                Automatik = 0,
                punktuell = 1,
                nur_Sonde_1 = 2,
                nur_Sonde_2 = 3,
                nur_Sonde_3 = 4,
                nur_Sonde_4 = 5,
                nur_Sonde_5 = 6,
                nur_Sonde_6 = 7,
                nur_Sonde_7 = 8,
                nur_Sonde_8 = 9,
                nur_Sonde_9 = 10,
                nur_Sonde_10 = 11,
                nur_Sonde_11 = 12,
                nur_Sonde_12 = 13,
            }
            public DINT verwendete_Saugsonden;
            public DINT Saugsonde_Minimalzeit;
            public DINT Saugsonde_Maximalzeit;
            public DINT Saugsonde_mit_Freisaugen_beginnen;
            public UDINT udSaugimpuls_SSUE;
        }
        public _KESSELTYP KESSELTYP;
        public enum _KESSELTYP : UDINT
        {
            Plus = 0,
            Top = 1,
            T2 = 2,
            HK_Regelung = 3,
            Elegance = 4,
            WP = 5,
            EcoTopZero = 6,
            Maximus = 7,
            EcoHackZero = 10,
        }
        public _Struct_Zuendung _ZUENDUNG;
        public _Struct_Saugzuggeblaese _SAUGZUGGEBLAESE_PELLETS;
        public _ANLAGENTYP ANLAGENTYP;
        public enum _ANLAGENTYP : UDINT
        {
            MANUELLE_BEFUELLUNG = 0,
            SAUGAUSTRAGUNG = 1,
            RAUMAUSTRAGUNG = 2,
            ManVORRATSBEH_INTERN = 3,
            STEIGSCHNECKE = 4,
            STEIGSCHNECKE_KNICKARM = 5,
        }
        public _SAUGZUGGEBLAESE_SG SAUGZUGGEBLAESE_SG;
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public unsafe struct _SAUGZUGGEBLAESE_SG
        {
            public UDINT udSGAnheizdrehzahl;
            public UDINT udSGLambauntergrenze;
            public UDINT udSGLambdaobergrenze;
            public UDINT udSGMax_Luefterdrehzahl;
            public UDINT udSGMinLuefterdrehzahl;
            public UDINT udSGNachlaufzeit;
            public UDINT udSGAutoAnheizdrehzahl;
            public UDINT udSGZeitLueferdrehzahlhoch;
            public UDINT udSGLDZLambdahoch;
            public UDINT udSGLDZniedrig;
            public UDINT udSGKesseltemperaturPBereich;
            public UDINT udSGRGTPBereichpositiv;
            public UDINT udSGRGTPBereichnegativ;
            public UDINT udSGRGTSoll;
            public UDINT udSGRGTAus;
            public UDINT udSGMinRampe;
            public UDINT udSGMaxRampe;
            public UDINT udLambdazeitRGTAus;
            public UDINT udLDZSchwelgasabsaugung;
            public DINT StructItem;
            public DINT StructItem0;
            public DINT StructItem1;
            public DINT StructItem2;
            public DINT StructItem3;
            public DINT StructItem4;
            public DINT StructItem5;
        }
        public _Struct_EinschubZuendung _EINSCHUB_ZUENDPHASE;
        public _Struct_EinschubBetrieb _EINSCHUB_BETRIEB;
        public _SGZ_DREHZAHL SGZ_DREHZAHL;
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public unsafe struct _SGZ_DREHZAHL
        {
            public UDINT udMaxLuefterdrehzahl;
            public UDINT udImpulseproUmdrehung;
            public DINT StructItem;
            public DINT StructItem0;
            public DINT StructItem1;
            public DINT StructItem3;
            public DINT StructItem2;
        }
        public _KESSELVERWALTUNG KESSELVERWALTUNG;
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public unsafe struct _KESSELVERWALTUNG
        {
            public UDINT udRGTWarmStart;
            public UDINT udRGTSoll;
            public UDINT udMinRGT;
            public UDINT udKesselsolltemperatur;
            public UDINT udKesselstartdifferenz;
            public UDINT udKesselsollueberhoeung;
            public UDINT udKesseltempPrimaerluftZu;
            public UDINT udESTempFreigabetemperatur;
            public UDINT udESMaxTemperatur;
            public UDINT udRGT_Hysterese;
            public UDINT udMaxKesseltemperatur;
            public UDINT udMaxRGT;
            public DINT SOFTWAREVERSION;
            public HDINT hdStart1;
            public HDINT hdEnde1;
            public HDINT hdStart2;
            public HDINT hdEnde2;
            public HDINT hdStart3;
            public HDINT hdEnde3;
            public DINT Fremdkesselfreigabe;
        }
        public _SEKUNDAERLUFTKLAPPE SEKUNDAERLUFTKLAPPE;
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public unsafe struct _SEKUNDAERLUFTKLAPPE
        {
            public DINT Totbereich;
            public UDINT udLaufzeit;
            public DINT Aktiv;
            public DINT LambdaSoll;
            public DINT StructItem;
            public DINT StructItem0;
            public DINT StructItem1;
            public DINT StructItem2;
            public DINT StructItem3;
        }
        public _BRENNER BRENNER;
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public unsafe struct _BRENNER
        {
            public UDINT udPEMaxStartRGT;
            public UDINT udPERGTStartdifferenz;
            public UDINT udSGMaxStartRGT;
            public UDINT udSGRGTStartdifferenz;
            public UDINT udLDZKonstantzeit;
            public UDINT udRGTAbfall_keine_Verkuerzung;
            public UDINT udRGTAbfall_max_Verkuerzung;
            public UDINT udRGT_maxImpulsverkuerzung;
            public UDINT udRGTAbfall_Stop;
            public UDINT udLambdaueberwachung;
            public UDINT udFehlerRGTmax;
            public DINT FehlerRGTmin;
            public UDINT udFehlerRGTZeitBetrieb;
            public UDINT udFehlerRGTZeitStart;
            public UDINT udZeitLambdaUeberwachung;
            public DINT RauchgasAbfallueberwachung;
            public DINT RauchgasfehlertempUeberw;
            public DINT RegelungnachVltSoll;
            public INT Einschalthysterese_Kessel;
            public INT Ausschalthysterese_Kessel;
            public INT iATFrostschutzTempKessel;
            public UINT uiAschenbox_vorhanden;
            public DINT dGleitendeKTSoll;
            public DINT StructItem1;
            public UDINT Kesselbetrieb_Verzoegerung;
            public DINT Raumluft_Modul_vorhanden;
            public DINT Brennwertmodul;
        }
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 100 + 1 - 0)]
        public UDINT[] StockerWerte;
        public _KESSELBETRIEBSART KESSELBETRIEBSART;
        public enum _KESSELBETRIEBSART : UDINT
        {
            KB_KESSEL_AUSGESCHALTET = 0,
            KB_KESSEL_AUTOMATIK = 1,
            KB_KESSEL_MANUELLER_START = 2,
            KB_KESSEL_KAMINFEGER = 3,
            EnumItem93 = 4,
            EnumItem94 = 5,
            EnumItem108 = 6,
        }
        public _HEIZKREISPARAMETER HEIZKREISPARAMETER;
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public unsafe struct _HEIZKREISPARAMETER
        {
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8 + 1 - 1)]
            public _HEIZKREIS_STRUCT[] HK_ARRAY;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4 + 1 - 1)]
            public _PUFFER[] PUFFER_ARRAY;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4 + 1 - 1)]
            public _BOILER[] BOILER_ARRAY;
            public HeizkreisKesselregler _HeizkreisKesselregler;
            [StructLayout(LayoutKind.Sequential, Pack = 1)]
            public unsafe struct HeizkreisKesselregler
            {
                public UDINT udmin_Kessellaufzeit;
                public _KesselreglerTyp KesselreglerTyp;
                public enum _KesselreglerTyp : UDINT
                {
                    HK_FixKT = 0,
                    HK_Gleitende_KT = 1,
                }
                public DINT Moulation_plus_VltSoll;
                public DINT Moulation_minus_VltSoll;
                public _PufferpumpeTyp PufferpumpeTyp;
                public enum _PufferpumpeTyp : UDINT
                {
                    Standart = 0,
                    Bypasspumpe = 1,
                }
                public DINT Modus_Analogausgang;
                public DINT Temp_10V;
                public DINT Temp_0V;
                public HDINT hdFG4_EndZeit;
                public HDINT hdFG5_StartZeit;
                public HDINT hdFG5_EndZeit;
                public _FreigabeArt FreigabeArt;
                public enum _FreigabeArt : UDINT
                {
                    ImmerAus = 0,
                    ImmerEin = 1,
                    Zeitregelung = 2,
                }
                public UDINT udPumpenImpuls;
                public UDINT udPumpenPause;
                public DINT WarmwasserSollTemp;
                public DINT Kontrollimpuls;
                public DINT Zweiter_Autokessel;
                public _FunktionX28 FunktionX28;
                public enum _FunktionX28 : UDINT
                {
                    X28_Fremdkessel = 0,
                    X28_Stoerung = 1,
                    X28_WP_in_Betrieb = 2,
                    X28_Elektro_Heizung = 3,
                    X28_Kuehlbetrieb = 4,
                }
                public DINT AussenFreigabeTemp;
                public DINT FKSolltemperatur;
                public _EnergiequellenTyp EnergiequellenTyp;
                public enum _EnergiequellenTyp : UDINT
                {
                    RGZ_Kessel = 0,
                    RGZ_Fernwaerme = 1,
                }
                public DINT StructItem4;
                public DINT StructItem5;
            }
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4 + 1 - 1)]
            public _FWM_PARAMETER_STRUCT[] FWM_ARRAY;
            public DINT AnzahlHKvorhanden;
            public DINT AnzahlPuffervorhanden;
            public DINT AnzahlBoilervorhanden;
            public DINT AnzahlFWM_Module;
            public DINT Anzahl_DFBG;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4 + 1 - 1)]
            public _SOLAR_STRUCT[] _SOLAR_ARRAY;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4 + 1 - 1)]
            public _WMZ_STRUCT[] _WMZ_ARRAY;
            public DINT Anzahl_SOL_vorhanden;
            public DINT Anzahl_WMZ_vorhanden;
            public DINT Octo_PO_Regelung;
            public DINT WMZ_Berechnung;
        }
        public _SYSTEMPARAMETER SYSTEMPARAMETER;
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public unsafe struct _SYSTEMPARAMETER
        {
            public DINT Standby_Ein;
            public DINT Standby_ZeitbisZfg;
            public UDINT udLbdSonde_Kalibrieren_Nach;
            public DINT KaskadeEIN;
            public UDINT udStandby_Zeitnachtouch;
            public DINT TuerKontrolleEINAUS;
            public DINT SensorTyp_Temp3;
            public DINT SensorTyp_Temp4;
            public DINT SensorTyp_Temp5;
            public DINT SensorTyp_Temp6;
            public DINT SensorTyp_Temp7;
            public DINT SensorTyp_Temp8;
            public DINT SensorTyp_Temp9;
            public DINT SensorTyp_Temp10;
            public DINT SensorTyp_Temp11;
            public DINT SensorTyp_Temp12;
            public DINT SensorTyp_Temp13;
            public DINT SensorTyp_Temp14;
            public DINT SensorTyp_Temp15;
            public DINT SensorTypSol_Temp1;
            public DINT SensorTypSol_Temp2;
            public DINT SensorTypSol_Temp3;
            public DINT SensorTypSol_Temp4;
            public DINT SensorTypSol_Temp5;
            public DINT WiderstandsOffset_RF1;
            public DINT WiderstandsOffset_RF2;
            public DINT Kundentrend_verschluesselt;
            public DINT AT_Abgleich_X42;
            public _FunktionX51 FunktionX51;
            public enum _FunktionX51 : UDINT
            {
                EXTERNE_ANF = 0,
                FREMDKESSEL = 1,
                EXT_NACHRICHT = 2,
                EnumItem110 = 3,
            }
            public _Sommer_Winter_Zeitumschaltung Sommer_Winter_Zeitumschaltung;
            public enum _Sommer_Winter_Zeitumschaltung : UDINT
            {
                _AUS = 0,
                _EUROPA = 1,
                _USA = 2,
            }
            public DINT HK_Kessel_wie_Puffer;
            public DINT WartungsterminEINAUS;
            public DINT Wartung_Monate;
            public _Funktion_Reserverelais Funktion_Reserverelais;
            public enum _Funktion_Reserverelais : UDINT
            {
                KEINE_FUNKTION = 0,
                KESSELBETRIEB = 1,
                DREI_WEGE_MOTORVENTIL = 2,
                VERSORGUNG_SSUE = 3,
                BRENNWERT_SPUELUNG = 4,
            }
            public UDINT udZeitverzoegerung_FK;
            public DINT AnzahlSlaves;
        }
        public _RLA RLA;
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public unsafe struct _RLA
        {
            public UDINT udRLAAusschalttemperatur;
            public UDINT udRLAStartdifferenz;
            public UDINT udRLADiff_KT_RLF_PU;
            public UDINT udRLADiff_KT_PO;
            public UDINT udRLA_Hysterese;
            public _Mischerfunktion Mischerfunktion;
            public enum _Mischerfunktion : UDINT
            {
                RLA_MISCHER_AUS = 0,
                RLA_MISCHER_EIN = 1,
            }
            public UINT uRLA_Solltemperatur;
            public UINT uMischerlaufzeit;
            public UINT uPulsdauer;
            public UINT uminPause;
            public UINT umaxPause;
            public UINT uPBereich;
            public UINT uiPumpe_ohne_Puffer;
            public UINT uRLA_Solltemperatur_Min;
        }
        public _PIDREGLER PIDREGLER;
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public unsafe struct _PIDREGLER
        {
            public _PID_PARAMETER KESSEL;
            public _PID_PARAMETER RAUCHGAS;
            public _PID_PARAMETER EXTERNEANFORDERUNG;
            public _PID_PARAMETER SAUGZUG;
            public _PID_PARAMETER SPEICHEROBEN;
        }
        public _ASCHENAUSTRAGUNG ASCHENAUSTRAGUNG;
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public unsafe struct _ASCHENAUSTRAGUNG
        {
            public UDINT udAschenaustr_Impuls;  //! <Type Comment="Sek" Name="_KESSELPARAMETER._ASCHENAUSTRAGUNG.udAschenaustr_Impuls"/>
            public UDINT udAschenaustr_Pause;  //! <Type Comment="Min" Name="_KESSELPARAMETER._ASCHENAUSTRAGUNG.udAschenaustr_Pause"/>
            public UDINT udEntleerungIntervall;
            public UDINT udmaxPelletsbetrieb_bis_Entleerhinweis;
            public UDINT Reinigungshinweis_Ein;
            public UINT uiMinLaufzeit_pro_h;
            public UINT uiSekLaufzeit_pro_h;
            public UDINT udAschenwalzefreigabezeit;
            public UDINT udAschenaustr_erster_Impuls;
            public UDINT udImpuls_nach_LNL;
        }
        public _TRINKWASSERBEREICH TRINKWASSERBEREICH;
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public unsafe struct _TRINKWASSERBEREICH
        {
            public StatusEnum AKTIV;
            public HDINT hdFreigabe1Start;
            public HDINT hdFreigabe1Ende;
            public HDINT hdFreigabe2Start;
            public HDINT hdFreigabe2Ende;
            public DINT Solltemperatur;
            public DINT Hysterese;
            public DINT EinmaligeFreigabe;
            public _TWR_VORRANG TWR_VORRANG;
            public enum _TWR_VORRANG : UDINT
            {
                AUS_AUTOMATIK = 0,
                EIN = 1,
                VERMINDERT = 2,
                TWR_VORR_Nur_HK_Min = 3,
            }
            public DINT leer3;
            public DINT leer;
            public DINT leer2;
            public DINT StructItem;
            public DINT StructItem0;
            public DINT StructItem1;
            public DINT StructItem2;
            public DINT StructItem3;
            public DINT StructItem4;
            public DINT StructItem5;
        }
        public _NETCONFIG NETCONFIG;
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public unsafe struct _NETCONFIG
        {
            public BYTE netz_teilnehmer;
            public BYTE anz_saugsondenumschaltungen;
            public BYTE anz_digfern;
            public BYTE anz_hzkrregler;
            public BYTE anz_drehstrommodul;
            public DINT anz_solar_mod;
            public DINT anz_frischwasser_mod;
            public DINT anz_hzkrregler_XL;
        }
        public _KESSELLEISTUNG KESSELLEISTUNG;
        public enum _KESSELLEISTUNG : UDINT
        {
            _9_9kW = 0,
            _14_9kW = 1,
            _35_kW = 2,
            _70_kW = 3,
            _49_kW = 4,
            _45_kW = 5,
            EnumItem99 = 6,
            EnumItem100 = 7,
            EnumItem101 = 8,
            EnumItem102 = 9,
            EnumItem103 = 10,
        }
        public _KASKADE KASKADE;
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public unsafe struct _KASKADE
        {
            public INT POPlusOffset_Leistung100;
            public INT POPlusOffset_Leistung0;
            public INT Zielpuffer;
            public INT KaskadenFreischaltung;
            public INT Leistung_Kessel_dazuschalten;
            public INT Leistung_Kessel_wegschalten;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 5 + 1 - 1)]
            public _KASKADEN_STRUCT[] _KASKADE_ARRAY;
            public INT Betriebsstunden_Kesselfolgewechsel;
            public INT Kesselwechsel;
        }
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 3 + 1 - 1)]
        public _NOTBETRIEB_STRUCT[] _NOTBETRIEB;
        public _DREI_WEGE_MOTVENT DREI_WEGE_MOTVENT;
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public unsafe struct _DREI_WEGE_MOTVENT
        {
            public INT Schalttemp_Ventil;
            public _VentStellung_stromlos VentStellung_stromlos;
            public enum _VentStellung_stromlos : UINT
            {
                Vent_to_Puffer = 0,
                Vent_to_FK = 1,
            }
            public INT FK_Fuehler_Steckplatz;
            public INT Uebertemperatur;
            public INT AusschalthystereseFK;
            public INT EinschalthystereseFK;
        }
        public _HK_XL_SETTINGS HK_XL_SETTINGS;
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public unsafe struct _HK_XL_SETTINGS
        {
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4 + 1 - 1)]
            public _HK_XL_SET_STRUCT[] HK_XL_SET_ARRAY;
        }
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4 + 1 - 1)]
        public _ZIRKULATION_STRUCT[] _ZIRKULATIONS_ARRAY;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4 + 1 - 1)]
        public _DIFF_REGLER_STRUCT[] _DIFFERENZREGLER_ARRAY;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 6 + 1 - 1)]
        public _BOILER_TEMPERATUREN_ZEITEN_STRUCT[] _BOILER_FREIGABEZEITEN;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 6 + 1 - 1)]
        public _ZIRKULATION_ZEIT_ARRAY[] _ZIRKULATION_FREIGABEZEITEN;
        public _DIFF_REGLER_OCTO_STRUCT _DIFF_REGLER_OCTOPLUS;
        public _WETTEREINSTELLPARAMETER WETTEREINSTELLPARAMETER;
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public unsafe struct _WETTEREINSTELLPARAMETER
        {
            public DINT Wettereinfluss_aktiv;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8 + 1 - 1)]
            public DINT[] HK_Wettereinfluss_Glasflaeche_EA;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8 + 1 - 1)]
            public DINT[] HK_Wettereinfluss_Glasflaeche;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8 + 1 - 1)]
            public DINT[] HK_Wettereinfluss;
            public DINT HK_Wettereinfluss_max_KelvinMinuten;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4 + 1 - 1)]
            public DINT[] TWS_Wettereinfluss;
            public DINT TWS_max_KelvinMinuten;
            public DINT TW_Bereich_Wettereinfluss;
            public DINT TW_Bereich_max_KelvinMinuten;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4 + 1 - 1)]
            public DINT[] Puffer_Wettereinfluss;
        }
        public _RLA_MISCHER RLA_MISCHER;
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public unsafe struct _RLA_MISCHER
        {
            public DINT min_KT_Pellets;
            public DINT Ausschalttemp_RLA_Differenz;
            public DINT RLA_Nachlaufzeit;
            public UDINT udPBereich_Plus;
            public UDINT RLA_Vorlaufzeit;
            public DINT StructItem2;
        }
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8 + 1 - 1)]
        public _HK_MODUL_SET_ARRAY[] _HK_MODUL_SETTINGS;
        public _WETTER_BRENNERSPERRE WETTER_BRENNERSPERRE;
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public unsafe struct _WETTER_BRENNERSPERRE
        {
            public DINT Brennersperre_Wetter;
            public DINT Brennersperre_AT;
            public DINT Brennersperre_Prognose;
            public DINT Prognosezeit_Pufferladung;
            public DINT StructItem3;
            public DINT StructItem4;
            public DINT StructItem5;
            public DINT StructItem6;
        }
        public _KASKADE2 KASKADE2;
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public unsafe struct _KASKADE2
        {
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8 + 1 - 1)]
            public _KASKADEN_STRUCT2[] _HK_ANF_ARRAY;
        }
        public _WP_SETTING_STRUCT _WP_PARA;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8 + 1 - 1)]
        public _HEIZKREIS_ERWEITERUNG_STRUCT[] _HK_ARRAY_ERWEITERUNG;
        public _WP_SGReady_Para_Struct _SG_Ready_Para;
        public _PE_STARTPHASE PE_STARTPHASE;
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public unsafe struct _PE_STARTPHASE
        {
            public DINT Startphase_Neu;
            public DINT O2_Grenze_PLM_Ein;
            public DINT O2_Grenze_PLM_Aus;
            public DINT LDZ_Totbereich;
            public DINT Lambda_Offset_Zuend;
            public DINT Rampe_min_LBD_kleiner_Soll;
            public DINT Rampe_max_LBD_kleiner_Soll;
            public DINT Rampe_min_LBD_groesser_Soll;
            public DINT Rampe_max_LBD_groesser_Soll;
            public DINT LDZ_PID_SampleTime;
            public DINT Lambda_DRZ_Offset_3punkt;
            public DINT Lambda_Offset_3punkt;
            public DINT Lambda_Offset_Start;
        }
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 1 + 1 - 0)]
        public _WP_EMERSON_SETTING2_STRUCT[] _WP_RCC_SETTING_2;
        public _ALLGEMEINE_EINSTELLUNGEN ALLGEMEINE_EINSTELLUNGEN;
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public unsafe struct _ALLGEMEINE_EINSTELLUNGEN
        {
            public Ventilstellung_Kuehlen Kuehlventil_invers;
            public _Ausgang_X7 Funktion_Ausgang_X7;
            public DINT hohe_Drehzahl_1_Zuendung;
            public DINT Pelletsverbrauchsanzeige_vorhanden;
            public DINT Pelletsverbrauch_Offset_prozentuell;
            public INT X51_Info_Alarm;
            public INT X51_quittierbar;
            public DINT HKXL_Modul;
            public UDINT Bildschirmschoner_nach;
            public BDINT FeuchteRF_vorhanden;
            public UDINT Spuelung_MinLz;
            public INT Spuelung_Impuls;
            public INT Spuelung_Pause;
            public _PRODUCT_SF_REVISION ProduktRevison;
            public DINT StrommessungAktiv_Asche;
            public DINT MaxStrom_Asche;
            public _Photovoltaik Photovoltaik;
            public enum _Photovoltaik : UDINT
            {
                _PV_nichtvorhanden = 0,
                _PV_SolarLog = 1,
                _PV_Fronius = 2,
                _PV_SolarEdge = 3,
                _PV_extern = 4,
                _PV_SolarEdgeMitBatterie = 5,
                _PV_FroniusGen24 = 6,
                _PV_FroniusGen24MitBatterie = 7,
            }
            public DINT Solarertag_beruecksichtigen;
            public DINT PV_Einschaltschwelle;
            public DINT PV_Ausschaltschwelle;
            public DINT NTP_verwenden;
            public INT Zeit_Server;
            public INT Zeitzone;
        }
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8 + 1 - 1)]
        public _HEIZKREIS_KUEHLEN_STRUCT[] _HK_KUEHLEN;
        public _PUFFER_ERWEITERUNG PUFFER_ERWEITERUNG;
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public unsafe struct _PUFFER_ERWEITERUNG
        {
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4 + 1 - 1)]
            public DINT[] Durchladen_bei_extAnf;
        }
        public _FWM_KASKADE_SAVE_STRUCT _FWM_Kaskade;
        public _WP_KASKADE_BZM_STRUCT _WP_BZM_KASKADE;
        public _FERNLEITUNG_SAVE_STRUCT _FERNLEITUNG;
        public _PUFFER_ERWEITERUNG2 PUFFER_ERWEITERUNG2;
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public unsafe struct _PUFFER_ERWEITERUNG2
        {
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4 + 1 - 1)]
            public DINT[] Laden_bei_Kesselbetrieb;
        }
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4 + 1 - 1)]
        public _DIFF_FWM_STRUCT[] _FWM_DIFF_ARRAY;
        public _KASKADE3 KASKADE3;
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public unsafe struct _KASKADE3
        {
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 5 + 1 - 0)]
            public _KASKADEN_STRUCT3[] Anf_Array;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4 + 1 - 1)]
            public DINT[] Leistungsbedarf_TWS;
        }
        public _ALLGEMEINE_EINSTELLUNGEN2 ALLGEMEINE_EINSTELLUNGEN2;
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public unsafe struct _ALLGEMEINE_EINSTELLUNGEN2
        {
            public DINT Brennwertmodul_taegliche_Spuelung;
            public DINT Partikelfilter_vorhanden;
            public DINT RegelungNachPTO;
            public UDINT Asche_maxEsLz;
            public DINT PTO_TempOffset;
            public UDINT SGT_Wartezeit;
            public UDINT BW_TaeglicherSpuelimpuls;
            public DINT Integrierte_WMErfassung;
            public DINT PelletEnergieinhalt;
            public E_FunktionAusgangX5 FunktionAusgangX5;
            public E_FunktionAusgangH1X7 FunktionAusgangH1X7;
            public E_FunktionAusgangH1X5 FunktionAusgangH1X5;
            public DINT StructItem11;
            public DINT StructItem12;
            public DINT StructItem13;
            public DINT StructItem14;
            public DINT StructItem15;
            public DINT StructItem16;
            public DINT StructItem17;
            public DINT StructItem18;
        }
        public _STEIGUNG_KT_RL STEIGUNG_KT_RL;
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public unsafe struct _STEIGUNG_KT_RL
        {
            public DINT Steigungserkennung;
            public DINT Steigungslimit;
            public DINT Abtastrate;
            public UDINT Steigunghoch_Verzoegerung;
            public DINT StructItem;
            public DINT StructItem0;
            public DINT StructItem1;
            public DINT StructItem2;
            public DINT StructItem3;
        }
        public _DIFFERENZDRUCK DIFFERENZDRUCK;
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public unsafe struct _DIFFERENZDRUCK
        {
            public DINT ES_Diffdruckgrenze_Ein;
            public DINT ES_Diffdruck_100LDZ;
            public DINT ES_Diffdruck_30LDZ;
            public DINT DiffDruckNotStop;
            public DINT DiffdruckStop;
            public DINT DiffdruckStop_30LDZ;
            public DINT DiffdruckStop_100Ldz;
            public DINT minDiffdruck_SGT;
            public DINT Notbetrieb;
            public DINT StructItem0;
            public DINT StructItem1;
            public DINT StructItem2;
            public DINT StructItem3;
        }
        public _PARTIKELABSCHEIDER PARTIKELABSCHEIDER;
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public unsafe struct _PARTIKELABSCHEIDER
        {
            public DINT AGT_Startfreigabe;
            public UDINT Rampe;
            public DINT maxStrom;
            public DINT maxSpannung;
            public DINT StructItem;
            public DINT StructItem0;
            public DINT StructItem1;
            public DINT StructItem2;
            public DINT StructItem3;
        }
        public _FUNK_RBG FUNK_RBG;
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public unsafe struct _FUNK_RBG
        {
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 16 + 1 - 1)]
            public _FUNK_RBG_HAINZL_PARA[] _RBG_HK_ARRAY;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 16 + 1 - 1)]
            public DINT[] _Empty_Space;
        }
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 472 + 1 - 0)]
        public DINT[] _Empty_Space2;
    }
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public unsafe struct _BOILER
    {
        public DINT Boiler_vorhanden;
        public DINT Energiequelle;
        public DINT Hysterese;
        public DINT EinmaligeFreigabe;
        public DINT Vorrang;
        public DINT Legionellenbetrieb;
        public DINT Legionellentemperatur;
        public DINT TempErhoehung;
        public DINT Ausschaltdifferenz_Anforderung;
        public _FreigabeArt FreigabeArt;
        public enum _FreigabeArt : UDINT
        {
            FG_IMMERAUS = 0,
            FG_IMMEREIN = 1,
            FG_ZEITEN = 2,
            FG_MOFRSASO = 3,
            FG_TAGWEISE = 4,
        }
        public DINT KT_Min;
        public _FREIGABEZEITEN FreigabeZeit;
        public DINT AnforderungsDifferenz;
        public DINT Pumpendifferenz;
        public DINT Solltemp1;
        public DINT Solltemp2;
        public DINT Solltemp3;
        public DINT MinimalTemp;
        public DINT Fremdkesselfreigabe;
        public UINT uiKesseluebertempabfuhr;
        public UINT uiBoilerladung_Ventil;
        public DINT Solltemp4;
        public DINT Solltemp5;
        public DINT TWS_Eigenschaft;
        public DINT Laden_bei_Kesselbetrieb;
        public DINT Ueberwaermeabfuhr_permanent;
        public DINT Ueberwaermeabfuhr_Solarertrag;
        public UINT uiVentilstellung_Invers;
        public UINT uiVentilBereitschaftTWS;
    }
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public unsafe struct _FREIGABEZEITEN
    {
        public HDINT hdStart1;
        public HDINT hdEnde1;
        public HDINT hdStart2;
        public HDINT hdEnde2;
        public HDINT hdStart3;
        public HDINT hdEnde3;
        public HDINT hdStart4;
        public HDINT hdEnde4;
        public HDINT hdStart5;
        public HDINT hdEnde5;
    }
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public unsafe struct _BOILER_TEMPERATUREN_ZEITEN_STRUCT
    {
        public _BOILER_TEMP_STRUCT Mo_Temperaturen;
        public _BOILER_TEMP_STRUCT Di_Temperaturen;
        public _BOILER_TEMP_STRUCT Mi_Temperaturen;
        public _BOILER_TEMP_STRUCT Do_Temperaturen;
        public _BOILER_TEMP_STRUCT Fr_Temperaturen;
        public _BOILER_TEMP_STRUCT Sa_Temperaturen;
        public _BOILER_TEMP_STRUCT So_Temperaturen;
        public _BOILER_TEMP_STRUCT Mo_bis_Fr_Temperaturen;
        public _BOILER_TEMP_STRUCT Sa_bis_So_Temperaturen;
        public _FREIGABEZEITEN Mo_Freigabezeit;
        public _FREIGABEZEITEN Di_Freigabezeit;
        public _FREIGABEZEITEN Mi_Freigabezeit;
        public _FREIGABEZEITEN Do_Freigabezeit;
        public _FREIGABEZEITEN Fr_Freigabezeit;
        public _FREIGABEZEITEN Sa_Freigabezeit;
        public _FREIGABEZEITEN So_Freigabezeit;
        public _FREIGABEZEITEN Mo_bis_Fr_Freigabezeit;
        public _FREIGABEZEITEN Sa_bis_So_Freigabezeit;
    }
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public unsafe struct _BOILER_TEMP_STRUCT
    {
        public DINT Solltemperatur_1;
        public DINT Solltemperatur_2;
        public DINT Solltemperatur_3;
        public DINT Solltemperatur_4;
        public DINT Solltemperatur_5;
        public DINT Hysterese;
        public DINT MinimaleTemperatur;
    }
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public unsafe struct _DIFF_FWM_STRUCT
    {
        public DINT RuechlaufeinschichtungAktiv;
        public DINT Diff_Einschaltdifferenz;
        public DINT Diff_Ausschaltdifferenz;
        public DINT Diff_Min_Temp_1_ein;
        public DINT Diff_Min_Temp_1_aus;
        public DINT Diff_Max_Temp_2_ein;
        public DINT Diff_Max_Temp_2_aus;
        public DINT Diff_Ausg_invers;
        public _DIFF_FUEHLERAUSWAHL Diff_Fuehlereingang2;
        public DINT StructItem0;
        public DINT StructItem1;
        public DINT StructItem2;
        public DINT StructItem3;
        public DINT StructItem4;
        public DINT StructItem5;
        public DINT StructItem6;
        public DINT StructItem7;
        public DINT StructItem8;
        public DINT StructItem9;
    }
    public enum _DIFF_FUEHLERAUSWAHL : UDINT
    {
        DFA_MODUL = 0,
        DFA_X30 = 1,
        DFA_X31 = 2,
        DFA_X32 = 3,
        DFA_X35 = 4,
        DFA_X36 = 5,
        DFA_X39 = 6,
        DFA_X44 = 7,
        DFA_H1I5 = 8,
        DFA_H1I6 = 9,
        DFA_H1I7 = 10,
        DFA_H2I5 = 11,
        DFA_H2I6 = 12,
        DFA_H2I7 = 13,
        DFA_H3I5 = 14,
        DFA_H3I6 = 15,
        DFA_H3I7 = 16,
        DFA_H4I6 = 17,
        DFA_H4I7 = 18,
        DFA_H4I10 = 19,
        DFA_H4I11 = 20,
        DFA_S1I1 = 21,
        DFA_S1I3 = 22,
        DFA_S1I4 = 23,
        DFA_S1I5 = 24,
        DFA_S1I9 = 25,
        DFA_D1I3 = 26,
        DFA_D1I4 = 27,
        DFA_D1I9 = 28,
        DFA_D1I10 = 29,
        DFA_D2I3 = 30,
        DFA_D2I4 = 31,
        DFA_D2I9 = 32,
        DFA_D2I10 = 33,
        DFA_D3I3 = 34,
        DFA_D3I4 = 35,
        DFA_D3I9 = 36,
        DFA_D3I10 = 37,
        DFA_D4I3 = 38,
        DFA_D4I4 = 39,
        DFA_D4I9 = 40,
        DFA_D4I10 = 41,
        DFA_S2I1 = 42,
        DFA_S2I3 = 43,
        DFA_S2I4 = 44,
        DFA_S2I5 = 45,
        DFA_S2I9 = 46,
    }
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public unsafe struct _DIFF_REGLER_OCTO_STRUCT
    {
        public _BETRIEBSART DIFF_BETRIEBSART;
        public DINT Diff_Einschaltdifferenz;
        public DINT Diff_Ausschaltdifferenz;
        public DINT Diff_Min_Temp_1_ein;
        public DINT Diff_Min_Temp_1_aus;
        public DINT Diff_Max_Temp_2_ein;
        public DINT Diff_Max_Temp_2_aus;
        public DINT Diff_Ausg_invers;
        public _DIFF_ZUSBED_PUMPENAUSWAHL Diff_Kreis_zusaetzlicheEinschaltbedingung;
        public DINT Diff_Puffer_max_unten_merker;
        public DINT Diff_Fachmannebene_immer_sichtbar;
        public _DIFF_FUEHLERAUSWAHL Diff_Fuehlereingang1;
        public _DIFF_FUEHLERAUSWAHL Diff_Fuehlereingang2;
        public DINT Diff_vorhanden;
        public _FUEHLERTYP Diff_Fuehlertyp1;
        public _FUEHLERTYP Diff_Fuehlertyp2;
        public DINT Blockadeschutz;
        public DINT StructItem1;
        public DINT StructItem2;
        public DINT StructItem3;
    }
    public enum _BETRIEBSART : UDINT
    {
        BA_Aus = 0,
        BA_Ein = 1,
        BA_Auto = 2,
    }
    public enum _DIFF_ZUSBED_PUMPENAUSWAHL : UDINT
    {
        _KEINE_ZUSBED = 0,
        _RLA_Pumpe = 1,
        _HK1_Pumpe = 2,
        _HK2_Pumpe = 3,
        _HK3_Pumpe = 4,
        _HK4_Pumpe = 5,
        _HK5_Pumpe = 6,
        _HK6_Pumpe = 7,
        _HK7_Pumpe = 8,
        _HK8_Pumpe = 9,
        _PU1_Pumpe = 10,
        _PU2_Pumpe = 11,
        _PU3_Pumpe = 12,
        _PU4_Pumpe = 13,
        _BO1_Pumpe = 14,
        _BO2_Pumpe = 15,
        _BO3_Pumpe = 16,
        _BO4_Pumpe = 17,
        _SOL1_O1 = 18,
        _SOL1_O2 = 19,
        _SOL2_O1 = 20,
        _SOL2_O2 = 21,
        _SOL3_O1 = 22,
        _SOL3_O2 = 23,
        _SOL4_O1 = 24,
        _SOL4_O2 = 25,
        _ZIRK1_Pumpe = 26,
        _ZIRK2_Pumpe = 27,
        _ZIRK3_Pumpe = 28,
        _ZIRK4_Pumpe = 29,
        _X51 = 30,
        _Reserverelais = 31,
        _MOD1_O1 = 32,
        _MOD1_O2 = 33,
        _MOD2_O1 = 34,
        _MOD2_O2 = 35,
        _MOD3_O1 = 36,
        _MOD3_O2 = 37,
        _MOD4_O1 = 38,
        _MOD4_O2 = 39,
        _PE_BETRIEB = 40,
        _TWS_ANF1 = 41,
        _TWS_ANF2 = 42,
        _TWS_ANF3 = 43,
        _TWS_ANF4 = 44,
        _NOT_RLA_PUMPE = 101,
        _NOT_HK1_Pumpe = 102,
        _NOT_HK2_Pumpe = 103,
        _NOT_HK3_Pumpe = 104,
        _NOT_HK4_Pumpe = 105,
        _NOT_HK5_Pumpe = 106,
        _NOT_HK6_Pumpe = 107,
        _NOT_HK7_Pumpe = 108,
        _NOT_HK8_Pumpe = 109,
        _NOT_PU1_Pumpe = 110,
        _NOT_PU2_Pumpe = 111,
        _NOT_PU3_Pumpe = 112,
        _NOT_PU4_Pumpe = 113,
        _NOT_BO1_Pumpe = 114,
        _NOT_BO2_Pumpe = 115,
        _NOT_BO3_Pumpe = 116,
        _NOT_BO4_Pumpe = 117,
        _NOT_SOL1_O1 = 118,
        _NOT_SOL1_O2 = 119,
        _NOT_SOL2_O1 = 120,
        _NOT_SOL2_O2 = 121,
        _NOT_SOL3_O1 = 122,
        _NOT_SOL3_O2 = 123,
        _NOT_SOL4_O1 = 124,
        _NOT_SOL4_O2 = 125,
        _NOT_ZIRK1_Pumpe = 126,
        _NOT_ZIRK2_Pumpe = 127,
        _NOT_ZIRK3_Pumpe = 128,
        _NOT_ZIRK4_Pumpe = 129,
        _NOT_X51 = 130,
        _NOT_Reserverelais = 131,
        _NOT_MOD1_O1 = 132,
        _NOT_MOD1_O2 = 133,
        _NOT_MOD2_O1 = 134,
        _NOT_MOD2_O2 = 135,
        _NOT_MOD3_O1 = 136,
        _NOT_MOD3_O2 = 137,
        _NOT_MOD4_O1 = 138,
        _NOT_MOD4_O2 = 139,
        _NOT_PE_BETRIEB = 140,
        _NOT_TWS_ANF1 = 141,
        _NOT_TWS_ANF2 = 142,
        _NOT_TWS_ANF3 = 143,
        _NOT_TWS_ANF4 = 144,
    }
    public enum _FUEHLERTYP : UDINT
    {
        KTY1K = 0,
        PT1000 = 1,
    }
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public unsafe struct _DIFF_REGLER_STRUCT
    {
        public DINT Differenzregelkreis_vorhanden;
        public _BETRIEBSART DIFF_BETRIEBSART_1;
        public _BETRIEBSART DIFF_BETRIEBSART_2;
        public _DIFF_FUEHLERAUSWAHL Diff_Fuehlereingang1;
        public _DIFF_FUEHLERAUSWAHL Diff_Fuehlereingang2;
        public _DIFF_FUEHLERAUSWAHL Diff_Fuehlereingang3;
        public _DIFF_FUEHLERAUSWAHL Diff_Fuehlereingang4;
        public DINT Diff_Einschaltdifferenz12;
        public DINT Diff_Ausschaltdifferenz12;
        public DINT Diff_Einschaltdifferenz34;
        public DINT Diff_Ausschaltdifferenz34;
        public DINT Diff_Min_Temp_1_ein;
        public DINT Diff_Min_Temp_1_aus;
        public DINT Diff_Min_Temp_3_ein;
        public DINT Diff_Min_Temp_3_aus;
        public DINT Diff_Max_Temp_2_ein;
        public DINT Diff_Max_Temp_2_aus;
        public DINT Diff_Max_Temp_4_ein;
        public DINT Diff_Max_Temp_4_aus;
        public DINT Diff_Ausg1_invers;
        public DINT Diff_Ausg2_invers;
        public _FUEHLERTYP Diff_Fuehlertyp_T1;
        public _FUEHLERTYP Diff_Fuehlertyp_T2;
        public _FUEHLERTYP Diff_Fuehlertyp_T3;
        public _FUEHLERTYP Diff_Fuehlertyp_T4;
        public DINT Diff_Fachmannebene_immer_sichtbar;
        public DINT Diff_Puffer_max_unten_merker;
        public _DIFF_ZUSBED_PUMPENAUSWAHL Diff_Kreis1_zusaetzlicheEinschaltbedingung;
        public _DIFF_ZUSBED_PUMPENAUSWAHL Diff_Kreis2_zusaetzlicheEinschaltbedingung;
        public DINT Blockadeschutz1;
        public DINT Blockadeschutz2;
        public DINT Diff_Modul_ist_PUTW_Modul;
        public DINT StructItem5;
        public DINT StructItem6;
        public DINT StructItem;
        public DINT StructItem0;
        public DINT StructItem7;
        public DINT StructItem8;
        public DINT StructItem9;
        public DINT StructItem10;
        public DINT StructItem11;
    }
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public unsafe struct _FERNLEITUNG_SAVE_STRUCT
    {
        public DINT Fernleitung_F_vorhanden;
        public DINT Fernleitung_E_vorhanden;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4 + 1 - 1)]
        public _FERNLEITUNG_SAVE_PARA[] Para_Save_Fernleitung;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4 + 1 - 1)]
        public _FERNLEITUNG_SAVE_MISCHER[] Para_Save_Fernleitung_Mischer;
    }
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public unsafe struct _FERNLEITUNG_SAVE_MISCHER
    {
        public DINT PID_Regler;
        public DINT Mischerlaufzeit;
        public DINT Impuls;
        public DINT Pause_Min;
        public DINT Pause_Max;
        public DINT P_Bereich;
        public DINT Anfahrentlastung;
        public DINT Begr_I_Anteil;
        public DINT P_Anteil;
        public DINT I_Anteil;
        public DINT D_Anteil;
        public UDINT Reserve_DINT_1;
        public DINT Reserve_DINT_2;
        public DINT Reserve_DINT_3;
        public DINT Reserve_DINT_4;
    }
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public unsafe struct _FERNLEITUNG_SAVE_PARA
    {
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 16 + 1 - 1)]
        public BOOL[] Heizkreis_aktiv;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8 + 1 - 1)]
        public BOOL[] Puffer_aktiv;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8 + 1 - 1)]
        public BOOL[] Trinkwasser_aktiv;
        public DINT Offset_Vorlaufsolltemp;
        public DINT Relaisausgang_benoetigt;
        public DINT dint_Reserve_2;
        public DINT dint_Reserve_3;
        public DINT dint_Reserve_4;
        public DINT dint_Reserve_5;
        public DINT dint_Reserve_6;
        public DINT dint_Reserve_7;
        public DINT dint_Reserve_8;
        public DINT dint_Reserve_9;
        public DINT dint_Reserve_10;
    }
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public unsafe struct _FUNK_RBG_HAINZL_PARA
    {
        public BOOL Vorhanden;
        public USINT GwIndex;
        public USINT RR35Index;
        public SINT Reserve;
    }
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public unsafe struct _FWM_PARAMETER_STRUCT
    {
        public DINT FWM_vorhanden;
        public _Pumpenbetriebsart Pumpenbetriebsart;
        public enum _Pumpenbetriebsart : UDINT
        {
            FWM_PUMPE_AUS = 0,
            FWM_PUMPE_EIN = 1,
            FWM_PUMPE_AUTO = 2,
        }
        public DINT Pumpe_Man_Vorgabe;
        public INT Solltemperatur;
        public DINT Hocheffizenzpumpe;
        public DINT PWM_Signal;
        public UDINT PID_Zeitintervall_Standartpumpe;
        public UINT PID_P_Standartpumpe;
        public UINT PID_I_Standartpumpe;
        public UINT PID_D_Standartpumpe;
        public UDINT PID_Max_Standartpumpe;
        public UDINT PID_Min_Standartpumpe;
        public UDINT PID_Zeitintervall_HE;
        public UINT PID_P_HE;
        public UINT PID_I_HE;
        public UINT PID_D_HE;
        public UDINT PID_Max_HE;
        public UDINT PID_Min_HE;
        public DINT Wellenpaket;
        public UINT PID_DIV_P;
        public UINT PID_DIV_I;
        public UINT PID_DIV_D;
        public UINT PID_ABS_I;
        public _Bezugsfuehler_SpeicherOben Bezugsfuehler_SpeicherOben;
        public enum _Bezugsfuehler_SpeicherOben : UDINT
        {
            SpeicherOben_OctoPlus = 0,
            FWModul_I4_KTY1k = 1,
            FWModul_I4_PT = 2,
            TWSF_2 = 3,
            TWSF_3 = 4,
            TWSF_4 = 5,
        }
        public UINT PID_ext_Regler;
        public DINT PWM_Invers;
        public UINT uiZirkAccess;
        public UINT uiFWM_konvent;  //! <Type Comment="0: FWM Leitner mit Str�mungsschalter und Sensor schnell&#13;&#10;10: FMW 20-40 l mit Sensor schnell und Durchflussmengengeber 2-40l/min&#13;&#10;20: FMW 50 l mit 4-100l/min" Name="_FWM_PARAMETER_STRUCT.uiFWM_konvent"/>
        public UINT uiMaxDF_Faktor;
        public UINT uiWT_Steigung;
        public UINT uiStructItem0;
        public DINT StructItem5;
        public DINT StructItem;
    }
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public unsafe struct _FWM_KASKADE_SAVE_STRUCT
    {
        public DINT Vorhanden;
        public DINT Anzahl_FWM_Kaskade;
        public DINT FWM_Typ;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 6 + 1 - 1)]
        public DINT[] Zuschaltarray;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 6 + 1 - 1)]
        public DINT[] Wegschaltarray;
        public DINT dintReserve_1;
        public DINT dintReserve_2;
        public DINT dintReserve_3;
        public DINT dintReserve_4;
        public DINT dintReserve_5;
        public DINT dintReserve_6;
        public DINT dintReserve_7;
        public DINT dintReserve_8;
        public DINT dintReserve_9;
        public DINT dintReserve_10;
        public INT intReserve_1;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 6 + 1 - 1)]
        public DINT[] dintReserveArray_1_6_1;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 6 + 1 - 1)]
        public DINT[] dintReserveArray_1_6_2;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 6 + 1 - 1)]
        public DINT[] dintReserveArray_1_6_3;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 6 + 1 - 1)]
        public DINT[] dintReserveArray_1_6_4;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 6 + 1 - 1)]
        public DINT[] dintReserveArray_1_6_5;
        public _FWM_PARAMETER_STRUCT FWM_Parameter;
    }
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public unsafe struct _HEIZKREIS_ERWEITERUNG_STRUCT
    {
        public DINT Ueberwaermeabfuhr_permanent;
        public DINT Ueberwaermeabfuhr_Solarertrag;
    }
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public unsafe struct _HEIZKREIS_KUEHLEN_STRUCT
    {
        public Blockzeiten _Blockzeiten;
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public unsafe struct Blockzeiten
        {
            public _FREIGABEZEITEN Mo_Fr_Zeiten;
            public _FREIGABEZEITEN Sa_So_Zeiten;
        }
        public Einzelzeiten _Einzelzeiten;
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public unsafe struct Einzelzeiten
        {
            public _FREIGABEZEITEN Sonntag;
            public _FREIGABEZEITEN Montag;
            public _FREIGABEZEITEN Dienstag;
            public _FREIGABEZEITEN Mittwoch;
            public _FREIGABEZEITEN Donnerstag;
            public _FREIGABEZEITEN Freitag;
            public _FREIGABEZEITEN Samstag;
        }
        public DINT KuehlfunktionEA;
        public UINT uiRaumsolltemperatur;
        public UINT uiRTOffset_min_Vlt;
        public DINT Aussenabschalttemperatur;
        public UDINT Mittelwertzeit;
        public _Zeitschaltung Zeitschaltung;
        public enum _Zeitschaltung : UDINT
        {
            HK_EINZELZEITEN = 0,
            HK_BLOCKZEITEN = 1,
        }
        public _Heizkreistyp Heizkreistyp;
        public enum _Heizkreistyp : UDINT
        {
            HK_Heizkoerper = 0,
            HK_Fussboden = 1,
            HK_Pool = 2,
        }
        public INT MaxMischer_bei_TWS;
        public INT Taupunkt_ignorieren;
        public INT kleinste_VLTSOLL;
        public INT iVltOffset_RTSoll;
    }
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public unsafe struct _HEIZKREIS_STRUCT
    {
        public _HKModus HKModus;
        public enum _HKModus : UDINT
        {
            HK_DAUERBETRIEB = 0,
            HK_ABSENKUNG = 1,
            HK_AUTOMATIK = 2,
            HK_AUS = 3,
            HK_FERIEN = 4,
            HK_ESTRICH = 5,
            HK_PARTY = 6,
            HK_KURZZEIT = 7,
        }
        public DINT Energiequelle;
        public DINT Heizkreis_vorhanden;
        public DINT SommerTemperaturHeizen;
        public DINT SommerTemperaturAbsenken;
        public DINT RaumSollHeizbetrieb1;
        public DINT RaumSollAbsenkbetrieb;
        public DINT Frostschutztemperatur;
        public DINT Kesseltemp_Min;
        public _Raumeinfluss Raumeinfluss;
        public enum _Raumeinfluss : UDINT
        {
            RAUM_AUS = 0,
            RAUM_EIN = 1,
            RAUM_GLEITEND = 2,
        }
        public DINT Min_Vorlauftemp;
        public Blockzeiten _Blockzeiten;
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public unsafe struct Blockzeiten
        {
            public _FREIGABEZEITEN Mo_FrBlock;
            public _FREIGABEZEITEN Sa_SoBlock;
        }
        public EinzelZeiten _EinzelZeiten;
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public unsafe struct EinzelZeiten
        {
            public _FREIGABEZEITEN Sonntag;
            public _FREIGABEZEITEN Montag;
            public _FREIGABEZEITEN Dienstag;
            public _FREIGABEZEITEN Mittwoch;
            public _FREIGABEZEITEN Donnerstag;
            public _FREIGABEZEITEN Freitag;
            public _FREIGABEZEITEN Samstag;
        }
        public _Betriebsart Betriebsart;
        public enum _Betriebsart : UDINT
        {
            Heizbetrieb = 0,
            Absenkbetrieb = 1,
        }
        public DINT AngelpunktPlus15;
        public DINT AngelpunktMinus15;
        public DINT Vorlaufabsenkung;
        public DINT AnfahrEntlastung;
        public DINT RaumTempAbgleich;
        public Estricheinstellungen _Estricheinstellungen;
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public unsafe struct Estricheinstellungen
        {
            public DINT StartTemperatur;
            public DINT Temperaturanstieg;
            public DINT MaximalTemperatur;
            public DINT Temperaturabsenkung;
            public DINT EndTemperatur;
            public UDINT udAufheizdauer;
            public UDINT udVerweilzeit;
            public UDINT udAbsenkzeit;
        }
        public DINT AnforderungPufferAus;
        public DINT Fremdkesselanforderung;
        public DINT SchaltHystereseRT;
        public DINT Pufferdifferenz;
        public _Zeitschaltung Zeitschaltung;
        public enum _Zeitschaltung : UDINT
        {
            EINZELSCHALTUNG = 0,
            BLOCKSCHALTUNG = 1,
        }
        public DINT VorlaufMaximum;
        public UDINT udMischerlaufzeit;
        public DINT PAnteilMischer;
        public UDINT udPulsdauer;
        public UDINT udPausendauerMin;
        public UDINT udPausendauerMax;
        public DINT Raumabweichungsfaktor;
        public UDINT udMittelwertBildungsdauer;
        public DINT TotBereichMischer;
        public INT Feriendauer;
        public INT Mischer_Anf_Starttemp;
        public DINT MischerP_Bereich;
        public DINT Angelpunkt_Mitte;
        public INT Heizkurve_3Punkt;
        public INT RaumSollHeizbetrieb2;
        public INT RaumSollHeizbetrieb3;
        public INT RaumSollHeizbetrieb4;
        public INT RaumSollHeizbetrieb5;
        public INT VltAenderungHeizzeit1;
        public INT VltAenderungHeizzeit2;
        public INT VltAenderungHeizzeit3;
        public INT VltAenderungHeizzeit4;
        public INT VltAenderungHeizzeit5;
    }
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public unsafe struct _HK_MODUL_SET_ARRAY
    {
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 1 + 1 - 0)]
        public UDINT[] Widerstandsoffset_RF_PT1000;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 1 + 1 - 0)]
        public UDINT[] Widerstandsoffset_RF_KTY;
    }
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public unsafe struct _HK_XL_SET_STRUCT
    {
        public BSINT Fuehler_1_8;
        public BSINT Fuehler_9_16;
        public BSINT Controlregister;
        public DINT StructItem;
    }
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public unsafe struct _KASKADEN_STRUCT
    {
        public INT AT_kein_Start;
        public INT Startverzoegerung;
        public INT AT_SofortStart;
    }
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public unsafe struct _KASKADEN_STRUCT2
    {
        public DINT Leistungsbedarf_kW;
    }
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public unsafe struct _KASKADEN_STRUCT3
    {
        public BDINT Anforderung;
    }
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public unsafe struct _Struct_Zuendung
    {
        public UDINT udMaxZuendzeit;
        public UDINT udZuendimpuls;
        public UDINT udZuendpause;
        public UDINT udErsterZuendimpuls;
        public UDINT udLambdaZuendimpulsAus;
        public UDINT udLambdaZuendimpulsSGAutoEin;
        public UDINT udLambdaZuendimpulsHGEin;
        public UDINT udPrimMagnet_Zuendphase_Ein;
        public UDINT udPrimMagnet_Zuendphase_Lz;
        public UDINT udLambdaZuendimpulsEin;
        public UINT uiZuendfoenzuendung;
        public _AusgaengeZuendung AusgaengeZuendung;
        public enum _AusgaengeZuendung : UINT
        {
            _Z_X4X4 = 0,
            _Z_X4X5 = 1,
            _Z_X4X7 = 2,
            _Z_X4X14 = 3,
        }
        public UDINT GeblaeseNachlauf;
        public DINT ZuendZuluft_Betrieb;
        public DINT FreigabeBrennraumtemp;
        public DINT LuftzahlGrenze;
        public DINT StructItem4;
        public DINT StructItem5;
    }
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public unsafe struct _Struct_Saugzuggeblaese
    {
        public UDINT udDrehzahlrueckfuehrung;
        public UDINT udPEAnheizdrehzahl;
        public UDINT udPEMaxDrehzahlZuendphase;
        public UDINT udPELambdagrenzeZuendphase;
        public UDINT udPEMinLuefterrampePelletsbetrieb;
        public UDINT udPEMaxLuefterrampePelletsbetrieb;
        public UDINT udPERGTAnstiegZuendphase;
        public UDINT udPELDZAnstieg;
        public UDINT udPEMax_Luefterdrehzahl;
        public UDINT udPEMin_Luefterdrehzahl;
        public UDINT udPELuefternachlauf_normal;
        public UDINT udPELuefternachlauf_lang;
        public UDINT udPELuefternachlauf_kurz;
        public UDINT udPELbdRegelung_Zugabe;
        public UDINT udPELbdRegelung_Verringerung;
        public UDINT udPEMax_LDZ_Startphase;
        public UDINT udPELambdaobergrenze;
        public UDINT udPELambdauntergrenze;
        public UDINT udPELambdarampenverlaengerung;
        public UDINT udPEMinRampeZuendphase;
        public UDINT udPEMaxRampeZuendphase;
        public UDINT udPEKesseltemperaturPBereich;
        public UDINT udPERGTPBereichpositiv;
        public UDINT udPERGTPBereichnegativ;
        public UDINT udPEVorLuefterzeit;
        public UDINT unPEStartLuefterdrehzahl;
        public UDINT udPEAusbrenndrehzahl;
        public UDINT udPEMinLuefterrampePelletsbetrieb_schnell;
        public UDINT udPEMaxLuefterrampePelletsbetrieb_schnell;
        public UDINT udPE_Saugen_Drz;
        public UDINT udRegelbereich_PID;
        public DINT StructItem3;
        public DINT StructItem4;
        public DINT StructItem5;
    }
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public unsafe struct _Struct_EinschubZuendung
    {
        public UDINT udESZLangerErsterEinschubimpuls;
        public UDINT udESZKurzerErsterEinschubimpuls;
        public UDINT udESZRauchgasanstieg;
        public UDINT udESZMaxEinschubimpuls;
        public UDINT udMinEinschubimpuls;
        public UDINT udMaxAnzahlEinschuebe;
        public UDINT udMinPause;
        public UDINT udMaxPause;
        public UDINT udEntleerimpuls;
        public UDINT udEinschubentleert;
        public UDINT udAnzahlSondereinschuebe;
        public UDINT udMaximalerStromEinschub;
        public DINT StrommessungAktiv;
        public DINT dBRT_Anstieg;
        public DINT StructItem1;
        public DINT StructItem2;
        public DINT StructItem3;
        public DINT StructItem4;
        public DINT StructItem5;
    }
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public unsafe struct _Struct_EinschubBetrieb
    {
        public UDINT udESMinEinschubimpuls;
        public UDINT udESMaxEinschubimpuls;
        public UDINT udESPause;
        public UDINT udESMaxImpulsverlaengerung;
        public UDINT udESMaxImpulsverkuerzung;
        public UDINT udESReduzierimpuls;
        public UDINT udESLambdaSollEinschub;
        public UDINT udESLambdauntergrenze;
        public UDINT udESLambdaobergrenze;
        public UDINT udESLbd_Tot_Bereich;
        public UDINT udESMitterwertzeit;
        public UDINT udESzulLDZAenderung;
        public UDINT udESFaktorImpAenderungLDZBereich;
        public UDINT udESEinschubimpuls100;
        public UDINT udESLbdMittelwert;
        public UDINT udESStartphase;
        public UDINT udESStartphaseMaxImpVerlaengerung;
        public DINT MaxAnzahlBlockaden;
        public UDINT udES_LDZMittelwert;
        public DINT LambdaoffsetTeillast;
        public UDINT udESLambdaUG;
        public UDINT udESLambdaOG;
        public UDINT StructItem2;
        public DINT StructItem3;
        public DINT StructItem4;
        public DINT StructItem5;
    }
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public unsafe struct _PUFFER
    {
        public DINT Puffer_vorhanden;
        public _ENERGIEQUELLE ENERGIEQUELLE;
        public enum _ENERGIEQUELLE : UDINT
        {
            EQ_KESSEL = 0,
            EQ_PUFFER1 = 1,
            EQ_PUFFER2 = 2,
            EQ_PUFFER3 = 3,
            EQ_PUFFER4 = 4,
        }
        public DINT min_Oben;
        public DINT max_Unten;
        public _Freigabeart Freigabeart;
        public enum _Freigabeart : UDINT
        {
            PUFFER_AUS = 0,
            PUFFER_EIN = 1,
            PUFFER_ZEIT = 2,
        }
        public _FREIGABEZEITEN Freigabezeit;
        public DINT Anforderungsdifferenz;
        public DINT Pumpendiff_oben;
        public DINT Pumpendiff_unten;
        public DINT KT_Min;
        public DINT Durchladen_Bei_Boileranforderung;
        public DINT Fremdkesselfreigabe;
        public DINT Weiche_Pk_Uebertemperatur;
        public DINT max_Unten_2;
        public DINT max_Unten_3;
        public DINT VltSoll_Max_unten;
        public DINT Ueberwaermeabfuhr_permanent;
        public DINT Ueberwaermeabfuhr_Solarertrag;
        public DINT max_Unten_FK;
    }
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public unsafe struct _SOLAR_STRUCT
    {
        public DINT Solarkreis_vorhanden;
        public _SOLARKREISREGLER SOLARKREISREGLER;
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public unsafe struct _SOLARKREISREGLER
        {
            public UDINT ud_PI_Intervall;
            public UDINT ud_PI_P_Faktor;
            public UDINT ud_PI_I_Faktor;
            public UDINT ud_Max_I_Anteil;
            public UDINT ud_UebertempBegrenzung;
            public UDINT udSolarAbschaltTemp;
            public UDINT udSolarFreigabeTemp;
            public UDINT udMaxStellgroesse;
            public UDINT udSchichtladeprogramm;
            public DINT PumpenNachlaufzeit;
            public UDINT ud_PID_D_Faktor;
            public UDINT ud_DIV_P;
            public UDINT ud_DIV_I;
            public UDINT ud_DIV_D;
            public UDINT ud_ABS_I;
            public HDINT hdKF_StartZeit;
            public HDINT hdKF_EndZeit;
            public INT iKuehlfunktionmaxSpeicherUnten;
            public INT iKuehlfunktionEA;
            public INT iSollDifferenz1;
            public INT iSollDifferenz2;
            public UDINT dPumpensteuerung2;
        }
        public _SOLARKREISSOLLWERTE SOLARKREISSOLLWERTE;
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public unsafe struct _SOLARKREISSOLLWERTE
        {
            public UDINT udPumpensteuerung;
            public HDINT hdFG1_StartZeit;
            public HDINT hdFG1_EndZeit;
            public UDINT udSolar_Ausdifferenz1;
            public UDINT udSolar_Ausdifferenz2;
            public UDINT udMax_Speicher1Temp_unten;
            public UDINT udMax_Speicher2Temp_unten;
            public UDINT udSolar_Eindifferenz1;
            public UDINT udSolar_Eindifferenz2;
            public UDINT udDrehzahlregelung;
            public UDINT udMinDrehzahl1;
            public UDINT udKollektor1_SollTemp;
            public UDINT udKollektor2_SollTemp;
            public DINT Kollektor1MinTemp;
            public DINT Kollektor2MinTemp;
            public INT iFrostschutz_E_A;
            public INT iFrostschutz_Einschalttemp;
            public INT iFrostschutz_Ausschalttemp;
            public INT iKollTemp_WaermeabfuhrEin;
            public INT iKollTemp_WaermeabfuhrAus;
            public INT iUeberwaermeabfuhr_E_A;
            public INT iMaxDrehzahl1;
            public INT iMinDrehzahl2;
            public INT iMaxDrehzahl2;
            public INT iPumpennachlauf2;
        }
        public _STARTFUNKTION STARTFUNKTION;
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public unsafe struct _STARTFUNKTION
        {
            public UDINT udStartfunktionEINAUS;
            public UDINT udMaxPause;
            public UDINT udMessspuelImpuls;
            public UDINT udMesszeit;
            public UDINT ud1Messanstieg;
            public UDINT ud2SpuelImpuls;
            public INT iKuehlfunktion2EA;
            public INT iKuehlfunktionmaxSpeicher2Unten;
            public HDINT hdKF_StartZeit2;
            public HDINT hdKF_EndZeit2;
            public HDINT hdFG2_StartZeit;
            public HDINT hdFG2_EndZeit;
        }
        public DINT Regelkreise;
        public _KOF_TYP Kollektorfuehlertyp;
        public DINT Wellenpaket;
        public DINT Signaltyp_AnOut1;
        public _Solarschema Solarschema;
        public enum _Solarschema : UDINT
        {
            _2_PUMPEN_1_FELD = 0,
            _PUMPE_UMSCHALTVENTIL = 1,
            _2Zonen = 2,
            _2Felder2Pumpen = 3,
            _2Felder_PumpeVentil = 4,
            _2_EKR = 5,
            EnumItem33 = 6,
            EnumItem44 = 7,
            EnumItem66 = 8,
            EnumItem64 = 9,
            _3K_3Pumpen = 10,
            _3K_Pumpe_Ventil_Pumpe = 11,
            _3K_Pumpe_Ventil_Ventil = 12,
            _3K_Schichtlademodul_Pumpe = 13,
            _2Felder_2Pumpen_2Speicher = 14,
        }
        public DINT PumpenEinteilung;
        public DINT Speichervorrang;
        public DINT Gesamtwartezeit_tw;
        public DINT Laufzeit_tL;
        public DINT Hep_min_Spannung1;
        public DINT Ventilruhestellung;
        public BSINT bsiFuehlertyp;
        public _SOL_SP_FUEHLER_TYP Speicherfuehler1Typ;
        public _SOL_SP_FUEHLER_TYP Speicherfuehler2Typ;
        public DINT PWM_AnOut1_Invers;
        public DINT PWM_AnOut2_Invers;
        public DINT Signaltyp_AnOut2;
        public INT iHep_min_Spannung2;
        public BYTE byVentilruhestellung2;
        public INT iVerzoegerungSekPumpe_SLM;
        public INT iDifferenzAktivierung2terKreis;
    }
    public enum _KOF_TYP : UDINT
    {
        Modul_PT1000 = 0,
        Modul_KTY = 1,
        S1I1_PT1000 = 2,
        S2I1_PT1000 = 3,
        S3I1_PT1000 = 4,
        S4I1_PT1000 = 5,
    }
    public enum _SOL_SP_FUEHLER_TYP : INT
    {
        Modul_SPF_KTY_1K = 0,
        Modul_SPF_PT1000 = 1,
        Modul_I8_KTY_2K = 2,
        Modul_I7_KTY_2K = 3,
        Octoplus_X32_SpUnten = 4,
        Octoplus_X31_SpMitte = 5,
        LT_X35_PufferMitte = 10,
        LT_X36_PufferUnten = 20,
        LT_X44_PufferOben = 21,
        HKMod1_I6 = 30,
        HKMod1_I5 = 31,
        HKMod2_I6 = 40,
        HKMod2_I5 = 41,
        HKMod3_I6 = 50,
        HKMod3_I5 = 51,
        HKMod4_I6 = 60,
        HKMod4_I5 = 61,
        D1i1 = 70,
        D1i2 = 71,
        D2i1 = 75,
        D2i2 = 76,
        D3i1 = 80,
        D3i2 = 81,
        D4i1 = 85,
        D4i2 = 86,
    }
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public unsafe struct _WMZ_STRUCT
    {
        public DINT WMZ_vorhanden;
        public DINT Durchflussrate;
        public DINT WMZ_Reset;
        public DINT Frostschutzkonzentration;
        public DINT DurchflussmengengeberTyp;
        public DINT WMZ_fuer_Kessel;
        public DINT UebertempZeit;
        public DINT StructItem1;
        public DINT StructItem;
        public _KOF_TYP Kollektorfuehlertyp_2;
    }
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public unsafe struct _PID_PARAMETER
    {
        public DINT PAnteil;
        public DINT IAnteil;
        public DINT DAnteil;
        public DINT I_Max;
        public DINT StructItem0;
        public DINT StructItem1;
        public DINT StructItem2;
        public DINT StructItem3;
    }
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public unsafe struct _NOTBETRIEB_STRUCT
    {
        public _NOTBETRIEB_OUTPUTS Notbetrieb_Ausgang_IN;
        public _NOTBETRIEB_OUTPUTS Notbetrieb_Ausgang_OUT;
    }
    public enum _NOTBETRIEB_OUTPUTS : UINT
    {
        keineFunktion = 0,
        HK1_Pumpe = 1,
        HK2_Pumpe = 2,
        BoilerPumpe = 3,
        Puffer_Pumpe = 4,
        RLAPumpe = 5,
        Zirkulationspumpe = 6,
        HK1_Mischer = 7,
        HK2_Mischer = 8,
        RLA_Mischer = 9,
        SGT = 10,
        Gluehstab = 11,
        WTR = 12,
        Einschubmotor = 13,
        RA_Saug = 14,
        RA_Direkt = 15,
        AschenaustrMotor = 16,
        Rueckbrandschieber = 17,
        Reserve = 18,
        Primaerluft = 19,
        Zuendzuluft = 20,
        Magnet_Reserve = 21,
    }
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public unsafe struct _ZIRKULATION_STRUCT
    {
        public DINT Zirkulation_Vorhanden;
        public HDINT hdFG1_StartZeit;
        public HDINT hdFG1_EndZeit;
        public HDINT hdFG2_StartZeit;
        public HDINT hdFG2_EndZeit;
        public HDINT hdFG3_StartZeit;
        public HDINT hdFG3_EndZeit;
        public HDINT hdFG4_StartZeit;
        public HDINT hdFG4_EndZeit;
        public HDINT hdFG5_StartZeit;
        public HDINT hdFG5_EndZeit;
        public _FreigabeArt FreigabeArt;
        public enum _FreigabeArt : UDINT
        {
            Zk_ImmerAus = 0,
            ZK_ImmerEin = 1,
            Zk_Zeitregelung = 2,
            Zk_Blockweise = 3,
            Zk_Tagweise = 4,
        }
        public UDINT udPumpenImpuls;
        public UDINT udPumpenPause;
        public DINT WarmwasserSollTemp;
        public DINT Kontrollimpuls;
        public DINT StructItem;
        public DINT StructItem0;
        public DINT StructItem1;
        public DINT StructItem2;
        public DINT StructItem3;
        public DINT StructItem4;
        public DINT StructItem5;
    }
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public unsafe struct _ZIRKULATION_ZEIT_ARRAY
    {
        public _FREIGABEZEITEN Mo_bis_Fr_Freigabezeit;
        public _FREIGABEZEITEN Sa_bis_So_Freigabezeit;
        public _FREIGABEZEITEN Mo_Freigabezeit;
        public _FREIGABEZEITEN Di_Freigabezeit;
        public _FREIGABEZEITEN Mi_Freigabezeit;
        public _FREIGABEZEITEN Do_Freigabezeit;
        public _FREIGABEZEITEN Fr_Freigabezeit;
        public _FREIGABEZEITEN Sa_Freigabezeit;
        public _FREIGABEZEITEN So_Freigabezeit;
    }
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public unsafe struct _WP_SETTING_STRUCT
    {
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 1 + 1 - 0)]
        public _WP_EMERSON_SETTING_STRUCT[] _RCC_SETTING;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 1 + 1 - 0)]
        public _WP_PRIMKREISPUMPE_STRUCT_ELEMENT[] _WP_PK_PUMPE_PARA;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 1 + 1 - 0)]
        public _LUEFTER_SETTING_ELEMENT[] _WP_LUEFTER_STRUCT;
        public _WP_SYSTEMREGLER_EINSTELL_STRUCT _WP_SYSREG_SET;
    }
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public unsafe struct _WP_EMERSON_SETTING_STRUCT
    {
        public _RCC_CopressorModel_Enum Compressor_Model;
        public _RCC_PressureSensorType_Enum Suction_Sensor_Type_P1;
        public _RCC_PressureSensorType_Enum Discharge_Sensor_Type_P2;
        public _RCC_PressureSensorType_Enum Intermediate_Sensor_Type_P3;
        public INT Cooling_control_mode;
        public INT Cooling_superheat_setpoint;
        public INT Defrost_startup_duration;
        public INT Defrost_statup_opening;
        public INT Heating_control_mode;
        public INT Heating_Superheat_Setpoint;
        public INT Heating_startup_duration;
        public INT Heating_startup_opening;
        public INT EVI_Superheat_Setpoint;
        public INT EVI_startup_duration;
        public INT EVI_startup_opening;
        public INT EVI_high_superheat_alarm_thershold;
        public INT EVI_high_superheat_alarm_delay;
        public INT EVI_control_mode;
        public INT Heating_startup_speed_duration;
        public INT Heating_startup_speed;
        public INT Cooling_start_up_speed_duration;
        public INT Defrost_startup_speed;
        public INT Low_superheat_alarm_threshold;
        public INT Low_superheat_alarm_delay;
        public INT Low_pressure_alarm_threshold;
        public INT Low_pressure_alarm_delay;
        public INT Low_pressure_alarm_band;
        public INT Alarm_pause;
        public INT Envelope_alarm_delay;
        public INT MOP_threshold;
        public INT High_superheat_alarm_threshold;
        public INT High_superheat_alarm_delay;
        public INT CompStopDelayAfterDefrost;
        public INT Freeze_alarm_delay;
        public INT Statorheater;
        public INT Defrost_interval;
        public INT Defrost_duration;
        public INT Defrost_endtemp;
        public INT Heating_EXV_Kp;
        public INT Heating_EXV_Kd;
        public INT Heating_EXV_Ki;
        public INT EVI_EXV_Kp;
        public INT EVI_EXV_Ki;
        public INT EVI_EXV_Kd;
        public INT Cooling_EXV_Kp;
        public INT Cooling_EXV_Ki;
        public INT Cooling_EXV_Kd;
        public INT Oilreturn_speed;
        public INT Oilreturn_threshold;
        public INT Oilreturn_duration;
        public INT Oilreturn_threshold_duration;
        public INT High_condensing_pressure_alarm_delay;
        public INT High_Condensing_pressure_alarm_threshold;
        public INT Fan_control_min_speed;
        public INT Defrost_delta_threshold;
        public INT Fan_ctrl_max_speed_temp_heat;
        public INT Fan_ctrl_min_speed_temp_heat;
        public INT FREMAR_frequency_band;
        public INT FREMAR_frequency_1;
        public INT FREMAR_frequency_2;
        public INT FREMAR_frequency_3;
        public INT Defrost_delta_absolute_threshold;
        public INT Max_comp_speed_during_defrost;
        public INT Max_comp_speed_during_heating;
        public INT Fan_control_max_speed;
        public INT Min_comp_speed_during_defrost;
        public INT Defrost_startup_speed_duration;
        public INT Capacity_input_mode;
        public _WP_RCC_DigIn1Enum Digital_input_1;
        public INT Digital_output_2;
        public INT Digital_output_3;
        public INT Accessory_power_inital_value;
        public INT Compressor_current_protection;
        public INT Cooling_startup_duration;
        public INT Cooling_startup_opening;
        public INT Cooling_startup_speed;
        public INT Defrost_slowdown_condensing_temp;
        public INT Defrost_slowdown_speed;
        public INT Modbus_slave_address;
        public INT Fan_delay_after_defrost;
        public INT Fan_ctrl_max_speed_temp_cooling;
        public INT Fan_ctrl_min_speed_temp_cooling;
        public INT Modbus_Baud_rate;
        public INT Modbus_Parity;
        public INT Compressor_stop_delay_before_defrost;
        public INT fourway_switching_speed;
        public INT fourway_switching_delta_pressure;
        public _WP_RCC_DigIn2_Enum Digital_input_2;
        public INT fourway_logic;
        public INT Comp_minSpeed;
        public INT FanCtrl_minSpeed_Cooling;
        public INT FanCtrl_maxSpeed_Cooling;
        public INT DriveSpeedRate_Reg108;
        public INT Startup_SH_SP_Inc_after_defrost_Reg007;
        public INT Startup_SH_SP_Inc_heating_startup_Reg008;
        public INT Startup_SH_SP_Inc_cooling_startup_Reg009;
        public INT Startup_SH_SP_Inc_timeout_Reg010;
        public INT Startup_SH_SP_Inc_speedlimit_Reg011;
        public INT Startup_SH_SP_Inc_speedduration_Reg012;
        public INT Pre_opening_phase_duration_Reg078;
        public INT Pre_opening_temp_Reg103;
        public INT Press_drop_sh_sp_shift_rate_Reg005;
        public INT CurcuitConfig_Reg086;  //! <Type Comment="1: Monobloc&#13;&#10;2: Split" Name="_WP_EMERSON_SETTING_STRUCT.CurcuitConfig_Reg086"/>
        public DINT reserviertReg_6_85;
    }
    public enum _RCC_CopressorModel_Enum : UDINT
    {
        RCC_CM_ZHW_16 = 0,
        RCC_CM_ZHW_08 = 1,
        RCC_CM_ZHW_08_K1_1 = 2,
        RCC_CM_ZHW_015K2 = 3,
        RCC_CM_ZHW_030K2 = 4,
        RCC_CM_ZHW_015K2_1Ph = 5,
        RCC_CM_ZHW_030K2_1Ph = 6,
    }
    public enum _RCC_PressureSensorType_Enum : UDINT
    {
        RCC_PST_7bar = 0,
        RCC_PST_18bar = 1,
        RCC_PST_30bar = 2,
        RCC_PST_50bar = 3,
    }
    public enum _WP_RCC_DigIn1Enum : INT
    {
        RCC_DI1_EVULock = 0,
        RCC_DI1_Defrost = 1,
        RCC_DI1_NoFunction = 2,
        RCC_DI1_EVULock_Inv = 10,
    }
    public enum _WP_RCC_DigIn2_Enum : INT
    {
        RCC_DI2_Cooling = 0,
        RCC_DI2_Defrost_Req = 1,
        RCC_DI2_NoFunction = 2,
        RCC_DI2_HP_Switch = 3,
        RCC_DI2_STL_Check = 10,
    }
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public unsafe struct _WP_PRIMKREISPUMPE_STRUCT_ELEMENT
    {
        public DINT Frostschutztemp;
        public UDINT Nachlaufzeit;
        public INT Frostschutz_Df_Soll;
        public INT Betrieb_min_Durchf;
        public INT Betrieb_max_Durchf;
        public INT Betrieb_max_Drehzahl;
        public INT Soll_Spreizung_Heizbetrieb;
        public INT Soll_Spreizung_Boilerladung;
        public _WP_PKP_TYP_ENUM Pumpentyp;
        public DINT Soll_Spreizung_Kuehlen;
        public INT FS_Taktung_E_A;
        public INT FlowCtrl_Defrost;
    }
    public enum _WP_PKP_TYP_ENUM : UDINT
    {
        PKPT_iPWM1 = 0,
        PKPT_iPWM2 = 1,
        PKPT_0_10V_DN15 = 2,
        PKPT_0_10V_DN20 = 3,
    }
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public unsafe struct _LUEFTER_SETTING_ELEMENT
    {
        public _WP_LUEFTER_TYP_ENUM Lueftertyp;
        public _FREIGABEZEITEN Freigabezeit;
        public INT Sollaenderung_FG1;
        public INT Sollaenderung_FG2;
        public INT Sollaenderung_FG3;
        public INT Sollaenderung_FG4;
        public INT Sollaenderung_FG5;
        public UINT Freiblasen_Ein;
        public UINT min_Betriebszeit_Freiblasen;
        public UINT Drehzahlbereich_Sperre1_Min;
        public UINT Drehzahlbereich_Sperre1_Max;
        public UINT Drehzahlbereich_Sperre2_Min;
        public UINT Drehzahlbereich_Sperre2_Max;
        public UINT Drehzahlbereich_Sperre3_Min;
        public UINT Drehzahlbereich_Sperre3_Max;
        public UINT uiCompSpeed_m20_minFan;
        public UINT uiCompSpeed_m20_maxFan;
        public UINT uiCompSpeed_p15_minFan;
        public UINT uiCompSpeed_p15_maxFan;
        public INT At_Freigabetemp;
        public INT At_Abschaltoffset;
        public UDINT At_max_Zeit;
        public UINT At_Drehzahl;
        public UINT At_NachlaufE_A;
    }
    public enum _WP_LUEFTER_TYP_ENUM : UDINT
    {
        WPL_ZiehlAbegg_RH50V_6IK = 0,
    }
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public unsafe struct _WP_SYSTEMREGLER_EINSTELL_STRUCT
    {
        public _FREIGABEZEITEN Sperrzeiten_Montag;
        public _FREIGABEZEITEN Sperrzeiten_Dienstag;
        public _FREIGABEZEITEN Sperrzeiten_Mittwoch;
        public _FREIGABEZEITEN Sperrzeiten_Donnerstag;
        public _FREIGABEZEITEN Sperrzeiten_Freitag;
        public _FREIGABEZEITEN Sperrzeiten_Samstag;
        public _FREIGABEZEITEN Sperrzeiten_Sonntag;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 5 + 1 - 1)]
        public _WP_SPERRE_ENUM[] Sperren_Montag;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 5 + 1 - 1)]
        public _WP_SPERRE_ENUM[] Sperren_Dienstag;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 5 + 1 - 1)]
        public _WP_SPERRE_ENUM[] Sperren_Mittwoch;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 5 + 1 - 1)]
        public _WP_SPERRE_ENUM[] Sperren_Donnerstag;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 5 + 1 - 1)]
        public _WP_SPERRE_ENUM[] Sperren_Freitag;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 5 + 1 - 1)]
        public _WP_SPERRE_ENUM[] Sperren_Samstag;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 5 + 1 - 1)]
        public _WP_SPERRE_ENUM[] Sperren_Sonntag;
        public DINT Aussenabschaltemp_ZH;
        public DINT Aussen_Freigabe_temp_ZH;
        public DINT Aussentemp_Sperre_WP;
        public _FREIGABEZEITEN EVU_Lock;
        public DINT E_Stab_max_ED;
        public DINT E_Stab_min_ED;
        public DINT E_Stab_Periodendauer;  //! <Type Comment="Periodendauer in Minuten&#13;&#10;" Name="_WP_SYSTEMREGLER_EINSTELL_STRUCT.E_Stab_Periodendauer"/>
        public DINT EStab_Kompensation;
        public DINT E_StabLeistung;
        public DINT E_StabAnforderung;
        public UDINT E_Stab_Zeitverzoegerung_TWS;
        public _FREIGABEZEITEN SilentMode_Zeiten;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 5 + 1 - 1)]
        public INT[] max_DrehzahlenLuefter;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 5 + 1 - 1)]
        public INT[] max_DRZ_Kompressor;
        public DINT max_Leistung_Boilerladung;
        public UINT uiZuheizen_bei_Boilerladung;
        public UINT VltSoll_Anhebung_vor_EVUSperre;
        public UDINT udWP_Periodendauer;
        public DINT maxUnterschreitungszeit;
        public DINT Demand_AnalogIn_EA;
        public INT iPowerWP_P_Faktor;
        public INT imaxCompdrz_minus5AT;
        public UDINT E_Stab_Zeitverzoegerung_Heizen;
    }
    public enum _WP_SPERRE_ENUM : UDINT
    {
        _WP_SPERRE_Zusatzheizung = 0,
        _WP_SPERRE_Verdichter = 1,
        _WP_SPERRE_ZH_u_Verdichter = 2,
    }
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public unsafe struct _WP_SGReady_Para_Struct
    {
        public _WPBZS4_Freigabe WPBZS4_Freigabe;
        public enum _WPBZS4_Freigabe : UDINT
        {
            WPBZS4_FG_nurKomp = 0,
            WPBZS4_FG_Komp_Zusatzheizung = 1,
        }
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 12 + 1 - 1)]
        public DINT[] WPSGR_BA3_VLTSOLL_AH;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 12 + 1 - 1)]
        public DINT[] WPSGR_BA4_RTSOLL_AH;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 6 + 1 - 1)]
        public DINT[] WPSGR_BA4_TWS_UEBERLADUNG;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4 + 1 - 1)]
        public DINT[] WPSGR_BA4_PUFFER;
        public _WP_BA_1_SPERRE_ENUM Sperre_BA1;  //! <Type Comment="#0848" Name="_WP_SGReady_Para_Struct.Sperre_BA1"/>
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 6 + 1 - 1)]
        public DINT[] WPSGR_TWS_Hysterese;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 13 + 1 - 0)]
        public DINT[] Reserve;
    }
    public enum _WP_BA_1_SPERRE_ENUM : UDINT
    {
        WP_BA1_ESTAB_WP = 0,
        WP_BA1_WP = 1,
    }
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public unsafe struct _WP_EMERSON_SETTING2_STRUCT
    {
        public INT ControlSpeedRate_Reg109;
        public INT Compressor_Speed_delta_sensitivity_Reg110;
        public INT Compressor_Speed_delta_scaling_Reg111;
        public INT Valvescaling_threshold_Reg112;
        public INT Valvescaling_minimum_Reg113;
        public DINT Invert_EVU_Lock;
        public DINT STB_In2_Check;
        public INT Oilreturn_ts_duration_Cooling;
        public UINT Statorheater_control_Source;  //! <Type Comment="0: Dicharge gas temp.&#13;&#10;1: Oil sump temp." Name="_WP_EMERSON_SETTING2_STRUCT.Statorheater_control_Source"/>
        public INT iStallagtitenprogEA;
        public INT iStallagtitenFanSpeed;
        public INT iLow_Setflowrate_Defrost;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 134 + 1 - 0)]
        public INT[] reserved;
    }
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public unsafe struct _WP_KASKADE_BZM_STRUCT
    {
        public DINT BZM_Aktiv;
        public UDINT BZM_TWS_Laufzeit;
        public UDINT BZM_HK_Laufzeit;
        public UDINT BZM_Pool_Laufzeit;
        public UDINT BZM_Kuehlen_Laufzeit;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 39 + 1 - 0)]
        public DINT[] Not_used;
    }
    public enum _Ausgang_X7 : UDINT
    {
        X7_Keine_Funktion = 0,
        X7_RLA_Pumpe = 1,
        X7_Kuehlventil = 2,
        X7_VentilSpeichergruppe1 = 3,
    }
    public enum StatusEnum : UDINT
    {
        INAKTIV = 0,
        AKTIV = 1,
    }
    public enum Ventilstellung_Kuehlen : UDINT
    {
        Stromlos_Puffer = 0,
        Stromlos_HK = 1,
    }
    public enum _PRODUCT_SF_REVISION : UDINT
    {
        K10_1to3 = 0,
        K10_4to5 = 1,
        K10_6 = 2,
        K15_10_to_11 = 65535,
        K15_12 = 65536,
    }
    public enum E_FunktionAusgangX5 : UDINT
    {
        X5_Standard = 0,
        X5_VentilSpeichergruppe1 = 1,
    }
    public enum E_FunktionAusgangH1X7 : UDINT
    {
        H1X7_Standard = 0,
        H1X7_VentilSpeichergruppe2 = 1,
    }
    public enum E_FunktionAusgangH1X5 : UDINT
    {
        H1X5_Standard = 0,
        H1X5_VentilSpeichergruppe2 = 1,
    }

}