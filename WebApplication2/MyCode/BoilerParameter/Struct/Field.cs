﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.Runtime.InteropServices;

using SINT = System.SByte;
using INT = System.Int16;
using DINT = System.Int32;
using USINT = System.Byte;
using UINT = System.UInt16;
using UDINT = System.UInt32;
using REAL = System.Single;
using LREAL = System.Double;
using BOOL = System.Byte;
using CHAR = System.SByte;
using BYTE = System.SByte;

namespace LogdataReader
{

    public enum FieldSearchOptions
    {
        None = 0,
        IgnoreUpperLower = 1,
        TotalMatch = 2,
    }


    /// <summary>
    /// Representiert ein Struct-Mitglied
    /// Da ein Struct-Mitglied jedoch auch ein Struct sein kann und 
    /// andere Felder enthalten kann werden diese rekursiv ermittelt und in value als Liste
    /// gespeichert. Ob es sich bei value um eine Liste oder einen normalen Wert handelt
    /// lässt sich von DataType ableiten.
    /// </summary>
    public struct Field
    {
        public string name;
        public object value;

        public DataType dataType;

        public Field(string Name, object Value)
        {
            name = Name;

            Type type = Value.GetType();

            _FieldInfo[] fieldInfos = type.GetFields();

            value = null;
            dataType = DataType.UNDEF;

            dataType = getDataType(type);

            if (dataType == DataType.STRUCT)
            {
                List<Field> fields = new List<Field>();

                foreach (FieldInfo fieldInfo in fieldInfos)
                {
                    fields.Add(new Field(fieldInfo.Name, fieldInfo.GetValue(Value)));
                }

                value = fields;
            }
            else
            {
                value = Value;
            }

        }

        private DataType getDataType(Type type)
        {
            Type[] normalTypes = {  typeof(BSINT), typeof(BDINT), typeof(HDINT),
                                    typeof(SINT), typeof(INT), typeof(DINT),
                                    typeof(USINT), typeof(UINT), typeof(UDINT),
                                    typeof(CHAR), typeof(BYTE), typeof(BOOL) };

            DataType result = DataType.UNDEF;

            if (type == typeof(BSINT))
                result = DataType.BSINT;
            else if (type == typeof(BDINT))
                result = DataType.BDINT;
            else if (type == typeof(HDINT))
                result = DataType.HDINT;
            else if (type == typeof(SINT))
                result = DataType.SINT;
            else if (type == typeof(INT))
                result = DataType.INT;
            else if (type == typeof(DINT))
                result = DataType.DINT;
            else if (type == typeof(USINT))
                result = DataType.USINT;
            else if (type == typeof(UINT))
                result = DataType.UINT;
            else if (type == typeof(UDINT))
                result = DataType.UDINT;
            else if (type == typeof(CHAR))
                result = DataType.CHAR;
            else if (type == typeof(BYTE))
                result = DataType.BYTE;
            else if (type == typeof(BOOL))
                result = DataType.BOOL;
            else if (type.IsValueType && !type.IsEnum)
                result = DataType.STRUCT;
            else if (type.IsEnum)
                result = DataType.ENUM;

            return result;
        }


        private static string depth = "";

        public override string ToString()
        {
            string result = depth + name + "\n";

            if (dataType == DataType.STRUCT)
                depth += "\t";

            if (value.GetType() == typeof(List<Field>))
            {
                List<Field> fields = (List<Field>)value;

                foreach (Field field in fields)
                {
                    result += field.ToString();
                }
            }

            if (dataType == DataType.STRUCT)
            {
                depth = depth.Remove(depth.Length - 1);
            }

            return result;
        }

        /// <summary>
        /// Untersucht falls vorhanden alle gegebenen Struct-Member nach Namen.
        /// </summary>
        /// <param name="search">Name von Feld das gesucht werden soll</param>
        /// <param name="returnAll">Sollen alle Suchtreffer zurückgegeben werden?</param>
        /// <param name="fieldSearchOptions">Gibt Möglichkeit Suchkriterien zu besimmen</param>
        /// <returns>Alle gefundenen Felder (im Fall returnAll = false nur das Erste)</returns>
        public Field[] searchAfterName(string search, bool returnAll, FieldSearchOptions fieldSearchOptions = FieldSearchOptions.IgnoreUpperLower)
        {
            List<Field> results = new List<Field>();

            string targetName = name;

            if (fieldSearchOptions == FieldSearchOptions.IgnoreUpperLower)
            {
                search = search.ToUpper();
                targetName = targetName.ToUpper();
            }
            if (targetName.Contains(search))
            {
                results.Add(this);
            }
            if (value.GetType() == typeof(List<Field>))
            {
                List<Field> childs = (List<Field>)value;
                if (childs != null)
                {
                    foreach (Field field in childs)
                    {
                        Field[] result = field.searchAfterName(search, returnAll, fieldSearchOptions);
                        if (result != null)
                        {
                            results.AddRange(result);
                            if (!returnAll)
                            {
                                break;
                            }
                        }
                    }
                }
            }

            return results.ToArray();
        }



    }

    class Einstellungen
    {

        public List<Field> fields = new List<Field>();

        public Einstellungen(object obj)
        {

            Type type = obj.GetType();

            _FieldInfo[] fieldInfos = type.GetFields();
            if (fieldInfos.Length > 0)
            {
                foreach (FieldInfo fieldInfo in fieldInfos)
                {
                    Object val = fieldInfo.GetValue(obj);
                    fields.Add(new Field(fieldInfo.Name, val));
                }
            }

        }/*

        public Field[] searchAfterName(string name, bool returnAll = false, FieldSearchOptions fieldSearchOptions = FieldSearchOptions.IgnoreUpperLower)
        {
            name = name.ToUpper();
            List<Field> results = new List<Field>();

            foreach(Field field in fields)
            {
                Field result = field.searchAfterName(name, returnAll, fieldSearchOptions);
                if(result.name != null)
                {
                    results.Add(result);
                    if(!returnAll)
                    {
                        break;
                    }
                }
            }

            return results.ToArray();
        }
        */

    }
}
