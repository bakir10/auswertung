﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Models.BoilerParameter;
using LogdataReader;
using System.IO;
using System.Runtime.InteropServices;
using System.Web.Hosting;



namespace LogdataReader
{

    //Da aus den temp_*.h files automatisch ein C-Struct erstellt wird werden hier statische Methoden, welche zusammen mit der Klasse verwendet werden können erstellt
    static public class KesselparameterMethoden
    {
        //Kesselparameter

        /// <summary>
        /// Erstellt ein Struct aus einem Einstellungen.dat file
        /// </summary>
        /// <param name="path">Pfad zum Einstellungen.dat file</param>
        /// <returns></returns>
        static public unsafe _KESSELPARAMETER createStruct(string path)
        {
            FileStream einstellungen = new FileStream(path, FileMode.Open);

            FileStream cTypes = new FileStream(HostingEnvironment.MapPath(@"~/App_Data/Kesselparameter/C_types.h").ToString(), FileMode.Open);
            FileStream cSharpStruct = new FileStream(HostingEnvironment.MapPath(@"~/App_Data/Kesselparameter/temp_kesselparameter.h").ToString(), FileMode.Open);

            convertCtoCSharpStruct(cTypes, cSharpStruct, "_KESSELPARAMETER");

            _KESSELPARAMETER kesselparameter = readKesselparameter(einstellungen);

            einstellungen.Close();

            return kesselparameter;
        }

        /// <summary>
        /// Liest aus File Kesselparameter ein.
        /// </summary>
        /// <param name="fs"></param>
        /// <returns></returns>
        static private _KESSELPARAMETER readKesselparameter(FileStream fs)
        {
            _KESSELPARAMETER result;

            // Einlesen Gesamter Datei
            byte[] bytes = new byte[Marshal.SizeOf(typeof(_KESSELPARAMETER))];
            fs.Read(bytes, 0, bytes.Length);

            // Da sich normale C# Variablen in verwaltetem Speicher befinden, müssen die Daten zuerst in unverwalteten Speicher geschrieben werden
            IntPtr ptr = Marshal.AllocHGlobal(bytes.Length);
            int index = 0;
            foreach (byte current in bytes)
            {
                Marshal.WriteByte(ptr, index, current);
                index++;
            }

            // Nun werden die unverwalteten Daten (ptr) auf die Struktur projeziert
            result = (_KESSELPARAMETER)Marshal.PtrToStructure(ptr, typeof(_KESSELPARAMETER));
            Marshal.FreeHGlobal(ptr);

            return result;
        }




        //Maximus

        /// <summary>
        /// Erstellt ein Struct aus einem Maximus.dat file
        /// </summary>
        /// <param name="path">Pfad zum Maximus.dat file</param>
        /// <returns></returns>
        static public unsafe _MAXIMUS createMaximusStruct(string path)
        {
            FileStream einstellungen = new FileStream(path, FileMode.Open);

            FileStream cTypes = new FileStream(HostingEnvironment.MapPath(@"~/App_Data/Kesselparameter/C_types.h").ToString(), FileMode.Open);
            FileStream cSharpStruct = new FileStream(HostingEnvironment.MapPath(@"~/App_Data/Kesselparameter/temp_maximus.h").ToString(), FileMode.Open);

            convertCtoCSharpStruct(cTypes, cSharpStruct, "_MAXIMUS");

            _MAXIMUS maximus = readMaximusparameter(einstellungen);

            einstellungen.Close();

            return maximus;

        }

        /// <summary>
        /// Liest aus File Maximus Parameter ein.
        /// </summary>
        /// <param name="fs"></param>
        /// <returns></returns>
        static private _MAXIMUS readMaximusparameter(FileStream fs)
        {
            _MAXIMUS result;

            // Einlesen Gesamter Datei
            byte[] bytes = new byte[Marshal.SizeOf(typeof(_MAXIMUS))];
            fs.Read(bytes, 0, bytes.Length);

            // Da sich normale C# Variablen in verwaltetem Speicher befinden, müssen die Daten zuerst in unverwalteten Speicher geschrieben werden
            IntPtr ptr = Marshal.AllocHGlobal(bytes.Length);
            int index = 0;
            foreach (byte current in bytes)
            {
                Marshal.WriteByte(ptr, index, current);
                index++;
            }

            // Nun werden die unverwalteten Daten (ptr) auf die Struktur projeziert
            result = (_MAXIMUS)Marshal.PtrToStructure(ptr, typeof(_MAXIMUS));
            Marshal.FreeHGlobal(ptr);

            return result;
        }




        //Allgemein

        /// <summary>
        /// Konvertiert eine C-Struktur zu einer C# Struktur.
        /// </summary>
        /// <param name="cStruct"></param>
        /// <param name="cSharpStruct"></param>
        static private void convertCtoCSharpStruct(FileStream cStruct, FileStream cSharpStruct, string structName)
        {
            StreamReader reader = new StreamReader(cStruct);
            StreamWriter writer = new StreamWriter(cSharpStruct);

            List<string> lines = new List<string>();

            while (!reader.EndOfStream)
            {
                lines.Add(reader.ReadLine());
            }

            const string header = "using System.Runtime.InteropServices;\n" +
                                    "using SINT = System.Byte;\n" +
                                    "using INT = System.Int16;\n" +
                                    "using DINT = System.Int32;\n" +
                                    "using USINT = System.Byte;\n" +
                                    "using UINT = System.UInt16;\n" +
                                    "using UDINT = System.UInt32;\n" +
                                    "using REAL = System.Single;\n" +
                                    "using LREAL = System.Double;\n" +
                                    "using BOOL = System.Byte;\n" +
                                    "using CHAR = System.SByte;\n" +
                                    "using BYTE = System.SByte;\n" +
                                    "\n" +
                                    "namespace LogdataReader\n" +
                                    "{\n";

            const string footer = "\n}";

            writer.Write(header);

            int pos;
            for (pos = 0; pos < lines.Count && !lines[pos].Contains(structName); pos++) ;

            Struct kesselParameter = new Struct(lines, pos, false);
            kesselParameter.writeAll(writer);

            writer.Write(footer);

            reader.Close();
            writer.Close();
        }

    }
}