﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
//using LogdataReader.LineWrappers;

namespace LogdataReader
{
    public class Struct
    {

        // Statische Liste, um zu verhindern, dass dasselbe Struct 2 mal geschrieben wird
        static List<string> writtenStructs = new List<string>();

        // Info
        public string name;
        public bool isInsideStruct;
        public int length;

        public List<string> requiredStructsAndEnums = new List<string>();

        public List<Line> lines = new List<Line>();

        public List<Struct> requiredStructs = new List<Struct>();

        public List<Enum> requiredEnums = new List<Enum>();

        /// <summary>
        /// Erstellt neues Struct.
        /// Structs können Variablen haben, dessen Typen ein anderes Struct oder ein Enum ist, daher werden alle Datentypen,
        /// die nicht zu den Standardtypen gehören (INT, DINT, UDINT, ...) in einer Liste gespeichert, 
        /// sodass die relevanten hinausgefiltert werden können.
        /// </summary>
        /// <param name="rawLines">Zeilenweise ganezs File</param>
        /// <param name="start">Zeile in der Struct startet</param>
        /// <param name="isInsideStruct">Befindet sich das Struct in einem Anderen?</param>
        public Struct(List<string> rawLines, int start, bool isInsideStruct)
        {
            this.isInsideStruct = isInsideStruct;

            LineType prevType = LineType.Undefined, nextType = LineType.Undefined;
            Line line, nextLine;
            int structs = 0;

            int i;
            for (i = start; i < rawLines.Count; i++)
            {
                if (i + 1 < rawLines.Count) // Achtung auf letzte Zeile
                {
                    nextLine = new Line(rawLines[i + 1], structs > 0);
                    nextType = nextLine.lineType;
                }

                line = new Line(rawLines[i], structs > 0, prevType, nextType);

                if (line.lineType == LineType.StructHeader)
                {
                    if (i == start) // StructHeader kann sich nur am Start befinden.
                    {
                        StructHeader structHeader = (StructHeader)line.lineWrapper;
                        name = structHeader.structName;
                    }
                }
                else if (line.lineType == LineType.Variable)
                {
                    // Bei Variablen muss der Datentyp geprüft werden, damit nötige Strucs und Enums hinzugefügt werden können.
                    Variable variable = (Variable)line.lineWrapper;
                    if (variable.isStructOrEnum && !requiredStructsAndEnums.Contains(variable.dataType))
                    {
                        requiredStructsAndEnums.Add(variable.dataType);
                    }
                }

                // Zählt Structtiefe mit, sodass Ende nicht übersehen wird.
                if (line.line.Contains("{"))
                {
                    structs++;
                }
                else if (line.line.Contains('}'))
                {
                    structs--;
                }

                if (structs == 0 && i > start)
                {
                    length = i - start;
                    i = rawLines.Count;
                }

                prevType = line.lineType;
                lines.Add(line);
            }

            findRequiredStructsAndEnums(rawLines);
        }

        /// <summary>
        /// Durchläuft ganze Datei und sucht dabei nach benötigten Structs und Enums, die im Konstruktor definiert wurden.
        /// Dabei werden diese wiederum als einzelne Structs und Enums erstellt, die gegebenenfalls wieder nach Structs suchen
        /// </summary>
        /// <param name="lines"></param>
        public void findRequiredStructsAndEnums(List<string> lines)
        {
            LineType prevType = LineType.Undefined, nextType = LineType.Undefined;
            Line line, nextLine;

            int i;
            for (i = 0; i < lines.Count && requiredStructsAndEnums.Count > 0; i++)
            {
                if (i + 1 < lines.Count)
                {
                    nextLine = new Line(lines[i + 1], false);
                    nextType = nextLine.lineType;
                }

                line = new Line(lines[i], false, prevType, nextType);

                if (line.lineType == LineType.StructHeader)
                {
                    StructHeader header = (StructHeader)line.lineWrapper;
                    if (requiredStructsAndEnums.Contains(header.structName))
                    {
                        Struct add = new Struct(lines, i, false);
                        requiredStructs.Add(add);
                        i += add.length;
                    }
                }
                else if (line.lineType == LineType.EnumHeader)
                {
                    EnumHeader header = (EnumHeader)line.lineWrapper;
                    if (requiredStructsAndEnums.Contains(header.enumName))
                    {
                        Enum add = new Enum(lines, i, false);
                        requiredEnums.Add(add);
                        i += add.length;
                    }
                }

                prevType = line.lineType;
            }
        }

        /// <summary>
        /// Schreibt alle Zeilen des Structs in den gegebenen Stream
        /// </summary>
        /// <param name="writer"></param>
        public void writeAll(StreamWriter writer)
        {
            if (!writtenStructs.Contains(this.name))
            {
                writtenStructs.Add(this.name);
                int i;
                for (i = 0; i < lines.Count; i++)
                {
                    string toWrite = lines[i].getFormattedLine();
                    if (i + 1 < lines.Count)
                    {
                        if (lines[i].lineType == LineType.EnumMember && lines[i + 1].lineType != LineType.EnumMember)
                        {
                            toWrite += "}\n";
                        }
                    }
                    writer.Write(toWrite);
                }
                foreach (Struct current in requiredStructs)
                {
                    current.writeAll(writer);
                }
                foreach (Enum current in requiredEnums)
                {
                    current.writeAll(writer);
                }
            }
        }
    }

}
