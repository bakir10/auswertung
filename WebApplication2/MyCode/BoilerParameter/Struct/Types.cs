﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;

namespace LogdataReader
{

    /*
     * Wenn einem bestimmten Datentypen extra Funktionalität verschafft werden soll,
     * ist diese hier in einem Struct mit Namen des entsprechenden Datentyps zu implementieren.
     * 
     * ACHTUNG!
     * Struct darf nur einen Wert enthalten, der der Größe des Datentyps entspricht!
     */

    [StructLayout(LayoutKind.Sequential, Pack = 1)] // Wird benötigt um sicherzustellen, dass Größe richtig definiert wird!
    public struct HDINT
    {
        public int value;

        public HDINT(int value)
        {
            this.value = value;
        }

        public override string ToString()
        {
            byte[] bytes = getTimeValues();
            return string.Format("{0:00}:{1:00}:{2:00}:{3:00}", bytes[3], bytes[2], bytes[1], bytes[0]);
        }

        /// <summary>
        /// Gibt die einzelnen Bytes des Werts zurück.
        /// index 3: Stunden
        /// index 2: Minuten
        /// index 1: Sekunden
        /// index 0: Millisekunden
        /// </summary>
        public byte[] getTimeValues()
        {
            byte[] bytes = BitConverter.GetBytes(value);

            /*
            miliseconds = bytes[0];
            seconds = bytes[1];
            minutes = bytes[2];
            hours = bytes[3];
            */

            return bytes;
        }

    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct BDINT
    {
        public int value;

        public BDINT(int value)
        {
            this.value = value;
        }

        /// <summary>
        /// Gibt die Positionen aller High Bits zurück.
        /// </summary>
        /// <returns></returns>
        public int[] getHighBits()
        {
            List<int> highs = new List<int>();

            /*
             * Um High-Bits zu ermitteln wird der Wert mit 2er Exponenten von 0-31 bitweise AND verknüpft
             * Bsp:
             * value: ....  0100
             * da 2^2 binär 0100 enstspricht bleibt nach bitweisem AND nur mehr das erste Bit übrig, alle anderen werden entfernt
             * bei 2^1 ergibt die Verknüpfung allerdings 0, das Bit an Stelle 2 ist also low.
             */
            int i;
            for (i = 0; i < sizeof(int) * 8; i++)
            {
                int copy = value;

                copy = copy & (int)(Math.Pow(2, i));
                if (copy != 0)
                {
                    highs.Add(i + 1);
                }
            }

            return highs.ToArray();
        }


    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct BSINT
    {
        public byte value;

        public BSINT(byte value)
        {
            this.value = value;
        }

        // siehe BDINT.getHighBits()
        public int[] getHighBits()
        {
            List<int> highs = new List<int>();

            int i;
            for (i = 0; i < sizeof(int) * 8; i++)
            {
                byte copy = value;
                copy = (byte)(copy & (byte)Math.Pow(2, i));
                if (copy != 0)
                {
                    highs.Add(i + 1);
                }
            }

            return highs.ToArray();
        }

    }

}