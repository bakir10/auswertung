﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogdataReader
{
    class BSINTFooter : LineWrapper
    {
        public BSINTFooter(string line) : base(line) { }

        public override string getFormattedLine()
        {
            string line = content;

            line = line.Remove(line.Length - 1);

            string name = line.Substring(searchFirstLetter(line));
            string space = line.Remove(searchFirstLetter(line) - 1);

            line = string.Format("{0}public BSINT {1};\n", space, name);

            return line;
        }
    }
}
