﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogdataReader
{
    class Variable : LineWrapper
    {
        public Variable(string line) : base(line)
        {
            formattedLine = getFormattedLine();
        }

        private string formattedLine;

        public bool isStructOrEnum
        {
            get;
            private set;
        }

        public string dataType
        {
            get;
            private set;
        }

        public override string getFormattedLine()
        {
            string line = content;

            if (line.Contains("["))
            {
                string[] lineSplit = line.Split(new char[] { '[', ']' }, StringSplitOptions.RemoveEmptyEntries);

                int pos = searchFirstLetter(line);
                while (pos < line.Length && line[pos] != ' ')
                {
                    pos++;
                }

                int firstLetter = searchFirstLetter(line);
                dataType = line.Substring(firstLetter, pos - firstLetter);
                isStructOrEnum = structOrEnum(dataType);

                line = line.Insert(pos, "[]");

                pos += 2 + "[]".Length;
                while (pos < line.Length && line[pos] != ' ')
                {
                    pos++;
                }

                line = line.Remove(pos) + ";";
                line = insertInLine(line, "public ");
                line = string.Format("[MarshalAs(UnmanagedType.ByValArray, SizeConst={0})]\n", lineSplit[1]) + line;
            }
            else
            {
                dataType = line.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries)[0];
                isStructOrEnum = structOrEnum(dataType);
                line = insertInLine(line, "public ");
            }

            return line + "\n";
        }

        private bool structOrEnum(string sDataType)
        {
            bool result = true;
            string[] dataTypes = { "SINT", "INT", "DINT", "USINT", "UINT", "UDINT", "REAL", "LREAL", "BOOL", "CHAR", "BSINT", "BINT", "BDINT", "HDINT", "BYTE" };

            if (containsOneOf(sDataType, dataTypes))
            {
                result = false;
            }

            return result;
        }

        private bool containsOneOf(string search, string[] strings)
        {
            bool result = false;
            foreach (string current in strings)
            {
                if (search.Contains(current))
                {
                    result = true;
                    break;
                }
            }
            return result;
        }

    }
}
