﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogdataReader
{
    class StructHeader : LineWrapper
    {
        public StructHeader(string line, bool isInsideStruct) : base(line)
        {
            this.isInsideStruct = isInsideStruct;

            formattedLine = getFormattedLine();
        }

        private string formattedLine;

        public string structName
        {
            get;
            private set;
        }

        public bool isInsideStruct
        {
            get;
            private set;
        }

        public override string getFormattedLine()
        {
            string line = content;

            if (line.Contains("typedef"))
            {
                line = line.Remove(0, "typedef".Length + 1);
            }

            string[] lineSplit = line.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            if (lineSplit.Length > 1)
            {
                structName = lineSplit[1];

                line = "[StructLayout(LayoutKind.Sequential, Pack = 1)]\n" + insertInLine(line, "public unsafe ");

                if (lineSplit.Length > 1 && isInsideStruct)
                {
                    string variableName;
                    if (structName[0] != '_')
                    {
                        variableName = structName.Insert(0, "_");
                    }
                    else
                    {
                        variableName = structName.Substring(1);
                    }
                    line = string.Format("public {0} {1};\n", structName, variableName) + line;
                }

                line += "\n";
            }

            return line;
        }
    }

}
