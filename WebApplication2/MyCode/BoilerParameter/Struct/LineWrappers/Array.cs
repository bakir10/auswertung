﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogdataReader.LineWrappers
{


    class ArrayLine : LineWrapper
    {
        public ArrayLine(string line) : base(line) { }

        public string dataType
        {
            get;
            private set;
        }

        public bool isStructOrEnum
        {
            get;
            private set;
        }

        public override string getFormattedLine()
        {
            string line = content;

            string[] lineSplit = line.Split(new char[] { '[', ']' });
            int pos = searchFirstLetter(line);
            while (pos < line.Length && line[pos] != ' ')
            {
                pos++;
            }

            dataType = line.Substring(0, pos);
            isStructOrEnum = structOrEnum(dataType);

            line = line.Insert(pos, "[]");

            pos += 2 + "[]".Length;
            while (pos < line.Length && line[pos] != ' ')
            {
                pos++;
            }

            line = line.Remove(pos) + ";";
            line = insertInLine(line, "public ");
            line = string.Format("[MarshalAs(UnmanagedType.ByValArray, SizeConst={0})]\n", lineSplit[1]) + line;

            return line + "\n";
        }

        private bool structOrEnum(string sDataType)
        {
            bool result = true;
            string[] dataTypes = { "SINT", "INT", "DINT", "USINT", "UINT", "UDINT", "REAL", "LREAL", "BOOL", "CHAR", "BSINT", "BINT", "BDINT", "HDINT" };

            if (containsOneOf(sDataType, dataTypes))
            {
                result = false;
            }

            return result;
        }

        private bool containsOneOf(string search, string[] strings)
        {
            bool result = false;
            foreach (string current in strings)
            {
                if (search.Contains(current))
                {
                    result = true;
                    break;
                }
            }
            return result;
        }

        private DataType getDataType(string sDataType)
        {

            throw new NotImplementedException();

        }

    }
}
