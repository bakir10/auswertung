﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogdataReader
{
    class StructFooter : LineWrapper
    {
        public StructFooter(string line) : base(line) { }

        public string name;

        public override string content
        {
            get => base.content;
            set
            {
                base.content = value;
                formattedLine = getFormattedLine();
            }
        }

        private string formattedLine;

        public override string getFormattedLine()
        {
            string line = content;

            int index = 1;
            foreach (char character in line)
            {
                if (character == '}')
                {
                    break;
                }
                index++;
            }
            name = line.Substring(index, line.Length - index - 1);
            // Entferne nachkommende Zeichen
            line = line.Remove(index, line.Length - index);

            return line + "\n";
        }
    }
}
