﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogdataReader
{
    class EnumHeader : LineWrapper
    {
        public EnumHeader(string line, bool isInsideStruct = true) : base(line)
        {
            this.isInsideStruct = isInsideStruct;
            getFormattedLine();
        }

        public bool isInsideStruct
        {
            get;
            private set;
        }

        public string enumName
        {
            get;
            private set;
        }

        public override string getFormattedLine()
        {
            string line = content;

            if (line.Contains("typedef"))
                line = line.Remove(0, "typedef".Length);

            string[] lineSplit = line.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

            lineSplit[1] = lineSplit[1].Remove(lineSplit[1].Length - 1);

            enumName = lineSplit[1];

            if (enumName[0] != '_' && isInsideStruct)
            {
                enumName = enumName.Insert(0, "_");
            }

            line = string.Format("public enum {0} : {1} {2}", enumName, lineSplit[0], "{");

            if (isInsideStruct)
            {
                if (enumName[0] != '_')
                {
                    enumName = enumName.Insert(0, "_");
                }
                line = string.Format("public {0} {1};\n", enumName, enumName.Substring(1)) + line;
            }

            return line + "\n";
        }
    }
}
