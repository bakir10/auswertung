﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogdataReader
{
    class EnumMember : LineWrapper
    {
        public EnumMember(string line) : base(line) { }

        public override string getFormattedLine()
        {
            string line = content;

            string[] lineSplit = line.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            line = string.Format("\t{0} = {1},", lineSplit[1], lineSplit[2]);

            return line + "\n";
        }

    }
}
