﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogdataReader
{
    class EnumFooter : LineWrapper
    {
        public EnumFooter(string line) : base(line) { }

        public override string getFormattedLine()
        {
            return new Line(content, true).getFormattedLine().Insert(0, "}\n");
        }
    }
}
