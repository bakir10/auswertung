﻿using System;
using System.Web.Optimization;

namespace WebApplication2.App_Start
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            //bundeles
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                    "~/Scripts/jquery-{version}.js"
                ));
            bundles.Add(new StyleBundle("~/Content/css").Include(
                 "~/Content/bootstrap.css",
                 //"~/Content/font-awesome.min.css",
                "~/Content/style.css"
                ));
          
            bundles.Add(new StyleBundle("~/Content/kendobootstrap/css").Include(
                    "~/Content/kendo/2021.2.616/kendo.bootstrap-v4.css"
                ));

            //Bootstrap
            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                    "~/Scripts/bootstrap.bundle.min.js",
                    "~/Scripts/bootstrap..min.js"
                ));

            //The kendo Javascript bundle ~/bundles/kendo/2021.2.616"
            bundles.Add(new Bundle("~/bundles/kendo").Include(
                    "~/Scripts/kendo/2021.2.616/jszip.min.js",
                    "~/Scripts/kendo/2021.2.616/kendo.all.min.js",
                    "~/Scripts/kendo/2021.2.616/kendo.aspnetmvc.min.js"
                ));

            //The Kendo css bundle
            bundles.Add(new StyleBundle("~/Content/kendo/2021.2.616/css").Include(
                    "~/Content/kendo/2021.2.616/kendo.common.min.css",
                    "~/Content/kendo/2021.2.616/kendo.default.min.css",
                    "~/Content/kendo/2021.2.616/kendo.dataviz.min.css",
                    "~/Content/kendo/2021.2.616/kendo.dataviz.default.min.css"
                ));
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                    "~/Scripts/modernizr-*"
                ));
          
            //Clear all items from the default ignore list to allow minified css and js files to be included in debug mode
            bundles.IgnoreList.Clear();

            //Add back some of the default ignore list rules
            
            bundles.IgnoreList.Ignore("*.intellisense.js");
            bundles.IgnoreList.Ignore("*.vsdoc.js");
            bundles.IgnoreList.Ignore("*.debug.js, OptimizationMode.WhenEnabled");
        }
    }
}