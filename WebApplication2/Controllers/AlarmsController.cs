﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using WebApplication2.Models;
using System.Data.Entity;
using Microsoft.WindowsAzure.Storage.Table;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage;
using System.Threading.Tasks;
using WebApplication2.Session;

namespace WebApplication2.Controllers
{
    [SessionTimeout]
    public class AlarmsController : Controller
    {
        TableOperations tableOperation;
        public readonly mysolarfocusTestContext _db = new mysolarfocusTestContext();
     
        public AlarmsController()
        {
            tableOperation = new TableOperations();
        }
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult UploadAlarm()
        {
            return View();
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Editing_Inline_Update([DataSourceRequest] DataSourceRequest request, Alarm alarm)
        {
            if (alarm != null && ModelState.IsValid)
            {
                _db.Entry(alarm).State = EntityState.Modified;
                _db.SaveChanges();
            }
            return Json(new[] { alarm }.ToDataSourceResult(request, ModelState));
        }
       
       public ActionResult AlarmTable(string Alarmfile)
        {
            if(Alarmfile.Equals("null"))
            {
                return View("NotFound");
            }
            
            DataTable dt_Alarmdaten = CSVtoDatatable(Alarmfile);
            dt_Alarmdaten = ConvertDataTable(dt_Alarmdaten);
            ViewBag.dt_Alarmdaten = dt_Alarmdaten;
            ViewBag.Alarmfile = Alarmfile;

            Session["alarmfile"] = Alarmfile;

            return View(dt_Alarmdaten);
        }

        public ActionResult Read_Alarm([DataSourceRequest] DataSourceRequest request, string Alarmfile)
        {
            ViewBag.Alarmfile = Alarmfile;
            DataTable dt_Alarmdaten = CSVtoDatatable(Alarmfile);
            dt_Alarmdaten = ConvertDataTable(dt_Alarmdaten);

            if (request.Aggregates.Any())
            {
                request.Aggregates.Each(agg =>
                {
                    agg.Aggregates.Each(a =>
                    {
                        a.MemberType = dt_Alarmdaten.Columns[agg.Member].DataType;
                    });
                });
            }
            return Json(dt_Alarmdaten.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        #region Upload
        public ActionResult Async_Save(IEnumerable<HttpPostedFileBase> files)
        {
            // The Name of the Upload component is "files"
            if (files != null)
            {
                string root = Path.Combine(System.Environment.GetEnvironmentVariable("Temp"));
                string subdir = root + "\\Alarm" + Session["sid"];
                if (!Directory.Exists(subdir))
                {
                    Directory.CreateDirectory(subdir);
                }
                foreach (var file in files)
                {
                    // Some browsers send file names with full path.
                    // We are only interested in the file name.
                    var fileName = Path.GetFileName(file.FileName);
                    var physicalPath = Path.Combine(System.Environment.GetEnvironmentVariable("Temp"), @"Alarm" + Session["sid"] + "\\", fileName);
                    file.SaveAs(physicalPath);
                }
            }

            // Return an empty string to signify success
            return Content("");
        }

        public ActionResult Async_Remove(string[] fileNames)
        {
            // The parameter of the Remove action must be called "fileNames"
            if (fileNames != null)
            {
                foreach (var fullName in fileNames)
                {
                    var fileName = Path.GetFileName(fullName);
                    var physicalPath = Path.Combine(System.Environment.GetEnvironmentVariable("Temp"), @"Alarm" + Session["sid"] + "\\", fileName);

                    // TODO: Verify user permissions

                    if (System.IO.File.Exists(physicalPath))
                    {
                        System.IO.File.Delete(physicalPath);
                    }
                }
            }

            // Return an empty string to signify success
            return Content("");
        }

        public ActionResult DeleteAlarmFile()
        {
            if (Directory.Exists(Path.Combine(System.Environment.GetEnvironmentVariable("Temp"), @"Alarm" + Session["sid"] + "\\")))
            {
                var l_fileNames = Directory.GetFiles(System.Environment.GetEnvironmentVariable("Temp"), @"Alarm" + Session["sid"] + "\\").Select(Path.GetFileName).ToList();
                if (l_fileNames != null)
                {
                    foreach (var file in l_fileNames)
                    {
                        var fileName = Path.GetFileName(file);
                        var physicalPath = Path.Combine(Environment.GetEnvironmentVariable("Temp"), @"Alarm" + Session["sid"] + "\\", fileName);

                        if (System.IO.File.Exists(physicalPath))
                        {
                            System.IO.File.Delete(physicalPath);
                        }
                    }
                }
            }
            return RedirectToAction("UploadAlarm", "Alarms");
        }
        #endregion
        public ActionResult FindAlarmName()
        {
            if(Directory.Exists(Path.Combine(System.Environment.GetEnvironmentVariable("Temp"), @"Alarm" + Session["sid"] + "\\")))
            { 
                string[] log_filenames = Directory.GetFiles(Environment.GetEnvironmentVariable("Temp"), @"Alarm" + Session["sid"] + "\\").Select(Path.GetFileName).ToArray();

                foreach (var item in log_filenames)
                {
                    if (item.Contains("alarm") || item.Contains("Alarm"))
                    {
                        var naziv = new { alarmfile = item };
                        return Json(naziv, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            return RedirectToAction("NotFound", "Alarms");
        }

        #region Dropdownlist Alarm file
        public JsonResult GetAlarmFiles()
        {
            if(Directory.Exists(Path.Combine(Environment.GetEnvironmentVariable("Temp"), @"Alarm" + Session["sid"] + "\\")))
            {
                var l_fileNames = Directory.GetFiles(System.Environment.GetEnvironmentVariable("Temp"), @"Alarm" + Session["sid"] + "\\").Select(Path.GetFileName).ToList();
                SelectList sl_fileNames = new SelectList(l_fileNames, "Value");
                return Json(sl_fileNames, JsonRequestBehavior.AllowGet);
            }
            SelectList emptyList = new SelectList("","Value");
            return Json(emptyList, JsonRequestBehavior.AllowGet);
        }

        #endregion
        #region Methoden zum einlesen des Datatables
        public DataTable CSVtoDatatable(string alarmfilename)
        {
            string strLine;                                         //String in dem die gelesene Zeile gespeichert wird
            string[] strArray;                                      //String Array in dem die einzelnen Werte der gelesenen Zeile gespeichert werden
            DataTable dt = new DataTable();                         //Datatable in dem die Werte gespeichert werden
            DataRow dr = dt.NewRow();
            string path = Path.Combine(System.Environment.GetEnvironmentVariable("Temp"), @"Alarm" + Session["sid"] + "\\" + alarmfilename);

            #region Therminator Alarmliste
            if (alarmfilename.Contains("Alarmliste_"))
            {
                FileStream tFile = new FileStream(path, FileMode.Open);              //File welches ausgewählt wurde wird "geöffnet"
                using (StreamReader sr = new StreamReader(tFile, System.Text.Encoding.Default))         //File wird gelesen
                {
                    //Header
                    strLine = sr.ReadLine();                              //Erste Zeile lesen (Überschriften)
                    strLine = strLine.Replace(".", string.Empty);
                    strLine = strLine.Replace("\x00", string.Empty);        //NUL loschen
                    strArray = strLine.Split(';');                 //Werte aus Zeile in ein Array aufteilen

                    foreach (String header in strArray)
                    {
                        dt.Columns.Add(header);
                    }

                    while (sr.Peek() > -1)
                    {
                        strLine = sr.ReadLine();
                        strArray = strLine.Split(';');
                        dt.Rows.Add(strArray);
                    }
                }
            }
            #endregion
            else
            {
                FileStream aFile = new FileStream(path, FileMode.Open);              //File welches ausgewählt wurde wird "geöffnet"
                using (StreamReader sr = new StreamReader(aFile, System.Text.Encoding.Default))         //File wird gelesen
                {
                    //Header
                    strLine = sr.ReadLine();                              //Erste Zeile lesen (Überschriften)

                    strLine = strLine.Replace(".", string.Empty);
                    strLine = strLine.Replace("#", "St");
                    strLine = strLine.Replace("Alarmdatum", "Datum");
                    strLine = strLine.Replace("Alarmzeit", "Zeit");
                    strLine = strLine.Replace("Alarmtext", "Text");
                    strLine = strLine.Replace("Alarmstatus", "Status");
                    strArray = strLine.Split('\t');                 //Werte aus Zeile in ein Array aufteilen

                    foreach (String header in strArray)
                    {
                        dt.Columns.Add(header);
                    }

                    while (sr.Peek() > -1)
                    {
                        strLine = sr.ReadLine();
                        strLine = strLine.Replace(",", string.Empty);       
                        strLine = strLine.Replace("\x00", string.Empty);    //NUL löschen
                        strLine = strLine.Replace("_", string.Empty);
                        strArray = strLine.Split('\t');

                        if (!strArray[0].Equals(string.Empty))
                        {
                            dt.Rows.Add(strArray);
                        }
                    }
                }

                foreach (DataRow row in dt.Rows)
                {
                    row["Zeit"] = row["Zeit"].ToString().Substring(0, 5);
                    dt.AcceptChanges();
                }
                if (alarmfilename.Contains("alarmstatistik")) { 
                    foreach (DataRow row in dt.Rows)
                    { 
                        if (IsInList(Convert.ToInt32(row[0])))
                        {
                            if (alarmfilename.Contains("alarmstatistik"))
                            {
                                int k = Convert.ToInt32(row[0]);
                                k += 400;
                                string code = k.ToString();
                                string alarmCode = "A"+code;
                                string warningCode = "W"+code;

                                var resAlarm = tableOperation.filterTable("AppAlarmsAndWarnings", alarmCode.ToString());
                                var resWarning = tableOperation.filterTable("AppAlarmsAndWarnings", warningCode.ToString());

                                if (resAlarm != null)
                                {
                                    row[4] = resAlarm.de;
                                }
                                else if (resWarning != null)
                                {
                                    row[4] = resWarning.de;
                                }
                                else
                                {
                                    row[4] = "Not Found !";
                                }
                            }
                        }
                        else
                        {
                            row[4] = "Not Found !";
                        }
                    }
                }
            }
        return dt;
        }

        public bool IsInList(int value)
        {
            bool condition = false;

            value += 400;
            string aCode = "A"+value;
            string wCode = "W"+value;

            var aStatus = tableOperation.filterTable("AppAlarmsAndWarnings", aCode);
            var wStatus = tableOperation.filterTable("AppAlarmsAndWarnings", wCode);

            if (aStatus != null || wStatus != null)
            {
                return true;
            }
               
            return condition;
        }

        private DataTable ConvertDataTable(DataTable datatable)
        {
            #region Allgemeine Konvertierungen
            int columns = datatable.Columns.Count;              //Variable mit Spalten des Datatables
            int rows = datatable.Rows.Count;                    //Variable mit Zeilen des Datatables

            for (int i = 0; i < columns; i++)
            {
                datatable.Columns[i].ColumnName = datatable.Columns[i].ColumnName.Replace("\0", string.Empty);  //Löscht alle Leerzeichen in den Spaltennamen (entstehen durch Leerzeichen in CSV)
            }

            for (int i = 0; i < columns; i++)                                                           //Durchläuft alle Spalten im Datatable
            {
                if (datatable.Columns[i].ColumnName == "Column1")                              //Gibt es eine Spalte mit dem gesuchten Namen?
                {
                    datatable.Columns.RemoveAt(i);
                }
            }

            columns = datatable.Columns.Count;
            rows = datatable.Rows.Count;
            #endregion

            #region Anderen Alarmliste
            //Nr.
            for (int i = 0; i < columns; i++)                                                         //Durchläuft alle Spalten im Datatable
            {
                if (datatable.Columns[i].ColumnName == "Nr")                                         //Gibt es eine Spalte mit dem gesuchten Namen?
                {
                    for (int x = 0; x < rows; x++)
                    {
                        datatable.Rows[x][i] = datatable.Rows[x][i];                                //Werte auf richtige Einheit bringen
                    }
                }
            }
           
            //#
            for (int i = 0; i < columns; i++)                                                           //Durchläuft alle Spalten im Datatable
            {
                if (datatable.Columns[i].ColumnName == "St")                              //Gibt es eine Spalte mit dem gesuchten Namen?
                {
                    for (int x = 0; x < rows; x++)
                    {
                        datatable.Rows[x][i] = datatable.Rows[x][i].ToString();             //Werte auf richtige Einheit bringen
                    }
                }
            }

            //Alarmdatum
            for (int i = 0; i < columns; i++)                                                           //Durchläuft alle Spalten im Datatable
            {
                if (datatable.Columns[i].ColumnName == "Datum")                              //Gibt es eine Spalte mit dem gesuchten Namen?
                {
                    for (int x = 0; x < rows; x++)
                    {
                        datatable.Rows[x][i] = datatable.Rows[x][i];             //Werte auf richtige Einheit bringen
                    }
                }
            }

            //Alarmzeit
            for (int i = 0; i < columns; i++)                                                           //Durchläuft alle Spalten im Datatable
            {
                if (datatable.Columns[i].ColumnName == "Zeit")                              //Gibt es eine Spalte mit dem gesuchten Namen?
                {
                    for (int x = 0; x < rows; x++)
                    {
                        datatable.Rows[x][i] = datatable.Rows[x][i];             //Werte auf richtige Einheit bringen
                    }
                }
            }

            //Alarmtext
            for (int i = 0; i < columns; i++)                                                           //Durchläuft alle Spalten im Datatable
            {
                if (datatable.Columns[i].ColumnName == "Text")                              //Gibt es eine Spalte mit dem gesuchten Namen?
                {
                    for (int x = 0; x < rows; x++)
                    {
                        datatable.Rows[x][i] = datatable.Rows[x][i];             //Werte auf richtige Einheit bringen
                    }
                }
            }
            #endregion

            #region Therminator Alarm
            //Zeitformat
            for (int i = 0; i < columns; i++)                                                         //Durchläuft alle Spalten im Datatable
            {
                if (datatable.Columns[i].ColumnName == "Zeitformat")                                         //Gibt es eine Spalte mit dem gesuchten Namen?
                {
                    for (int x = 0; x < rows; x++)
                    {
                        datatable.Rows[x][i] = datatable.Rows[x][i];                                //Werte auf richtige Einheit bringen
                    }
                }
            }

            //Alarmgruppennummer
            for (int i = 0; i < columns; i++)                                                           //Durchläuft alle Spalten im Datatable
            {
                if (datatable.Columns[i].ColumnName == "Alarmgruppennummer")                              //Gibt es eine Spalte mit dem gesuchten Namen?
                {
                    for (int x = 0; x < rows; x++)
                    {
                        datatable.Rows[x][i] = datatable.Rows[x][i].ToString();             //Werte auf richtige Einheit bringen
                    }
                }
            }

            //Alarmnummer
            for (int i = 0; i < columns; i++)                                                           //Durchläuft alle Spalten im Datatable
            {
                if (datatable.Columns[i].ColumnName == "Alarmnummer")                              //Gibt es eine Spalte mit dem gesuchten Namen?
                {
                    for (int x = 0; x < rows; x++)
                    {
                        datatable.Rows[x][i] = datatable.Rows[x][i];             //Werte auf richtige Einheit bringen
                    }
                }
            }

            //Alarmtext
            for (int i = 0; i < columns; i++)                                                           //Durchläuft alle Spalten im Datatable
            {
                if (datatable.Columns[i].ColumnName == "Alarmtext")                              //Gibt es eine Spalte mit dem gesuchten Namen?
                {
                    for (int x = 0; x < rows; x++)
                    {
                        datatable.Rows[x][i] = datatable.Rows[x][i];             //Werte auf richtige Einheit bringen
                    }
                }
            }
           
            //Quittierstatus
            for (int i = 0; i < columns; i++)                                                           //Durchläuft alle Spalten im Datatable
            {
                if (datatable.Columns[i].ColumnName == "Quittierstatus")                              //Gibt es eine Spalte mit dem gesuchten Namen?
                {
                    for (int x = 0; x < rows; x++)
                    {
                        datatable.Rows[x][i] = datatable.Rows[x][i];             //Werte auf richtige Einheit bringen
                    }
                }
            }

            //Alarmstatus
            for (int i = 0; i < columns; i++)                                                           //Durchläuft alle Spalten im Datatable
            {
                if (datatable.Columns[i].ColumnName == "Alarmstatus")                              //Gibt es eine Spalte mit dem gesuchten Namen?
                {
                    for (int x = 0; x < rows; x++)
                    {
                        datatable.Rows[x][i] = datatable.Rows[x][i];             //Werte auf richtige Einheit bringen
                    }
                }
            }
            #endregion

            return datatable;
        }
        #endregion
    }
}