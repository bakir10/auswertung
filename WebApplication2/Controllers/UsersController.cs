﻿using System;
using System.Web;
using System.Web.Mvc;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using WebApplication2.Models;
using WebApplication2.Controllers;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using WebApplication2.Session;

namespace WebApplication2.Controllers
{
    public class UsersController : Controller
    {
        public readonly mysolarfocusTestContext _db = new mysolarfocusTestContext();
        public virtual DbSet<User> Users { get; set; }
       
        public ActionResult Index([DataSourceRequest] DataSourceRequest request)
        {
            List<User> UserList = new List<User>();
            UserList = _db.Users.ToList();
            var result = UserList.ToDataSourceResult(request);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Login()
        {
            return View();
        }

        public ActionResult Logout()
        {
            Session.Abandon();
            return RedirectToAction("Login","Users");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(string username, string password)
        {
            if (ModelState.IsValid)
            {
                var userInfo = _db.Users.Where(u => u.Username.Equals(username) && u.Password.Equals(password)).FirstOrDefault();
                if(userInfo != null)
                {
                    Session["sid"] = userInfo.Id.ToString();
                    Session["username"] = userInfo.Username.ToString();
                    return Redirect("../Home/Index");
                }
            }
            TempData["ErrorMessage"] = "Not Valid Data!";
           
            return View();
        }
    }
}