﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Globalization;
using System.Data;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using WebApplication2.Models;

namespace WebApplication2.Controllers
{
    [OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
    public class ValidationsController : Controller
    {
        public ValidationsController() { }
        public readonly mysolarfocusTestContext db = new mysolarfocusTestContext();

        /**public JsonResult IsAlarmCode_Available(int AlarmCode)
        {
            Alarm existingAlarmCode = db.Alarms.FirstOrDefault(alarm => alarm.AlarmCode == AlarmCode);

            if (existingAlarmCode != null)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }

            return Json(true, JsonRequestBehavior.AllowGet);
        }*/

        public JsonResult IsValid_KesselTyp(string KesselTyp)
        {
            if (KesselTyp.Equals("Therminator") ||
               KesselTyp.Equals("EcoTop | EcoTopZero | Pellet Elegance | OctoPlus | Maximus | Regelzentrale") ||
               KesselTyp.Equals("VampAir K8/K10 | VampAir K12/K15"))
            {
                return Json(true, JsonRequestBehavior.AllowGet);
            }

            return Json(false, JsonRequestBehavior.AllowGet);
        }

        /* [HttpPost]
          public JsonResult IsUsername_Available(string Username)
          {
              User checkUsername = db.Users.FirstOrDefault(user => user.Username == Username);

              if(checkUsername != null)
              {
                  return Json(false, JsonRequestBehavior.AllowGet);
              }
              else
              {
                  return Json(false, JsonRequestBehavior.AllowGet);
              }
              return Json(!db.Users.Any(user => user.Username == Username), JsonRequestBehavior.AllowGet);
          }*/
    }
}