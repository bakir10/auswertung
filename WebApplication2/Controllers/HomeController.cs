﻿using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using LogdataReader;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using WebApplication2.Models;
using WebApplication2.Session;

namespace WebApplication2.Controllers
{
    [SessionTimeout]
    public class HomeController : Controller
    {
        public readonly mysolarfocusTestContext _db = new mysolarfocusTestContext();
     
        #region chart
        // GET: Home
        public ActionResult Index()
        {
            if(Session["sid"] != null) { 
                return View("Index");
            }
            return RedirectToAction("Login", "Users");
        }


        public ActionResult PreviousNext(string page, string logfile, string Produktserie, string[]  visibleseries)
        {
            string[] l_fileNames = Directory.GetFiles(Environment.GetEnvironmentVariable("Temp"), @"Files" + Session["sid"] + "\\").Select(Path.GetFileName).ToArray();
            Array.Sort(l_fileNames);

            int maxPage = l_fileNames.Count();
            string currentLogfile = logfile;
            string produktserie = Produktserie;
            int currentPage = 0;

            //index of current page
            for (int i = 0; i < maxPage; i++)
            {
                if (currentLogfile == l_fileNames[i])
                {
                    currentPage = i;
                }
            }

            if (page == "next" && (currentPage + 1) < maxPage)
            {
                if ((currentPage + 1) <= maxPage)
                {
                    var result = new { Produktserie = produktserie, Logfile = l_fileNames[currentPage + 1], Visibleseries = visibleseries };
                    return Json(result, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    var result = new { Produktserie = produktserie, Logfile = l_fileNames[currentPage], Visibleseries = visibleseries };
                    return Json(result, JsonRequestBehavior.AllowGet);
                }
            }
            else if (page == "previous")
            {
                if ((currentPage - 1) > -1)
                {
                    var result = new { Produktserie = produktserie, Logfile = l_fileNames[currentPage - 1], Visibleseries = visibleseries };
                    return Json(result, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    var result = new { Produktserie = produktserie, Logfile = l_fileNames[currentPage], Visibleseries = visibleseries };
                    return Json(result, JsonRequestBehavior.AllowGet);
                }
            }

            var res = new { Produktserie = produktserie, Logfile = l_fileNames[currentPage], Visibleseries = visibleseries };
            return Json(res, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Auswertung(string Produktserie, string Logfile, string[] Visibleseries)
        {

            string[] visibleseries = Visibleseries;

            try
            {
                visibleseries = Visibleseries[0].Split(',');
            }
            catch (Exception)
            {
                visibleseries = Visibleseries;
            }

            var l_fileNames = Directory.GetFiles(Environment.GetEnvironmentVariable("Temp"), @"Files" + Session["sid"] + "\\").Select(Path.GetFileName).ToList();
            var countFiles = l_fileNames.Count();

            if (l_fileNames == null)
            {
                l_fileNames.Add("");
            }

            var datum = DateFromLogfile(Logfile, Produktserie);
            var noviDatum = datum.ToString().Substring(2, 7);
            SelectList sl_fileNames = new SelectList(l_fileNames, "Value");
            List<string> alarmCategories = new List<string>();
            ViewBag.Datum = datum;
            ViewBag.files = l_fileNames.ToArray();
            ViewBag.alarmCategories = alarmCategories;
           
            DataTable dt_Logdaten = CSVtoDatatable(Logfile, Produktserie);
            dt_Logdaten = ConvertDataTable(dt_Logdaten, Produktserie);

            if (Produktserie.Equals("VampAir K08/K10"))
            {
                Session["VampAirKesselTyp"] = "VampAir K08/K10";
            }
            else
            {
                Session["VampAirKesselTyp"] = "VampAir K12/K15";
            }

            ViewBag.dt_Logdaten = dt_Logdaten;
            ViewBag.Produktserie = Produktserie;
            ViewBag.Logfile = Logfile;
            ViewBag.VisibleSeries = visibleseries;
            Session["logfile"] = Logfile;
            Session["typ"] = Produktserie;

            return View(dt_Logdaten);
        }

        public ActionResult AuswertungTable(string Produktserie, string Logfile, string[] Visibleseries)
        {
            string[] visibleseries = Visibleseries;

            try
            {
                visibleseries = Visibleseries[0].Split(',');
            }
            catch (Exception)
            {
                visibleseries = Visibleseries;
            }

            var l_fileNames = Directory.GetFiles(System.Environment.GetEnvironmentVariable("Temp"), @"Files" + Session["sid"] + "\\").Select(Path.GetFileName).ToList();
            var countFiles = l_fileNames.Count();
            if (l_fileNames == null)
            {
                l_fileNames.Add("");
            }

            var datum = DateFromLogfile(Logfile, Produktserie);
            ViewBag.Datum = datum;
            SelectList sl_fileNames = new SelectList(l_fileNames, "Value");
            ViewBag.files = l_fileNames.ToArray();

            DataTable dt_Logdaten = CSVtoDatatable(Logfile, Produktserie);
            dt_Logdaten = ConvertDataTable(dt_Logdaten, Produktserie);

            ViewBag.dt_Logdaten = dt_Logdaten;
            ViewBag.Produktserie = Produktserie;
            ViewBag.Logfile = Logfile;
            ViewBag.VisibleSeries = visibleseries;

            return View(dt_Logdaten);
        }

        public ActionResult FindLogDate(string logDate)
        {
            if (Directory.Exists(Path.Combine(System.Environment.GetEnvironmentVariable("Temp"), @"Files" + Session["sid"] + "\\"))) 
            {
                string[] log_filenames = Directory.GetFiles(System.Environment.GetEnvironmentVariable("Temp"), @"Files" + Session["sid"] + "\\").Select(Path.GetFileName).ToArray();

                for (int i = 0; i < log_filenames.Length; i++)
                {
                    if (log_filenames[i].Contains("log") || log_filenames[i].Contains("Log"))
                    {
                        #region VampAir K08/K10 | VampAir K12/K15
                        if (log_filenames[i].Contains("wp") && Session["VampAirKesselTyp"] != null)
                        {
                            var fileDate = DateFromLogfile(log_filenames[i], Session["VampAirKesselTyp"].ToString());
                            if (fileDate.Equals(logDate))
                            {
                                return RedirectToAction("Auswertung", "Home", new { Produktserie = Session["VampAirKesselTyp"].ToString(), Logfile = log_filenames[i], Visibleseries = "spaceholder" });
                            }
                        }
                        #endregion
                        #region EcoTop | EcoTopZero | Pellet Elegance | OctoPlus | Maximus | Regelzentrale
                        else
                        {
                            
                            var kessel = "EcoTop | EcoTopZero | Pellet Elegance | OctoPlus | Maximus | Regelzentrale";
                            var fileDate = DateFromLogfile(log_filenames[i], kessel);
                            if (fileDate.Equals(logDate))
                            {
                                return RedirectToAction("Auswertung", "Home", new { Produktserie = kessel, Logfile = log_filenames[i], Visibleseries = "spaceholder" });
                            }
                        }
                        #endregion
                    }
                    #region Therminator
                    else
                    {
                        
                        var kessel = "Therminator";
                        var fileDate = DateFromLogfile(log_filenames[i], kessel);
                        if (fileDate.Equals(logDate))
                        {
                            var result = new { Produktserie = kessel, Logfile = log_filenames[i] };
                            return RedirectToAction("Auswertung", "Home", new { Produktserie = kessel, Logfile = log_filenames[i], Visibleseries = "spaceholder" });
                        }
                    }
                    #endregion
                }
            }
            return View("Notfound");
        }

        #region Upload

        public ActionResult Async_Save(IEnumerable<HttpPostedFileBase> files)
        {
            // The Name of the Upload component is "files"
            if (files != null)
            {
                string root = Path.Combine(System.Environment.GetEnvironmentVariable("Temp"));
                string subdir = root + "\\Files"+Session["sid"];
                if (!Directory.Exists(subdir))
                {
                    Directory.CreateDirectory(subdir);
                }
                foreach (var file in files)
                {
                    // Some browsers send file names with full path.
                    // We are only interested in the file name.
                    var fileName = Path.GetFileName(file.FileName);
                    var physicalPath = Path.Combine(System.Environment.GetEnvironmentVariable("Temp"), @"Files"+Session["sid"]+"\\", fileName);
                    file.SaveAs(physicalPath);
                }
            }
            // Return an empty string to signify success
            return Content("");
        }

        public ActionResult Async_Remove(string[] fileNames)
        {
            // The parameter of the Remove action must be called "fileNames"
            if (fileNames != null)
            {
                foreach (var fullName in fileNames)
                {
                    var fileName = Path.GetFileName(fullName);
                    var physicalPath = Path.Combine(System.Environment.GetEnvironmentVariable("Temp"), @"Files"+Session["sid"]+"\\", fileName);

                    // TODO: Verify user permissions

                    if (System.IO.File.Exists(physicalPath))
                    {
                        System.IO.File.Delete(physicalPath);
                    }
                }
            }

            // Return an empty string to signify success
            return Content("");
        }

        public ActionResult DeleteLogFile()
        {
            if (Directory.Exists(Path.Combine(System.Environment.GetEnvironmentVariable("Temp"), @"Files" + Session["sid"] + "\\")))
            {
                var l_fileNames = Directory.GetFiles(System.Environment.GetEnvironmentVariable("Temp"), @"Files" + Session["sid"] + "\\").Select(Path.GetFileName).ToList();
                if (l_fileNames != null)
                {
                    foreach (var file in l_fileNames)
                    {
                        var fileName = Path.GetFileName(file);
                        var physicalPath = Path.Combine(Environment.GetEnvironmentVariable("Temp"), @"Files" + Session["sid"] + "\\", fileName);

                        if (System.IO.File.Exists(physicalPath))
                        {
                            System.IO.File.Delete(physicalPath);
                        }
                    }
                }
            }
            return RedirectToAction("Index", "Home");
        }

        #endregion

        #region Dropdownlist Logdaten

        public JsonResult GetLogfiles()
        {
            SelectList emptyList = new SelectList("", "Value");
            if (Directory.Exists(Path.Combine(System.Environment.GetEnvironmentVariable("Temp"), @"Files" + Session["sid"] + "\\")))
            {
                var l_fileNames = Directory.GetFiles(System.Environment.GetEnvironmentVariable("Temp"), @"Files" + Session["sid"] + "\\" ).Select(Path.GetFileName).ToList();
                var countFiles = l_fileNames.Count();
                if (l_fileNames == null)
                {
                    l_fileNames.Add("");
                }
                SelectList sl_fileNames = new SelectList(l_fileNames, "Value");
                return Json(sl_fileNames, JsonRequestBehavior.AllowGet);
            }
            return Json(emptyList);
        }

        public ActionResult GetTempLogFiles()
        {
            var l_fileNames = Directory.GetFiles(Path.GetTempPath()+"\\Files").Select(Path.GetFileName).ToList();
            SelectList sl_fileNames = new SelectList(l_fileNames, "Value");

            return Json(l_fileNames, JsonRequestBehavior.AllowGet);
        }

        public ActionResult FilterLogFiles([DataSourceRequest] DataSourceRequest request, string kessel)
        {
            SelectList emptyList = new SelectList("", "Value");
            if (Directory.Exists(Path.Combine(System.Environment.GetEnvironmentVariable("Temp"), @"Files" + Session["sid"] + "\\")))
            {
                var l_fileNames = Directory.GetFiles(Path.GetTempPath() + "\\Files" + Session["sid"] + "\\").Select(Path.GetFileName).ToList();
                SelectList sl_fileNames = new SelectList(l_fileNames, "Value");

                if (kessel.Contains("VampAir"))
                {
                    if (l_fileNames == null)
                    {
                        l_fileNames.Add("");
                    }
                    return Json(sl_fileNames.Where(f => f.Text.Contains("wp")), JsonRequestBehavior.AllowGet);
                }
                else if (kessel.Contains("EcoTop"))
                {
                    if (l_fileNames == null)
                    {
                        l_fileNames.Add("");
                    }
                    return Json(sl_fileNames.Where(f => (f.Text.Contains("log")) && (!f.Text.Contains("wp"))), JsonRequestBehavior.AllowGet);
                }
                else
                {
                    if (l_fileNames == null)
                    {
                        l_fileNames.Add("");
                    }
                    return Json(sl_fileNames.Where(f => (!f.Text.Contains("log")) && (!f.Text.Contains("wp"))), JsonRequestBehavior.AllowGet);
                }
            }
            return Json(emptyList);
        }

        #endregion

        public ActionResult Read([DataSourceRequest] DataSourceRequest request, string Logfile, string Produktserie)
        {
            //ViewBag.Produktserie = Produktserie;
            //ViewBag.Logfile = Logfile;
            DataTable dt_Logdaten = CSVtoDatatable(Logfile, Produktserie);
            dt_Logdaten = ConvertDataTable(dt_Logdaten, Produktserie);

            if (request.Aggregates.Any())
            {
                request.Aggregates.Each(agg =>
                {
                    agg.Aggregates.Each(a =>
                    {
                        a.MemberType = dt_Logdaten.Columns[agg.Member].DataType;
                    });
                });
            }

            return Json(dt_Logdaten.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Methoden zum einlesen des Datatables

        private DataTable CSVtoDatatable(string logfilename, string produktserie)
        {
            int emptycolumns = 0;                                   //Variable, die mitzählt, wie viele "empty" Spalten es gibt
            bool maxlength = false;                                 //Variable, die sicherstellt, dass nur 24h eingelesen werden (oft ist mehr in einem Logfile gespeichert)
            bool firstrow = true;                                   //Variable, die die erste Zeile (Überschriften) aus dem Datatabel filtert
            int rowsperday = 0;                                     //Variable die mitzählt, wie viele Reihen pro Tag eingelsen wurden (es sind manchmal mehr als 24 Stunden in einem File gespeichert)
            string strLine = "";                                         //String in dem die gelesene Zeile gespeichert wird
            string[] strArray;                                      //String Array in dem die einzelnen Werte der gelesenen Zeile gespeichert werden
            DataTable dt = new DataTable();                         //Datatable in dem die Werte gespeichert werden
            DataRow dr = dt.NewRow();                               //Reihe im Datatable
            string path = Path.Combine(Environment.GetEnvironmentVariable("Temp"), @"Files"+Session["sid"]+"\\" + logfilename);
            FileStream aFile = new FileStream(path, FileMode.Open);              //File welches ausgewählt wurde wird "geöffnet"
            using (StreamReader sr = new StreamReader(aFile, System.Text.Encoding.Default))         //File wird gelesen
            {
                strLine = sr.ReadLine();                              //Erste Zeile lesen (Überschriften)
                strLine = strLine.Replace("\x00", string.Empty);
                strLine = strLine.Replace("\x20", "_");
                strLine = strLine.Replace(".", string.Empty);
                strLine = strLine.Replace("-", "_");
                strLine = strLine.Replace("/", "_");
                strLine = strLine.Replace("+","$");
                strArray = strLine.Split(';');                        //Werte aus Zeile in ein Array aufteilen

                foreach (string value in strArray)                    //Für jeden Wert im Array eine Spalte erstellen
                {
                    if (value == "empty")                             //Wenn Spalte "empty heißt"
                    {
                        emptycolumns++;                               //emptycolumns um 1 erhöhen um die Werte in diesen Reihen später nicht hinzuzufügen
                    }
                    else
                    {
                        dt.Columns.Add(value.Trim(), typeof(string)); //spalte hinzufügen
                    }
                }

            }
            int columns = dt.Columns.Count;
            dt.Columns.Add("Datum", typeof(string)).SetOrdinal(0);          //Spalte für Datum an 0ter Stelle einfügen

            aFile = new FileStream(path, FileMode.Open);         //File welches ausgewählt wurde wird "geöffnet"
            using (StreamReader sr = new StreamReader(aFile, System.Text.Encoding.Default))         //File wird gelesen
            {
                while (sr.Peek() > -1 && maxlength == false)        //While Schleife -> Läuft so lange wie noch Zeichen zu lesen vorhanden sind UND die Maximallänge (24 Stunden) nicht überschritten wurde
                {
                    rowsperday++;
                    strLine = DateFromLogfile(logfilename, produktserie) + ";" + sr.ReadLine();  //Datum + gelesene Zeile als String speichern
                    strLine = strLine.Replace("\x00", string.Empty);        //NUL aus Therminator Logfiles löschen
                    strArray = strLine.Split(';');                  //Werte aus Zeile in ein Array aufteilen                       
                    if (firstrow == true)                           //Sollte es die erste Zeile sein (Überschriften) wird diese nicht hinzugefügt
                    {
                        firstrow = false;                           //Firstrow auf false setzen
                    }
                    else
                    {
                        strArray = strArray.Take(strArray.Length - emptycolumns - 1).ToArray();      //Alle Werte aus leeren Spalten "empty" aus den eingelesenen array löschen (-1 da zuvor Datum auch hinzugefügt wurde und somit Array um 1 Größer ist)
                        dt.Rows.Add(strArray);                                                       //sonst wird Reihe zu Datatable hinzugefügt
                    }

                    if (produktserie == "EcoTop | EcoTopZero | Pellet Elegance | OctoPlus | Maximus | Regelzentrale" || produktserie == "VampAir K08/K10" || produktserie == "VampAir K12/K15")
                    {
                        if (rowsperday == 1440)                         //In den *.xls Dateien (alle Logdaten außer Therminator) sind nach 23:59 noch weitere irrelevante Werte gespeichert, diese werden mit dieser Abfrage weggeschnitten
                        {
                            maxlength = true;
                        }
                    }
                }
                rowsperday = 0;                                         //Werte wieder zurücksetzen 
                maxlength = false;
                firstrow = true;
            }

            return dt;
        }

        private DataTable ConvertDataTable(DataTable datatable, string produktserie)
        {
            #region Allgemeine Konvertierungen
            int columns = datatable.Columns.Count;              //Variable mit Spalten des Datatables
            int rows = datatable.Rows.Count;                    //Variable mit Zeilen des Datatables

            for (int i = 0; i < columns; i++)
            {
                datatable.Columns[i].ColumnName = datatable.Columns[i].ColumnName.Replace("\0", string.Empty);  //Löscht alle Leerzeichen in den Spaltennamen (entstehen durch Leerzeichen in CSV)
            }

            for (int i = 0; i < columns; i++)                                                           //Durchläuft alle Spalten im Datatable
            {
                if (datatable.Columns[i].ColumnName == "Column1")                              //Gibt es eine Spalte mit dem gesuchten Namen?
                {
                    datatable.Columns.RemoveAt(i);
                }
            }

            if (produktserie.Contains("Therminator"))
            {
                //Removing empty rows from new therminator log
                for (int j = rows - 1; j >= 0; j--)
                {
                    if (datatable.Rows[j][1] == DBNull.Value)
                    {
                        datatable.Rows.RemoveAt(j);
                    }
                }
            }
            
            
            columns = datatable.Columns.Count;
            rows = datatable.Rows.Count;
            #endregion

            #region EcoTop | EcoTopZero | Pellet Elegance | OctoPlus | Maximus | Regelzentrale

            if (produktserie.Contains("EcoTop | EcoTopZero | Pellet Elegance | OctoPlus | Maximus | Regelzentrale"))
            {
                //Kesseltemperatur X31
                for (int i = 0; i < columns; i++)                                                           //Durchläuft alle Spalten im Datatable
                {
                    if (datatable.Columns[i].ColumnName == "Kesseltemperatur_X31")                              //Gibt es eine Spalte mit dem gesuchten Namen?
                    {
                        for (int x = 0; x < rows; x++)
                        {
                            datatable.Rows[x][i] = Convert.ToDouble(datatable.Rows[x][i]) / 10;             //Werte auf richtige Einheit bringen
                        }
                    }
                }
                //Rauchgastemperatur X34
                for (int i = 0; i < columns; i++)                                                           //Durchläuft alle Spalten im Datatable
                {
                    if (datatable.Columns[i].ColumnName == "Rauchgastemperatur_X34")                              //Gibt es eine Spalte mit dem gesuchten Namen?
                    {
                        //[°C]                                                                                      //Einheit zu Überschrift hinzufügen
                        for (int x = 0; x < rows; x++)
                        {
                            datatable.Rows[x][i] = Convert.ToDouble(datatable.Rows[x][i]) / 10;             //Werte auf richtige Einheit bringen
                        }
                    }       
                }
                //Einschubtemperatur X33
                for (int i = 0; i < columns; i++)                                                           //Durchläuft alle Spalten im Datatable
                {
                    if (datatable.Columns[i].ColumnName == "Einschubtemperatur_X33")                              //Gibt es eine Spalte mit dem gesuchten Namen?
                    {
                        //[°C]
                        for (int x = 0; x < rows; x++)
                        {
                            datatable.Rows[x][i] = Convert.ToDouble(datatable.Rows[x][i]) / 10;             //Werte auf richtige Einheit bringen
                        }
                        
                    }
                }
                //Luftzahl
                for (int i = 0; i < columns; i++)                                                           //Durchläuft alle Spalten im Datatable
                {
                    if (datatable.Columns[i].ColumnName == "Luftzahl")                              //Gibt es eine Spalte mit dem gesuchten Namen?
                    {
                        for (int x = 0; x < rows; x++)
                        {
                            datatable.Rows[x][i] = Convert.ToDouble(datatable.Rows[x][i]) / 100;             //Werte auf richtige Einheit bringen
                        }
                    }
                }
                //FWM-Austrittstemp.
                for (int i = 0; i < columns; i++)                                                           //Durchläuft alle Spalten im Datatable
                {
                    if (datatable.Columns[i].ColumnName == "FWM_Austrittstemp")                              //Gibt es eine Spalte mit dem gesuchten Namen?
                    {
                        //[°C]
                        for (int x = 0; x < rows; x++)
                        {
                            datatable.Rows[x][i] = Convert.ToDouble(datatable.Rows[x][i]) / 10;             //Werte auf richtige Einheit bringen
                        }
                    }
                }
                //Einschublaufzeit SGA
                for (int i = 0; i < columns; i++)                                                           //Durchläuft alle Spalten im Datatable
                {
                    if (datatable.Columns[i].ColumnName == "Einschublaufzeit_SGA")                              //Gibt es eine Spalte mit dem gesuchten Namen?
                    {
                         //[min]
                        for (int x = 0; x < rows; x++)
                        {
                            datatable.Rows[x][i] = datatable.Rows[x][i];             //Werte auf richtige Einheit bringen
                        }
                    }
                }
                //Statuszeile
                for (int i = 0; i < columns; i++)                                                           //Durchläuft alle Spalten im Datatable
                {
                    if (datatable.Columns[i].ColumnName == "Statuszeile")                               //Gibt es eine Spalte mit dem gesuchten Namen?
                    {
                        for(int x=0; x < rows; x++)
                        {
                            datatable.Rows[x][i] = Convert.ToDouble(datatable.Rows[x][i]);
                        }
                        int status;
                        for (int x = 0; x < rows; x++)
                        {
                            status = Convert.ToInt32(datatable.Rows[x][i]);
                            switch (status)
                            {
                                case 0:
                                    datatable.Rows[x][i] = "Bereitschaft";
                                    break;
                                case 1:
                                    datatable.Rows[x][i] = "Zündphase";
                                    break;
                                case 2:
                                    datatable.Rows[x][i] = "Pelletsbetrieb";
                                    break;
                                case 3:
                                    datatable.Rows[x][i] = "Kesselsolltemperatur erreicht, Nachlauf";
                                    break;
                                case 4:
                                    datatable.Rows[x][i] = "Nachlauf";
                                    break;
                                case 5:
                                    datatable.Rows[x][i] = "Keine Anforderung, Nachlauf";
                                    break;
                                case 6:
                                    datatable.Rows[x][i] = "Brenner ausgeschaltet";
                                    break;
                                case 7:
                                    datatable.Rows[x][i] = "Pelletsvorratsbehälter ist leer, Nachlauf";
                                    break;
                                case 8:
                                    datatable.Rows[x][i] = "Wärmetauscherbereinigung ist aktiv";
                                    break;
                                case 9:
                                    datatable.Rows[x][i] = "Wärmetauscherbereinigung benötigt, Nachlauf";
                                    break;
                                case 10:
                                    datatable.Rows[x][i] = "Stromausfall, Nachlauf";
                                    break;
                                case 11:
                                    datatable.Rows[x][i] = "Zweiter Zündversuch, Nachlauf";
                                    break;
                                case 12:
                                    datatable.Rows[x][i] = "Füllraumtemperatur überschritten, Nachlauf";
                                    break;
                                case 13:
                                    datatable.Rows[x][i] = "Einschub überlastet, Nachlauf";
                                    break;
                                case 14:
                                    datatable.Rows[x][i] = "Fremdkessel Aktiv, Nachlauf";
                                    break;
                                case 15:
                                    datatable.Rows[x][i] = "Fremdkessel Aktiv, Bereitschaft";
                                    break;
                                case 16:
                                    datatable.Rows[x][i] = "Brenner ausgeschaltet, Nachlauf";
                                    break;
                                case 17:
                                    datatable.Rows[x][i] = "Sicherheitskette ist offen";
                                    break;
                                case 18:
                                    datatable.Rows[x][i] = "Sicherheitskette ist offen, Nachlauf";
                                    break;
                                case 19:
                                    datatable.Rows[x][i] = "Restsauerstoffgehalt zu hoch, Nachlauf";
                                    break;
                                case 20:
                                    datatable.Rows[x][i] = "Abgastemperatur zu gering, Nachlauf";
                                    break;
                                case 21:
                                    datatable.Rows[x][i] = "Abgastemperatur zu hoch, Bereitschaft";
                                    break;
                                case 22:
                                    datatable.Rows[x][i] = "Abgasfühler ist defekt, Bereitschaft";
                                    break;
                                case 23:
                                    datatable.Rows[x][i] = "Einschubtemperatur zu hoch, Bereitschaft";
                                    break;
                                case 24:
                                    datatable.Rows[x][i] = "Lambdasonde ist defekt, Nachlauf";
                                    break;
                                case 25:
                                    datatable.Rows[x][i] = "Kesseltemperatur ist ausreichend, Bereitschaft";
                                    break;
                                case 26:
                                    datatable.Rows[x][i] = "Kein Stromfluss Einschubmotor, Nachlauf";
                                    break;
                                case 27:
                                    datatable.Rows[x][i] = "Kesselfühler ist defekt, Bereitschaft";
                                    break;
                                case 28:
                                    datatable.Rows[x][i] = "Kesselfühler ist defekt, Nachlauf";
                                    break;
                                case 29:
                                    datatable.Rows[x][i] = "Lambdasonde wird beheizt";
                                    break;
                                case 30:
                                    datatable.Rows[x][i] = "Wärmetauscherreinigung wartet auf eine Freigabe, Zeit";
                                    break;
                                case 31:
                                    datatable.Rows[x][i] = "Wärmetauscherreinigung wartet auf eine Freigabe, AGT";
                                    break;
                                case 32:
                                    datatable.Rows[x][i] = "Wärmetauscherreinigung wartet auf eine Freigabe, KT";
                                    break;
                                case 33:
                                    datatable.Rows[x][i] = "Saugaustragung wartet auf eine Freigabezeit";
                                    break;
                                case 34:
                                    datatable.Rows[x][i] = "Pelletsvorratsbehälter wird befüllt";
                                    break;
                                case 35:
                                    datatable.Rows[x][i] = "Einschubfühlerkurzschluss, Bereitschaft";
                                    break;
                                case 36:
                                    datatable.Rows[x][i] = "Rückbrandschieber öffnet";
                                    break;
                                case 37:
                                    datatable.Rows[x][i] = "Kessel wird befüllt";
                                    break;
                                case 38:
                                    datatable.Rows[x][i] = "Lambdasonde wird kalibriert";
                                    break;
                                case 39:
                                    datatable.Rows[x][i] = "Kaminkehrer Messfreigabe";
                                    break;
                                case 40:
                                    datatable.Rows[x][i] = "Alarm aktiv";
                                    break;
                                case 41:
                                    datatable.Rows[x][i] = "Kesseltür ist offen, Bereitschaft";
                                    break;
                                case 42:
                                    datatable.Rows[x][i] = "Tür war zu lange offen, Nachlauf";
                                    break;
                                case 43:
                                    datatable.Rows[x][i] = "Keine Brennerfreigabe";
                                    break;
                                case 44:
                                    datatable.Rows[x][i] = "Keine Brennerzeitfreigabe, Nachlauf";
                                    break;
                                case 45:
                                    datatable.Rows[x][i] = "Luftzahl Zündphase zu tief, Nachlauf";
                                    break;
                                case 46:
                                    datatable.Rows[x][i] = "Ascheaustragung aktiv";
                                    break;
                                case 47:
                                    datatable.Rows[x][i] = "Kesseltür/Aschebox ist offen, Bereitschaft";
                                    break;
                                case 48:
                                    datatable.Rows[x][i] = "Kesseltür/Aschebox wurde geöffnet, Nachlauf";
                                    break;
                                case 49:
                                    datatable.Rows[x][i] = "Kessel aktiv";
                                    break;
                                case 50:
                                    datatable.Rows[x][i] = "Saugaustragung wartet auf Umschalteinheit";
                                    break;
                                case 51:
                                    datatable.Rows[x][i] = "Brenner ausgeschaltet, Mindestkessellaufzeit aktiv";
                                    break;
                                case 52:
                                    datatable.Rows[x][i] = "Kaminkehrermessung beendet, Nachlauf";
                                    break;
                                case 53:
                                    datatable.Rows[x][i] = "Warten bis die Raumluftklappe offen ist ";
                                    break;
                                case 54:
                                    datatable.Rows[x][i] = "Stromausfall, Bereitschaft";
                                    break;
                                case 55:
                                    datatable.Rows[x][i] = "Stromausfall, Nachlauf";
                                    break;
                                case 56:
                                    datatable.Rows[x][i] = "WT-Spülung Brennwertmodul aktiv";
                                    break;
                                case 57:
                                    datatable.Rows[x][i] = "Differenzdruckschalter hat ausgelöst, Nachlauf";
                                    break;
                                case 58:
                                    datatable.Rows[x][i] = "Warten auf Freigabe von Differenzdruckschalter";
                                    break;
                                case 59:
                                    datatable.Rows[x][i] = "WT-Spülung Brennwertmodul wartet auf eine Freigabe";
                                    break;
                                case 60:
                                    datatable.Rows[x][i] = "Referenzierung elektro. Staubabscheider";
                                    break;
                                case 61:
                                    datatable.Rows[x][i] = "Elektro. Staubabscheider wartet auf Freigabe";
                                    break;
                                case 62:
                                    datatable.Rows[x][i] = "Behälterbefüllung in einigen Minuten";
                                    break;
                                case 63:
                                    datatable.Rows[x][i] = "Ascheaustragung benötigt, Nachlauf";
                                    break;
                                case 64:
                                    datatable.Rows[x][i] = "Ascheaustragung wartet auf eine Freigabe, Zeit";
                                    break;
                                case 65:
                                    datatable.Rows[x][i] = "Ascheaustragung aktiv";
                                    break;
                                case 66:
                                    datatable.Rows[x][i] = "Ascheaustragung aktiv";
                                    break;
                                case 67:
                                    datatable.Rows[x][i] = "Hackgutbetrieb";
                                    break;
                                case 68:
                                    datatable.Rows[x][i] = "Anlagendruck zu tief/hoch, Bereitschaft";
                                    break;
                                case 69:
                                    datatable.Rows[x][i] = "Anlagendruck zu tief/hoch, Nachlauf";
                                    break;
                                case 70:
                                    datatable.Rows[x][i] = "Brennraumtemperatur zu hoch, Bereitschaft";
                                    break;
                                case 71:
                                    datatable.Rows[x][i] = "Einschub wird entleert";
                                    break;
                                case 72:
                                    datatable.Rows[x][i] = "Raumaustragung Sicherheitskette offen";
                                    break;
                                case 73:
                                    datatable.Rows[x][i] = "Brennraumfühler ist defekt, Bereitschaft";
                                    break;
                                case 74:
                                    datatable.Rows[x][i] = "Keine Brennerfreigabe, Kaminkehrer";
                                    break;
                                case 75:
                                    datatable.Rows[x][i] = "Kaminkehrer-Programm benötigt Abschaltung, Nachlauf";
                                    break;
                                case 76:
                                    datatable.Rows[x][i] = "Sicherheitskette ist offen";
                                    break;
                                case 77:
                                    datatable.Rows[x][i] = "Pelletsbetrieb, Nachtabsenkung";
                                    break;
                                case 78:
                                    datatable.Rows[x][i] = "Hackgutbetrieb, Nachtabsenkung";
                                    break;
                                case 79:
                                    datatable.Rows[x][i] = "Aschebox offen";
                                    break;
                                case 80:
                                    datatable.Rows[x][i] = "Aschebox offen";
                                    break;
                                case 81:
                                    datatable.Rows[x][i] = "Aschebox offen, Nachlauf";
                                    break;
                                case 82:
                                    datatable.Rows[x][i] = "Referenzierung Luftklappen";
                                    break;
                            }
                        }
                    }
                }
                //RLA Mischerposition
                for (int i = 0; i < columns; i++)                                                           //Durchläuft alle Spalten im Datatable
                {
                    if (datatable.Columns[i].ColumnName == "RLA_Mischerposition")                              //Gibt es eine Spalte mit dem gesuchten Namen?
                    {
                        //[%]
                        for (int x = 0; x < rows; x++)
                        {
                            datatable.Rows[x][i] = Convert.ToDouble(datatable.Rows[x][i]);             //Werte auf richtige Einheit bringen
                        }
                    }
                }
                //SZG Istdrehzahl
                for (int i = 0; i < columns; i++)                                                           //Durchläuft alle Spalten im Datatable
                {
                    if (datatable.Columns[i].ColumnName == "SZG_Istdrehzahl")                              //Gibt es eine Spalte mit dem gesuchten Namen?
                    {
                        //[%]
                        for (int x = 0; x < rows; x++)
                        {
                            datatable.Rows[x][i] = Convert.ToDouble(datatable.Rows[x][i]) / 10;             //Werte auf richtige Einheit bringen
                        }
                    }
                }
                //Ruecklauftemperatur X32
                for (int i = 0; i < columns; i++)                                                           //Durchläuft alle Spalten im Datatable
                {
                    if (datatable.Columns[i].ColumnName == "Ruecklauftemperatur_X32")                              //Gibt es eine Spalte mit dem gesuchten Namen?
                    {
                        //[°C]
                        for (int x = 0; x < rows; x++)
                        {
                            datatable.Rows[x][i] = Convert.ToDouble(datatable.Rows[x][i]) / 10;             //Werte auf richtige Einheit bringen
                        }
                    }
                }
                //Einschubimpuls LDZ 
                for (int i = 0; i < columns; i++)                                                           //Durchläuft alle Spalten im Datatable
                {
                    if (datatable.Columns[i].ColumnName == "Einschubimpuls_LDZ")                              //Gibt es eine Spalte mit dem gesuchten Namen?
                    {
                        for (int x = 0; x < rows; x++)
                        {
                            datatable.Rows[x][i] = Convert.ToDouble(datatable.Rows[x][i]) / 10;             //Werte auf richtige Einheit bringen
                        }
                    }
                }
                //Aussentemperatur X42
                for (int i = 0; i < columns; i++)                                                           //Durchläuft alle Spalten im Datatable
                {
                    if (datatable.Columns[i].ColumnName == "Aussentemperatur_X42")                              //Gibt es eine Spalte mit dem gesuchten Namen?
                    {
                        for (int x = 0; x < rows; x++)
                        {
                            datatable.Rows[x][i] = Convert.ToDouble(datatable.Rows[x][i]) / 10;             //Werte auf richtige Einheit bringen
                        }
                    }
                }
                //PufferOben
                for (int i = 0; i < columns; i++)                                                           //Durchläuft alle Spalten im Datatable
                {
                    if (datatable.Columns[i].ColumnName == "PufferOben")                              //Gibt es eine Spalte mit dem gesuchten Namen?
                    {
                        for (int x = 0; x < rows; x++)
                        {
                            datatable.Rows[x][i] = Convert.ToDouble(datatable.Rows[x][i]) / 10;             //Werte auf richtige Einheit bringen
                        }
                    }
                }
                //PufferUnten
                for (int i = 0; i < columns; i++)                                                           //Durchläuft alle Spalten im Datatable
                {
                    if (datatable.Columns[i].ColumnName == "PufferUnten")                              //Gibt es eine Spalte mit dem gesuchten Namen?
                    {
                        for (int x = 0; x < rows; x++)
                        {
                            datatable.Rows[x][i] = Convert.ToDouble(datatable.Rows[x][i]) / 10;             //Werte auf richtige Einheit bringen
                        }
                    }
                }
                //TWS_Temperatur
                for (int i = 0; i < columns; i++)                                                           //Durchläuft alle Spalten im Datatable
                {
                    if (datatable.Columns[i].ColumnName == "TWS_Temperatur")                              //Gibt es eine Spalte mit dem gesuchten Namen?
                    {
                        for (int x = 0; x < rows; x++)
                        {
                            datatable.Rows[x][i] = Convert.ToDouble(datatable.Rows[x][i]) / 10;             //Werte auf richtige Einheit bringen
                        }
                    }
                }
                //Wetter_Prognose
                for (int i = 0; i < columns; i++)                                                           //Durchläuft alle Spalten im Datatable
                {
                    if (datatable.Columns[i].ColumnName == "Wetter_Prognose")                               //Gibt es eine Spalte mit dem gesuchten Namen?
                    {
                        for (int x = 0; x < rows; x++)
                        {
                            datatable.Rows[x][i] = Convert.ToDouble(datatable.Rows[x][i]) / 10;             //Werte auf richtige Einheit bringen
                        }
                    }
                }
                //Vlt_1
                for (int i = 0; i < columns; i++)                                                           //Durchläuft alle Spalten im Datatable
                {
                    if (datatable.Columns[i].ColumnName == "Vlt_1")                              //Gibt es eine Spalte mit dem gesuchten Namen?
                    {
                        for (int x = 0; x < rows; x++)
                        {
                            datatable.Rows[x][i] = Convert.ToDouble(datatable.Rows[x][i]) / 10;             //Werte auf richtige Einheit bringen
                        }
                    }
                }
                //Vlst_1
                for (int i = 0; i < columns; i++)                                                           //Durchläuft alle Spalten im Datatable
                {
                    if (datatable.Columns[i].ColumnName == "Vlst_1")                              //Gibt es eine Spalte mit dem gesuchten Namen?
                    {
                        // datatable.Columns[i].ColumnName = datatable.Columns[i].ColumnName + " [°C]";        //Einheit zu Überschrift hinzufügen
                        for (int x = 0; x < rows; x++)
                        {
                            datatable.Rows[x][i] = Convert.ToDouble(datatable.Rows[x][i]) / 10;             //Werte auf richtige Einheit bringen
                        }
                    }
                }
                //RT_1
                for (int i = 0; i < columns; i++)                                                           //Durchläuft alle Spalten im Datatable
                {
                    if (datatable.Columns[i].ColumnName == "RT_1")                              //Gibt es eine Spalte mit dem gesuchten Namen?
                    {
                        // datatable.Columns[i].ColumnName = datatable.Columns[i].ColumnName + " [°C]";        //Einheit zu Überschrift hinzufügen
                        for (int x = 0; x < rows; x++)
                        {
                            datatable.Rows[x][i] = Convert.ToDouble(datatable.Rows[x][i]) / 10;             //Werte auf richtige Einheit bringen
                        }
                    }
                }
                //HK_P_1
                for (int i = 0; i < columns; i++)                                                           //Durchläuft alle Spalten im Datatable
                {
                    if (datatable.Columns[i].ColumnName == "HK_P_1")                              //Gibt es eine Spalte mit dem gesuchten Namen?
                    {
                        //  datatable.Columns[i].ColumnName = datatable.Columns[i].ColumnName + " [°C]";        //Einheit zu Überschrift hinzufügen
                        for (int x = 0; x < rows; x++)
                        {
                            datatable.Rows[x][i] = Convert.ToInt32(datatable.Rows[x][i]);             //Werte auf richtige Einheit bringen
                        }
                    }
                }
                //HK_M_1
                for (int i = 0; i < columns; i++)                                                           //Durchläuft alle Spalten im Datatable
                {
                    if (datatable.Columns[i].ColumnName == "HK_M_1")                              //Gibt es eine Spalte mit dem gesuchten Namen?
                    {
                        //  datatable.Columns[i].ColumnName = datatable.Columns[i].ColumnName + " [%]";        //Einheit zu Überschrift hinzufügen
                        for (int x = 0; x < rows; x++)
                        {
                            datatable.Rows[x][i] = Convert.ToDouble(datatable.Rows[x][i]);             //Werte auf richtige Einheit bringen
                        }
                    }
                }
                //Vlt_2
                for (int i = 0; i < columns; i++)                                                           //Durchläuft alle Spalten im Datatable
                {
                    if (datatable.Columns[i].ColumnName == "Vlt_2")                              //Gibt es eine Spalte mit dem gesuchten Namen?
                    {
                        //  datatable.Columns[i].ColumnName = datatable.Columns[i].ColumnName + " [°C]";        //Einheit zu Überschrift hinzufügen
                        for (int x = 0; x < rows; x++)
                        {
                            datatable.Rows[x][i] = Convert.ToDouble(datatable.Rows[x][i]) / 10;             //Werte auf richtige Einheit bringen
                        }
                    }
                }
                //Vlst_2
                for (int i = 0; i < columns; i++)                                                           //Durchläuft alle Spalten im Datatable
                {
                    if (datatable.Columns[i].ColumnName == "Vlst_2")                              //Gibt es eine Spalte mit dem gesuchten Namen?
                    {
                        // datatable.Columns[i].ColumnName = datatable.Columns[i].ColumnName + " [°C]";        //Einheit zu Überschrift hinzufügen
                        for (int x = 0; x < rows; x++)
                        {
                            datatable.Rows[x][i] = Convert.ToDouble(datatable.Rows[x][i]) / 10;             //Werte auf richtige Einheit bringen
                        }
                    }
                }
                //RT_2
                for (int i = 0; i < columns; i++)                                                           //Durchläuft alle Spalten im Datatable
                {
                    if (datatable.Columns[i].ColumnName == "RT_2")                              //Gibt es eine Spalte mit dem gesuchten Namen?
                    {
                        // datatable.Columns[i].ColumnName = datatable.Columns[i].ColumnName + " [°C]";        //Einheit zu Überschrift hinzufügen
                        for (int x = 0; x < rows; x++)
                        {
                            datatable.Rows[x][i] = Convert.ToDouble(datatable.Rows[x][i]) / 100;             //Werte auf richtige Einheit bringen
                        }
                    }
                }
                //HK_M_2
                for (int i = 0; i < columns; i++)                                                           //Durchläuft alle Spalten im Datatable
                {
                    if (datatable.Columns[i].ColumnName == "HK_M_2")                              //Gibt es eine Spalte mit dem gesuchten Namen?
                    {
                        //  datatable.Columns[i].ColumnName = datatable.Columns[i].ColumnName + " [%]";        //Einheit zu Überschrift hinzufügen
                    }
                }
                //SpeichertempMitte X31
                for (int i = 0; i < columns; i++)                                                           //Durchläuft alle Spalten im Datatable
                {
                    if (datatable.Columns[i].ColumnName == "SpeichertempMitte_X31")                              //Gibt es eine Spalte mit dem gesuchten Namen?
                    {
                        //datatable.Columns[i].ColumnName = datatable.Columns[i].ColumnName + " [°C]";        //Einheit zu Überschrift hinzufügen
                        for (int x = 0; x < rows; x++)
                        {
                            datatable.Rows[x][i] = Convert.ToDouble(datatable.Rows[x][i]) / 10;             //Werte auf richtige Einheit bringen
                        }
                    }
                }
                //FWM-Eintrittstemp.
                for (int i = 0; i < columns; i++)                                                           //Durchläuft alle Spalten im Datatable
                {
                    if (datatable.Columns[i].ColumnName == "FWM_Eintrittstemp")                              //Gibt es eine Spalte mit dem gesuchten Namen?
                    {
                        // datatable.Columns[i].ColumnName = datatable.Columns[i].ColumnName + " [°C]";        //Einheit zu Überschrift hinzufügen
                        for (int x = 0; x < rows; x++)
                        {
                            datatable.Rows[x][i] = Convert.ToDouble(datatable.Rows[x][i]) / 10;             //Werte auf richtige Einheit bringen
                        }
                    }
                }
                //SpeichertempOben X30
                for (int i = 0; i < columns; i++)                                                           //Durchläuft alle Spalten im Datatable
                {
                    if (datatable.Columns[i].ColumnName == "SpeichertempOben_X30")                              //Gibt es eine Spalte mit dem gesuchten Namen?
                    {
                        // datatable.Columns[i].ColumnName = datatable.Columns[i].ColumnName + " [°C]";        //Einheit zu Überschrift hinzufügen
                        for (int x = 0; x < rows; x++)
                        {
                            datatable.Rows[x][i] = Convert.ToDouble(datatable.Rows[x][i]) / 10;             //Werte auf richtige Einheit bringen
                        }
                    }
                }
                //SpeichertempUnten X32
                for (int i = 0; i < columns; i++)                                                           //Durchläuft alle Spalten im Datatable
                {
                    if (datatable.Columns[i].ColumnName == "SpeichertempUnten_X32")                              //Gibt es eine Spalte mit dem gesuchten Namen?
                    {
                        // datatable.Columns[i].ColumnName = datatable.Columns[i].ColumnName + " [°C]";        //Einheit zu Überschrift hinzufügen
                        for (int x = 0; x < rows; x++)
                        {
                            datatable.Rows[x][i] = Convert.ToDouble(datatable.Rows[x][i]) / 10;             //Werte auf richtige Einheit bringen
                        }
                    }
                }
                //Differenzdruck X61 
                for (int i = 0; i < columns; i++)                                                           //Durchläuft alle Spalten im Datatable
                {
                    if (datatable.Columns[i].ColumnName == "Differenzdruck_X61")                              //Gibt es eine Spalte mit dem gesuchten Namen?
                    {
                        //  datatable.Columns[i].ColumnName = datatable.Columns[i].ColumnName + " [mbar]";        //Einheit zu Überschrift hinzufügen
                        for (int x = 0; x < rows; x++)
                        {
                            datatable.Rows[x][i] = Convert.ToDouble(datatable.Rows[x][i]) / 100;             //Werte auf richtige Einheit bringen
                        }
                    }
                }
                //Filter Spannung 
                for (int i = 0; i < columns; i++)                                                           //Durchläuft alle Spalten im Datatable
                {
                    if (datatable.Columns[i].ColumnName == "Filter_Spannung")                              //Gibt es eine Spalte mit dem gesuchten Namen?
                    {
                        //   datatable.Columns[i].ColumnName = datatable.Columns[i].ColumnName + " [kV]";        //Einheit zu Überschrift hinzufügen
                        for (int x = 0; x < rows; x++)
                        {
                            datatable.Rows[x][i] = Convert.ToDouble(datatable.Rows[x][i]);             //Werte auf richtige Einheit bringen
                        }
                    }
                }
                //Filter Strom 
                for (int i = 0; i < columns; i++)                                                           //Durchläuft alle Spalten im Datatable
                {
                    if (datatable.Columns[i].ColumnName == "Filter_Strom")                              //Gibt es eine Spalte mit dem gesuchten Namen?
                    {
                        //   datatable.Columns[i].ColumnName = datatable.Columns[i].ColumnName + " [mA]";        //Einheit zu Überschrift hinzufügen
                        for (int x = 0; x < rows; x++)
                        {
                            datatable.Rows[x][i] = Convert.ToDouble(datatable.Rows[x][i]) / 100;             //Werte auf richtige Einheit bringen
                        }
                    }
                }
                //Brennraumtemperatur
                for (int i = 0; i < columns; i++)                                                           //Durchläuft alle Spalten im Datatable
                {
                    if (datatable.Columns[i].ColumnName == "Brennraumtemperatur")                              //Gibt es eine Spalte mit dem gesuchten Namen?
                    {
                        // datatable.Columns[i].ColumnName = datatable.Columns[i].ColumnName + " [°C]";        //Einheit zu Überschrift hinzufügen
                        for (int x = 0; x < rows; x++)
                        {
                            datatable.Rows[x][i] = Convert.ToDouble(datatable.Rows[x][i]) / 10;             //Werte auf richtige Einheit bringen
                        }
                    }
                }
                //Filter Spg 
                for (int i = 0; i < columns; i++)                                                           //Durchläuft alle Spalten im Datatable
                {
                    if (datatable.Columns[i].ColumnName == "Filter_Spg")                              //Gibt es eine Spalte mit dem gesuchten Namen?
                    {
                        //  datatable.Columns[i].ColumnName = datatable.Columns[i].ColumnName + " [kV]";        //Einheit zu Überschrift hinzufügen
                        for (int x = 0; x < rows; x++)
                        {
                            datatable.Rows[x][i] = Convert.ToDouble(datatable.Rows[x][i]) /10;             //Werte auf richtige Einheit bringen
                        }
                    }
                }
                //Puffer_X35
                for (int i = 0; i < columns; i++)                                                           //Durchläuft alle Spalten im Datatable
                {
                    if (datatable.Columns[i].ColumnName == "Puffer_X35")                              //Gibt es eine Spalte mit dem gesuchten Namen?
                    {
                        //  datatable.Columns[i].ColumnName = datatable.Columns[i].ColumnName + " [°C]";        //Einheit zu Überschrift hinzufügen
                        for (int x = 0; x < rows; x++)
                        {
                            datatable.Rows[x][i] = Convert.ToDouble(datatable.Rows[x][i]) / 100;             //Werte auf richtige Einheit bringen
                        }
                    }
                }
                //Primaerluftklappe
                for (int i = 0; i < columns; i++)                                                           //Durchläuft alle Spalten im Datatable
                {
                    if (datatable.Columns[i].ColumnName == "Primaerluftklappe")                              //Gibt es eine Spalte mit dem gesuchten Namen?
                    {
                        //  datatable.Columns[i].ColumnName = datatable.Columns[i].ColumnName + " [%]";        //Einheit zu Überschrift hinzufügen
                        for (int x = 0; x < rows; x++)
                        {
                            datatable.Rows[x][i] = Convert.ToDouble(datatable.Rows[x][i]) / 100;             //Werte auf richtige Einheit bringen
                        }
                    }
                }
                //Sekundaerluftklappe
                for (int i = 0; i < columns; i++)                                                           //Durchläuft alle Spalten im Datatable
                {
                    if (datatable.Columns[i].ColumnName == "Sekundaerluftklappe")                              //Gibt es eine Spalte mit dem gesuchten Namen?
                    {
                        for (int x = 0; x < rows; x++)
                        {
                            datatable.Rows[x][i] = Convert.ToDouble(datatable.Rows[x][i]) / 100;             //Werte auf richtige Einheit bringen
                        }
                    }
                }
                //Rezirkulationsluftklappe
                for (int i = 0; i < columns; i++)                                                           //Durchläuft alle Spalten im Datatable
                {
                    if (datatable.Columns[i].ColumnName == "Rezirkulationsluftklappe")                              //Gibt es eine Spalte mit dem gesuchten Namen?
                    {  
                        for (int x = 0; x < rows; x++)
                        {
                            datatable.Rows[x][i] = Convert.ToDouble(datatable.Rows[x][i]) / 100;             //Werte auf richtige Einheit bringen
                        }
                    }
                }
                //AGT-Anhebungsklappe
                for (int i = 0; i < columns; i++)                                                           //Durchläuft alle Spalten im Datatable
                {
                    if (datatable.Columns[i].ColumnName == "AGT_Anhebungsklappe")                              //Gibt es eine Spalte mit dem gesuchten Namen?
                    {
                        for (int x = 0; x < rows; x++)
                        {
                            datatable.Rows[x][i] = Convert.ToDouble(datatable.Rows[x][i]) / 100;             //Werte auf richtige Einheit bringen
                        }
                    }
                }
                //Vorlaufsolltemperatur Kessel
                for (int i = 0; i < columns; i++)                                                           //Durchläuft alle Spalten im Datatable
                {
                    if (datatable.Columns[i].ColumnName == "Vorlaufsolltemperatur_Kessel")                  //Gibt es eine Spalte mit dem gesuchten Namen?
                    {
                        for (int x = 0; x < rows; x++)
                        {
                            datatable.Rows[x][i] = Convert.ToDouble(datatable.Rows[x][i]) / 10;             //Werte auf richtige Einheit bringen
                        }
                    }
                }
                //Kesseltemperatur
                for (int i = 0; i < columns; i++)                                                           //Durchläuft alle Spalten im Datatable
                {
                    if (datatable.Columns[i].ColumnName == "Kesseltemperatur")                              //Gibt es eine Spalte mit dem gesuchten Namen?
                    {
                        for (int x = 0; x < rows; x++)
                        {
                            datatable.Rows[x][i] = Convert.ToDouble(datatable.Rows[x][i]) / 10;             //Werte auf richtige Einheit bringen
                        }
                    }
                }
                //Aussentemp.
                for (int i = 0; i < columns; i++)                                                           //Durchläuft alle Spalten im Datatable
                {
                    if (datatable.Columns[i].ColumnName == "Aussentemp")                              //Gibt es eine Spalte mit dem gesuchten Namen?
                    {
                        for (int x = 0; x < rows; x++)
                        {
                            datatable.Rows[x][i] = Convert.ToDouble(datatable.Rows[x][i]) / 10;             //Werte auf richtige Einheit bringen
                        }
                    }
                }
                //Leistung
                for (int i = 0; i < columns; i++)                                                           //Durchläuft alle Spalten im Datatable
                {
                    if (datatable.Columns[i].ColumnName == "Leistung")                              //Gibt es eine Spalte mit dem gesuchten Namen?
                    {
                        //  datatable.Columns[i].ColumnName = datatable.Columns[i].ColumnName + " [%]";        //Einheit zu Überschrift hinzufügen
                    }
                }
                //benoetigte Kesseltemperatur
                for (int i = 0; i < columns; i++)                                                           //Durchläuft alle Spalten im Datatable
                {
                    if (datatable.Columns[i].ColumnName == "benoetigte_Kesseltemperatur")                              //Gibt es eine Spalte mit dem gesuchten Namen?
                    {
                        //datatable.Columns[i].ColumnName = datatable.Columns[i].ColumnName + " [°C]";        //Einheit zu Überschrift hinzufügen
                        for (int x = 0; x < rows; x++)
                        {
                            datatable.Rows[x][i] = Convert.ToDouble(datatable.Rows[x][i]) / 10;             //Werte auf richtige Einheit bringen
                        }
                    }
                }
                //Puffertemp.Unten
                for (int i = 0; i < columns; i++)                                                           //Durchläuft alle Spalten im Datatable
                {
                    if (datatable.Columns[i].ColumnName == "PuffertempUnten")                              //Gibt es eine Spalte mit dem gesuchten Namen?
                    {
                        // datatable.Columns[i].ColumnName = datatable.Columns[i].ColumnName + " [°C]";        //Einheit zu Überschrift hinzufügen
                        for (int x = 0; x < rows; x++)
                        {
                            datatable.Rows[x][i] = Convert.ToDouble(datatable.Rows[x][i]) / 10;             //Werte auf richtige Einheit bringen
                        }
                    }
                }
                //Trinkwassersp. 1
                for (int i = 0; i < columns; i++)                                                           //Durchläuft alle Spalten im Datatable
                {
                    if (datatable.Columns[i].ColumnName == "Trinkwassersp_1")                              //Gibt es eine Spalte mit dem gesuchten Namen?
                    {
                        //  datatable.Columns[i].ColumnName = datatable.Columns[i].ColumnName + " [°C]";        //Einheit zu Überschrift hinzufügen
                        for (int x = 0; x < rows; x++)
                        {
                            datatable.Rows[x][i] = Convert.ToDouble(datatable.Rows[x][i]) / 10;             //Werte auf richtige Einheit bringen
                        }
                    }
                }
                //Fuehlereingang D1I3
                for (int i = 0; i < columns; i++)                                                           //Durchläuft alle Spalten im Datatable
                {
                    if (datatable.Columns[i].ColumnName == "Fuehlereingang_D1I3")                              //Gibt es eine Spalte mit dem gesuchten Namen?
                    {
                        for (int x = 0; x < rows; x++)
                        {
                            datatable.Rows[x][i] = Convert.ToDouble(datatable.Rows[x][i]);             //Werte auf richtige Einheit bringen
                        }
                    }
                }
                //Puffertemp.Oben
                for (int i = 0; i < columns; i++)                                                           //Durchläuft alle Spalten im Datatable
                {
                    if (datatable.Columns[i].ColumnName == "PuffertempOben")                              //Gibt es eine Spalte mit dem gesuchten Namen?
                    {
                        //  datatable.Columns[i].ColumnName = datatable.Columns[i].ColumnName + " [°C]";        //Einheit zu Überschrift hinzufügen
                        for (int x = 0; x < rows; x++)
                        {
                            datatable.Rows[x][i] = Convert.ToDouble(datatable.Rows[x][i]) / 10;             //Werte auf richtige Einheit bringen
                        }
                    }
                }
                //HK_P_2
                for (int i = 0; i < columns; i++)                                                           //Durchläuft alle Spalten im Datatable
                {
                    if (datatable.Columns[i].ColumnName == "HK_P_2")                              //Gibt es eine Spalte mit dem gesuchten Namen?
                    {
                        //  datatable.Columns[i].ColumnName = datatable.Columns[i].ColumnName + " [°C]";        //Einheit zu Überschrift hinzufügen
                        for (int x = 0; x < rows; x++)
                        {
                            datatable.Rows[x][i] = Convert.ToInt32(datatable.Rows[x][i]) / 10;             //Werte auf richtige Einheit bringen
                        }
                    }
                }
                //Diff.Modulausgang1
                for (int i = 0; i < columns; i++)                                                           //Durchläuft alle Spalten im Datatable
                {
                    if (datatable.Columns[i].ColumnName == "Diff_Modulausgang1")                              //Gibt es eine Spalte mit dem gesuchten Namen?
                    {
                        //  datatable.Columns[i].ColumnName = datatable.Columns[i].ColumnName + " [°C]";        //Einheit zu Überschrift hinzufügen
                        for (int x = 0; x < rows; x++)
                        {
                            datatable.Rows[x][i] = Convert.ToDouble(datatable.Rows[x][i]);             //Werte auf richtige Einheit bringen
                        }
                    }
                }
                //Vorlauftemperatur 1 
                for (int i = 0; i < columns; i++)                                                           //Durchläuft alle Spalten im Datatable
                {
                    if (datatable.Columns[i].ColumnName == "Vorlauftemperatur_1")                              //Gibt es eine Spalte mit dem gesuchten Namen?
                    {
                        //  datatable.Columns[i].ColumnName = datatable.Columns[i].ColumnName + " [°C]";        //Einheit zu Überschrift hinzufügen
                        for (int x = 0; x < rows; x++)
                        {
                            datatable.Rows[x][i] = Convert.ToDouble(datatable.Rows[x][i]);             //Werte auf richtige Einheit bringen
                        }
                    }
                }
            }
            #endregion EcoTop | EcoTopZero | Pellet Elegance | OctoPlus | Maximus | Regelzentrale

            #region VampAir K08/K10 & K12/K15
            if (produktserie.Contains("VampAir"))
            {
                //Spalten für Überhitzung Verdampfer und Überhitzung Kompressor hinzufügen (werden erst später befüllt)
                datatable.Columns.Add("Überhitzung_Verdampfer");
                datatable.Columns.Add("Überhitzung_Kompressor");

                #region Zusammengefügte Werte aufteilen und in einzelne Spalten schreiben
               
                //Statuszeile WP + Betriebszustand Luefter
                if (!datatable.Columns.Contains("Statuszeile") || !datatable.Columns.Contains("Betriebszustand_Luefter"))
                {
                    int index = 0;
                    datatable.Columns.Add("Statuszeile_WP");                                                //Neue Spalte mit 1. Teil der alten Spalte erstellen
                    datatable.Columns.Add("Betriebszustand_Luefter");                                       //Neue Spalte mit 2. Teil der alten Spalte erstellen
                                                                                        //Variable die den Reihenindex mitzählt
                    foreach (DataRow row in datatable.Rows)                                                 //foreach Schleife -> Läuft so oft durch wie Reihen im Datentable sind
                    {
                        datatable.Rows[index]["Statuszeile_WP"] = SplitColumn(Convert.ToInt32(datatable.Rows[index]["Statuszeile_WP_$_Betriebszustand_Luefter"]), true);                     //Zellen der 1. neuen Spalte werden mit 1.Byte (0-255) der alten Spalte der selben Reihe befüllt 
                        datatable.Rows[index]["Betriebszustand_Luefter"] = SplitColumn(Convert.ToInt32(datatable.Rows[index]["Statuszeile_WP_$_Betriebszustand_Luefter"]), false);           //Zellen der 2. neuen Spalte werden mit 2.Byte (256-511) der alten Spalte der selben Reihe befüllt 
                        index++;
                    }
                    datatable.Columns.Remove("Statuszeile_WP_$_Betriebszustand_Luefter");                   //Alte Spalte löschen
                }
                //330 Heating State + 368 Defrost Reason
                if (datatable.Columns.Contains("330_Heating_State_$_368_Defrost_Reason"))
                {
                    int index = 0;
                    datatable.Columns.Add("Heating_State_330");                                                //Neue Spalte mit 1. Teil der alten Spalte erstellen
                    datatable.Columns.Add("Defrost_Reason_368");                                       //Neue Spalte mit 2. Teil der alten Spalte erstellen
                    index = 0;                                                                          //Variable die den Reihenindex mitzählt
                    foreach (DataRow row in datatable.Rows)                                                 //foreach Schleife -> Läuft so oft durch wie Reihen im Datentable sind
                    {
                        datatable.Rows[index]["Heating_State_330"] = SplitColumn(Convert.ToInt32(datatable.Rows[index]["330_Heating_State_$_368_Defrost_Reason"]), true);                     //Zellen der 1. neuen Spalte werden mit 1.Byte (0-255) der alten Spalte der selben Reihe befüllt 
                        datatable.Rows[index]["Defrost_Reason_368"] = SplitColumn(Convert.ToInt32(datatable.Rows[index]["330_Heating_State_$_368_Defrost_Reason"]), false);           //Zellen der 2. neuen Spalte werden mit 2.Byte (256-511) der alten Spalte der selben Reihe befüllt 
                        index++;
                    }
                    datatable.Columns.Remove("330_Heating_State_$_368_Defrost_Reason");                   //Alte Spalte löschen
                }
                


                //E-Stab $ SG Ready
                if(!datatable.Columns.Contains("E_Stab") || !datatable.Columns.Contains("SG_Ready"))
                {
                    int index = 0;
                    datatable.Columns.Add("E_Stab");                                                        //Neue Spalte mit 1. Teil der alten Spalte erstellen
                    datatable.Columns.Add("SG_Ready");                                                      //Neue Spalte mit 2. Teil der alten Spalte erstellen
                    index = 0;                                                                              //Variable die den Reihenindex mitzählt
                    foreach (DataRow row in datatable.Rows)                                                 //foreach Schleife -> Läuft so oft durch wie Reihen im Datentable sind
                    {
                        datatable.Rows[index]["E_Stab"] = SplitColumn(Convert.ToInt32(datatable.Rows[index]["E_Stab_$_SG_Ready"]), true);                     //Zellen der 1. neuen Spalte werden mit 1.Byte (0-255) der alten Spalte der selben Reihe befüllt 
                        datatable.Rows[index]["SG_Ready"] = SplitColumn(Convert.ToInt32(datatable.Rows[index]["E_Stab_$_SG_Ready"]), false);           //Zellen der 2. neuen Spalte werden mit 2.Byte (256-511) der alten Spalte der selben Reihe befüllt 
                        index++;
                    }
                    datatable.Columns.Remove("E_Stab_$_SG_Ready");                   //Alte Spalte löschen

                }

                columns = datatable.Columns.Count;          //Columns erneuern, da es mehr geworden sind

                #endregion


                //Vorlauftemperatur
                for (int i = 0; i < columns; i++)                                                           //Durchläuft alle Spalten im Datatable
                {
                    if (datatable.Columns[i].ColumnName == "Vorlauftemp")                              //Gibt es eine Spalte mit dem gesuchten Namen?
                    {
                        //[°C]
                        for (int x = 0; x < rows; x++)
                        {
                            datatable.Rows[x][i] = Convert.ToDouble(datatable.Rows[x][i]) / 100;             //Werte auf richtige Einheit bringen
                        }
                    }
                }

                //Rücklauftemperatur
                for (int i = 0; i < columns; i++)                                                           //Durchläuft alle Spalten im Datatable
                {
                    if (datatable.Columns[i].ColumnName == "Ruecklauftemp")                              //Gibt es eine Spalte mit dem gesuchten Namen?
                    {
                        //  datatable.Columns[i].ColumnName = datatable.Columns[i].ColumnName + " [°C]";        //Einheit zu Überschrift hinzufügen
                        for (int x = 0; x < rows; x++)
                        {
                            datatable.Rows[x][i] = Convert.ToDouble(datatable.Rows[x][i]) / 100;             //Werte auf richtige Einheit bringen
                        }
                    }
                }

                //TW-Speichertemperatur
                for (int i = 0; i < columns; i++)                                                           //Durchläuft alle Spalten im Datatable
                {
                    if (datatable.Columns[i].ColumnName == "TW_Speichertemp")                              //Gibt es eine Spalte mit dem gesuchten Namen?
                    {
                        //   datatable.Columns[i].ColumnName = datatable.Columns[i].ColumnName + " [°C]";        //Einheit zu Überschrift hinzufügen
                        for (int x = 0; x < rows; x++)
                        {
                            datatable.Rows[x][i] = Convert.ToDouble(datatable.Rows[x][i]) / 100;             //Werte auf richtige Einheit bringen
                        }
                    }
                }

                //T1 VLT WP
                for (int i = 0; i < columns; i++)                                                           //Durchläuft alle Spalten im Datatable
                {
                    if (datatable.Columns[i].ColumnName == "T1_VLT_WP")                              //Gibt es eine Spalte mit dem gesuchten Namen?
                    {
                        //  datatable.Columns[i].ColumnName = datatable.Columns[i].ColumnName + " [°C]";        //Einheit zu Überschrift hinzufügen
                        for (int x = 0; x < rows; x++)
                        {
                            datatable.Rows[x][i] = Convert.ToDouble(datatable.Rows[x][i]) / 100;             //Werte auf richtige Einheit bringen
                        }
                    }
                }

                //Aussentemperatur
                for (int i = 0; i < columns; i++)                                                           //Durchläuft alle Spalten im Datatable
                {
                    if (datatable.Columns[i].ColumnName == "Aussentemp")                              //Gibt es eine Spalte mit dem gesuchten Namen?
                    {
                        // datatable.Columns[i].ColumnName = datatable.Columns[i].ColumnName + " [°C]";        //Einheit zu Überschrift hinzufügen
                        for (int x = 0; x < rows; x++)
                        {
                            datatable.Rows[x][i] = Convert.ToDouble(datatable.Rows[x][i]) / 100;             //Werte auf richtige Einheit bringen
                        }
                    }
                }

                //Verdampfungstemperatur
                for (int i = 0; i < columns; i++)                                                           //Durchläuft alle Spalten im Datatable
                {
                    if (datatable.Columns[i].ColumnName == "Verdampfungstemp")                              //Gibt es eine Spalte mit dem gesuchten Namen?
                    {
                        //  datatable.Columns[i].ColumnName = datatable.Columns[i].ColumnName + " [°C]";        //Einheit zu Überschrift hinzufügen
                        for (int x = 0; x < rows; x++)
                        {
                            datatable.Rows[x][i] = Convert.ToDouble(datatable.Rows[x][i])/100;             //Werte auf richtige Einheit bringen
                        }
                    }
                }

                //T4 Verd.eintrittstemperatur
                for (int i = 0; i < columns; i++)                                                           //Durchläuft alle Spalten im Datatable
                {
                    if (datatable.Columns[i].ColumnName == "T4_Verdeintrittstemp")                              //Gibt es eine Spalte mit dem gesuchten Namen?
                    {
                        // datatable.Columns[i].ColumnName = datatable.Columns[i].ColumnName + " [°C]";        //Einheit zu Überschrift hinzufügen
                        for (int x = 0; x < rows; x++)
                        {
                            datatable.Rows[x][i] = Convert.ToDouble(datatable.Rows[x][i]) / 100;             //Werte auf richtige Einheit bringen
                        }
                    }
                }

                //Kondensationstemperatur
                for (int i = 0; i < columns; i++)                                                           //Durchläuft alle Spalten im Datatable
                {
                    if (datatable.Columns[i].ColumnName == "Kondensationstemp")                              //Gibt es eine Spalte mit dem gesuchten Namen?
                    {
                        //   datatable.Columns[i].ColumnName = datatable.Columns[i].ColumnName + " [°C]";        //Einheit zu Überschrift hinzufügen
                        for (int x = 0; x < rows; x++)
                        {
                            datatable.Rows[x][i] = Convert.ToDouble(datatable.Rows[x][i]) / 100;             //Werte auf richtige Einheit bringen
                        }
                    }
                }

                //PufferOben
                for (int i = 0; i < columns; i++)                                                           //Durchläuft alle Spalten im Datatable
                {
                    if (datatable.Columns[i].ColumnName == "PufferOben")                              //Gibt es eine Spalte mit dem gesuchten Namen?
                    {
                        //  datatable.Columns[i].ColumnName = datatable.Columns[i].ColumnName + " [°C]";        //Einheit zu Überschrift hinzufügen
                        for (int x = 0; x < rows; x++)
                        {
                            datatable.Rows[x][i] = Convert.ToDouble(datatable.Rows[x][i]) / 100;             //Werte auf richtige Einheit bringen
                        }
                    }
                }

                //PufferUnten
                for (int i = 0; i < columns; i++)                                                           //Durchläuft alle Spalten im Datatable
                {
                    if (datatable.Columns[i].ColumnName == "PufferUnten")                              //Gibt es eine Spalte mit dem gesuchten Namen?
                    {
                        //   datatable.Columns[i].ColumnName = datatable.Columns[i].ColumnName + " [°C]";        //Einheit zu Überschrift hinzufügen
                        for (int x = 0; x < rows; x++)
                        {
                            datatable.Rows[x][i] = Convert.ToDouble(datatable.Rows[x][i]) / 100;             //Werte auf richtige Einheit bringen
                        }
                    }
                }

                //Kompressordrehzahl 
                for (int i = 0; i < columns; i++)                                                           //Durchläuft alle Spalten im Datatable
                {
                    if (datatable.Columns[i].ColumnName == "Kompressordrehzahl")                              //Gibt es eine Spalte mit dem gesuchten Namen?
                    {
                        //datatable.Columns[i].ColumnName = datatable.Columns[i].ColumnName + " [U/min]";        //Einheit zu Überschrift hinzufügen
                    }
                }

                //T3 Sauggastemperatur
                for (int i = 0; i < columns; i++)                                                           //Durchläuft alle Spalten im Datatable
                {
                    if (datatable.Columns[i].ColumnName == "T3_Sauggastemp")                              //Gibt es eine Spalte mit dem gesuchten Namen?
                    {
                        //    datatable.Columns[i].ColumnName = datatable.Columns[i].ColumnName + " [°C]";        //Einheit zu Überschrift hinzufügen
                        for (int x = 0; x < rows; x++)
                        {
                            datatable.Rows[x][i] = Convert.ToDouble(datatable.Rows[x][i]) / 100;             //Werte auf richtige Einheit bringen
                        }
                    }
                }
                //Überhitzung Verdampfer
                for (int i = 0; i < columns; i++)                                                           //Durchläuft alle Spalten im Datatable
                {
                    if (datatable.Columns[i].ColumnName == "Überhitzung_Verdampfer")                              //Gibt es eine Spalte mit dem gesuchten Namen?
                    {
                        //   datatable.Columns[i].ColumnName = datatable.Columns[i].ColumnName + " [°C]";        //Einheit zu Überschrift hinzufügen
                        for (int x = 0; x < rows; x++)
                        {
                            datatable.Rows[x][i] = (Convert.ToDouble(datatable.Rows[x]["T3_Sauggastemp"]) - Convert.ToDouble(datatable.Rows[x]["Verdampfungstemp"]));             //Werte berechnen
                        }
                    }
                }
                //Überhitzung Kompressor
                for (int i = 0; i < columns; i++)                                                           //Durchläuft alle Spalten im Datatable
                {
                    if (datatable.Columns[i].ColumnName == "Überhitzung_Kompressor")                              //Gibt es eine Spalte mit dem gesuchten Namen?
                    {
                        //   datatable.Columns[i].ColumnName = datatable.Columns[i].ColumnName + " [°C]";        //Einheit zu Überschrift hinzufügen
                        for (int x = 0; x < rows; x++)
                        {
                            datatable.Rows[x][i] = Convert.ToDouble(datatable.Rows[x]["T4_Verdeintrittstemp"]) - Convert.ToDouble(datatable.Rows[x]["Verdampfungstemp"]);             //Werte berechnen
                        }
                    }
                }
                //T6 Fluessigkeitstemperatur
                for (int i = 0; i < columns; i++)                                                           //Durchläuft alle Spalten im Datatable
                {
                    if (datatable.Columns[i].ColumnName == "T6_Fluessigkeitstemp")                              //Gibt es eine Spalte mit dem gesuchten Namen?
                    {
                        // datatable.Columns[i].ColumnName = datatable.Columns[i].ColumnName + " [°C]";        //Einheit zu Überschrift hinzufügen
                        for (int x = 0; x < rows; x++)
                        {
                            datatable.Rows[x][i] = Convert.ToDouble(datatable.Rows[x][i]) / 100;             //Werte auf richtige Einheit bringen
                        }
                    }
                }
                //Heizleistung
                for (int i = 0; i < columns; i++)                                                           //Durchläuft alle Spalten im Datatable
                {
                    if (datatable.Columns[i].ColumnName == "Heizleistung")                              //Gibt es eine Spalte mit dem gesuchten Namen?
                    {
                        //     datatable.Columns[i].ColumnName = datatable.Columns[i].ColumnName + " [W]";        //Einheit zu Überschrift hinzufügen
                        for (int x = 0; x < rows; x++)
                        {
                            datatable.Rows[x][i] = Convert.ToDouble(datatable.Rows[x][i]);             //Werte auf richtige Einheit bringen
                        }
                    }
                }
                //Leistungsanforderung 
                for (int i = 0; i < columns; i++)                                                           //Durchläuft alle Spalten im Datatable
                {
                    if (datatable.Columns[i].ColumnName == "Leistungsanforderung")                              //Gibt es eine Spalte mit dem gesuchten Namen?
                    {
                        //datatable.Columns[i].ColumnName = datatable.Columns[i].ColumnName + " [W]";        //Einheit zu Überschrift hinzufügen
                    }
                }
                //Anforderung
                for (int i = 0; i < columns; i++)                                                           //Durchläuft alle Spalten im Datatable
                {
                    if (datatable.Columns[i].ColumnName == "Anforderung")                              //Gibt es eine Spalte mit dem gesuchten Namen?
                    {
                        //[W]                                                                          //Einheit zu Überschrift hinzufügen
                        for (int x = 0; x < rows; x++)
                        {
                            datatable.Rows[x][i] = Convert.ToDouble(datatable.Rows[x][i]) / 10;             //Werte auf richtige Einheit bringen
                        }
                    }
                }
                //Heizung COP 
                for (int i = 0; i < columns; i++)                                                           //Durchläuft alle Spalten im Datatable
                {
                    if (datatable.Columns[i].ColumnName == "Heizung_COP")                              //Gibt es eine Spalte mit dem gesuchten Namen?
                    {
                        for (int x = 0; x < rows; x++)
                        {
                            datatable.Rows[x][i] = Convert.ToDouble(datatable.Rows[x][i]) / 1000;             //Werte auf richtige Einheit bringen
                        }
                    }
                }
                //Oeffnungsgrad EVI Ventil 
                for (int i = 0; i < columns; i++)                                                           //Durchläuft alle Spalten im Datatable
                {
                    if (datatable.Columns[i].ColumnName == "Oeffnungsgrad_EVI_Ventil")                              //Gibt es eine Spalte mit dem gesuchten Namen?
                    {
                        //   datatable.Columns[i].ColumnName = datatable.Columns[i].ColumnName + " [%]";        //Einheit zu Überschrift hinzufügen
                        for (int x = 0; x < rows; x++)
                        {
                            datatable.Rows[x][i] = Convert.ToDouble(datatable.Rows[x][i]) / 100;             //Werte auf richtige Einheit bringen
                        }
                    }
                }
                //Cooling Valve Opening 
                for (int i = 0; i < columns; i++)                                                           //Durchläuft alle Spalten im Datatable
                {
                    if (datatable.Columns[i].ColumnName == "Cooling_Valve_Opening")                              //Gibt es eine Spalte mit dem gesuchten Namen?
                    {
                        // datatable.Columns[i].ColumnName = datatable.Columns[i].ColumnName + " [%]";        //Einheit zu Überschrift hinzufügen
                        for (int x = 0; x < rows; x++)
                        {
                            datatable.Rows[x][i] = Convert.ToDouble(datatable.Rows[x][i]) / 100;             //Werte auf richtige Einheit bringen
                        }
                    }
                }
                //Superheat Setpoint
                for (int i = 0; i < columns; i++)                                                           //Durchläuft alle Spalten im Datatable
                {
                    if (datatable.Columns[i].ColumnName == "Superheat_Setpoint")                              //Gibt es eine Spalte mit dem gesuchten Namen?
                    {
                        //    datatable.Columns[i].ColumnName = datatable.Columns[i].ColumnName + " [°C]";        //Einheit zu Überschrift hinzufügen
                        for (int x = 0; x < rows; x++)
                        {
                            datatable.Rows[x][i] = Convert.ToDouble(datatable.Rows[x][i]) / 100;             //Werte auf richtige Einheit bringen
                        }
                    }
                }
                //EVI_Saturation_Temp
                for (int i = 0; i < columns; i++)                                                           //Durchläuft alle Spalten im Datatable
                {
                    if (datatable.Columns[i].ColumnName == "EVI_Saturation_Temp")                              //Gibt es eine Spalte mit dem gesuchten Namen?
                    {
                        //    datatable.Columns[i].ColumnName = datatable.Columns[i].ColumnName + " [°C]";        //Einheit zu Überschrift hinzufügen
                        for (int x = 0; x < rows; x++)
                        {
                            datatable.Rows[x][i] = Convert.ToDouble(datatable.Rows[x][i]) / 100;             //Werte auf richtige Einheit bringen
                        }
                    }
                }
                //Oeffnungsgrad Ex-Ventil Heizen
                for (int i = 0; i < columns; i++)                                                           //Durchläuft alle Spalten im Datatable
                {
                    if (datatable.Columns[i].ColumnName == "Oeffnungsgrad_Ex_Ventil_Heizen")                              //Gibt es eine Spalte mit dem gesuchten Namen?
                    {
                        //    datatable.Columns[i].ColumnName = datatable.Columns[i].ColumnName + " [%]";        //Einheit zu Überschrift hinzufügen
                        for (int x = 0; x < rows; x++)
                        {
                            datatable.Rows[x][i] = Convert.ToDouble(datatable.Rows[x][i]) / 100;             //Werte auf richtige Einheit bringen
                        }
                    }
                }
                //Ventilatordrehzahl
                for (int i = 0; i < columns; i++)                                                           //Durchläuft alle Spalten im Datatable
                {
                    if (datatable.Columns[i].ColumnName == "Ventilatordrehzahl")                              //Gibt es eine Spalte mit dem gesuchten Namen?
                    {
                        //       datatable.Columns[i].ColumnName = datatable.Columns[i].ColumnName + " [U/min]";        //Einheit zu Überschrift hinzufügen
                        for (int x = 0; x < rows; x++)
                        {
                            datatable.Rows[x][i] = Convert.ToDouble(datatable.Rows[x][i]);             //Werte auf richtige Einheit bringen
                        }
                    }
                }
                //Ventilatorleistung
                for (int i = 0; i < columns; i++)                                                           //Durchläuft alle Spalten im Datatable
                {
                    if (datatable.Columns[i].ColumnName == "Ventilatorleistung")                              //Gibt es eine Spalte mit dem gesuchten Namen?
                    {
                        //      datatable.Columns[i].ColumnName = datatable.Columns[i].ColumnName + " [W]";        //Einheit zu Überschrift hinzufügen
                    }
                }
                //Ölsumpftemp.
                for (int i = 0; i < columns; i++)                                                           //Durchläuft alle Spalten im Datatable
                {
                    if (datatable.Columns[i].ColumnName == "Ölsumpftemp")                              //Gibt es eine Spalte mit dem gesuchten Namen?
                    {
                        //     datatable.Columns[i].ColumnName = datatable.Columns[i].ColumnName + " [°C]";        //Einheit zu Überschrift hinzufügen
                        for (int x = 0; x < rows; x++)
                        {
                            datatable.Rows[x][i] = Convert.ToDouble(datatable.Rows[x][i]) / 100;             //Werte auf richtige Einheit bringen
                        }
                    }
                }
                //Verdichteraustrittgastemp.
                for (int i = 0; i < columns; i++)                                                           //Durchläuft alle Spalten im Datatable
                {
                    if (datatable.Columns[i].ColumnName == "Verdichteraustrittgastemp")                              //Gibt es eine Spalte mit dem gesuchten Namen?
                    {
                        //     datatable.Columns[i].ColumnName = datatable.Columns[i].ColumnName + " [°C]";        //Einheit zu Überschrift hinzufügen
                        for (int x = 0; x < rows; x++)
                        {
                            datatable.Rows[x][i] = Convert.ToDouble(datatable.Rows[x][i]) / 100;             //Werte auf richtige Einheit bringen
                        }
                    }
                }
                //EVI Suction gas temp.
                for (int i = 0; i < columns; i++)                                                           //Durchläuft alle Spalten im Datatable
                {
                    if (datatable.Columns[i].ColumnName == "EVI_Suction_gas_temp")                              //Gibt es eine Spalte mit dem gesuchten Namen?
                    {
                        //   datatable.Columns[i].ColumnName = datatable.Columns[i].ColumnName + " [°C]";        //Einheit zu Überschrift hinzufügen
                        for (int x = 0; x < rows; x++)
                        {
                            datatable.Rows[x][i] = Convert.ToDouble(datatable.Rows[x][i]) / 100;             //Werte auf richtige Einheit bringen
                        }
                    }
                }
                //Vorlaufsoll WP
                for (int i = 0; i < columns; i++)                                                           //Durchläuft alle Spalten im Datatable
                {
                    if (datatable.Columns[i].ColumnName == "Vorlaufsoll_WP")                              //Gibt es eine Spalte mit dem gesuchten Namen?
                    {
                        //   datatable.Columns[i].ColumnName = datatable.Columns[i].ColumnName + " [°C]";        //Einheit zu Überschrift hinzufügen
                        for (int x = 0; x < rows; x++)
                        {
                            datatable.Rows[x][i] = Convert.ToDouble(datatable.Rows[x][i]) / 10;             //Werte auf richtige Einheit bringen
                        }
                    }
                }
                //Statuszeile WP
                for (int i = 0; i < columns; i++)                                                           //Durchläuft alle Spalten im Datatable
                {
                    if (datatable.Columns[i].ColumnName == "Statuszeile_WP")                              //Gibt es eine Spalte mit dem gesuchten Namen?
                    {
                        int status;
                        for (int x = 0; x < rows; x++)
                        {
                            status = Convert.ToInt32(datatable.Rows[x][i]);
                            switch (status)
                            {
                                case 0:
                                    datatable.Rows[x][i] = "Wärmepumpe ist ausgeschaltet";
                                    break;
                                case 1:
                                    datatable.Rows[x][i] = "Wärmepumpe aktiv, Kühlmodus";
                                    break;
                                case 2:
                                    datatable.Rows[x][i] = "Wärmepumpe aktiv, Heizmodus";
                                    break;
                                case 3:
                                    datatable.Rows[x][i] = "Wärmepumpe aktiv, TWS-Ladung";
                                    break;
                                case 4:
                                    datatable.Rows[x][i] = "Wärmepumpe aktiv, Verdampfungstemp. gering";
                                    break;
                                case 5:
                                    datatable.Rows[x][i] = "Wärmepumpe aktiv, Verdampfungstemp. hoch";
                                    break;
                                case 6:
                                    datatable.Rows[x][i] = "Wärmepumpe aktiv, Kondensationstemp. hoch";
                                    break;
                                case 7:
                                    datatable.Rows[x][i] = "Wärmepumpe aktiv, Kondensationstemp. gering";
                                    break;
                                case 8:
                                    datatable.Rows[x][i] = "Wärmepumpe aktiv, Kältemittelverlust?";
                                    break;
                                case 9:
                                    datatable.Rows[x][i] = "Wärmepumpe aktiv, hohe Überhitzung";
                                    break;
                                case 10:
                                    datatable.Rows[x][i] = "Wärmepumpe aktiv, Pumpenwarnung";
                                    break;
                                case 11:
                                    datatable.Rows[x][i] = "Primärkreispumpenwarnung";
                                    break;
                                case 12:
                                    datatable.Rows[x][i] = "Bereitschaft";
                                    break;
                                case 13:
                                    datatable.Rows[x][i] =  "Alarm";
                                    break;
                                case 14:
                                    datatable.Rows[x][i] = "Wärmepumpe startet Kühlmodul";
                                    break;
                                case 15:
                                    datatable.Rows[x][i] = "Wärmepumpe enteist";
                                    break;
                                case 16:
                                    datatable.Rows[x][i] = "Wärmepumpe startet in Kürze";
                                    break;
                                case 17:
                                    datatable.Rows[x][i] = "Bereitschaft, Aus";
                                    break;
                                case 18:
                                    datatable.Rows[x][i] = "Wärmepumpe startet Heizmodus";
                                    break;
                                case 19:
                                    datatable.Rows[x][i] = "Wärmepumpe stoppt";
                                    break;
                                case 20:
                                    datatable.Rows[x][i] = "Wärmepumpe in Handbetrieb";
                                    break;
                                case 21:
                                    datatable.Rows[x][i] = "Kompressor startet";
                                    break;
                                case 22:
                                    datatable.Rows[x][i] = "EVU Lock aktiv";
                                    break;
                                case 23:
                                    datatable.Rows[x][i] = "Wärmepumpe im Frostschutzmodus";
                                    break;
                                case 24:
                                    datatable.Rows[x][i] = "Ölsumpfheizung Verdichter aktiv";
                                    break;
                                case 25:
                                    datatable.Rows[x][i] = "keine Zeitfreigabe";
                                    break;
                                case 26:
                                    datatable.Rows[x][i] = "keine Aussentemperaturfreigabe";
                                    break;
                                case 27:
                                    datatable.Rows[x][i] = "Rücklauftempertur zu gering, E-Stab aktiv";
                                    break;
                                case 28:
                                    datatable.Rows[x][i] = "Rücklauftempertur zu gering";
                                    break;
                                case 29:
                                    datatable.Rows[x][i] = "Elektroheizstab aktiv";
                                    break;
                                case 30:
                                    datatable.Rows[x][i] = "Bereitschaft, Kessel aktiv";
                                    break;
                                case 31:
                                    datatable.Rows[x][i] = "Kühlanforderung";
                                    break;
                                case 32:
                                    datatable.Rows[x][i] = "Wärmepumpe im Handbetrieb";
                                    break;
                            }
                        }
                    }
                }
                //Betriebszustand Luefter 
                for (int i = 0; i < columns; i++)                                                           //Durchläuft alle Spalten im Datatable
                {
                    if (datatable.Columns[i].ColumnName == "Betriebszustand_Luefter")                              //Gibt es eine Spalte mit dem gesuchten Namen?
                    {
                        int status;
                        for (int x = 0; x < rows; x++)
                        {
                            status = Convert.ToInt32(datatable.Rows[x][i]);
                            switch (status)
                            {
                                case 0:
                                    datatable.Rows[x][i] = "in operation";
                                    break;
                                case 1:
                                    datatable.Rows[x][i] = "Stop";
                                    break;
                                case 2:
                                    datatable.Rows[x][i] = "Temp. Management";
                                    break;
                                case 4:
                                    datatable.Rows[x][i] = "IGBT Fault Check";
                                    break;
                                case 8:
                                    datatable.Rows[x][i] = "int Systemfehler";
                                    break;
                                case 16:
                                    datatable.Rows[x][i] = "falsche Drehrichtung";
                                    break;
                                case 32:
                                    datatable.Rows[x][i] = "Feuer";
                                    break;
                                case 64:
                                    datatable.Rows[x][i] = "Feldschwaechung";
                                    break;
                                case 128:
                                    datatable.Rows[x][i] = "Strombegrenzung";
                                    break;
                            }
                        }
                    }
                }
                //330 Heating State
                for (int i = 0; i < columns; i++)                                                           //Durchläuft alle Spalten im Datatable
                {
                    if (datatable.Columns[i].ColumnName == "Heating_State_330")                              //Gibt es eine Spalte mit dem gesuchten Namen?
                    {
                        int status;
                        for (int x = 0; x < rows; x++)
                        {
                            status = Convert.ToInt32(datatable.Rows[x][i]);
                            switch (status)
                            {
                                case 0:
                                    datatable.Rows[x][i] = "Off";
                                    break;
                                case 1:
                                    datatable.Rows[x][i] = "Startup";
                                    break;
                                case 2:
                                    datatable.Rows[x][i] = "Transition";
                                    break;
                                case 3:
                                    datatable.Rows[x][i] = "Control";
                                    break;
                                case 4:
                                    datatable.Rows[x][i] = "MOP protection";
                                    break;
                            }
                        }
                    }
                }
                //368 Defrost Reason
                for (int i = 0; i < columns; i++)                                                           //Durchläuft alle Spalten im Datatable
                {
                    if (datatable.Columns[i].ColumnName == "Defrost_Reason_368")                              //Gibt es eine Spalte mit dem gesuchten Namen?
                    {
                        int status;
                        for (int x = 0; x < rows; x++)
                        {
                            status = Convert.ToInt32(datatable.Rows[x][i]);
                            switch (status)
                            {
                                case 0:
                                    datatable.Rows[x][i] = "no Reason";
                                    break;
                                case 1:
                                    datatable.Rows[x][i] = "Demand Modbus";
                                    break;
                                case 2:
                                    datatable.Rows[x][i] = "Timer Interval";
                                    break;
                                case 3:
                                    datatable.Rows[x][i] = "DigIn 1";
                                    break;
                                case 4:
                                    datatable.Rows[x][i] = "Absolute Trigger";
                                    break;
                                case 5:
                                    datatable.Rows[x][i] = "Defrost delta variation";
                                    break;
                                case 6:
                                    datatable.Rows[x][i] = "LOP recovery";
                                    break;
                            }
                        }
                    }
                }
                //E-Stab
                for (int i = 0; i < columns; i++)                                                           //Durchläuft alle Spalten im Datatable
                {
                    if (datatable.Columns[i].ColumnName == "E_Stab")                              //Gibt es eine Spalte mit dem gesuchten Namen?
                    {
                        int condition;
                        for (int x = 0; x < rows; x++)
                        {
                            condition = Convert.ToInt32(datatable.Rows[x][i]);
                            switch (condition)
                            {
                                case 0:
                                    datatable.Rows[x][i] = "Aus"; //Aus 0
                                    break;
                                case 1:
                                    datatable.Rows[x][i] = "Ein"; //Ein 1
                                    break;
                            }
                        }
                    }
                }

                //SG Ready
                for (int i = 0; i < columns; i++)                                                           //Durchläuft alle Spalten im Datatable
                {
                    if (datatable.Columns[i].ColumnName == "SG_Ready")                              //Gibt es eine Spalte mit dem gesuchten Namen?
                    {
                        int status;
                        for (int x = 0; x < rows; x++)
                        {
                            status = Convert.ToInt32(datatable.Rows[x][i]);
                            switch (status)
                            {
                                case 0:
                                    datatable.Rows[x][i] = "EVU Lock";
                                    break;
                                case 1:
                                    datatable.Rows[x][i] = "Normalbetrieb";
                                    break;
                                case 2:
                                    datatable.Rows[x][i] = "Einschaltempf.";
                                    break;
                                case 3:
                                    datatable.Rows[x][i] = "Einschalten Soll";
                                    break;
                            }
                        }
                    }
                }

                //Primaerkreis iPWM/Durchfluss K10
                if (produktserie == "VampAir K08/K10")
                {
                    for (int i = 0; i < columns; i++)                                                           //Durchläuft alle Spalten im Datatable
                    {
                        if (datatable.Columns[i].ColumnName == "Primaerkreis_iPWM_Durchfluss")                              //Gibt es eine Spalte mit dem gesuchten Namen?
                        {
                            datatable.Columns.Add("iPWM_state");
                            // datatable.Columns[i].ColumnName = datatable.Columns[i].ColumnName + " [L/h]";        //Einheit zu Überschrift hinzufügen
                            for (int x = 0; x < rows; x++)
                            {
                                if (Convert.ToInt32(datatable.Rows[x][i]) < 100)
                                {
                                    datatable.Rows[x][i] = 0;
                                    datatable.Rows[x]["iPWM_state"] = "Overvoltage on interface";
                                }
                                else if (Convert.ToInt32(datatable.Rows[x][i]) >= 100 && Convert.ToInt32(datatable.Rows[x][i]) <= 350)
                                {
                                    datatable.Rows[x][i] = 0;
                                    datatable.Rows[x]["iPWM_state"] = "Standby OK";
                                }
                                else if (Convert.ToInt32(datatable.Rows[x][i]) >= 351 && Convert.ToInt32(datatable.Rows[x][i]) <= 7725)
                                {
                                    datatable.Rows[x][i] = Convert.ToDouble(datatable.Rows[x][i]) * 0.3;
                                    if (Convert.ToDouble(datatable.Rows[x][i]) < 150)
                                    {
                                        datatable.Rows[x][i] = 0;
                                    }
                                    datatable.Rows[x]["iPWM_state"] = "Pump is running";
                                }
                                else if (Convert.ToInt32(datatable.Rows[x][i]) >= 7726 && Convert.ToInt32(datatable.Rows[x][i]) <= 8250)
                                {
                                    datatable.Rows[x][i] = Convert.ToDouble(datatable.Rows[x][i]) * 0.3;
                                    if (Convert.ToDouble(datatable.Rows[x][i]) < 150)
                                    {
                                        datatable.Rows[x][i] = 0;
                                    }
                                    datatable.Rows[x]["iPWM_state"] = "Abnormal running";
                                }
                                else if (Convert.ToInt32(datatable.Rows[x][i]) >= 8251 && Convert.ToInt32(datatable.Rows[x][i]) <= 8750)
                                {
                                    datatable.Rows[x][i] = 0;
                                    datatable.Rows[x]["iPWM_state"] = "Abnormal function";
                                }
                                else if (Convert.ToInt32(datatable.Rows[x][i]) >= 8751 && Convert.ToInt32(datatable.Rows[x][i]) <= 9250)
                                {
                                    datatable.Rows[x][i] = 0;
                                    datatable.Rows[x]["iPWM_state"] = "Failure check installation";
                                }
                                else if (Convert.ToInt32(datatable.Rows[x][i]) >= 9251 && Convert.ToInt32(datatable.Rows[x][i]) <= 9750)
                                {
                                    datatable.Rows[x][i] = 0;
                                    datatable.Rows[x]["iPWM_state"] = "Failure pump blocked";
                                }
                                else if (Convert.ToInt32(datatable.Rows[x][i]) >= 9751 && Convert.ToInt32(datatable.Rows[x][i]) <= 11000)
                                {
                                    datatable.Rows[x][i] = 0;
                                    datatable.Rows[x]["iPWM_state"] = "Failure PWM connection";
                                }
                                else
                                {
                                    datatable.Rows[x][i] = 0;
                                    datatable.Rows[x]["iPWM_state"] = "undefined";
                                }

                            }
                        }
                    }
                }
                //Primaerkreis iPWM/Durchfluss K15
                if (produktserie.Contains("VampAir K12/K15"))
                {
                    for (int i = 0; i < columns; i++)                                                           //Durchläuft alle Spalten im Datatable
                    {
                        if (datatable.Columns[i].ColumnName == "Primaerkreis_iPWM_Durchfluss")                              //Gibt es eine Spalte mit dem gesuchten Namen?
                        {
                            //datatable.Columns[i].ColumnName = datatable.Columns[i].ColumnName + " [L/h]";        //Einheit zu Überschrift hinzufügen
                        }
                    }
                }

            }
            #endregion

            #region Therminator
            if (produktserie == "Therminator")
            { 
                //Kesseltemperatur
                for (int i = 0; i < columns; i++)                                                           //Durchläuft alle Spalten im Datatable
                {
                    if (datatable.Columns[i].ColumnName == "Kesseltemperatur")                              //Gibt es eine Spalte mit dem gesuchten Namen?
                    {
                        //[°C]
                        for (int x = 0; x < rows; x++)
                        {
                            datatable.Rows[x][i] = Convert.ToDouble(datatable.Rows[x][i]) / 10;             //Werte auf richtige Einheit bringen
                        }
                    }
                }

                //Rücklauftemperatur
                for (int i = 0; i < columns; i++)                                                           //Durchläuft alle Spalten im Datatable
                {
                    if (datatable.Columns[i].ColumnName == "Ruecklauftemperatur")                           //Gibt es eine Spalte mit dem gesuchten Namen
                    {
                        //[°C]
                        for (int x = 0; x < rows; x++)
                        {
                            datatable.Rows[x][i] = Convert.ToDouble(datatable.Rows[x][i]) / 10;             //Werte auf richtige Einheit bringen
                        }
                    }
                }

                //ES_Laufzeit_SGA
                for (int i = 0; i < columns; i++)                                                           //Durchläuft alle Spalten im Datatable
                {
                    if (datatable.Columns[i].ColumnName == "ES_Laufzeit_SGA")                               //Gibt es eine Spalte mit dem gesuchten Namen?
                    {
                        //[min]
                        for (int x = 0; x < rows; x++)
                        {
                            datatable.Rows[x][i] = Convert.ToDouble(datatable.Rows[x][i]) / 10;             //Werte auf richtige Einheit bringen
                        }
                    }
                }

                //SZG_Istdrehzahl
                for (int i = 0; i < columns; i++)                                                           //Durchläuft alle Spalten im Datatable
                {
                    if (datatable.Columns[i].ColumnName == "SZG_Istdrehzahl")                              //Gibt es eine Spalte mit dem gesuchten Namen?
                    {
                        //[%]
                        for (int x = 0; x < rows; x++)
                        {
                            datatable.Rows[x][i] = Convert.ToDouble(datatable.Rows[x][i]) / 10;             //Werte auf richtige Einheit bringen
                        }
                    }
                }

                //Abgastemperatur
                for (int i = 0; i < columns; i++)                                                           //Durchläuft alle Spalten im Datatable
                {
                    if (datatable.Columns[i].ColumnName == "Abgastemperatur")                               //Gibt es eine Spalte mit dem gesuchten Namen?
                    {
                        //[°C]
                        for (int x = 0; x < rows; x++)
                        {
                            datatable.Rows[x][i] = Convert.ToDouble(datatable.Rows[x][i]) / 10;             //Werte auf richtige Einheit bringen
                        }
                    }
                }

                //Einschubtemperatur
                for (int i = 0; i < columns; i++)                                                           //Durchläuft alle Spalten im Datatable
                {
                    if (datatable.Columns[i].ColumnName == "Einschubtemperatur")                               //Gibt es eine Spalte mit dem gesuchten Namen?
                    {
                        // [°C]
                        for (int x = 0; x < rows; x++)
                        {
                            datatable.Rows[x][i] = Convert.ToDouble(datatable.Rows[x][i]) / 10;             //Werte auf richtige Einheit bringen
                        }
                    }
                }

                //Luftzahl
                for (int i = 0; i < columns; i++)                                                           //Durchläuft alle Spalten im Datatable
                {
                    if (datatable.Columns[i].ColumnName == "Luftzahl")                               //Gibt es eine Spalte mit dem gesuchten Namen?
                    {
                        for (int x = 0; x < rows; x++)
                        {
                            datatable.Rows[x][i] = Convert.ToDouble(datatable.Rows[x][i]) / 100;             //Werte auf richtige Einheit bringen
                        }
                    }
                }

                //Aussentemperatur
                for (int i = 0; i < columns; i++)                                                           //Durchläuft alle Spalten im Datatable
                {
                    if (datatable.Columns[i].ColumnName == "Aussentemperatur")                               //Gibt es eine Spalte mit dem gesuchten Namen?
                    {
                        //[°C]
                        for (int x = 0; x < rows; x++)
                        {
                            datatable.Rows[x][i] = Convert.ToDouble(datatable.Rows[x][i]) / 10;             //Werte auf richtige Einheit bringen
                        }
                    }
                }

                //SLK_IstLaufzeit
                for (int i = 0; i < columns; i++)                                                           //Durchläuft alle Spalten im Datatable
                {
                    if (datatable.Columns[i].ColumnName == "SLK_Ist_Laufzeit")                               //Gibt es eine Spalte mit dem gesuchten Namen?
                    {
                        //[sek]
                        for (int x = 0; x < rows; x++)
                        {
                            datatable.Rows[x][i] = Convert.ToDouble(datatable.Rows[x][i]) / 100;             //Werte auf richtige Einheit bringen
                        }
                    }
                }

                //DRZ_Impuls
                for (int i = 0; i < columns; i++)                                                           //Durchläuft alle Spalten im Datatable
                {
                    if (datatable.Columns[i].ColumnName == "DRZ_Impuls")                               //Gibt es eine Spalte mit dem gesuchten Namen?
                    {
                        for (int x = 0; x < rows; x++)
                        {
                            datatable.Rows[x][i] = Convert.ToDouble(datatable.Rows[x][i]) / 10;             //Werte auf richtige Einheit bringen
                        }
                    }
                }

                //Puffertemperatur_oben
                for (int i = 0; i < columns; i++)                                                           //Durchläuft alle Spalten im Datatable
                {
                    if (datatable.Columns[i].ColumnName == "Puffertemperatur_oben")                               //Gibt es eine Spalte mit dem gesuchten Namen?
                    {
                        for (int x = 0; x < rows; x++)
                        {
                            datatable.Rows[x][i] = Convert.ToDouble(datatable.Rows[x][i]) / 10;             //Werte auf richtige Einheit bringen
                        }
                    }
                }

                //Puffertemperatur_unten
                for (int i = 0; i < columns; i++)                                                           //Durchläuft alle Spalten im Datatable
                {
                    if (datatable.Columns[i].ColumnName == "Puffertemperatur_unten")                               //Gibt es eine Spalte mit dem gesuchten Namen?
                    {
                        for (int x = 0; x < rows; x++)
                        {
                            datatable.Rows[x][i] = Convert.ToDouble(datatable.Rows[x][i]) / 10;             //Werte auf richtige Einheit bringen
                        }
                    }
                }

                //X35
                for (int i = 0; i < columns; i++)                                                           //Durchläuft alle Spalten im Datatable
                {
                    if (datatable.Columns[i].ColumnName == "X35")                               //Gibt es eine Spalte mit dem gesuchten Namen?
                    {
                        for (int x = 0; x < rows; x++)
                        {
                            datatable.Rows[x][i] = Convert.ToDouble(datatable.Rows[x][i]) / 10;             //Werte auf richtige Einheit bringen
                        }
                    }
                }

                //TWS_Temperatur
                for (int i = 0; i < columns; i++)                                                           //Durchläuft alle Spalten im Datatable
                {
                    if (datatable.Columns[i].ColumnName == "TWS_Temperatur")                               //Gibt es eine Spalte mit dem gesuchten Namen?
                    {
                        for (int x = 0; x < rows; x++)
                        {
                            datatable.Rows[x][i] = Convert.ToDouble(datatable.Rows[x][i]) / 10;             //Werte auf richtige Einheit bringen
                        }
                    }
                }

                //Filter_Strom
                for (int i = 0; i < columns; i++)                                                           //Durchläuft alle Spalten im Datatable
                {
                    if (datatable.Columns[i].ColumnName == "Filter_Strom")                               //Gibt es eine Spalte mit dem gesuchten Namen?
                    {
                        //[mA]   
                        for (int x = 0; x < rows; x++)
                        {
                            datatable.Rows[x][i] = Convert.ToDouble(datatable.Rows[x][i]) / 100;             //Werte auf richtige Einheit bringen
                        }
                    }
                }

                //Filter_Spannung
                for (int i = 0; i < columns; i++)                                                           //Durchläuft alle Spalten im Datatable
                {
                    if (datatable.Columns[i].ColumnName == "Filter_Spg")                               //Gibt es eine Spalte mit dem gesuchten Namen?
                    {
                        //[V]
                        for (int x = 0; x < rows; x++)
                        {
                            datatable.Rows[x][i] = Convert.ToDouble(datatable.Rows[x][i]) / 10;             //Werte auf richtige Einheit bringen
                        }
                    }
                }
                //Filter_Spannung
                for (int i = 0; i < columns; i++)                                                           //Durchläuft alle Spalten im Datatable
                {
                    if (datatable.Columns[i].ColumnName == "Filter_Spannung")                               //Gibt es eine Spalte mit dem gesuchten Namen?
                    {
                        //[V]
                        for (int x = 0; x < rows; x++)
                        {
                            datatable.Rows[x][i] = Convert.ToDouble(datatable.Rows[x][i]) / 10;             //Werte auf richtige Einheit bringen
                        }
                    }
                }

                //ES_Laufzeit_SGA
                for (int i = 0; i < columns; i++)                                                           //Durchläuft alle Spalten im Datatable
                {
                    if (datatable.Columns[i].ColumnName == "ES_Laufzeit_SGA")                               //Gibt es eine Spalte mit dem gesuchten Namen?
                    {
                        //[sek]
                        for (int x = 0; x < rows; x++)
                        {
                            datatable.Rows[x][i] = Convert.ToDouble(datatable.Rows[x][i]) / 100;             //Werte auf richtige Einheit bringen
                        }
                    }
                }

                //HG_Modultemperatur
                for (int i = 0; i < columns; i++)                                                           //Durchläuft alle Spalten im Datatable
                {
                    if (datatable.Columns[i].ColumnName == "HG_Modultemperatur")                               //Gibt es eine Spalte mit dem gesuchten Namen?
                    {
                        //[°C]
                        for (int x = 0; x < rows; x++)
                        {
                            datatable.Rows[x][i] = Convert.ToDouble(datatable.Rows[x][i]) / 10;             //Werte auf richtige Einheit bringen
                        }
                    }
                }

                //Statuszeile
                for (int i = 0; i < columns; i++)                                                           //Durchläuft alle Spalten im Datatable
                {
                    if (datatable.Columns[i].ColumnName == "Statuszeile")                               //Gibt es eine Spalte mit dem gesuchten Namen?
                    {
                        int status;
                        for (int x = 0; x < rows; x++)
                        {
                            status = Convert.ToInt32(datatable.Rows[x][i]);
                            switch (status)
                            {
                                case 0:
                                    datatable.Rows[x][i] = "Bereitschaft";
                                    break;
                                case 1:
                                    datatable.Rows[x][i] = "Zündphase";
                                    break;
                                case 2:
                                    datatable.Rows[x][i] = "Pelletsbetrieb";
                                    break;
                                case 3:
                                    datatable.Rows[x][i] = "Kesselsolltemperatur_erreicht_Nachlauf";
                                    break;
                                case 4:
                                    datatable.Rows[x][i] = "Nachlauf";
                                    break;
                                case 5:
                                    datatable.Rows[x][i] = "Keine_Anforderung_Nachlauf";
                                    break;
                                case 6:
                                    datatable.Rows[x][i] = "Brenner_ausgeschaltet_Nachlauf";
                                    break;
                                case 7:
                                    datatable.Rows[x][i] = "Pelletsvorratsbehälter_ist_leer_Nachlauf";
                                    break;
                                case 8:
                                    datatable.Rows[x][i] = "Wärmetauscherbereinigung_ist_aktiv";
                                    break;
                                case 9:
                                    datatable.Rows[x][i] = "Wärmetauscherbereinigung_benötigt_Nachlauf";
                                    break;
                                case 10:
                                    datatable.Rows[x][i] = "Stromausfall_Nachlauf";
                                    break;
                                case 12:
                                    datatable.Rows[x][i] = "Einschubtemperatur_zu hoch_Nachlauf";
                                    break;
                                case 16:
                                    datatable.Rows[x][i] = "Brenner_ausgeschaltet_Nachlauf";
                                    break;
                                case 17:
                                    datatable.Rows[x][i] = "Sicherheitskette_ist_offen";
                                    break;
                                case 18:
                                    datatable.Rows[x][i] = "Sicherheitskette_ist_offen_Nachlauf";
                                    break;
                                case 19:
                                    datatable.Rows[x][i] = "Restsauerstoffgehalt_zu_hoch_oder_zu_niedrig_Nachlauf";
                                    break;
                                case 20:
                                    datatable.Rows[x][i] = "Abgastemperaturabfall_Nachlauf";
                                    break;
                                case 21:
                                    datatable.Rows[x][i] = "Abgastemperatur_ist_noch_größer_als_RGT_Start_Bereitschaft";
                                    break;
                                case 22:
                                    datatable.Rows[x][i] = "Abgasfühler_ist_defekt_Bereitschaft";
                                    break;
                                case 23:
                                    datatable.Rows[x][i] = "Einschubtemperatur_zu_hoch_Bereitschaft";
                                    break;
                                case 24:
                                    datatable.Rows[x][i] = "Lambdasonde_ist_defekt_Nachlauf";
                                    break;
                                case 25:
                                    datatable.Rows[x][i] = "Kesseltemperatur_ist_ausreichend_Bereitschaft";
                                    break;
                                case 27:
                                    datatable.Rows[x][i] = "Kesselfühler_ist_defekt_Bereitschaft";
                                    break;
                                case 28:
                                    datatable.Rows[x][i] = "Kesselfühler_ist_defekt_Nachlauf";
                                    break;
                                case 29:
                                    datatable.Rows[x][i] = "Lambdasonde_wird_beheizt";
                                    break;
                                case 30:
                                    datatable.Rows[x][i] = "Wärmetauscherreinigung_wartet_auf_eine_Freigabe_Zeit";
                                    break;
                                case 31:
                                    datatable.Rows[x][i] = "Wärmetauscherreinigung_wartet_auf_eine_Freigabe_AGT";
                                    break;
                                case 32:
                                    datatable.Rows[x][i] = "Wärmetauscherreinigung_wartet_auf_eine_Freigabe_KT";
                                    break;
                                case 33:
                                    datatable.Rows[x][i] = "Saugaustragung_wartet_auf_eine_Freigabe";
                                    break;
                                case 34:
                                    datatable.Rows[x][i] = "Pelletsvorratsbehälter_wird_befüllt";
                                    break;
                                case 35:
                                    datatable.Rows[x][i] = "Einschubfühlerkurzschluss_Bereitschaft";
                                    break;
                                case 36:
                                    datatable.Rows[x][i] = "Rückbrandschieber_öffnet";
                                    break;
                                case 37:
                                    datatable.Rows[x][i] = "Kessel_wird_befüllt";
                                    break;
                                case 38:
                                    datatable.Rows[x][i] = "Lambdasonde_wird_kalibriert";
                                    break;
                                case 39:
                                    datatable.Rows[x][i] = "Kaminkehrer_Messfreigabe";
                                    break;
                                case 40:
                                    datatable.Rows[x][i] = "Alarm aktiv";
                                    break;
                                case 41:
                                    datatable.Rows[x][i] = "Tür_schließen_Überwachung_AGT_und_Restsauerstoff_inaktiv";
                                    break;
                                case 42:
                                    datatable.Rows[x][i] = "Tür_war_zu_lange_offen_Nachlauf";
                                    break;
                                case 43:
                                    datatable.Rows[x][i] = "Keine_Brennerfreigabe_Zeit";
                                    break;
                                case 44:
                                    datatable.Rows[x][i] = "Keine_Brennerzeitfreigabe_Nachlauf";
                                    break;
                                case 45:
                                    datatable.Rows[x][i] = "Lambdasonde_muss_kalibriert_werden_Nachlauf";
                                    break;
                                case 46:
                                    datatable.Rows[x][i] = "Kesselreinigung";
                                    break;
                                case 100:
                                    datatable.Rows[x][i] = "Brenner_ausgeschaltet_Vorratsbehälter_wird_befüllt";
                                    break;
                                case 101:
                                    datatable.Rows[x][i] = "Abbrand_Stückholz_ist_beendet";
                                    break;
                                case 102:
                                    datatable.Rows[x][i] = "Warten_bis_Einschub_entleert_ist";
                                    break;
                                case 103:
                                    datatable.Rows[x][i] = "Stückholz_manuell_anzünden";
                                    break;
                                case 104:
                                    datatable.Rows[x][i] = "Automatische_Zündung_Stückholz";
                                    break;
                                case 105:
                                    datatable.Rows[x][i] = "Stückholz";
                                    break;
                                case 106:
                                    datatable.Rows[x][i] = "Stückholz_Teillast_Tür_nicht_öffnen";
                                    break;
                                case 107:
                                    datatable.Rows[x][i] = "Stückholz_anschließend_Pellets";
                                    break;
                                case 108:
                                    datatable.Rows[x][i] = "Verbrennung_Stückholz_gestoppt_Tür_nicht_öffnen";
                                    break;
                                case 109:
                                    datatable.Rows[x][i] = "Tür_ist_offen";
                                    break;
                                case 110:
                                    datatable.Rows[x][i] = "Startphase";
                                    break;
                                case 111:
                                    datatable.Rows[x][i] = "Hackgutbetrieb";
                                    break;
                                case 112:
                                    datatable.Rows[x][i] = "Blockade_Einschub_erkannt";
                                    break;
                                case 113:
                                    datatable.Rows[x][i] = "Kein_Stromfluss_Einschubmotor";
                                    break;
                                case 114:
                                    datatable.Rows[x][i] = "Blockade_Raumaustragung_erkannt";
                                    break;
                                case 115:
                                    datatable.Rows[x][i] = "Kein_Stromfluss_Austragungsmotor";
                                    break;
                                case 116:
                                    datatable.Rows[x][i] = "Stückholz_Automatik_wartet_auf_nächste_Zeitfreigabe";
                                    break;
                                case 117:
                                    datatable.Rows[x][i] = "Alarm_Alle_Ausgaenge_ausgeschaltet";
                                    break;
                                case 118:
                                    datatable.Rows[x][i] = "Tür_ist_offen_Ein_Kesselstart_ist_nicht_möglich";
                                    break;
                                case 119:
                                    datatable.Rows[x][i] = "Tür_ist_offen_Tür_schließen";
                                    break;
                                case 120:
                                    datatable.Rows[x][i] = "Störung_Nachlauf";
                                    break;
                                case 121:
                                    datatable.Rows[x][i] = "Positionsfahrt_Saugsondenumschalteinheit";
                                    break;
                                case 122:
                                    datatable.Rows[x][i] = "Ausgangtest_Kessel";
                                    break;
                                case 123:
                                    datatable.Rows[x][i] = "Freisaugen_Saugsonde_aktiv";
                                    break;
                                case 124:
                                    datatable.Rows[x][i] = "Rüttler_manuell";
                                    break;
                                case 125:
                                    datatable.Rows[x][i] = "Rüttler_ist_aktiv";
                                    break;
                                case 126:
                                    datatable.Rows[x][i] = "Drehrichtungsänderung_M1_Typ_HG";
                                    break;
                                case 127:
                                    datatable.Rows[x][i] = "Kein Stromfluss_M1_Typ_HG_erkannt";
                                    break;
                                case 128:
                                    datatable.Rows[x][i] = "Kein Stromfluss_M1_Typ_HG_erkannt";
                                    break;
                                case 129:
                                    datatable.Rows[x][i] = "Blockade_Einschub_erkannt";
                                    break;
                                case 130:
                                    datatable.Rows[x][i] = "Kein_Stromfluss_Einschub_erkannt";
                                    break;
                                case 131:
                                    datatable.Rows[x][i] = "Kein_Stromfluss_M2_Typ_HG1_erkannt";
                                    break;
                                case 132:
                                    datatable.Rows[x][i] = "Blockade_M2_Typ_HG1_erkannt";
                                    break;
                                case 133:
                                    datatable.Rows[x][i] = "Fremdkessel_Aktiv_Nachlauf";
                                    break;
                                case 134:
                                    datatable.Rows[x][i] = "Fremdkessel_Aktiv_Bereitschaft";
                                    break;
                                case 135:
                                    datatable.Rows[x][i] = "Login_Administrator";
                                    break;
                                case 136:
                                    datatable.Rows[x][i] = "Login_Servicepersonal";
                                    break;
                                case 137:
                                    datatable.Rows[x][i] = "Login_Fachpersonal";
                                    break;
                                case 138:
                                    datatable.Rows[x][i] = "Kaminkehrermessung_beendet_Nachlauf ";
                                    break;
                                case 139:
                                    datatable.Rows[x][i] = "Warten_bis_die_Raumluftklappe_offen_ist";
                                    break;
                                case 140:
                                    datatable.Rows[x][i] = "Keine_Brennerfreigabe_Außentemperatur_Prognose";
                                    break;
                                case 141:
                                    datatable.Rows[x][i] = "Keine_Brennerfreigabe_Außentemperatur";
                                    break;
                                case 142:
                                    datatable.Rows[x][i] = "Puffertemperatur_ausreichend_Bereitschaft";
                                    break;
                                case 143:
                                    datatable.Rows[x][i] = "Differenzdruckschalter_hat_ausgelöst_Nachlauf";
                                    break;
                                case 144:
                                    datatable.Rows[x][i] = "WT_Spülung_Brennwertmodul_aktiv";
                                    break;
                                case 145:
                                    datatable.Rows[x][i] = "Warten_auf_Freigabe_von_Brennwertmodul";
                                    break;
                                case 146:
                                    datatable.Rows[x][i] = "WT_Spülung_Brennwertmodul_wartet_auf_eine_Freigabe";
                                    break;

                            }
                        }
                    }
                }
            }

            #endregion

            return datatable;
        }

        private int SplitColumn(int ColumnValue, bool FirstorSecondValue)
        {
            int Splitnumber;
            string splitbinary;
            string binary = Convert.ToString(ColumnValue, 2).PadLeft(16, '0');
            if (FirstorSecondValue)
            {
                splitbinary = binary.Substring(binary.Length / 2, binary.Length / 2);
                Splitnumber = Convert.ToInt32(splitbinary, 2);
            }
            else
            {
                splitbinary = binary.Substring(0, binary.Length / 2);
                Splitnumber = Convert.ToInt32(splitbinary, 2);
            }
            return Splitnumber;
        }

        private string DateFromLogfile(string logfilename, string produktserie)
        {
            string date;
            string day;
            string month;
            string year;
            string stunde;
            string minute;

            if (produktserie == "Therminator")
            {
                year = logfilename.Substring(logfilename.Length - 12, 4);
                month = logfilename.Substring(logfilename.Length - 8, 2);
                day = logfilename.Substring(logfilename.Length - 6, 2);
                date = day + "." + month + "." + year;

            }
            else if (produktserie == "EcoTop | EcoTopZero | Pellet Elegance | OctoPlus | Maximus | Regelzentrale")
            {

                if (logfilename.Contains("sernr"))
                {
                    year = logfilename.Substring(4, 4);
                    month = logfilename.Substring(9, 2);
                    day = logfilename.Substring(12, 2);
                    stunde = logfilename.Substring(13, 2);
                    minute = logfilename.Substring(16, 2);
                    ViewBag.minute = stunde + ":" + minute;
                    date = day + "." + month + "." + year;
                }
                else
                {
                    if(logfilename.Length > 10)
                    {
                        year = logfilename.Substring(logfilename.Length - 20, 4);
                        month = logfilename.Substring(logfilename.Length - 15, 2);
                        day = logfilename.Substring(logfilename.Length - 12, 2);
                        stunde = logfilename.Substring(18, 2);
                        minute = logfilename.Substring(21, 2);
                        ViewBag.minute = stunde + ":" + minute;
                        date = day + "." + month + "." + year;
                    }
                    else
                    {
                        date = "Unknown";
                    }
                }
            }
            else
            {
                if (logfilename.Contains("sernr"))
                {
                    year = logfilename.Substring(4, 4);
                    month = logfilename.Substring(9, 2);
                    day = logfilename.Substring(12, 2);
                    stunde = logfilename.Substring(13, 2);
                    minute = logfilename.Substring(16, 2);
                    ViewBag.minute = stunde + ":" + minute;
                    date = day + "." + month + "." + year;
                }
                else
                {
                    year = logfilename.Substring(logfilename.Length - 23, 4);
                    month = logfilename.Substring(logfilename.Length - 18, 2);
                    day = logfilename.Substring(logfilename.Length - 15, 2);
                    stunde = logfilename.Substring(18, 2);
                    minute = logfilename.Substring(21, 2);
                    ViewBag.minute = stunde + ":" + minute;
                    date = day + "." + month + "." + year;
                    
                }
            }
            
            return date;
        }
        #endregion
    }
}