﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using WebApplication2.MyCode;
using WebApplication2.MyCode.BoilerParameter;
using LogdataReader;
using System.IO;
using System.Data;
using System.Net;
using System.Web.Hosting;
using WebApplication2.Session;

namespace WebApplication2.Controllers
{
    [SessionTimeout]
    public class BoilerParameterController : Controller
    {
        public static string kesseltyp { get; set; }
        public static string PelletsOderHackgut { get; set; }
      
        public ActionResult Maximus()
        {
            return View();
        }

        public ActionResult IndexKessel()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Initialize(string Parameter, string Parameter2)
        {
           if (!Parameter2.Contains("Empty"))
           {
                string einstellungen = Directory.GetFiles(Environment.GetEnvironmentVariable("Temp"), @"Einstellungen" + Session["sid"] + "\\" + Parameter).First();
               
                Datasets.boilerParameter = KesselparameterMethoden.createStruct(einstellungen);
                kesseltyp = Datasets.boilerParameter.KESSELTYP.ToString();
                Session["kesseltypparameter"] = kesseltyp;
                Session["kesseltyp"] = kesseltyp;
              
                Formate_Translate.getZaehlerstaende(Parameter2);

                if (kesseltyp == "HK_Regelung")
                {
                    return View("CustomerMenuRZ");
                }
                else if (kesseltyp == "WP")
                {
                    return View("CustomerMenuWP");
                }
                else
                {
                    return View("CustomerMenu");
                }
            }
            string einstellungenPath = Directory.GetFiles(Environment.GetEnvironmentVariable("Temp"), @"Einstellungen" + Session["sid"] + "\\" + Parameter).First();
          
            Datasets.boilerParameter = KesselparameterMethoden.createStruct(einstellungenPath);
            kesseltyp = Datasets.boilerParameter.KESSELTYP.ToString();
            Session["kesseltyp"] = kesseltyp;

            if (kesseltyp == "HK_Regelung")
            {
                return View("CustomerMenuRZ");
            }
            else if (kesseltyp == "WP")
            {
                return View("CustomerMenuWP");
            }
            else
            {
                return View("CustomerMenu");
            }
            
        }

        #region Maximus
        [HttpGet]
        public ActionResult InitializeMaximus(string Parameter, string Parameter2, string Parameter3)
        {
            if(!Parameter3.Contains("Empty"))
            {
                string einstellungen = Directory.GetFiles(Environment.GetEnvironmentVariable("Temp"), @"Einstellungen" + Session["sid"] + "\\" + Parameter).First();
                string maximus = Directory.GetFiles(Environment.GetEnvironmentVariable("Temp"), @"Einstellungen" + Session["sid"] + "\\" + Parameter2).First();
               
                Datasets.boilerParameter = KesselparameterMethoden.createStruct(einstellungen);
                Datasets.maximusParameter = KesselparameterMethoden.createMaximusStruct(maximus);
                Formate_Translate.getZaehlerstaende(Parameter3);

                kesseltyp = Datasets.boilerParameter.KESSELTYP.ToString();
                PelletsOderHackgut = Datasets.maximusParameter.ALLGEMEINE_EINSTELLUNGEN.KBA.ToString();
                

                Session["kesseltyp"] = kesseltyp;
                Session["kdaten"] = Parameter3;

                return View("CustomerMenu");
            }

            string pathEinstellungen = Directory.GetFiles(Environment.GetEnvironmentVariable("Temp"), @"Einstellungen" + Session["sid"] + "\\" + Parameter).First();
            string pathMaximus = Directory.GetFiles(Environment.GetEnvironmentVariable("Temp"), @"Einstellungen" + Session["sid"] + "\\" + Parameter2).First();
            
            Datasets.boilerParameter = KesselparameterMethoden.createStruct(pathEinstellungen);
            Datasets.maximusParameter = KesselparameterMethoden.createMaximusStruct(pathMaximus);

            kesseltyp = Datasets.boilerParameter.KESSELTYP.ToString();
            PelletsOderHackgut = Datasets.maximusParameter.ALLGEMEINE_EINSTELLUNGEN.KBA.ToString();
           
            Session["kesseltyp"] = kesseltyp;
            Session["kdaten"] = "Empty";

            return View("CustomerMenu");
        }
        #endregion

        #region Einstellungen datei
        public ActionResult FindEinstellungenName()
        {
            if(Directory.Exists(Path.Combine(System.Environment.GetEnvironmentVariable("Temp"), @"Einstellungen" + Session["sid"] + "\\")))
            {
                string[] einstellungen_filenames = Directory.GetFiles(Environment.GetEnvironmentVariable("Temp"), @"Einstellungen" + Session["sid"] + "\\").Select(Path.GetFileName).ToArray();
                
                foreach(var item in einstellungen_filenames)
                {
                    if (item.Contains("einstellungen.dat"))
                    {
                        var einstellungen_name = new { einstellungen = item };
                        return Json(einstellungen_name, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            return RedirectToAction("NotFound", "BoilerParameter");
        }
        #endregion

        #region Upload Einstellungen.dat
        public ActionResult Async_Save(IEnumerable<HttpPostedFileBase> files)
        {
            // The Name of the Upload component is "files"
            if (files != null)
            {
                string root = Path.Combine(System.Environment.GetEnvironmentVariable("Temp"));
                string subdir = root + "\\Einstellungen" + Session["sid"];
                if (!Directory.Exists(subdir))
                {
                    Directory.CreateDirectory(subdir);
                }
                foreach (var file in files)
                {
                    // Some browsers send file names with full path.
                    // We are only interested in the file name.
                    var fileName = Path.GetFileName(file.FileName);
                    var physicalPath = Path.Combine(System.Environment.GetEnvironmentVariable("Temp"), @"Einstellungen" + Session["sid"] + "\\", fileName);
                    file.SaveAs(physicalPath);
                }
            }
            // Return an empty string to signify success
            return Content("");
        }

        public ActionResult Async_Remove(string[] fileNames)
        {
            // The parameter of the Remove action must be called "fileNames"
            if (fileNames != null)
            {
                foreach (var fullName in fileNames)
                {
                    var fileName = Path.GetFileName(fullName);
                    var physicalPath = Path.Combine(Environment.GetEnvironmentVariable("Temp"), @"Einstellungen" + Session["sid"] + "\\", fileName);

                    // TODO: Verify user permissions
                    if (System.IO.File.Exists(physicalPath))
                    {
                        System.IO.File.Delete(physicalPath);
                    }
                }
            }

            // Return an empty string to signify success
            return Content("");
        }
        #endregion

        #region Dropdown
        public JsonResult GetEinstellungen()
        {
            if (Directory.Exists(Path.Combine(Environment.GetEnvironmentVariable("Temp"), @"Einstellungen" + Session["sid"] + "\\")))
            {
                var l_fileNames = Directory.GetFiles(System.Environment.GetEnvironmentVariable("Temp"), @"Einstellungen" + Session["sid"] + "\\").Select(Path.GetFileName).ToList();
                SelectList sl_fileNames = new SelectList(l_fileNames, "Value");
                return Json(sl_fileNames, JsonRequestBehavior.AllowGet);
            }
            SelectList emptyList = new SelectList("", "Value");
            return Json(emptyList, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Delete Einstellungen.dat
        public ActionResult DeleteEinstellungenDat()
        {
            if (Directory.Exists(Path.Combine(Environment.GetEnvironmentVariable("Temp"), @"Einstellungen" + Session["sid"] + "\\")))
            {
                var l_fileNames = Directory.GetFiles(System.Environment.GetEnvironmentVariable("Temp"), @"Einstellungen" + Session["sid"] + "\\").Select(Path.GetFileName).ToList();
                if (l_fileNames != null)
                {
                    foreach (var file in l_fileNames)
                    {
                        var fileName = Path.GetFileName(file);
                        var physicalPath = Path.Combine(Environment.GetEnvironmentVariable("Temp"), @"Einstellungen" + Session["sid"] + "\\", fileName);

                        if (System.IO.File.Exists(physicalPath))
                        {
                            System.IO.File.Delete(physicalPath);
                        }
                    }
                }
            }
            return RedirectToAction("IndexKessel", "BoilerParameter");
        }
        #endregion

        public ActionResult ChangeLanguage(LANGUAGE language)   //not included yet
        {
            Datasets.language = language;
            return View("CustomerMenu");
        }

        #region Views
        public ActionResult CustomerMenu()
        {
            ViewBag.kesseltyp = kesseltyp;
            return View();
        }

        public ActionResult CustomerMenuWP()
        {
            ViewBag.kesseltyp = kesseltyp;
            return View();
        }

        public ActionResult CustomerMenuRZ()
        {
            ViewBag.kesseltyp = kesseltyp;
            return View();
        }

        public ActionResult ServiceMenu()
        {
            ViewBag.kesseltyp = kesseltyp;
            return View();
        }

        public ActionResult ServiceMenuMaximus()
        {
            ViewBag.kesseltyp = kesseltyp;
            return View();
        }

        public ActionResult ServiceMenuWP()
        {
            ViewBag.kesseltyp = kesseltyp;
            return View();
        }

        public ActionResult ServiceMenuRegelzentrale()
        {
            ViewBag.kesseltyp = kesseltyp;
            return View();
        }

        public ActionResult HeatingCircuits()
        {
            ViewBag.kesseltyp = kesseltyp;
            return View();
        }

        public ActionResult HeatingCircuitsWP()
        {
            ViewBag.kesseltyp = kesseltyp;
            return View();
        }

        public ActionResult DrinkWaterTanks()
        {
            ViewBag.kesseltyp = kesseltyp;
            return View();
        }

        public ActionResult FreshWaterModules()
        {
            ViewBag.kesseltyp = kesseltyp;
            return View();
        }

        public ActionResult Circulations()
        {
            ViewBag.kesseltyp = kesseltyp;
            return View();
        }

        public ActionResult Buffers()
        {
            ViewBag.kesseltyp = kesseltyp;
            return View();
        }

        public ActionResult BuffersMaximus()
        {
            ViewBag.kesseltyp = kesseltyp;
            return View();
        }

        public ActionResult Solar()
        {
            ViewBag.kesseltyp = kesseltyp;
            return View();
        }

        public ActionResult DifferenceModuls()
        {
            ViewBag.kesseltyp = kesseltyp;
            return View();
        }

        public ActionResult Octoplus()
        {
            ViewBag.kesseltyp = kesseltyp;
            return View();
        }

        public ActionResult Wetterfrosch()
        {
            ViewBag.kesseltyp = kesseltyp;
            return View();
        }
        public ActionResult KdatenMaximus()
        {
            ViewBag.kesseltyp = kesseltyp;
            return View();
        }
        public ActionResult KdatenWP()
        {
            ViewBag.kesseltyp = kesseltyp;
            return View();
        }
        public ActionResult KdatenRZ()
        {
            ViewBag.kesseltyp = kesseltyp;
            return View();
        }
        public ActionResult Kdaten()
        {
            ViewBag.kesseltyp = kesseltyp;
            return View();
        }

        #endregion


        #region Grid_Read

        #region Kundenmenü
        [HttpPost]
        public ActionResult ReadCustomerMenu([DataSourceRequest] DataSourceRequest request)
        {
            var model = Datasets.customerMenu();

            return Json(model.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ReadGridCustomerMenuWP1([DataSourceRequest] DataSourceRequest request)
        {
            var model = Datasets.customerMenuWP1();

            return Json(model.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ReadGridCustomerMenuWPBlockingTimes([DataSourceRequest] DataSourceRequest request)
        {
            var model = Datasets.customerMenuWPBlockingTimes();

            return Json(model.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ReadGridCustomerMenuWPKaskade([DataSourceRequest] DataSourceRequest request)
        {
            var model = Datasets.customerMenuWPKaskade();

            return Json(model.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ReadCustomerMenuRZ([DataSourceRequest] DataSourceRequest request)
        {
            var model = Datasets.customerMenuRZ();

            return Json(model.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Servicemenü

        #region Kessel
        [HttpPost]
        public ActionResult ReadServiceMenuRLA([DataSourceRequest] DataSourceRequest request)
        {
            var model = Datasets.serviceMenuRLA();

            return Json(model.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ReadServiceMenuPZ([DataSourceRequest] DataSourceRequest request)
        {
            var model = Datasets.serviceMenuPZ();

            return Json(model.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ReadServiceMenuPZF([DataSourceRequest] DataSourceRequest request)
        {
            var model = Datasets.ServiceMenuPZF();

            return Json(model.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ReadServiceMenuPSF([DataSourceRequest] DataSourceRequest request)
        {
            var model = Datasets.serviceMenuPSF();

            return Json(model.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult ReadServiceMenuPB([DataSourceRequest] DataSourceRequest request)
        {
            var model = Datasets.serviceMenuPB();

            return Json(model.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult ReadServiceMenuPBF([DataSourceRequest] DataSourceRequest request)
        {
            var model = Datasets.serviceMenuPBF();

            return Json(model.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult ReadServiceMenuSA([DataSourceRequest] DataSourceRequest request)
        {
            var model = Datasets.serviceMenuSA();

            return Json(model.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ReadServiceMenuSAF([DataSourceRequest] DataSourceRequest request)
        {
            var model = Datasets.serviceMenuSAF();

            return Json(model.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ReadServiceMenuSPP([DataSourceRequest] DataSourceRequest request)
        {
            var model = Datasets.serviceMenuSPP();

            return Json(model.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ReadServiceMenuSPPF([DataSourceRequest] DataSourceRequest request)
        {
            var model = Datasets.serviceMenuSPPF();

            return Json(model.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ReadServiceMenuSP([DataSourceRequest] DataSourceRequest request)
        {
            var model = Datasets.serviceMenuSP();

            return Json(model.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region WP
        [HttpPost]
        public ActionResult ReadGridServiceMenuWP([DataSourceRequest] DataSourceRequest request)
        {
            var model = Datasets.serviceMenuWP();

            return Json(model.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ReadGridServiceMenuWPHeatingRod([DataSourceRequest] DataSourceRequest request)
        {
            var model = Datasets.serviceMenuWPHeatingRod();

            return Json(model.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult ReadGridServiceMenuWPExternalBoiler([DataSourceRequest] DataSourceRequest request)
        {
            var model = Datasets.serviceMenuWPExternalBoiler();

            return Json(model.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ReadGridServiceMenuWPSmartGrid([DataSourceRequest] DataSourceRequest request)
        {
            var model = Datasets.serviceMenuWPSmartGrid();

            return Json(model.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ReadGridServiceMenuWPPriority([DataSourceRequest] DataSourceRequest request)
        {
            var model = Datasets.serviceMenuWPPriority();

            return Json(model.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ReadGridServiceMenuWPParameter([DataSourceRequest] DataSourceRequest request)
        {
            var model = Datasets.serviceMenuWPParameter();

            return Json(model.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Regelzentrale
        [HttpPost]
        public ActionResult ReadServiceMenuRZSollwerte([DataSourceRequest] DataSourceRequest request)
        {
            var model = Datasets.serviceMenuRZSollwerte();

            return Json(model.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        public ActionResult ReadServiceMenuRZErweiterungsmodul([DataSourceRequest] DataSourceRequest request)
        {
            var model = Datasets.serviceMenuRZErweiterungsmodul();

            return Json(model.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Maximus
       
        [HttpPost]
        public ActionResult ReadServiceMenuMaximusRLA([DataSourceRequest] DataSourceRequest request)
        {
            var model = Datasets.serviceMenuMaximusRLA();

            return Json(model.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ReadServiceMenuMaximusPZ([DataSourceRequest] DataSourceRequest request)
        {
            var model = Datasets.serviceMenuMaximusPZ();

            return Json(model.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ReadServiceMenuMaximusPZF([DataSourceRequest] DataSourceRequest request)
        {
            var model = Datasets.serviceMenuMaximusPZF();

            return Json(model.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ReadServiceMenuMaximusPB([DataSourceRequest] DataSourceRequest request)
        {
            var model = Datasets.serviceMenuMaximusPB();

            return Json(model.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ReadServiceMenuMaximusPBF([DataSourceRequest] DataSourceRequest request)
        {
            var model = Datasets.serviceMenuMaximusPBF();

            return Json(model.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ReadServiceMenuMaximusRTG([DataSourceRequest] DataSourceRequest request)
        {
            var model = Datasets.serviceMenuMaximusRTG();

            return Json(model.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ReadServiceMenuMaximusSA([DataSourceRequest] DataSourceRequest request)
        {
            var model = Datasets.serviceMenuMaximusSA();

            return Json(model.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ReadServiceMenuMaximusSAF([DataSourceRequest] DataSourceRequest request)
        {
            var model = Datasets.serviceMenuMaximusSAF();

            return Json(model.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ReadServiceMenuMaximusLK([DataSourceRequest] DataSourceRequest request)
        {
            var model = Datasets.serviceMenuMaximusLK();

            return Json(model.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ReadServiceMenuMaximusLKRF([DataSourceRequest] DataSourceRequest request)
        {
            var model = Datasets.serviceMenuMaximusLKRF();

            return Json(model.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ReadServiceMenuMaximusVS([DataSourceRequest] DataSourceRequest request)
        {
            var model = Datasets.serviceMenuMaximusVS();

            return Json(model.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ReadServiceMenuMaximusSP([DataSourceRequest] DataSourceRequest request)
        {
            var model = Datasets.serviceMenuMaximusSP();

            return Json(model.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ReadServiceMenuMaximusSPF([DataSourceRequest] DataSourceRequest request)
        {
            var model = Datasets.serviceMenuMaximusSPF();

            return Json(model.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ReadServiceMenuMaximusSPA([DataSourceRequest] DataSourceRequest request)
        {
            var model = Datasets.serviceMenuMaximusSPA();

            return Json(model.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        #endregion


        #endregion

        #region Heizkreise
        [HttpPost]
        public ActionResult ReadHeatingCircuits([DataSourceRequest] DataSourceRequest request)
        {
            var model = Datasets.ReadHeatingCircuits();
            return Json(model.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult ReadHeatingCircuitsDaily([DataSourceRequest] DataSourceRequest request)
        {
            var model = Datasets.heatingCircuitsDaily();

            return Json(model.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ReadHeatingCircuitsDailyCooling([DataSourceRequest] DataSourceRequest request)
        {
            var model = Datasets.heatingCircuitsDailyCooling();

            return Json(model.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ReadHeatingCircuitsBlock([DataSourceRequest] DataSourceRequest request)
        {
            var model = Datasets.heatingCircuitsBlock();

            return Json(model.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult ReadHeatingCircuitsBlockCooling([DataSourceRequest] DataSourceRequest request)
        {
            var model = Datasets.heatingCircuitsBlockCooling();

            return Json(model.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ReadHeatingCircuitsOperationMode([DataSourceRequest] DataSourceRequest request)
        {
            var model = Datasets.heatingCircuitsOperationMode();

            return Json(model.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ReadHeatingCircuitsSettings([DataSourceRequest] DataSourceRequest request)
        {
            var model = Datasets.heatingCircuitsSettings();

            return Json(model.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Drinkwasserspeicher
        [HttpPost]
        public ActionResult ReadGridDrinkWaterTanksOperationMode([DataSourceRequest] DataSourceRequest request)
        {
            var model = Datasets.drinkWaterTanksOperationMode();

            return Json(model.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ReadDrinkWaterTanksDaily([DataSourceRequest] DataSourceRequest request)
        {
            var model = Datasets.drinkWaterTanksDaily();

            return Json(model.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ReadGridDrinkWaterTanksBlock([DataSourceRequest] DataSourceRequest request)
        {
            var model = Datasets.drinkWaterTanksBlock();

            return Json(model.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ReadGridDrinkWaterTanksSettings([DataSourceRequest] DataSourceRequest request)
        {
            var model = Datasets.drinkWaterTanksSettings();

            return Json(model.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Frischwassermodul
        [HttpPost]
        public ActionResult ReadGridFreshWaterModulesSettings([DataSourceRequest] DataSourceRequest request)
        {
            var model = Datasets.freshWaterModulesSettings();

            return Json(model.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ReadGridFreshWaterModulesParameters([DataSourceRequest] DataSourceRequest request)
        {
            var model = Datasets.freshWaterModulesParameters();

            return Json(model.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ReadGridCirculationsSettings([DataSourceRequest] DataSourceRequest request)
        {
            var model = Datasets.circulationsSettings();

            return Json(model.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Zirkulation
        [HttpPost]
        public ActionResult ReadGridCirculationsDaily([DataSourceRequest] DataSourceRequest request)
        {
            var model = Datasets.circulationsDaily();

            return Json(model.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ReadGridCirculationsBlock([DataSourceRequest] DataSourceRequest request)
        {
            var model = Datasets.circulationsBlock();

            return Json(model.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Puffer
        [HttpPost]
        public ActionResult ReadGridBuffers([DataSourceRequest] DataSourceRequest request)
        {
            var model = Datasets.buffers();

            return Json(model.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Puffer Maximus
        [HttpPost]
        public ActionResult ReadGridMaximusBuffers([DataSourceRequest] DataSourceRequest request)
        {
            var model = Datasets.buffersMaximus();

            return Json(model.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Solar
        [HttpPost]
        public ActionResult ReadGridSolarTargetValues([DataSourceRequest] DataSourceRequest request)
        {
            var model = Datasets.solarTargetValues();

            return Json(model.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ReadGridSolarAdditionalFunctions([DataSourceRequest] DataSourceRequest request)
        {
            var model = Datasets.solarAdditionalFunctions();

            return Json(model.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ReadGridSolarParameters([DataSourceRequest] DataSourceRequest request)
        {
            var model = Datasets.solarParameters();

            return Json(model.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }



        #endregion

        #region Differenzmodul
        [HttpPost]
        public ActionResult ReadGridDifferenceModuls1([DataSourceRequest] DataSourceRequest request)
        {
            var model = Datasets.differenceModuls1();

            return Json(model.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ReadGridDifferenceModuls2([DataSourceRequest] DataSourceRequest request)
        {
            var model = Datasets.differenceModuls2();

            return Json(model.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }





        #endregion

        #region Octoplus
        [HttpPost]
        public ActionResult ReadGridOctoplusDifferenceLoadingCircle([DataSourceRequest] DataSourceRequest request)
        {
            var model = Datasets.octoplusDifferenceLoadingCircle();

            return Json(model.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ReadGridOctoplusDrinkingWater([DataSourceRequest] DataSourceRequest request)
        {
            var model = Datasets.octoplusDrinkingWater();

            return Json(model.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Wetterfrosch
        public ActionResult ReadGridWetterfrosch([DataSourceRequest] DataSourceRequest request)
        {
            var model = Datasets.wetterfrosch();

            return Json(model.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }
        #endregion

        #endregion

    }
}