using System;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Optimization;
using WebApplication2.App_Start;
using WebApplication2.Models;
using System.Data.Entity;


namespace WebApplication2
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            Database.SetInitializer<mysolarfocusTestContext>(null);
            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }
    }
}
